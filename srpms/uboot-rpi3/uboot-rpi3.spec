%global debug_package %{nil}

Name:         uboot-rpi3
License:      MulanPSLv2
Version:      2022.10
Release:      1.0.0%{?dist}
Summary:      u-boot for raspberry pi 3
Group:        System Environment/Kernel
Source0:      u-boot-%{version}.tar.bz2
Source1:   arm-boards
Source2:   arm-chromebooks
Source3:   aarch64-boards
Source4:   aarch64-chromebooks
Source5:   10-devicetree.install

# Fedoraisms patches
Patch1:    uefi-use-Fedora-specific-path-name.patch

# general fixes
Patch2:    uefi-distro-load-FDT-from-any-partition-on-boot-device.patch
Patch3:    usb-kbd-fixes.patch
Patch4:    Add-fix-for-Pine64-gigabit-throughput-issues.patch
Patch5:    rk33xx-make_fit_atf-fix.patch
Patch6:    rk33xx-enable-make-itb.patch
Patch7:    net-Only-access-network-devices-after-init.patch

# Board fixes and enablement
Patch10:   dragonboard-fixes.patch
Patch11:   BeagleBoard.org-PocketBeagle.patch
Patch12:   mx6cuboxi-add-support-for-detecting-Revision-1.5-SoM.patch
Patch13:   rpi-3-plus-support.patch
# Patch19:   mvebu-enable-generic-distro-boot-config.patch

#Patch from Oracle
Patch100:	      allow-32M-sizeable-kernel.patch
Patch101:	      set_bootcmd.patch

Patch203:	      0003-rpi-fix-uboot-bootup-issue.patch
Patch204:	      0004-rpi-Complete-table-of-models-with-new-revision-code-.patch	
Patch205:	      0005-uboot-rpi-Load-board-specific-dtb-fdtfile-to-address.patch
Patch206: 	      0001-Raspberry-Pi-UEFI-Runtime-regions-are-not-aligned-to.patch
Patch207: 	      use-core-clock-frequency-in-bcm2835-sdhost.patch
Patch208: 	      0001-rpi-Boot-from-mmc-and-on-failure-fallback-to-usb.patch

BuildRequires: dtc

%description
u-boot for raspberry pi 3

%package -n dtc
Summary:	Device Tree Compiler
Group:		Development/Tools

%description -n dtc
The Device Tree Compiler generates flattened Open Firmware style device trees for use with PowerPC machines that lack an Open Firmware implementation

%prep
%setup -q -n u-boot-%{version}
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch10 -p1
%patch11 -p1
%patch12 -p1
%patch13 -p1

%patch100 -p0
%patch101 -p0

%patch203 -p1
%patch204 -p1
%patch205 -p1
%patch206 -p1
%patch207 -p1
%patch208 -p1

%build

# rpi3
make O=rpi3 rpi_3_defconfig
make O=rpi3 %{?_smp_mflags}

%install
mkdir -p %{buildroot}/boot/efi
cp -v rpi3/u-boot.bin %{buildroot}/boot/efi/kernel8.img

%files
/boot/efi/kernel8.img

%changelog
* Tue Oct 04 2022 Willard Hu <mingkunhu2000@gmail.com> [2022.10-1.0.0]
- Initial build
