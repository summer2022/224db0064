From 27f774fcebf731e3cae695b5a407ef52e5b1f5f9 Mon Sep 17 00:00:00 2001
From: Petr Benes <petr.benes@oracle.com>
Date: Thu, 28 Jun 2018 08:21:04 +0000
Subject: [PATCH] Raspberry Pi: UEFI Runtime regions are not aligned to 64 KB
 -- buggy firmware?

Orabug: 28075127
Signed-off-by: petr.benes@oracle.com
---
 arch/arm/cpu/armv8/u-boot.lds |  2 +-
 common/board_f.c              |  2 +-
 include/efi.h                 |  1 +
 lib/efi_loader/efi_memory.c   | 25 +++++++++++++++++++------
 lib/efi_loader/efi_smbios.c   |  3 ++-
 5 files changed, 24 insertions(+), 9 deletions(-)

diff --git a/arch/arm/cpu/armv8/u-boot.lds b/arch/arm/cpu/armv8/u-boot.lds
index 7b76e0f..5a6f9d7 100644
--- a/arch/arm/cpu/armv8/u-boot.lds
+++ b/arch/arm/cpu/armv8/u-boot.lds
@@ -97,7 +97,7 @@ SECTIONS
 		KEEP(*(SORT(.u_boot_list*)));
 	}
 
-	. = ALIGN(8);
+	. = ALIGN(0x10000);
 
 	.efi_runtime : {
                 __efi_runtime_start = .;
diff --git a/common/board_f.c b/common/board_f.c
index c6bc53e..e7c3539 100644
--- a/common/board_f.c
+++ b/common/board_f.c
@@ -402,7 +402,7 @@ static int reserve_uboot(void)
 	 */
 	gd->relocaddr -= gd->mon_len;
 	gd->relocaddr &= ~(4096 - 1);
-#if defined(CONFIG_E500) || defined(CONFIG_MIPS)
+#if defined(CONFIG_E500) || defined(CONFIG_MIPS) || defined(CONFIG_ARM64)
 	/* round down to next 64 kB limit so that IVPR stays aligned */
 	gd->relocaddr &= ~(65536 - 1);
 #endif
diff --git a/include/efi.h b/include/efi.h
index 98bddba..657644b 100644
--- a/include/efi.h
+++ b/include/efi.h
@@ -200,6 +200,7 @@ struct efi_mem_desc {
 #define EFI_ALLOCATE_MAX_ADDRESS	1
 #define EFI_ALLOCATE_ADDRESS		2
 #define EFI_MAX_ALLOCATE_TYPE		3
+#define EFI_ALLOCATE_64K_ALIGN		4
 
 /* Types and defines for Time Services */
 #define EFI_TIME_ADJUST_DAYLIGHT 0x1
diff --git a/lib/efi_loader/efi_memory.c b/lib/efi_loader/efi_memory.c
index ff0edf3..9130819 100644
--- a/lib/efi_loader/efi_memory.c
+++ b/lib/efi_loader/efi_memory.c
@@ -239,7 +239,8 @@ uint64_t efi_add_memory_map(uint64_t start, uint64_t pages, int memory_type,
 	return start;
 }
 
-static uint64_t efi_find_free_memory(uint64_t len, uint64_t max_addr)
+static uint64_t efi_find_free_memory(uint64_t len, uint64_t max_addr,
+				        uint64_t alignment)
 {
 	struct list_head *lhandle;
 
@@ -264,6 +265,10 @@ static uint64_t efi_find_free_memory(uint64_t len, uint64_t max_addr)
 		if ((ret + len) > desc_end)
 			continue;
 
+		/* alignment must be power of 2 (typically size of page) or 0.*/
+		if (alignment != 0)
+			ret &= ~(alignment - 1);
+
 		/* Out of bounds for lower map limit */
 		if (ret < desc->physical_start)
 			continue;
@@ -292,26 +297,34 @@ efi_status_t efi_allocate_pages(int type, int memory_type,
 	uint64_t addr;
 
 	switch (type) {
-	case 0:
+	case EFI_ALLOCATE_ANY_PAGES:
 		/* Any page */
-		addr = efi_find_free_memory(len, gd->start_addr_sp);
+		addr = efi_find_free_memory(len, gd->start_addr_sp, 0);
 		if (!addr) {
 			r = EFI_NOT_FOUND;
 			break;
 		}
 		break;
-	case 1:
+	case EFI_ALLOCATE_MAX_ADDRESS:
 		/* Max address */
-		addr = efi_find_free_memory(len, *memory);
+		addr = efi_find_free_memory(len, *memory, 0);
 		if (!addr) {
 			r = EFI_NOT_FOUND;
 			break;
 		}
 		break;
-	case 2:
+	case EFI_ALLOCATE_ADDRESS:
 		/* Exact address, reserve it. The addr is already in *memory. */
 		addr = *memory;
 		break;
+	case EFI_ALLOCATE_64K_ALIGN:
+		/* Need address aligned to 64K */
+		addr = efi_find_free_memory(len, *memory, 0x10000);
+		if (!addr) {
+			r = EFI_NOT_FOUND;
+			break;
+		}
+		break;
 	default:
 		/* UEFI doesn't specify other allocation types */
 		r = EFI_INVALID_PARAMETER;
diff --git a/lib/efi_loader/efi_smbios.c b/lib/efi_loader/efi_smbios.c
index ac412e7..44cd040 100644
--- a/lib/efi_loader/efi_smbios.c
+++ b/lib/efi_loader/efi_smbios.c
@@ -21,7 +21,8 @@ void efi_smbios_register(void)
 	uint64_t pages = 1;
 	int memtype = EFI_RUNTIME_SERVICES_DATA;
 
-	if (efi_allocate_pages(1, memtype, pages, &dmi) != EFI_SUCCESS)
+	if (efi_allocate_pages(EFI_ALLOCATE_64K_ALIGN,
+				memtype, pages, &dmi) != EFI_SUCCESS)
 		return;
 
 	/* Generate SMBIOS tables */
-- 
1.8.3.1

