# We have to override the new %%install behavior because, well... the kernel is special.
%global __spec_install_pre %{___build_pre}

# For a kernel released for public testing, released_kernel should be 1.
# For internal testing builds during development, it should be 0.
%global released_kernel 1

# Sign modules on all arches
%global signmodules 0

# define buildid .local

%define dist .an8
%define rpmversion %{?KVER:%{KVER}}%{?!KVER:5.10.134}
%define pkgrelease %{?KREL:%{KREL}}%{?!KREL:12}

# allow pkg_release to have configurable %%{?dist} tag
%define specrelease %{pkgrelease}%{?dist}

%define pkg_release %{specrelease}%{?buildid}

# What parts do we want to build?  We must build at least one kernel.
# These are the kernels that are built IF the architecture allows it.
# All should default to 1 (enabled) and be flipped to 0 (disabled)
# by later arch-specific checks.

# The following build options are enabled by default.
# Use either --without <opt> in your rpmbuild command or force values
# to 0 in here to disable them.
#
# standard kernel
%define with_up        %{?_without_up:        0} %{?!_without_up:        1}
# kernel-debug
%define with_debug     %{?_without_debug:     0} %{?!_without_debug:     1}
# kernel-doc
%define with_doc       %{?_without_doc:       0} %{?!_without_doc:       1}
# kernel-headers
%define with_headers   %{?_without_headers:   0} %{?!_without_headers:   1}
# perf
%define with_perf      %{?_without_perf:      0} %{?!_without_perf:      1}
# tools
%define with_tools     %{?_without_tools:     0} %{?!_without_tools:     1}
# bpf tool
%define with_bpftool   %{?_without_bpftool:   0} %{?!_without_bpftool:   1}
# kernel-debuginfo
%define with_debuginfo %{?_without_debuginfo: 0} %{?!_without_debuginfo: 1}
#
# Additional options for user-friendly one-off kernel building:
#
# Only build the base kernel (--with baseonly):
%define with_baseonly  %{?_with_baseonly:     1} %{?!_with_baseonly:     0}
# Only build the debug kernel (--with dbgonly):
%define with_dbgonly   %{?_with_dbgonly:      1} %{?!_with_dbgonly:      0}
#
# should we do C=1 builds with sparse
%define with_sparse    %{?_with_sparse:       1} %{?!_with_sparse:       0}

# The kernel tarball/base version
%define kversion 5.10

%define with_gcov %{?_with_gcov: 1} %{?!_with_gcov: 0}

# turn off debug kernel for gcov builds
%if %{with_gcov}
%define with_debug 0
%endif

%define make_target bzImage
%define image_install_path boot

%define KVERREL %{version}-%{release}.%{_target_cpu}
%define KVERREL_RE %(echo %KVERREL | sed 's/+/[+]/g')
%define hdrarch %_target_cpu
%define asmarch %_target_cpu

%if !%{with_debuginfo}
%define _enable_debug_packages 0
%endif
%define debuginfodir /usr/lib/debug
# Needed because we override almost everything involving build-ids
# and debuginfo generation. Currently we rely on the old alldebug setting.
%global _build_id_links alldebug

# if requested, only build base kernel
%if %{with_baseonly}
%define with_debug 0
%endif

# if requested, only build debug kernel
%if %{with_dbgonly}
%define with_up 0
%define with_tools 0
%define with_perf 0
%define with_bpftool 0
%endif

# Overrides for generic default options

# only package docs noarch
%ifnarch noarch
%define with_doc 0
%define doc_build_fail true
%endif

# don't build noarch kernels or headers (duh)
%ifarch noarch
%define with_up 0
%define with_headers 0
%define with_tools 0
%define with_perf 0
%define with_bpftool 0
%define with_debug 0
%define with_doc 0
%define all_arch_configs %{name}-%{version}-*.config
%endif

# Per-arch tweaks

%ifarch x86_64
%define asmarch x86
%define all_arch_configs %{name}-%{version}-x86_64*.config
%define kernel_image arch/x86/boot/bzImage
%endif

%ifarch aarch64
%define all_arch_configs %{name}-%{version}-aarch64*.config
%define asmarch arm64
%define hdrarch arm64
%define make_target Image.gz
%define kernel_image arch/arm64/boot/Image.gz
%endif

# To temporarily exclude an architecture from being built, add it to
# %%nobuildarches. Do _NOT_ use the ExclusiveArch: line, because if we
# don't build kernel-headers then the new build system will no longer let
# us use the previous build of that package -- it'll just be completely AWOL.
# Which is a BadThing(tm).

# We only build kernel-headers on the following...
%define nobuildarches i386 i686

%ifarch %nobuildarches
%define with_up 0
%define with_debug 0
%define with_debuginfo 0
%define with_perf 0
%define with_tools 0
%define with_bpftool 0
%define _enable_debug_packages 0
%endif

# Architectures we build tools/cpupower on
%define cpupowerarchs x86_64 aarch64


#
# Packages that need to be installed before the kernel is, because the %%post
# scripts use them.
#
%define kernel_prereq  coreutils, systemd >= 203-2, /usr/bin/kernel-install
%define initrd_prereq  dracut >= 027


Name: kernel%{?variant}
Group: System Environment/Kernel
License: GPLv2 and Redistributable, no modification permitted
URL: http://www.kernel.org/
Version: %{rpmversion}
Release: %{pkg_release}
Summary: The Linux kernel, based on version %{version}, heavily modified with backports
# DO NOT CHANGE THE 'ExclusiveArch' LINE TO TEMPORARILY EXCLUDE AN ARCHITECTURE BUILD.
# SET %%nobuildarches (ABOVE) INSTEAD
ExclusiveArch: noarch i686 x86_64 aarch64
ExclusiveOS: Linux
%ifnarch %{nobuildarches}
Requires: %{name}-core-uname-r = %{KVERREL}%{?variant}
Requires: %{name}-modules-uname-r = %{KVERREL}%{?variant}
%endif


#
# List the packages used during the kernel build
#
BuildRequires: kmod, patch, bash, coreutils, tar, git, which
BuildRequires: bzip2, xz, findutils, gzip, m4, perl-interpreter, perl-Carp, perl-devel, perl-generators, make, diffutils, gawk
BuildRequires: gcc, binutils, redhat-rpm-config, hmaccalc, python3-devel
BuildRequires: net-tools, hostname, bc, bison, flex, elfutils-devel, dwarves
%if %{with_doc}
BuildRequires: xmlto, asciidoc, python3-sphinx
%endif
%if %{with_headers}
BuildRequires: rsync
%endif
%if %{with_sparse}
BuildRequires: sparse
%endif
%if %{with_perf}
BuildRequires: zlib-devel binutils-devel newt-devel perl(ExtUtils::Embed) bison flex xz-devel
BuildRequires: audit-libs-devel
BuildRequires: java-devel
%ifnarch s390x
BuildRequires: numactl-devel
%endif
%endif
%if %{with_tools}
BuildRequires: gettext ncurses-devel
BuildRequires: libcap-devel libcap-ng-devel
%ifnarch s390x
BuildRequires: pciutils-devel
%endif
%endif
%if %{with_bpftool}
BuildRequires: python3-docutils
BuildRequires: zlib-devel binutils-devel
%endif
BuildConflicts: rhbuildsys(DiskFree) < 500Mb
%if %{with_debuginfo}
BuildRequires: rpm-build, elfutils
#BuildConflicts: rpm < 4.13.0.1-19
# Most of these should be enabled after more investigation
%undefine _include_minidebuginfo
%undefine _find_debuginfo_dwz_opts
%undefine _unique_build_ids
%undefine _unique_debug_names
%undefine _unique_debug_srcs
%undefine _debugsource_packages
%undefine _debuginfo_subpackages
%global _find_debuginfo_opts -r
%global _missing_build_ids_terminate_build 1
%global _no_recompute_build_ids 1
%endif

BuildRequires: openssl openssl-devel

# These below are required to build man pages
%if %{with_perf}
BuildRequires: xmlto
%endif
%if %{with_perf} || %{with_tools}
BuildRequires: asciidoc
%endif

Source0: linux-%{rpmversion}-%{pkg_release}.tar.xz

Source16: mod-extra.list
Source17: mod-blacklist.sh

Source90: filter-x86_64.sh
Source93: filter-aarch64.sh
Source99: filter-modules.sh

Source20: kernel-%{version}-aarch64.config
Source21: kernel-%{version}-aarch64-debug.config
Source39: kernel-%{version}-x86_64.config
Source40: kernel-%{version}-x86_64-debug.config
Source43: generate_bls_conf.sh
Source44: mod-internal.list



# Sources for kernel-tools
Source2000: cpupower.service
Source2001: cpupower.config

## Patches needed for building this package

# %%PATCH_LIST%%

# END OF PATCH DEFINITIONS

BuildRoot: %{_tmppath}/%{name}-%{KVERREL}-root

%description
This is the package which provides the Linux kernel for Alibaba Cloud Linux.
It is based on upstream Linux at version %{version} and maintains kABI
compatibility of a set of approved symbols, however it is heavily modified with
backports and fixes pulled from newer upstream Linux %{name} releases. This means
this is not a %{version} kernel anymore: it includes several components which come
from newer upstream linux versions, while maintaining a well tested and stable
core. Some of the components/backports that may be pulled in are: changes like
updates to the core kernel (eg.: scheduler, cgroups, memory management, security
fixes and features), updates to block layer, supported filesystems, major driver
updates for supported hardware in Alibaba Cloud Linux, enhancements for
enterprise customers, etc.

#
# This macro does requires, provides, conflicts, obsoletes for a kernel package.
#	%%kernel_reqprovconf <subpackage>
# It uses any kernel_<subpackage>_conflicts and kernel_<subpackage>_obsoletes
# macros defined above.
#
%define kernel_reqprovconf \
Provides: %{name} = %{rpmversion}-%{pkg_release}\
Provides: %{name}-%{_target_cpu} = %{rpmversion}-%{pkg_release}%{?1:+%{1}}\
Provides: kernel-drm-nouveau = 16\
Provides: %{name}-uname-r = %{KVERREL}%{?variant}%{?1:+%{1}}\
Requires(pre): %{kernel_prereq}\
Requires(pre): %{initrd_prereq}\
Requires(pre): linux-firmware >= 20190516-94.git711d3297\
Requires(preun): systemd >= 200\
Conflicts: xfsprogs < 4.3.0-1\
Conflicts: xorg-x11-drv-vmmouse < 13.0.99\
%{expand:%%{?kernel%{?1:_%{1}}_conflicts:Conflicts: %%{kernel%{?1:_%{1}}_conflicts}}}\
%{expand:%%{?kernel%{?1:_%{1}}_obsoletes:Obsoletes: %%{kernel%{?1:_%{1}}_obsoletes}}}\
%{expand:%%{?kernel%{?1:_%{1}}_provides:Provides: %%{kernel%{?1:_%{1}}_provides}}}\
# We can't let RPM do the dependencies automatic because it'll then pick up\
# a correct but undesirable perl dependency from the module headers which\
# isn't required for the kernel proper to function\
AutoReq: no\
AutoProv: yes\
%{nil}


%package doc
Summary: Various documentation bits found in the kernel source
Group: Documentation
%description doc
This package contains documentation files from the kernel
source. Various bits of information about the Linux kernel and the
device drivers shipped with it are documented in these files.

You'll want to install this package if you need a reference to the
options that can be passed to Linux kernel modules at load time.


%package headers
Summary: Header files for the Linux kernel for use by glibc
Group: Development/System
Obsoletes: glibc-kernheaders < 3.0-46
Provides: glibc-kernheaders = 3.0-46
%if "0%{?variant}"
Obsoletes: kernel-headers < %{rpmversion}-%{pkg_release}
Provides: kernel-headers = %{rpmversion}-%{pkg_release}
%endif
%description headers
Kernel-headers includes the C header files that specify the interface
between the Linux kernel and userspace libraries and programs.  The
header files define structures and constants that are needed for
building most standard programs and are also needed for rebuilding the
glibc package.

%package debuginfo-common-%{_target_cpu}
Summary: Kernel source files used by %{name}-debuginfo packages
Group: Development/Debug
Provides: installonlypkg(kernel)
%description debuginfo-common-%{_target_cpu}
This package is required by %{name}-debuginfo subpackages.
It provides the kernel source files common to all builds.

%if %{with_perf}
%package -n perf
Summary: Performance monitoring for the Linux kernel
Group: Development/System
Requires: bzip2
License: GPLv2
%description -n perf
This package contains the perf tool, which enables performance monitoring
of the Linux kernel.

%package -n perf-debuginfo
Summary: Debug information for package perf
Group: Development/Debug
Requires: %{name}-debuginfo-common-%{_target_cpu} = %{version}-%{release}
AutoReqProv: no
%description -n perf-debuginfo
This package provides debug information for the perf package.

# Note that this pattern only works right to match the .build-id
# symlinks because of the trailing nonmatching alternation and
# the leading .*, because of find-debuginfo.sh's buggy handling
# of matching the pattern against the symlinks file.
%{expand:%%global _find_debuginfo_opts %{?_find_debuginfo_opts} -p '.*%%{_bindir}/perf(\.debug)?|.*%%{_libexecdir}/perf-core/.*|.*%%{_libdir}/traceevent/plugins/.*|.*%%{_libdir}/libperf-jvmti.so(\.debug)?|XXX' -o perf-debuginfo.list}

%package -n python3-perf
Summary: Python bindings for apps which will manipulate perf events
Group: Development/Libraries
%description -n python3-perf
The python3-perf package contains a module that permits applications
written in the Python programming language to use the interface
to manipulate perf events.

%package -n python3-perf-debuginfo
Summary: Debug information for package perf python bindings
Group: Development/Debug
Requires: %{name}-debuginfo-common-%{_target_cpu} = %{version}-%{release}
AutoReqProv: no
%description -n python3-perf-debuginfo
This package provides debug information for the perf python bindings.

# the python_sitearch macro should already be defined from above
%{expand:%%global _find_debuginfo_opts %{?_find_debuginfo_opts} -p '.*%%{python3_sitearch}/perf.*so(\.debug)?|XXX' -o python3-perf-debuginfo.list}

# with_perf
%endif

%if %{with_tools}
%package -n %{name}-tools
Summary: Assortment of tools for the Linux kernel
Group: Development/System
License: GPLv2
%ifarch %{cpupowerarchs}
Provides:  cpupowerutils = 1:009-0.6.p1
Obsoletes: cpupowerutils < 1:009-0.6.p1
Provides:  cpufreq-utils = 1:009-0.6.p1
Provides:  cpufrequtils = 1:009-0.6.p1
Obsoletes: cpufreq-utils < 1:009-0.6.p1
Obsoletes: cpufrequtils < 1:009-0.6.p1
Obsoletes: cpuspeed < 1:1.5-16
Requires: %{name}-tools-libs = %{version}-%{release}
%endif
%define __requires_exclude ^%{_bindir}/python
%description -n %{name}-tools
This package contains the tools/ directory from the kernel source
and the supporting documentation.

%package -n %{name}-tools-libs
Summary: Libraries for the %{name}-tools
Group: Development/System
License: GPLv2
%description -n %{name}-tools-libs
This package contains the libraries built from the tools/ directory
from the kernel source.

%package -n %{name}-tools-libs-devel
Summary: Assortment of tools for the Linux kernel
Group: Development/System
License: GPLv2
Requires: %{name}-tools = %{version}-%{release}
%ifarch %{cpupowerarchs}
Provides:  cpupowerutils-devel = 1:009-0.6.p1
Obsoletes: cpupowerutils-devel < 1:009-0.6.p1
%endif
Requires: %{name}-tools-libs = %{version}-%{release}
Provides: %{name}-tools-devel
%description -n %{name}-tools-libs-devel
This package contains the development files for the tools/ directory from
the kernel source.

%package -n %{name}-tools-debuginfo
Summary: Debug information for package %{name}-tools
Group: Development/Debug
Requires: %{name}-debuginfo-common-%{_target_cpu} = %{version}-%{release}
AutoReqProv: no
%description -n %{name}-tools-debuginfo
This package provides debug information for package %{name}-tools.

# Note that this pattern only works right to match the .build-id
# symlinks because of the trailing nonmatching alternation and
# the leading .*, because of find-debuginfo.sh's buggy handling
# of matching the pattern against the symlinks file.
%{expand:%%global _find_debuginfo_opts %{?_find_debuginfo_opts} -p '.*%%{_bindir}/centrino-decode(\.debug)?|.*%%{_bindir}/powernow-k8-decode(\.debug)?|.*%%{_bindir}/cpupower(\.debug)?|.*%%{_libdir}/libcpupower.*|.*%%{_bindir}/turbostat(\.debug)?|.*%%{_bindir}/x86_energy_perf_policy(\.debug)?|.*%%{_bindir}/tmon(\.debug)?|.*%%{_bindir}/lsgpio(\.debug)?|.*%%{_bindir}/gpio-hammer(\.debug)?|.*%%{_bindir}/gpio-event-mon(\.debug)?|.*%%{_bindir}/iio_event_monitor(\.debug)?|.*%%{_bindir}/iio_generic_buffer(\.debug)?|.*%%{_bindir}/lsiio(\.debug)?|XXX' -o %{name}-tools-debuginfo.list}

# with_tools
%endif

%if %{with_bpftool}

%package -n bpftool
Summary: Inspection and simple manipulation of eBPF programs and maps
License: GPLv2
%description -n bpftool
This package contains the bpftool, which allows inspection and simple
manipulation of eBPF programs and maps.

%package -n bpftool-debuginfo
Summary: Debug information for package bpftool
Group: Development/Debug
Requires: %{name}-debuginfo-common-%{_target_cpu} = %{version}-%{release}
AutoReqProv: no
%description -n bpftool-debuginfo
This package provides debug information for the bpftool package.

%{expand:%%global _find_debuginfo_opts %{?_find_debuginfo_opts} -p '.*%%{_sbindir}/bpftool(\.debug)?|XXX' -o bpftool-debuginfo.list}

# with_bpftool
%endif

%if %{with_gcov}
%package gcov
Summary: gcov graph and source files for coverage data collection.
Group: Development/System
%description gcov
kernel-gcov includes the gcov graph and source files for gcov coverage collection.
%endif

#
# This macro creates a kernel-<subpackage>-debuginfo package.
#	%%kernel_debuginfo_package <subpackage>
#
%define kernel_debuginfo_package() \
%package %{?1:%{1}-}debuginfo\
Summary: Debug information for package %{name}%{?1:-%{1}}\
Group: Development/Debug\
Requires: %{name}-debuginfo-common-%{_target_cpu} = %{version}-%{release}\
Provides: %{name}%{?1:-%{1}}-debuginfo-%{_target_cpu} = %{version}-%{release}\
Provides: installonlypkg(kernel)\
AutoReqProv: no\
%description %{?1:%{1}-}debuginfo\
This package provides debug information for package %{name}%{?1:-%{1}}.\
This is required to use SystemTap with %{name}%{?1:-%{1}}-%{KVERREL}.\
%{expand:%%global _find_debuginfo_opts %{?_find_debuginfo_opts} -p '/.*/%%{KVERREL_RE}%{?1:[+]%{1}}/.*|/.*%%{KVERREL_RE}%{?1:\+%{1}}(\.debug)?' -o debuginfo%{?1}.list}\
%{nil}

#
# This macro creates a kernel-<subpackage>-devel package.
#	%%kernel_devel_package <subpackage> <pretty-name>
#
%define kernel_devel_package() \
%package %{?1:%{1}-}devel\
Summary: Development package for building kernel modules to match the %{?2:%{2} }kernel\
Group: System Environment/Kernel\
Provides: %{name}%{?1:-%{1}}-devel-%{_target_cpu} = %{version}-%{release}\
Provides: %{name}-devel-%{_target_cpu} = %{version}-%{release}%{?1:+%{1}}\
Provides: %{name}-devel-uname-r = %{KVERREL}%{?variant}%{?1:+%{1}}\
Provides: installonlypkg(kernel)\
AutoReqProv: no\
Requires(pre): findutils\
Requires: findutils\
Requires: perl-interpreter\
%description %{?1:%{1}-}devel\
This package provides kernel headers and makefiles sufficient to build modules\
against the %{?2:%{2} }kernel package.\
%{nil}

#
# This macro creates a kernel-<subpackage>-modules-internal package.
#	%%kernel_modules_internal_package <subpackage> <pretty-name>
#
%define kernel_modules_internal_package() \
%package %{?1:%{1}-}modules-internal\
Summary: Extra kernel modules to match the %{?2:%{2} }kernel\
Group: System Environment/Kernel\
Provides: %{name}%{?1:-%{1}}-modules-internal-%{_target_cpu} = %{version}-%{release}\
Provides: %{name}%{?1:-%{1}}-modules-internal-%{_target_cpu} = %{version}-%{release}%{?1:+%{1}}\
Provides: %{name}%{?1:-%{1}}-modules-internal = %{version}-%{release}%{?1:+%{1}}\
Provides: installonlypkg(kernel-module)\
Provides: %{name}%{?1:-%{1}}-modules-internal-uname-r = %{KVERREL}%{?variant}%{?1:+%{1}}\
Requires: %{name}-uname-r = %{KVERREL}%{?variant}%{?1:+%{1}}\
Requires: %{name}%{?1:-%{1}}-modules-uname-r = %{KVERREL}%{?variant}%{?1:+%{1}}\
AutoReq: no\
AutoProv: yes\
%description %{?1:%{1}-}modules-internal\
This package provides kernel modules for the %{?2:%{2} }kernel package for Alibaba Cloud Linux internal usage.\
%{nil}

#
# This macro creates a kernel-<subpackage>-modules-extra package.
#	%%kernel_modules_extra_package <subpackage> <pretty-name>
#
%define kernel_modules_extra_package() \
%package %{?1:%{1}-}modules-extra\
Summary: Extra kernel modules to match the %{?2:%{2} }kernel\
Group: System Environment/Kernel\
Provides: %{name}%{?1:-%{1}}-modules-extra-%{_target_cpu} = %{version}-%{release}\
Provides: %{name}%{?1:-%{1}}-modules-extra-%{_target_cpu} = %{version}-%{release}%{?1:+%{1}}\
Provides: %{name}%{?1:-%{1}}-modules-extra = %{version}-%{release}%{?1:+%{1}}\
Provides: installonlypkg(kernel-module)\
Provides: %{name}%{?1:-%{1}}-modules-extra-uname-r = %{KVERREL}%{?variant}%{?1:+%{1}}\
Requires: %{name}-uname-r = %{KVERREL}%{?variant}%{?1:+%{1}}\
Requires: %{name}%{?1:-%{1}}-modules-uname-r = %{KVERREL}%{?variant}%{?1:+%{1}}\
AutoReq: no\
AutoProv: yes\
%description %{?1:%{1}-}modules-extra\
This package provides less commonly used kernel modules for the %{?2:%{2} }kernel package.\
%{nil}

#
# This macro creates a kernel-<subpackage>-modules package.
#	%%kernel_modules_package <subpackage> <pretty-name>
#
%define kernel_modules_package() \
%package %{?1:%{1}-}modules\
Summary: kernel modules to match the %{?2:%{2}-}core kernel\
Group: System Environment/Kernel\
Provides: %{name}%{?1:-%{1}}-modules-%{_target_cpu} = %{version}-%{release}\
Provides: %{name}-modules-%{_target_cpu} = %{version}-%{release}%{?1:+%{1}}\
Provides: %{name}-modules = %{version}-%{release}%{?1:+%{1}}\
Provides: installonlypkg(kernel-module)\
Provides: %{name}%{?1:-%{1}}-modules-uname-r = %{KVERREL}%{?variant}%{?1:+%{1}}\
Requires: %{name}-uname-r = %{KVERREL}%{?variant}%{?1:+%{1}}\
AutoReq: no\
AutoProv: yes\
%description %{?1:%{1}-}modules\
This package provides commonly used kernel modules for the %{?2:%{2}-}core kernel package.\
%{nil}

#
# this macro creates a kernel-<subpackage> meta package.
#	%%kernel_meta_package <subpackage>
#
%define kernel_meta_package() \
%package %{1}\
summary: kernel meta-package for the %{1} kernel\
group: system environment/kernel\
Requires: %{name}-%{1}-core-uname-r = %{KVERREL}%{?variant}+%{1}\
Requires: %{name}-%{1}-modules-uname-r = %{KVERREL}%{?variant}+%{1}\
Provides: installonlypkg(kernel)\
%description %{1}\
The meta-package for the %{1} kernel\
%{nil}

#
# This macro creates a %%{name}-<subpackage> and its -devel and -debuginfo too.
#	%%define variant_summary The Linux kernel compiled for <configuration>
#	%%kernel_variant_package [-n <pretty-name>] <subpackage>
#
%define kernel_variant_package(n:) \
%package %{?1:%{1}-}core\
Summary: %{variant_summary}\
Group: System Environment/Kernel\
Provides: %{name}-%{?1:%{1}-}core-uname-r = %{KVERREL}%{?variant}%{?1:+%{1}}\
Provides: installonlypkg(kernel)\
%{expand:%%kernel_reqprovconf}\
%if %{?1:1} %{!?1:0} \
%{expand:%%kernel_meta_package %{?1:%{1}}}\
%endif\
%{expand:%%kernel_devel_package %{?1:%{1}} %{!?-n:%{?1:%{1}}}%{?-n:%{-n*}}}\
%{expand:%%kernel_modules_package %{?1:%{1}} %{!?-n:%{?1:%{1}}}%{?-n:%{-n*}}}\
%{expand:%%kernel_modules_extra_package %{?1:%{1}} %{!?-n:%{?1:%{1}}}%{?-n:%{-n*}}}\
%{expand:%%kernel_modules_internal_package %{?1:%{1}} %{!?-n:%{?1:%{1}}}%{?-n:%{-n*}}}\
%{expand:%%kernel_debuginfo_package %{?1:%{1}}}\
%{nil}

# Now, each variant package.

%define variant_summary The Linux kernel compiled with extra debugging enabled
%kernel_variant_package debug
%description debug-core
The kernel package contains the Linux kernel (vmlinuz), the core of any
Linux operating system.  The kernel handles the basic functions
of the operating system:  memory allocation, process allocation, device
input and output, etc.

This variant of the kernel has numerous debugging options enabled.
It should only be installed when trying to gather additional information
on kernel bugs, as some of these options impact performance noticably.

# And finally the main -core package

%define variant_summary The Linux kernel
%kernel_variant_package
%description core
The kernel package contains the Linux kernel (vmlinuz), the core of any
Linux operating system.  The kernel handles the basic functions
of the operating system: memory allocation, process allocation, device
input and output, etc.

%prep
# do a few sanity-checks for --with *only builds
%if %{with_baseonly}
%if !%{with_up}
echo "Cannot build --with baseonly, up build is disabled"
exit 1
%endif
%endif

# more sanity checking; do it quietly
if [ "%{patches}" != "%%{patches}" ] ; then
  for patch in %{patches} ; do
    if [ ! -f $patch ] ; then
      echo "ERROR: Patch  ${patch##/*/}  listed in specfile but is missing"
      exit 1
    fi
  done
fi 2>/dev/null

patch_command='patch -p1 -F1 -s'
ApplyPatch()
{
  local patch=$1
  shift
  if [ ! -f $RPM_SOURCE_DIR/$patch ]; then
    exit 1
  fi
  if ! grep -E "^Patch[0-9]+: $patch\$" %{_specdir}/${RPM_PACKAGE_NAME%%%%%{?variant}}.spec ; then
    if [ "${patch:0:8}" != "patch-4." ] ; then
      echo "ERROR: Patch  $patch  not listed as a source patch in specfile"
      exit 1
    fi
  fi 2>/dev/null
  case "$patch" in
  *.bz2) bunzip2 < "$RPM_SOURCE_DIR/$patch" | sed -n '/^---$/,$p' | $patch_command ${1+"$@"} ;;
  *.gz)  gunzip  < "$RPM_SOURCE_DIR/$patch" | sed -n '/^---$/,$p' | $patch_command ${1+"$@"} ;;
  *.xz)  unxz    < "$RPM_SOURCE_DIR/$patch" | sed -n '/^---$/,$p' | $patch_command ${1+"$@"} ;;
  *) sed -n '/^---$/,$p' "$RPM_SOURCE_DIR/$patch" | $patch_command ${1+"$@"} ;;
  esac
}

# don't apply patch if it's empty
ApplyOptionalPatch()
{
  local patch=$1
  shift
  if [ ! -f $RPM_SOURCE_DIR/$patch ]; then
    exit 1
  fi
  local C=$(wc -l $RPM_SOURCE_DIR/$patch | awk '{print $1}')
  if [ "$C" -gt 9 ]; then
    ApplyPatch $patch ${1+"$@"}
  fi
}

%setup -q -n %{name}-%{rpmversion}-%{pkg_release} -c
mv linux-%{rpmversion}-%{pkg_release} linux-%{KVERREL}

cd linux-%{KVERREL}

# Drop some necessary files from the source dir into the buildroot
cp $RPM_SOURCE_DIR/kernel-%{version}-*.config .

# %%PATCH_APPLICATION%%

# END OF PATCH APPLICATIONS

# Any further pre-build tree manipulations happen here.

chmod +x scripts/checkpatch.pl
mv COPYING COPYING-%{version}

# This Prevents scripts/setlocalversion from mucking with our version numbers.
touch .scmversion

# Do not use "ambiguous" python shebangs. RHEL 8 now has a new script
# (/usr/lib/rpm/redhat/brp-mangle-shebangs), which forces us to specify a
# "non-ambiguous" python shebang for scripts we ship in buildroot. This
# script throws an error like below:
# *** ERROR: ambiguous python shebang in /usr/bin/kvm_stat: #!/usr/bin/python. Change it to python3 (or python2) explicitly.
# We patch all sources below for which we got a report/error.
for path in \
	scripts/show_delta \
	scripts/diffconfig \
	scripts/bloat-o-meter \
	scripts/jobserver-exec \
	scripts/run-clang-tools.py \
	scripts/gen_compile_commands.py \
	scripts/clang-tools/run-clang-tools.py \
	scripts/clang-tools/gen_compile_commands.py \
	tools/perf/tests/attr.py \
	tools/perf/scripts/python/stat-cpi.py \
	tools/perf/scripts/python/sched-migration.py \
	Documentation
do
  if [ -e $path ]; then
    pathfix.py -i %{__python3} -p -n $path
  fi
done

%define make make HOSTCFLAGS="%{?build_hostcflags}" HOSTLDFLAGS="%{?build_hostldflags}"

# only deal with configs if we are going to build for the arch
%ifnarch %nobuildarches

rm -rf configs
mkdir configs

# Remove configs not for the buildarch
for cfg in kernel-%{version}-*.config; do
  if [ `echo %{all_arch_configs} | grep -c $cfg` -eq 0 ]; then
    rm -f $cfg
  fi
done

# enable GCOV kernel config options if gcov is on
%if %{with_gcov}
for i in *.config
do
  sed -i 's/# CONFIG_GCOV_KERNEL is not set/CONFIG_GCOV_KERNEL=y\nCONFIG_GCOV_PROFILE_ALL=y\n/' $i
done
%endif

# now run oldconfig over all the config files
for i in *.config
do
  mv $i .config
  Arch=`sed -n 3p .config | cut -d' ' -f2 | cut -d'/' -f2`
  make ARCH=$Arch listnewconfig | grep -E '^CONFIG_' >.newoptions || true
  if [ -s .newoptions ]; then
    cat .newoptions
    #exit 1
  fi
  rm -f .newoptions
  make ARCH=$Arch olddefconfig
  echo "# $Arch" > configs/$i
  cat .config >> configs/$i
done
# end of kernel config
%endif

# # End of Configs stuff

# get rid of unwanted files resulting from patch fuzz
find . \( -name "*.orig" -o -name "*~" \) -exec rm -f {} \; >/dev/null

# remove unnecessary SCM files
find . -name .gitignore -exec rm -f {} \; >/dev/null

cd ..

###
### build
###
%build

%if %{with_sparse}
%define sparse_mflags	C=1
%endif

cp_vmlinux()
{
  eu-strip --remove-comment -o "$2" "$1"
}

BuildKernel() {
    MakeTarget=$1
    KernelImage=$2
    Flavour=$3
    Flav=${Flavour:++${Flavour}}
    InstallName=${5:-vmlinuz}

    DoModules=1

    # Pick the right config file for the kernel we're building
    Config=kernel-%{version}-%{_target_cpu}${Flavour:+-${Flavour}}.config
    DevelDir=/usr/src/kernels/%{KVERREL}${Flav}

    # When the bootable image is just the ELF kernel, strip it.
    # We already copy the unstripped file into the debuginfo package.
    if [ "$KernelImage" = vmlinux ]; then
      CopyKernel=cp_vmlinux
    else
      CopyKernel=cp
    fi

    KernelVer=%{version}-%{release}.%{_target_cpu}${Flav}
    echo BUILDING A KERNEL FOR ${Flavour} %{_target_cpu}...

    # make sure EXTRAVERSION says what we want it to say
    perl -p -i -e "s/^EXTRAVERSION.*/EXTRAVERSION = -%{release}.%{_target_cpu}${Flav}/" Makefile

    # and now to start the build process

    %{make} -s %{?_smp_mflags} mrproper
    cp configs/$Config .config

    %if %{signmodules}
    cp %{SOURCE11} certs/.
    cp %{SOURCE12} certs/.
    %endif

    Arch=`head -1 .config | cut -b 3-`
    echo USING ARCH=$Arch

    KCFLAGS="%{?kcflags}"

    # add kpatch flags for base kernel
    if [ "$Flavour" == "" ]; then
        KCFLAGS="$KCFLAGS %{?kpatch_kcflags}"
    fi

    %{make} -s ARCH=$Arch olddefconfig >/dev/null
    %{make} -s ARCH=$Arch V=1 %{?_smp_mflags} KCFLAGS="$KCFLAGS" WITH_GCOV="%{?with_gcov}" $MakeTarget %{?sparse_mflags} %{?kernel_mflags}
    if [ $DoModules -eq 1 ]; then
	%{make} -s ARCH=$Arch V=1 %{?_smp_mflags} KCFLAGS="$KCFLAGS" WITH_GCOV="%{?with_gcov}" modules %{?sparse_mflags} || exit 1
    fi

    mkdir -p $RPM_BUILD_ROOT/%{image_install_path}
    mkdir -p $RPM_BUILD_ROOT/lib/modules/$KernelVer
%if %{with_debuginfo}
    mkdir -p $RPM_BUILD_ROOT%{debuginfodir}/%{image_install_path}
%endif

%ifarch aarch64
    %{make} -s ARCH=$Arch V=1 dtbs dtbs_install INSTALL_DTBS_PATH=$RPM_BUILD_ROOT/%{image_install_path}/dtb-$KernelVer
    cp -r $RPM_BUILD_ROOT/%{image_install_path}/dtb-$KernelVer $RPM_BUILD_ROOT/lib/modules/$KernelVer/dtb
    find arch/$Arch/boot/dts -name '*.dtb' -type f | xargs rm -f
%endif

    # Start installing the results
    install -m 644 .config $RPM_BUILD_ROOT/boot/config-$KernelVer
    install -m 644 .config $RPM_BUILD_ROOT/lib/modules/$KernelVer/config
    install -m 644 System.map $RPM_BUILD_ROOT/boot/System.map-$KernelVer
    install -m 644 System.map $RPM_BUILD_ROOT/lib/modules/$KernelVer/System.map

    # We estimate the size of the initramfs because rpm needs to take this size
    # into consideration when performing disk space calculations. (See bz #530778)
    dd if=/dev/zero of=$RPM_BUILD_ROOT/boot/initramfs-$KernelVer.img bs=1M count=20

    if [ -f arch/$Arch/boot/zImage.stub ]; then
      cp arch/$Arch/boot/zImage.stub $RPM_BUILD_ROOT/%{image_install_path}/zImage.stub-$KernelVer || :
      cp arch/$Arch/boot/zImage.stub $RPM_BUILD_ROOT/lib/modules/$KernelVer/zImage.stub-$KernelVer || :
    fi

    $CopyKernel $KernelImage \
                $RPM_BUILD_ROOT/%{image_install_path}/$InstallName-$KernelVer
    chmod 755 $RPM_BUILD_ROOT/%{image_install_path}/$InstallName-$KernelVer
    cp $RPM_BUILD_ROOT/%{image_install_path}/$InstallName-$KernelVer $RPM_BUILD_ROOT/lib/modules/$KernelVer/$InstallName

    # hmac sign the kernel for FIPS
    echo "Creating hmac file: $RPM_BUILD_ROOT/%{image_install_path}/.vmlinuz-$KernelVer.hmac"
    ls -l $RPM_BUILD_ROOT/%{image_install_path}/$InstallName-$KernelVer
    sha512hmac $RPM_BUILD_ROOT/%{image_install_path}/$InstallName-$KernelVer | sed -e "s,$RPM_BUILD_ROOT,," > $RPM_BUILD_ROOT/%{image_install_path}/.vmlinuz-$KernelVer.hmac;
    cp $RPM_BUILD_ROOT/%{image_install_path}/.vmlinuz-$KernelVer.hmac $RPM_BUILD_ROOT/lib/modules/$KernelVer/.vmlinuz.hmac

    if [ $DoModules -eq 1 ]; then
	# Override $(mod-fw) because we don't want it to install any firmware
	# we'll get it from the linux-firmware package and we don't want conflicts
	%{make} -s %{?_smp_mflags} ARCH=$Arch INSTALL_MOD_PATH=$RPM_BUILD_ROOT modules_install KERNELRELEASE=$KernelVer mod-fw=
    fi

%if %{with_gcov}
    # install gcov-needed files to $BUILDROOT/$BUILD/...:
    #   gcov_info->filename is absolute path
    #   gcno references to sources can use absolute paths (e.g. in out-of-tree builds)
    #   sysfs symlink targets (set up at compile time) use absolute paths to BUILD dir
    find . \( -name '*.gcno' -o -name '*.[chS]' \) -exec install -D '{}' "$RPM_BUILD_ROOT/$(pwd)/{}" \;
%endif

    if [ $DoVDSO -ne 0 ]; then
        %{make} -s ARCH=$Arch INSTALL_MOD_PATH=$RPM_BUILD_ROOT vdso_install KERNELRELEASE=$KernelVer
        if [ ! -s ldconfig-kernel.conf ]; then
          echo > ldconfig-kernel.conf "\
    # Placeholder file, no vDSO hwcap entries used in this kernel."
        fi
        %{__install} -D -m 444 ldconfig-kernel.conf \
            $RPM_BUILD_ROOT/etc/ld.so.conf.d/%{name}-$KernelVer.conf
        rm -rf $RPM_BUILD_ROOT/lib/modules/$KernelVer/vdso/.build-id
    fi

    # And save the headers/makefiles etc for building modules against
    #
    # This all looks scary, but the end result is supposed to be:
    # * all arch relevant include/ files
    # * all Makefile/Kconfig files
    # * all script/ files

    rm -f $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
    rm -f $RPM_BUILD_ROOT/lib/modules/$KernelVer/source
    mkdir -p $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
    (cd $RPM_BUILD_ROOT/lib/modules/$KernelVer ; ln -s build source)
    # dirs for additional modules per module-init-tools, kbuild/modules.txt
    mkdir -p $RPM_BUILD_ROOT/lib/modules/$KernelVer/updates
    mkdir -p $RPM_BUILD_ROOT/lib/modules/$KernelVer/weak-updates
    # first copy everything
    cp --parents `find  -type f -name "Makefile*" -o -name "Kconfig*"` $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
    cp Module.symvers $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
    cp System.map $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
    if [ -s Module.markers ]; then
      cp Module.markers $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
    fi

    # create the kABI metadata for use in packaging
    # NOTENOTE: the name symvers is used by the rpm backend
    # NOTENOTE: to discover and run the /usr/lib/rpm/fileattrs/kabi.attr
    # NOTENOTE: script which dynamically adds exported kernel symbol
    # NOTENOTE: checksums to the rpm metadata provides list.
    # NOTENOTE: if you change the symvers name, update the backend too
    echo "**** GENERATING kernel ABI metadata ****"
    gzip -c9 < Module.symvers > $RPM_BUILD_ROOT/boot/symvers-$KernelVer.gz
    cp $RPM_BUILD_ROOT/boot/symvers-$KernelVer.gz $RPM_BUILD_ROOT/lib/modules/$KernelVer/symvers.gz

    # then drop all but the needed Makefiles/Kconfig files
    rm -rf $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/Documentation
    rm -rf $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/scripts
    rm -rf $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/include
    cp .config $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
    cp -a scripts $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
    rm -rf $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/scripts/tracing
    rm -f $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/scripts/spdxcheck.py
    if [ -f tools/objtool/objtool ]; then
      cp -a tools/objtool/objtool $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/tools/objtool/ || :
    fi
    if [ -d arch/$Arch/scripts ]; then
      cp -a arch/$Arch/scripts $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/arch/%{_arch} || :
    fi
    if [ -f arch/$Arch/*lds ]; then
      cp -a arch/$Arch/*lds $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/arch/%{_arch}/ || :
    fi
    if [ -f arch/%{asmarch}/kernel/module.lds ]; then
      cp -a --parents arch/%{asmarch}/kernel/module.lds $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    fi
    rm -f $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/scripts/*.o
    rm -f $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/scripts/*/*.o
    if [ -d arch/%{asmarch}/include ]; then
      cp -a --parents arch/%{asmarch}/include $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    fi
%ifarch aarch64
    # arch/arm64/include/asm/xen references arch/arm
    cp -a --parents arch/arm/include/asm/xen $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    # arch/arm64/include/asm/opcodes.h references arch/arm
    cp -a --parents arch/arm/include/asm/opcodes.h $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
%endif
    cp -a include $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/include
%ifarch x86_64
    # files for 'make prepare' to succeed with kernel-devel
    cp -a --parents arch/x86/entry/syscalls/syscall_32.tbl $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    cp -a --parents arch/x86/entry/syscalls/syscalltbl.sh $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    cp -a --parents arch/x86/entry/syscalls/syscallhdr.sh $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    cp -a --parents arch/x86/entry/syscalls/syscall_64.tbl $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    cp -a --parents arch/x86/tools/relocs_32.c $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    cp -a --parents arch/x86/tools/relocs_64.c $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    cp -a --parents arch/x86/tools/relocs.c $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    cp -a --parents arch/x86/tools/relocs_common.c $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    cp -a --parents arch/x86/tools/relocs.h $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    cp -a --parents tools/include/tools/le_byteshift.h $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    cp -a --parents arch/x86/purgatory/purgatory.c $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    cp -a --parents arch/x86/purgatory/stack.S $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    cp -a --parents arch/x86/purgatory/setup-x86_64.S $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    cp -a --parents arch/x86/purgatory/entry64.S $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    cp -a --parents arch/x86/boot/string.h $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    cp -a --parents arch/x86/boot/string.c $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
    cp -a --parents arch/x86/boot/ctype.h $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
%endif
    # Make sure the Makefile and version.h have a matching timestamp so that
    # external modules can be built
    touch -r $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/Makefile $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/include/generated/uapi/linux/version.h

    # Copy .config to include/config/auto.conf so "make prepare" is unnecessary.
    cp $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/.config $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/include/config/auto.conf

%if %{with_debuginfo}
    eu-readelf -n vmlinux | grep "Build ID" | awk '{print $NF}' > vmlinux.id
    cp vmlinux.id $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/vmlinux.id

    #
    # save the vmlinux file for kernel debugging into the kernel-debuginfo rpm
    #
    mkdir -p $RPM_BUILD_ROOT%{debuginfodir}/lib/modules/$KernelVer
    cp vmlinux $RPM_BUILD_ROOT%{debuginfodir}/lib/modules/$KernelVer
%endif

    find $RPM_BUILD_ROOT/lib/modules/$KernelVer -name "*.ko" -type f >modnames

    # mark modules executable so that strip-to-file can strip them
    xargs --no-run-if-empty chmod u+x < modnames

    # Generate a list of modules for block and networking.

    grep -F /drivers/ modnames | xargs --no-run-if-empty nm -upA |
    sed -n 's,^.*/\([^/]*\.ko\):  *U \(.*\)$,\1 \2,p' > drivers.undef

    collect_modules_list()
    {
      sed -r -n -e "s/^([^ ]+) \\.?($2)\$/\\1/p" drivers.undef |
        LC_ALL=C sort -u > $RPM_BUILD_ROOT/lib/modules/$KernelVer/modules.$1
      if [ ! -z "$3" ]; then
        sed -r -e "/^($3)\$/d" -i $RPM_BUILD_ROOT/lib/modules/$KernelVer/modules.$1
      fi
    }

    collect_modules_list networking \
      'register_netdev|ieee80211_register_hw|usbnet_probe|phy_driver_register|rt(l_|2x00)(pci|usb)_probe|register_netdevice'
    collect_modules_list block \
      'ata_scsi_ioctl|scsi_add_host|scsi_add_host_with_dma|blk_alloc_queue|blk_init_queue|register_mtd_blktrans|scsi_esp_register|scsi_register_device_handler|blk_queue_physical_block_size' 'pktcdvd.ko|dm-mod.ko'
    collect_modules_list drm \
      'drm_open|drm_init'
    collect_modules_list modesetting \
      'drm_crtc_init'

    # detect missing or incorrect license tags
    ( find $RPM_BUILD_ROOT/lib/modules/$KernelVer -name '*.ko' | xargs /sbin/modinfo -l | \
        grep -E -v 'GPL( v2)?$|Dual BSD/GPL$|Dual MPL/GPL$|GPL and additional rights$' ) && exit 1

    # remove files that will be auto generated by depmod at rpm -i time
    pushd $RPM_BUILD_ROOT/lib/modules/$KernelVer/
        rm -f modules.{alias*,builtin.bin,dep*,*map,symbols*,devname,softdep}
    popd

    # Identify modules in the kernel-modules-extras package
    %{SOURCE17} $RPM_BUILD_ROOT lib/modules/$KernelVer %{SOURCE16}
    # Identify modules in the kernel-modules-internal package
    %{SOURCE17} $RPM_BUILD_ROOT lib/modules/$KernelVer %{SOURCE44} internal

    #
    # Generate the kernel-core and kernel-modules files lists
    #

    # Copy the System.map file for depmod to use, and create a backup of the
    # full module tree so we can restore it after we're done filtering
    cp System.map $RPM_BUILD_ROOT/.
    pushd $RPM_BUILD_ROOT
    mkdir restore
    cp -r lib/modules/$KernelVer/* restore/.

    # don't include anything going into kernel-modules-extra in the file lists
    xargs rm -rf < mod-extra.list
    # don't include anything going int kernel-modules-internal in the file lists
    xargs rm -rf < mod-internal.list

    if [ $DoModules -eq 1 ]; then
	# Find all the module files and filter them out into the core and
	# modules lists.  This actually removes anything going into -modules
	# from the dir.
	find lib/modules/$KernelVer/kernel -name *.ko | sort -n > modules.list
	cp $RPM_SOURCE_DIR/filter-*.sh .
	%{SOURCE99} modules.list %{_target_cpu}
	rm filter-*.sh

	# Run depmod on the resulting module tree and make sure it isn't broken
	depmod -b . -aeF ./System.map $KernelVer &> depmod.out
	if [ -s depmod.out ]; then
	    echo "Depmod failure"
	    cat depmod.out
	    exit 1
	else
	    rm depmod.out
	fi
    else
	# Ensure important files/directories exist to let the packaging succeed
	echo '%%defattr(-,-,-)' > modules.list
	echo '%%defattr(-,-,-)' > k-d.list
	mkdir -p lib/modules/$KernelVer/kernel
	# Add files usually created by make modules, needed to prevent errors
	# thrown by depmod during package installation
	touch lib/modules/$KernelVer/modules.order
	touch lib/modules/$KernelVer/modules.builtin
    fi

    # remove files that will be auto generated by depmod at rpm -i time
    pushd $RPM_BUILD_ROOT/lib/modules/$KernelVer/
        rm -f modules.{alias*,builtin.bin,dep*,*map,symbols*,devname,softdep}
    popd

    # Go back and find all of the various directories in the tree.  We use this
    # for the dir lists in kernel-core
    find lib/modules/$KernelVer/kernel -mindepth 1 -type d | sort -n > module-dirs.list

    # Cleanup
    rm System.map
    cp -r restore/* lib/modules/$KernelVer/.
    rm -rf restore
    popd

    # Make sure the files lists start with absolute paths or rpmbuild fails.
    # Also add in the dir entries
    sed -e 's/^lib*/\/lib/' %{?zipsed} $RPM_BUILD_ROOT/k-d.list > ../%{name}${Flavour:+-${Flavour}}-modules.list
    sed -e 's/^lib*/%dir \/lib/' %{?zipsed} $RPM_BUILD_ROOT/module-dirs.list > ../%{name}${Flavour:+-${Flavour}}-core.list
    sed -e 's/^lib*/\/lib/' %{?zipsed} $RPM_BUILD_ROOT/modules.list >> ../%{name}${Flavour:+-${Flavour}}-core.list
    sed -e 's/^lib*/\/lib/' %{?zipsed} $RPM_BUILD_ROOT/mod-extra.list >> ../%{name}${Flavour:+-${Flavour}}-modules-extra.list
    sed -e 's/^lib*/\/lib/' %{?zipsed} $RPM_BUILD_ROOT/mod-internal.list >> ../%{name}${Flavour:+-${Flavour}}-modules-internal.list

    # Cleanup
    rm -f $RPM_BUILD_ROOT/k-d.list
    rm -f $RPM_BUILD_ROOT/modules.list
    rm -f $RPM_BUILD_ROOT/module-dirs.list
    rm -f $RPM_BUILD_ROOT/mod-extra.list
    rm -f $RPM_BUILD_ROOT/mod-internal.list

%if %{signmodules}
    if [ $DoModules -eq 1 ]; then
	# Save the signing keys so we can sign the modules in __modsign_install_post
	cp certs/signing_key.pem certs/signing_key.pem.sign${Flav}
	cp certs/signing_key.x509 certs/signing_key.x509.sign${Flav}
    fi
%endif

    # Move the devel headers out of the root file system
    mkdir -p $RPM_BUILD_ROOT/usr/src/kernels
    mv $RPM_BUILD_ROOT/lib/modules/$KernelVer/build $RPM_BUILD_ROOT/$DevelDir

    # This is going to create a broken link during the build, but we don't use
    # it after this point.  We need the link to actually point to something
    # when kernel-devel is installed, and a relative link doesn't work across
    # the F17 UsrMove feature.
    ln -sf $DevelDir $RPM_BUILD_ROOT/lib/modules/$KernelVer/build

    # prune junk from kernel-devel
    find $RPM_BUILD_ROOT/usr/src/kernels -name ".*.cmd" -exec rm -f {} \;

    # build a BLS config for this kernel
    %{SOURCE43} "$KernelVer" "$RPM_BUILD_ROOT" "%{?variant}"
}

###
# DO it...
###

# prepare directories
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/boot
mkdir -p $RPM_BUILD_ROOT%{_libexecdir}

cd linux-%{KVERREL}


%if %{with_debug}
BuildKernel %make_target %kernel_image debug
%endif

%if %{with_up}
BuildKernel %make_target %kernel_image 
%endif

%global perf_make \
  make EXTRA_CFLAGS="${RPM_OPT_FLAGS}" LDFLAGS="%{__global_ldflags}" -C tools/perf V=1 NO_PERF_READ_VDSO32=1 NO_PERF_READ_VDSOX32=1 WERROR=0 NO_LIBUNWIND=1 HAVE_CPLUS_DEMANGLE=1 NO_GTK2=1 NO_STRLCPY=1 NO_BIONIC=1 prefix=%{_prefix} PYTHON=%{__python3}
%if %{with_perf}
# perf
# make sure check-headers.sh is executable
chmod +x tools/perf/check-headers.sh
%{perf_make} DESTDIR=$RPM_BUILD_ROOT all
%endif

%global tools_make \
  %{make} V=1

%if %{with_tools}
%ifarch %{cpupowerarchs}
# cpupower
# make sure version-gen.sh is executable.
chmod +x tools/power/cpupower/utils/version-gen.sh
%{tools_make} -C tools/power/cpupower CPUFREQ_BENCH=false DEBUG=false
%ifarch x86_64
    pushd tools/power/cpupower/debug/x86_64
    %{tools_make} centrino-decode powernow-k8-decode
    popd
%endif
%ifarch x86_64
   pushd tools/power/x86/x86_energy_perf_policy/
   %{tools_make}
   popd
   pushd tools/power/x86/turbostat
   %{tools_make}
   popd
%endif #turbostat/x86_energy_perf_policy
%endif
pushd tools/thermal/tmon/
%{tools_make}
popd
pushd tools/iio/
%{tools_make}
popd
pushd tools/gpio/
%{tools_make}
popd
%endif

%global bpftool_make \
  make EXTRA_CFLAGS="${RPM_OPT_FLAGS}" EXTRA_LDFLAGS="%{__global_ldflags}" DESTDIR=$RPM_BUILD_ROOT V=1
%if %{with_bpftool}
pushd tools/bpf/bpftool
%{bpftool_make}
popd
%endif

%if %{with_doc}
# Make the HTML pages.
make htmldocs || %{doc_build_fail}

# sometimes non-world-readable files sneak into the kernel source tree
chmod -R a=rX Documentation
find Documentation -type d | xargs chmod u+w
%endif

# In the modsign case, we do 3 things.  1) We check the "flavour" and hard
# code the value in the following invocations.  This is somewhat sub-optimal
# but we're doing this inside of an RPM macro and it isn't as easy as it
# could be because of that.  2) We restore the .tmp_versions/ directory from
# the one we saved off in BuildKernel above.  This is to make sure we're
# signing the modules we actually built/installed in that flavour.  3) We
# grab the arch and invoke mod-sign.sh command to actually sign the modules.
#
# We have to do all of those things _after_ find-debuginfo runs, otherwise
# that will strip the signature off of the modules.

%define __modsign_install_post \
  if [ "%{signmodules}" -eq "1" ]; then \
    if [ "%{with_debug}" -ne "0" ]; then \
      %{modsign_cmd} certs/signing_key.pem.sign+debug certs/signing_key.x509.sign+debug $RPM_BUILD_ROOT/lib/modules/%{KVERREL}+debug/ \
    fi \
    if [ "%{with_up}" -ne "0" ]; then \
      %{modsign_cmd} certs/signing_key.pem.sign certs/signing_key.x509.sign $RPM_BUILD_ROOT/lib/modules/%{KVERREL}/ \
    fi \
  fi \
%{nil}

###
### Special hacks for debuginfo subpackages.
###

# This macro is used by %%install, so we must redefine it before that.
%define debug_package %{nil}

%if %{with_debuginfo}

%ifnarch noarch
%global __debug_package 1
%files -f debugfiles.list debuginfo-common-%{_target_cpu}
%defattr(-,root,root)
%endif

%endif

#
# Disgusting hack alert! We need to ensure we sign modules *after* all
# invocations of strip occur, which is in __debug_install_post if
# find-debuginfo.sh runs, and __os_install_post if not.
#
%define __spec_install_post \
  %{?__debug_package:%{__debug_install_post}}\
  %{__arch_install_post}\
  %{__os_install_post}\
  %{__modsign_install_post}

###
### install
###

%install

cd linux-%{KVERREL}

%if %{with_doc}
docdir=$RPM_BUILD_ROOT%{_datadir}/doc/kernel-doc-%{rpmversion}

# copy the source over
mkdir -p $docdir
tar -h -f - --exclude=man --exclude='.*' -c Documentation | tar xf - -C $docdir

# with_doc
%endif

# We have to do the headers install before the tools install because the
# kernel headers_install will remove any header files in /usr/include that
# it doesn't install itself.

%if %{with_headers}
# Install kernel headers
%{make} ARCH=%{hdrarch} INSTALL_HDR_PATH=$RPM_BUILD_ROOT/usr headers_install

find $RPM_BUILD_ROOT/usr/include \
     \( -name .install -o -name .check -o \
        -name ..install.cmd -o -name ..check.cmd \) | xargs rm -f

%endif

%if %{with_perf}
# perf tool binary and supporting scripts/binaries
%{perf_make} DESTDIR=$RPM_BUILD_ROOT lib=%{_lib} install-bin install-traceevent-plugins
# remove the 'trace' symlink.
rm -f %{buildroot}%{_bindir}/trace

# For both of the below, yes, this should be using a macro but right now
# it's hard coded and we don't actually want it anyway right now.
# Whoever wants examples can fix it up!

# remove examples
rm -rf %{buildroot}/usr/lib/examples/perf
# remove the stray header file that somehow got packaged in examples
rm -rf %{buildroot}/usr/lib/include/perf/bpf/bpf.h

# remove perf-bpf examples
rm -rf %{buildroot}/usr/lib/perf/examples
rm -rf %{buildroot}/usr/lib/perf/include

# python-perf extension
%{perf_make} DESTDIR=$RPM_BUILD_ROOT install-python_ext

# perf man pages (note: implicit rpm magic compresses them later)
mkdir -p %{buildroot}/%{_mandir}/man1
%{perf_make} DESTDIR=$RPM_BUILD_ROOT install-man
%endif

%if %{with_tools}
%ifarch %{cpupowerarchs}
%{make} -C tools/power/cpupower DESTDIR=$RPM_BUILD_ROOT libdir=%{_libdir} mandir=%{_mandir} CPUFREQ_BENCH=false install
rm -f %{buildroot}%{_libdir}/*.{a,la}
%find_lang cpupower
mv cpupower.lang ../
%ifarch x86_64
    pushd tools/power/cpupower/debug/x86_64
    install -m755 centrino-decode %{buildroot}%{_bindir}/centrino-decode
    install -m755 powernow-k8-decode %{buildroot}%{_bindir}/powernow-k8-decode
    popd
%endif
chmod 0755 %{buildroot}%{_libdir}/libcpupower.so*
mkdir -p %{buildroot}%{_unitdir} %{buildroot}%{_sysconfdir}/sysconfig
install -m644 %{SOURCE2000} %{buildroot}%{_unitdir}/cpupower.service
install -m644 %{SOURCE2001} %{buildroot}%{_sysconfdir}/sysconfig/cpupower
%endif
%ifarch x86_64
   mkdir -p %{buildroot}%{_mandir}/man8
   pushd tools/power/x86/x86_energy_perf_policy
   %{tools_make} DESTDIR=%{buildroot} install
   popd
   pushd tools/power/x86/turbostat
   %{tools_make} DESTDIR=%{buildroot} install
   popd
%endif #turbostat/x86_energy_perf_policy
pushd tools/thermal/tmon
%{tools_make} INSTALL_ROOT=%{buildroot} install
popd
pushd tools/iio
%{tools_make} DESTDIR=%{buildroot} install
popd
pushd tools/gpio
%{tools_make} DESTDIR=%{buildroot} install
popd
pushd tools/kvm/kvm_stat
make INSTALL_ROOT=%{buildroot} install-tools
make INSTALL_ROOT=%{buildroot} install-man
popd
%endif

%if %{with_bpftool}
pushd tools/bpf/bpftool
%{bpftool_make} prefix=%{_prefix} bash_compdir=%{_sysconfdir}/bash_completion.d/ mandir=%{_mandir} install doc-install
popd
%endif

# We have to do the headers checksum calculation after the tools install because
# these might end up installing their own set of headers on top of kernel's
%if %{with_headers}
# compute a content hash to export as Provides: kernel-headers-checksum
HEADERS_CHKSUM=$(export LC_ALL=C; find $RPM_BUILD_ROOT/usr/include -type f -name "*.h" \
			! -path $RPM_BUILD_ROOT/usr/include/linux/version.h | \
		 sort | xargs cat | sha1sum - | cut -f 1 -d ' ');
# export the checksum via usr/include/linux/version.h, so the dynamic
# find-provides can grab the hash to update it accordingly
echo "#define KERNEL_HEADERS_CHECKSUM \"$HEADERS_CHKSUM\"" >> $RPM_BUILD_ROOT/usr/include/linux/version.h
%endif

###
### clean
###

%clean
rm -rf $RPM_BUILD_ROOT

###
### scripts
###

%if %{with_tools}
%post -n %{name}-tools-libs
/sbin/ldconfig

%postun -n %{name}-tools-libs
/sbin/ldconfig
%endif

#
# This macro defines a %%post script for a kernel*-devel package.
#	%%kernel_devel_post [<subpackage>]
#
%define kernel_devel_post() \
%{expand:%%post %{?1:%{1}-}devel}\
if [ -f /etc/sysconfig/kernel ]\
then\
    . /etc/sysconfig/kernel || exit $?\
fi\
if [ "$HARDLINK" != "no" -a -x /usr/sbin/hardlink ]\
then\
    (cd /usr/src/kernels/%{KVERREL}%{?1:+%{1}} &&\
     /usr/bin/find . -type f | while read f; do\
       hardlink -c /usr/src/kernels/*%{?dist}.*/$f $f\
     done)\
fi\
%{nil}

#
# This macro defines a %%post script for a kernel*-modules-extra package.
# It also defines a %%postun script that does the same thing.
#	%%kernel_modules_extra_post [<subpackage>]
#
%define kernel_modules_extra_post() \
%{expand:%%post %{?1:%{1}-}modules-extra}\
/sbin/depmod -a %{KVERREL}%{?1:+%{1}}\
%{nil}\
%{expand:%%postun %{?1:%{1}-}modules-extra}\
/sbin/depmod -a %{KVERREL}%{?1:+%{1}}\
%{nil}

#
# This macro defines a %%post script for a kernel*-modules-internal package.
# It also defines a %%postun script that does the same thing.
#	%%kernel_modules_internal_post [<subpackage>]
#
%define kernel_modules_internal_post() \
%{expand:%%post %{?1:%{1}-}modules-internal}\
/sbin/depmod -a %{KVERREL}%{?1:+%{1}}\
%{nil}\
%{expand:%%postun %{?1:%{1}-}modules-internal}\
/sbin/depmod -a %{KVERREL}%{?1:+%{1}}\
%{nil}

#
# This macro defines a %%post script for a kernel*-modules package.
# It also defines a %%postun script that does the same thing.
#	%%kernel_modules_post [<subpackage>]
#
%define kernel_modules_post() \
%{expand:%%post %{?1:%{1}-}modules}\
/sbin/depmod -a %{KVERREL}%{?1:+%{1}}\
%{nil}\
%{expand:%%postun %{?1:%{1}-}modules}\
/sbin/depmod -a %{KVERREL}%{?1:+%{1}}\
%{nil}

# This macro defines a %%posttrans script for a kernel*-modules package.
#       %%kernel_modules_posttrans [<subpackage>]
# More text can follow to go at the end of this modules' %%post.
#
%define kernel_modules_posttrans() \
%{expand:%%posttrans %{?1:%{1}-}modules}\
/bin/kernel-install add %{KVERREL}%{?1:+%{1}} /lib/modules/%{KVERREL}%{?1:+%{1}}/vmlinuz || exit $?\
%{nil}

# This macro defines a %%posttrans script for a kernel package.
#	%%kernel_variant_posttrans [<subpackage>]
# More text can follow to go at the end of this variant's %%post.
#
%define kernel_variant_posttrans() \
%{expand:%%posttrans %{?1:%{1}-}core}\
if [ -x %{_sbindir}/weak-modules ]\
then\
    %{_sbindir}/weak-modules --add-kernel %{KVERREL}%{?1:+%{1}} || exit $?\
fi\
/bin/kernel-install add %{KVERREL}%{?1:+%{1}} /lib/modules/%{KVERREL}%{?1:+%{1}}/vmlinuz || exit $?\
%{nil}

#
# This macro defines a %%post script for a kernel package and its devel package.
#	%%kernel_variant_post [-v <subpackage>] [-r <replace>]
# More text can follow to go at the end of this variant's %%post.
#
%define kernel_variant_post(v:r:) \
%{expand:%%kernel_devel_post %{?-v*}}\
%{expand:%%kernel_modules_post %{?-v*}}\
%{expand:%%kernel_modules_extra_post %{?-v*}}\
%{expand:%%kernel_modules_internal_post %{?-v*}}\
%{expand:%%kernel_modules_posttrans %{?-v*}}\
%{expand:%%kernel_variant_posttrans %{?-v*}}\
%{expand:%%post %{?-v*:%{-v*}-}core}\
%{-r:\
if [ `uname -i` == "x86_64" -o `uname -i` == "i386" ] &&\
   [ -f /etc/sysconfig/kernel ]; then\
  /bin/sed -r -i -e 's/^DEFAULTKERNEL=%{-r*}$/DEFAULTKERNEL=kernel%{?-v:-%{-v*}}/' /etc/sysconfig/kernel || exit $?\
fi}\
%{nil}

#
# This macro defines a %%preun script for a kernel package.
#	%%kernel_variant_preun <subpackage>
#
%define kernel_variant_preun() \
%{expand:%%preun %{?1:%{1}-}core}\
/bin/kernel-install remove %{KVERREL}%{?1:+%{1}} /lib/modules/%{KVERREL}%{?1:+%{1}}/vmlinuz || exit $?\
if [ -x %{_sbindir}/weak-modules ]\
then\
    %{_sbindir}/weak-modules --remove-kernel %{KVERREL}%{?1:+%{1}} || exit $?\
fi\
%{nil}

%kernel_variant_preun
%kernel_variant_post -r kernel-smp

%kernel_variant_preun debug
%kernel_variant_post -v debug

if [ -x /sbin/ldconfig ]
then
    /sbin/ldconfig -X || exit $?
fi

###
### file lists
###

%if %{with_headers}
%files headers
%defattr(-,root,root)
/usr/include/*
%endif

# only some architecture builds need kernel-doc
%if %{with_doc}
%files doc
%defattr(-,root,root)
%{_datadir}/doc/kernel-doc-%{rpmversion}/Documentation/*
%dir %{_datadir}/doc/kernel-doc-%{rpmversion}/Documentation
%dir %{_datadir}/doc/kernel-doc-%{rpmversion}
%endif

%if %{with_perf}
%files -n perf
%defattr(-,root,root)
%{_bindir}/perf
%{_libdir}/libperf-jvmti.so
%dir %{_libdir}/traceevent/plugins
%{_libdir}/traceevent/plugins/*
%dir %{_libexecdir}/perf-core
%{_libexecdir}/perf-core/*
%{_datadir}/perf-core/*
%{_mandir}/man[1-8]/perf*
%{_sysconfdir}/bash_completion.d/perf
%doc linux-%{KVERREL}/tools/perf/Documentation/examples.txt
%{_docdir}/perf-tip/tips.txt

%files -n python3-perf
%defattr(-,root,root)
%{python3_sitearch}/*

%if %{with_debuginfo}
%files -f perf-debuginfo.list -n perf-debuginfo
%defattr(-,root,root)

%files -f python3-perf-debuginfo.list -n python3-perf-debuginfo
%defattr(-,root,root)
%endif
# with_perf
%endif

%if %{with_tools}
%ifarch %{cpupowerarchs}
%defattr(-,root,root)
%files -n %{name}-tools -f cpupower.lang
%{_bindir}/cpupower
%{_datadir}/bash-completion/completions/cpupower
%ifarch x86_64
%{_bindir}/centrino-decode
%{_bindir}/powernow-k8-decode
%endif
%{_unitdir}/cpupower.service
%{_mandir}/man[1-8]/cpupower*
%config(noreplace) %{_sysconfdir}/sysconfig/cpupower
%ifarch x86_64
%{_bindir}/x86_energy_perf_policy
%{_mandir}/man8/x86_energy_perf_policy*
%{_bindir}/turbostat
%{_mandir}/man8/turbostat*
%endif
# !cpupowerarchs
%else
%files -n %{name}-tools
%defattr(-,root,root)
# cpupowerarchs
%endif
%{_bindir}/tmon
%{_bindir}/iio_event_monitor
%{_bindir}/iio_generic_buffer
%{_bindir}/lsiio
%{_bindir}/lsgpio
%{_bindir}/gpio-hammer
%{_bindir}/gpio-event-mon
%{_bindir}/gpio-watch
%{_mandir}/man1/kvm_stat*
%{_bindir}/kvm_stat

%if %{with_debuginfo}
%files -f %{name}-tools-debuginfo.list -n %{name}-tools-debuginfo
%defattr(-,root,root)
%endif

%ifarch %{cpupowerarchs}
%files -n %{name}-tools-libs
%{_libdir}/libcpupower.so.0
%{_libdir}/libcpupower.so.0.0.1

%files -n %{name}-tools-libs-devel
%{_libdir}/libcpupower.so
%{_includedir}/cpufreq.h
%endif
# with_tools
%endif

%if %{with_bpftool}
%files -n bpftool
%{_sbindir}/bpftool
%{_sysconfdir}/bash_completion.d/bpftool
%{_mandir}/man8/bpftool-cgroup.8.gz
%{_mandir}/man8/bpftool-map.8.gz
%{_mandir}/man8/bpftool-prog.8.gz
%{_mandir}/man8/bpftool-perf.8.gz
%{_mandir}/man8/bpftool.8.gz
%{_mandir}/man8/bpftool-btf.8.gz
%{_mandir}/man8/bpftool-feature.8.gz
%{_mandir}/man8/bpftool-gen.8.gz
%{_mandir}/man8/bpftool-iter.8.gz
%{_mandir}/man8/bpftool-link.8.gz
%{_mandir}/man8/bpftool-net.8.gz
%{_mandir}/man8/bpftool-struct_ops.8.gz
%{_mandir}/man7/bpf-helpers.7.gz

%if %{with_debuginfo}
%files -f bpftool-debuginfo.list -n bpftool-debuginfo
%defattr(-,root,root)
%endif
%endif

# empty meta-package
%ifnarch %nobuildarches noarch
%files
%defattr(-,root,root)
%endif

%if %{with_gcov}
%ifarch x86_64 aarch64
%files gcov
%defattr(-,root,root)
%{_builddir}
%endif
%endif

# This is %%{image_install_path} on an arch where that includes ELF files,
# or empty otherwise.
%define elf_image_install_path %{?kernel_image_elf:%{image_install_path}}

#
# This macro defines the %%files sections for a kernel package
# and its devel and debuginfo packages.
#	%%kernel_variant_files [-k vmlinux] <condition> <subpackage> <without_modules>
#
%define kernel_variant_files(k:) \
%if %{1}\
%{expand:%%files -f %{name}-%{?2:%{2}-}core.list %{?2:%{2}-}core}\
%defattr(-,root,root)\
%{!?_licensedir:%global license %%doc}\
%license linux-%{KVERREL}/COPYING-%{version}\
/lib/modules/%{KVERREL}%{?2:+%{2}}/%{?-k:%{-k*}}%{!?-k:vmlinuz}\
%ghost /%{image_install_path}/%{?-k:%{-k*}}%{!?-k:vmlinuz}-%{KVERREL}%{?2:+%{2}}\
/lib/modules/%{KVERREL}%{?2:+%{2}}/.vmlinuz.hmac \
%ghost /%{image_install_path}/.vmlinuz-%{KVERREL}%{?2:+%{2}}.hmac \
%ifarch aarch64\
/lib/modules/%{KVERREL}%{?2:+%{2}}/dtb \
%ghost /%{image_install_path}/dtb-%{KVERREL}%{?2:+%{2}} \
%endif\
%attr(0600, root, root) /lib/modules/%{KVERREL}%{?2:+%{2}}/System.map\
%attr(0600, root, root) /boot/System.map-%{KVERREL}%{?2:+%{2}}\
/lib/modules/%{KVERREL}%{?2:+%{2}}/symvers.gz\
/lib/modules/%{KVERREL}%{?2:+%{2}}/config\
%attr(0600, root, root) /boot/symvers-%{KVERREL}%{?2:+%{2}}.gz\
%attr(0600, root, root) /boot/initramfs-%{KVERREL}%{?2:+%{2}}.img\
%attr(0644, root, root) /boot/config-%{KVERREL}%{?2:+%{2}}\
%dir /lib/modules\
%dir /lib/modules/%{KVERREL}%{?2:+%{2}}\
%dir /lib/modules/%{KVERREL}%{?2:+%{2}}/kernel\
/lib/modules/%{KVERREL}%{?2:+%{2}}/build\
/lib/modules/%{KVERREL}%{?2:+%{2}}/source\
/lib/modules/%{KVERREL}%{?2:+%{2}}/updates\
/lib/modules/%{KVERREL}%{?2:+%{2}}/weak-updates\
/lib/modules/%{KVERREL}%{?2:+%{2}}/bls.conf\
/lib/modules/%{KVERREL}%{?2:+%{2}}/modules.*\
%{expand:%%files -f kernel-%{?2:%{2}-}modules.list %{?2:%{2}-}modules}\
%defattr(-,root,root)\
%{expand:%%files %{?2:%{2}-}devel}\
%defattr(-,root,root)\
%defverify(not mtime)\
/usr/src/kernels/%{KVERREL}%{?2:+%{2}}\
%{expand:%%files -f %{name}-%{?2:%{2}-}modules-extra.list %{?2:%{2}-}modules-extra}\
%defattr(-,root,root)\
%config(noreplace) /etc/modprobe.d/*-blacklist.conf\
%{expand:%%files -f %{name}-%{?2:%{2}-}modules-internal.list %{?2:%{2}-}modules-internal}\
%if %{with_debuginfo}\
%ifnarch noarch\
%{expand:%%files -f debuginfo%{?2}.list %{?2:%{2}-}debuginfo}\
%defattr(-,root,root)\
%endif\
%endif\
%if %{?2:1} %{!?2:0}\
%{expand:%%files %{2}}\
%defattr(-,root,root)\
%endif\
%endif\
%{nil}

%kernel_variant_files %{with_up}
%kernel_variant_files %{with_debug} debug

# plz don't put in a version string unless you're going to tag
# and build.
#
#
%changelog
* Tue Sep 06 2022 Qiao Ma <mqaio@linux.alibaba.com> [5.10.134-12.an8]
- ip: fix triggering of 'icmp redirect' (Nicolas Dichtel)
- anolis: Revert "anolis: net/smc: remove locks smc_client_lgr_pending and smc_server_lgr_pending" (Wen Gu)
- anolis: Revert "anolis: net/smc: fix SMC_CLC_DECL_ERR_REGRMB without smc_server_lgr_pending" (Wen Gu)
- anolis: net/smc: Fix NULL sndbuf_desc in smc_cdc_tx_handler() (D. Wythe)
- anolis: net/smc: Stop the CLC flow if there's no link to map RMBs on (Wen Gu)
- anolis: net/smc: Fix UAF when fallback after lgr create (Tony Lu)
- anolis: net/smc: Avoid use-after-free of smcibcq tasklet (Wen Gu)
- anolis: net/smc: Restore rx queue depth (Tony Lu)
- anolis: net/smc: Fix task hung in SMC-R terminating all link groups (Wen Gu)

* Tue Aug 30 2022 Qiao Ma <mqaio@linux.alibaba.com> [5.10.134-12_rc4.an8]
- anolis: net/smc: fix panic smc_tcp_syn_recv_sock() while closing listen socket (D. Wythe)
- anolis: net/smc: fix deadlock when lgr terminating (D. Wythe)
- anolis: Revert "anolis: net/smc: fix deadlock when lgr terminating" (D. Wythe)
- anolis: net/smc: Reuse wr_id to recognize wc for tx or rx (Tony Lu)
- anolis: net/smc: delay RDMA resource release until connecitons freed (Wen Gu)
- anolis: net/smc: Use SO_REUSEADDR if keep clcsock (Tony Lu)
- anolis: net/smc: fix SMC_CLC_DECL_ERR_REGRMB without smc_server_lgr_pending (D. Wythe)
- anolis: net/smc: remove locks smc_client_lgr_pending and smc_server_lgr_pending (D. Wythe)
- anolis: Revert "anolis: net/smc: remove locks smc_client_lgr_pending and smc_server_lgr_pending" (Tony Lu)
- anolis: Revert "anolis: net/smc: fix SMC_CLC_DECL_ERR_REGRMB without smc_server_lgr_pending" (Tony Lu)
- anolis: net/smc: Fix wr tasklet handler for rx/tx wc (Tony Lu)
- anolis: net/smc: check clcsock->sk validity in fallback restore (Wen Gu)
- anolis: net/smc: Keep first contact clcsock (Tony Lu)
- anolis: Revert "anolis: net/smc: Keep first contact clcsock" (Wen Gu)
- anolis: configs: let PCIe PMU driver be built in (Shuai Xue)
- anolis: drivers/perf: add a separate config for DesignWare PCIe controller PMU (Shuai Xue)
- anolis: drivers/perf: clean up unused variable (Shuai Xue)
- anolis: drivers/perf: simplify extended capability founding (Shuai Xue)
- anolis: drivers/perf: simplify Root Port check (Shuai Xue)
- fs: fix UAF/GPF bug in nilfs_mdt_destroy (Dongliang Mu) {CVE-2022-2978}
- anolis: mm: fcm: do not unconditionally call fixup ops (Xu Yu)
- anolis: Revert "anolis: mm: fcm: do not unconditionally call fcm_fixup_vma" (Xu Yu)
- anolis: arm_mpam: Fix the problem that error interrupt handling cannot exit normally (Shawn Wang)
- anolis: arm_mpam: Fix wrong type conversion for dev_id in mpam_irq_handler() (Shawn Wang)
- anolis: arm64: Remove GPL from pv_ops export for ARM/ARM64 (Shawn Wang)
- anolis: fs/resctrl: Refine the outputs of last_cmd_status (Shawn Wang)
- anolis: fs/resctrl: Extend the maximum number of CLOSIDs to 64 (Shawn Wang)
- anolis: fs/resctrl: Clear the staged configs when destroying schemata list (Shawn Wang)
- anolis: fs/resctrl: Fix the wrong if statement in resctrl_closid_is_dirty() (Shawn Wang)

* Wed Aug 24 2022 Qiao Ma <mqaio@linux.alibaba.com> [5.10.134-12_rc3.an8]
- mm/hwpoison: check the subpage, not the head page (Matthew Wilcox (Oracle))
- filemap: Correct the conditions for marking a folio as accessed (Matthew Wilcox (Oracle))
- anolis: arm64: drivers/perf: fix DDRSS PMU interrupt initialization (Shuai Xue)
- net_sched: cls_route: remove from list when handle is 0 (Thadeu Lima de Souza Cascardo) {CVE-2022-2588}
- posix-cpu-timers: Cleanup CPU timers before freeing them during exec (Thadeu Lima de Souza Cascardo)
- netfilter: nf_tables: do not allow RULE_ID to refer to another chain (Thadeu Lima de Souza Cascardo) {CVE-2022-2586}
- netfilter: nf_tables: do not allow CHAIN_ID to refer to another table (Thadeu Lima de Souza Cascardo) {CVE-2022-2586}
- netfilter: nf_tables: do not allow SET_ID to refer to another table (Thadeu Lima de Souza Cascardo) {CVE-2022-2586}
- anolis: net/smc: Fix NULL pointer in smc_getname while first contact clcsock release (D. Wythe)
- anolis: net/smc: skip smc_llc_flow_initiate while SMC-1RTT (D. Wythe)
- anolis: net/smc: fix SMC_CLC_DECL_ERR_REGRMB without smc_server_lgr_pending (D. Wythe)
- anolis: net/smc: remove locks smc_client_lgr_pending and smc_server_lgr_pending (D. Wythe)
- anolis: Revert "anolis: net/smc: remove locks smc_client_lgr_pending and smc_server_lgr_pending" (D. Wythe)
- anolis: Revert "anolis: net/smc: fix SMC_CLC_DECL_ERR_REGRMB without smc_server_lgr_pending" (D. Wythe)
- anolis: Revert "anolis: net/smc: skip smc_llc_flow_initiate while SMC-1RTT" (D. Wythe)
- anolis: mm: fcm: do not unconditionally call fcm_fixup_vma (Xu Yu)
- anolis: net/smc: Introduce smc dim (Guangguan Wang)
- anolis: Revert "net/smc: Introduce rdma dim for smc" (Guangguan Wang)
- anolis: Revert "f2fs: remove false alarm on iget failure during GC" (Gao Xiang)
- netfilter: nf_queue: do not allow packet truncation below transport header offset (Florian Westphal) {CVE-2022-36946}
- anolis: arm64: spectre: added a description of the parameter nospectre_bhb (Liu Song)
- anolis: arm64: spectre: increase parameters that can be used to turn off bhb mitigation individually (Liu Song)
- anolis: random: speed up the initialization of module (Xingjun Liu)
- anolis: random: introduce the initialization seed (Xingjun Liu)

* Tue Aug 09 2022 Qiao Ma <mqaio@linux.alibaba.com> [5.10.134-12_rc2.an8]
- anolis: arm_mpam: Add hypervisor mode detection before accessing MPAM registers (Shawn Wang)

* Tue Aug 02 2022 Qiao Ma <mqaio@linux.alibaba.com> [5.10.134-12_rc1.an8]
- configs: refresh the ANCK default configs (Qiao Ma)
- bump kernel to 5.10.134 (Qiao Ma)
- anolis: mm: fix an compiler issue when memcg disabled (zhongjiang-ali)
- anolis: mm: Do not scan cold slab when fails to allocate memory (zhongjiang-ali)
- anolis: mm: fix cold slab memory leak (zhongjiang-ali)
- anolis: mm: fix an memory exception issue when kmem accouting (zhongjiang-ali)
- selftests: bpf: Don't run sk_lookup in verifier tests (Lorenz Bauer)
- selftests: bpf: Check that PROG_TEST_RUN repeats as requested (Lorenz Bauer)
- bpf: Add PROG_TEST_RUN support for sk_lookup programs (Lorenz Bauer)
- bpf: Consolidate shared test timing code (Lorenz Bauer)
- anolis: correct fd attach ioctl arg type (Joseph Qi)
- kvm: x86: enqcmd: pass through MSR_IA32_PASID (Wu Hao)
- kvm: x86: enqcmd: update PASID translation table in VMX root mode only (Wu Hao)
- kvm: x86: rename KVM_REQ_MCLOCK_INPROGRESS to BLOCK_VMENTRY (Wu Hao)
- kvm: x86: add ENQCMD/ENQCMDS support (Wu Hao)
- KVM: x86: Load guest fpu state when accessing MSRs managed by XSAVES (Sean Christopherson)
- KVM: x86: Refresh CPUID on writes to MSR_IA32_XSS (Yang Weijiang)
- KVM: x86: Add guest_supported_xss placholder (Sean Christopherson)
- KVM: x86: Report XSS as an MSR to be saved if there are supported features (Sean Christopherson)
- anolis: mm: duptext: fix build error when !CONFIG_MEMCG (Xu Yu)
- anolis: mm: hugetext: pad binary mapping for THP candidate (Rongwei Wang)
- efi/cper: Reformat CPER memory error location to more readable (Shuai Xue)
- EDAC/ghes: Unify CPER memory error location reporting (Shuai Xue)
- efi/cper: Add a cper_mem_err_status_str() to decode error description (Shuai Xue)
- iommu/vt-d: iommu/vt-d: report non-recoverable faults to device (Jacob Pan)
- iommu: add fault reporting flags (Jacob Pan)
- iommu: add place holder for PASID suspend (Jacob Pan)
- iommu: Pass user pointer to iommu_page_response() (Liu Yi L)
- trace/iommu: Add sva trace events and Use device fault trace event (Jean-Philippe Brucker)
- iommu: handle page response timeout (Jacob Pan)
- iommu: Add a timeout parameter for PRQ response (Jacob Pan)
- iommu/vt-d: Loop subdevices in iommu_domain_get_attr (Liu Yi L)
- iommu/vt-d: Fix bug in page response (Liu Yi L)
- iommu: Fix an issue in iommu_page_response() flags check (Jacob Pan)
- vfio/type1: Report iommu nesting info to userspace (Liu Yi L)
- iommu/vt-d: Support reporting nesting capability info (Liu Yi L)
- iommu/vt-d: Move capability check code to cap_audit files (Kyung Min Park)
- iommu/vt-d: Audit IOMMU Capabilities and add helper functions (Kyung Min Park)
- iommu: Report domain nesting info (Liu Yi L)
- iommu/vt-d: Remove mm reference for guest SVA (Jacob Pan)
- ioasid: Fix unexpected ioasid_set free (Liu Yi L)
- iommu/ioasid: fix ioaisd_notifier_register_mm (Wu Hao)
- Add back quota to avoid no space issue when trying to open /dev/ioasid in the second time. (Liu Yi L)
- iommu/vt-d: Check ownership in sva cache invalidate (Liu Yi L)
- ioasid: Add /dev/ioasid for userspace (Liu Yi L)
- docs: cgroup-v1: Add IOASIDs controller (Jacob Pan)
- iommu/ioasid: Consult IOASIDs cgroup for allocation (Jacob Pan)
- cgroup: Introduce ioasids controller (Jacob Pan)
- iommu/ioasid: Add ownership check in guest bind (Jacob Pan)
- anolis: block: I/O error occurs during SATA disk stress test (Gu Mi)
- anolis: net/smc: skip smc_llc_flow_initiate while SMC-1RTT (D. Wythe)
- anolis: net/smc: fix SMC_CLC_DECL_ERR_REGRMB without smc_server_lgr_pending (D. Wythe)
- anolis: net/smc: remove locks smc_client_lgr_pending and smc_server_lgr_pending (D. Wythe)
- anolis: net/smc: change clcsock_release_lock from mutex to rw_semaphore (Guangguan Wang)
- anolis: net/smc: check sk_ack_backlog before kernel_accept (Guangguan Wang)
- anolis: net/smc: do not call cancel smc_listen_work when smc use fallback (Guangguan Wang)
- anolis: net/smc: only cancel connect_work when connect nonblock (Guangguan Wang)
- anolis: net/smc: Introduce multiple tcp listen works to enhance tcp_listen_work (Guangguan Wang)
- anolis: net/smc: smc_sock_alloc after kernel_accept (Guangguan Wang)
- anolis: net/smc: move sk_acceptq_{removed,add} into accept_q_lock's protection (Guangguan Wang)
- anolis: net/smc: remove sock lock in smc_tcp_listen_work (Guangguan Wang)
- anolis: net/smc: optimize for smc_accept_poll (Guangguan Wang)
- anolis: net/smc: double check whether accept queue is empty before schedule_timeout (Guangguan Wang)
- anolis: net/smc: only add wait queue when smc_accept_dequeue get null (Guangguan Wang)
- anolis: net/smc: do not use free work if fallback (Guangguan Wang)
- anolis: net/smc: queue free_work to smc_close_wq instead of smc_hs_wq (D. Wythe)
- anolis: net/smc: remove useless path (Guangguan Wang)
- anolis: net/smc: Change listen wq to unbound highpri wq (Guangguan Wang)
- anolis: net/smc: Use diff TCP EXPR MAGIC to avoid network middleware do simple echo (D. Wythe)
- anolis: net/smc: fix deadlock when lgr terminating (D. Wythe)
- anolis: net/smc: Fix potential leaks on queued_smc_hs (D. Wythe)
- anolis: net/smc: remove solicited flag for wr send (Guangguan Wang)
- anolis: net/smc: remove redundant ib_req_notify_cq (Guangguan Wang)
- anolis: net/smc: clear ib_cq errno when ib_create_cq failed (Guangguan Wang)
- anolis: net/smc: Introduce rdma dim for smc (Guangguan Wang)
- anolis: net/smc: introduce 1RTT to SMC-R (D. Wythe)
- anolis: net/smc: poll_cq one more time if the polled cqe is less than SMC_WR_MAX_POLL_CQE (Guangguan Wang)
- anolis: net/smc: remove redundant ib_req_notify_cq (Guangguan Wang)
- anolis: net/smc: combine send cq and recv cq into one cq (Guangguan Wang)
- anolis: net/smc: move wc loop out of smc_wr_rx_process_cqes (Guangguan Wang)
- anolis: net/smc: Avoid syscall block by async smc_conn_free (D. Wythe)
- anolis: net/smc: Disable confirm rkey message exchange when only one link exists (D. Wythe)
- anolis: net/smc: Release lock before waiting for CLC accept message (D. Wythe)
- anolis: net/smc: compress frequency of credits announcement by cdc msg (Guangguan Wang)
- anolis: net/smc: do not send msg in receiving process when tx is not blocked. (Guangguan Wang)
- anolis: net/smc: Avoid clcsock access panic (Wen Gu)
- anolis: net/smc: Fix NULL sk pointer when access clcsock (Tony Lu)
- anolis: net/smc: don't req_notify until all CQEs drained (Wen Gu)
- anolis: net/smc: Introduce rtoken validity check before sending (Wen Gu)
- anolis: net/smc: Introduce a sysctl to disable {a}symmetric link group (Wen Gu)
- anolis: net/smc: Keep first contact clcsock (Tony Lu)
- anolis: net/smc: Multiple CQs per IB devices (Tony Lu)
- anolis: net/smc: Introduce smc_ib_cq to bind link and cq (Tony Lu)
- anolis: net/smc: Introduce link-related proc file (Guangguan Wang)
- anolis: net/smc: Support rq flow control in smc-r link layer (Guangguan Wang)
- anolis: net/smc: Add sysctl conrtol for handshake limiation (D. Wythe)
- anolis: net/smc: Avoid unmapping bufs from unused links (Wen Gu)
- anolis: net/smc: allow different subnet communication (Tony Lu)
- anolis: net/smc: don't call ib_req_notify_cq in the send routine (Dust Li)
- anolis: net/smc: Add TX and RX diagnosis information (Tony Lu)
- anolis: net/smc: Introduce TCP to SMC replacement netlink commands (Tony Lu)
- anolis: net/smc: Introduce SMC-R-related proc files (Tony Lu)
- anolis: net/smc: Introduce sysctl tcp2smc (Tony Lu)
- anolis: net/smc: Expose SMCPROTO_SMC and SMCPROTO_SMC6 to userspace (Tony Lu)
- anolis: net/smc: Introduce tunable sysctls for sndbuf and RMB size (Wen Gu)
- net/smc: Extend SMC-R link group netlink attribute (Wen Gu)
- net/smc: Allow virtually contiguous sndbufs or RMBs for SMC-R (Wen Gu)
- net/smc: Use sysctl-specified types of buffers in new link group (Wen Gu)
- net/smc: Introduce a sysctl for setting SMC-R buffer type (Wen Gu)
- net/smc: optimize for smc_sndbuf_sync_sg_for_device and smc_rmb_sync_sg_for_cpu (Guangguan Wang)
- net/smc: remove redundant dma sync ops (Guangguan Wang)
- net/smc: set ini->smcrv2.ib_dev_v2 to NULL if SMC-Rv2 is unavailable (liuyacan)
- Revert "net/smc: fix listen processing for SMC-Rv2" (liuyacan)
- net/smc: fix listen processing for SMC-Rv2 (liuyacan)
- net/smc: postpone sk_refcnt increment in connect() (liuyacan)
- net/smc: rdma write inline if qp has sufficient inline space (Guangguan Wang)
- net/smc: send cdc msg inline if qp has sufficient inline space (Guangguan Wang)
- net/smc: align the connect behaviour with TCP (Guangguan Wang)
- net/smc: Fix slab-out-of-bounds issue in fallback (Wen Gu)
- net/smc: Only save the original clcsock callback functions (Wen Gu)
- anolis: Revert "anolis: net/smc: Introduce tunable sysctls for sndbuf and RMB size" (D. Wythe)
- anolis: Revert "anolis: net/smc: Expose SMCPROTO_SMC and SMCPROTO_SMC6 to userspace" (D. Wythe)
- anolis: Revert "anolis: net/smc: Introduce sysctl tcp2smc" (D. Wythe)
- anolis: Revert "anolis: net/smc: Introduce SMC-R-related proc files" (D. Wythe)
- anolis: Revert "anolis: net/smc: Introduce TCP to SMC replacement netlink commands" (D. Wythe)
- anolis: Revert "anolis: net/smc: Add TX and RX diagnosis information" (D. Wythe)
- anolis: Revert "anolis: net/smc: don't call ib_req_notify_cq in the send routine" (D. Wythe)
- anolis: Revert "anolis: net/smc: allow different subnet communication" (D. Wythe)
- anolis: Revert "anolis: net/smc: Avoid unmapping bufs from unused links" (D. Wythe)
- anolis: Revert "anolis: net/smc: Add sysctl conrtol for handshake limiation" (D. Wythe)
- anolis: Revert "anolis: net/smc: Support rq flow control in smc-r link layer" (D. Wythe)
- anolis: Revert "anolis: net/smc: Introduce link-related proc file" (D. Wythe)
- anolis: Revert "anolis: net/smc: Introduce smc_ib_cq to bind link and cq" (D. Wythe)
- anolis: Revert "anolis: net/smc: Multiple CQs per IB devices" (D. Wythe)
- anolis: Revert "anolis: net/smc: Keep first contact clcsock" (D. Wythe)
- anolis: Revert "anolis: net/smc: Introduce a sysctl to disable {a}symmetric link group" (D. Wythe)
- anolis: Revert "anolis: net/smc: Introduce rtoken validity check before sending" (D. Wythe)
- anolis: Revert "anolis: net/smc: Introduce link dimension req & comp debug info" (D. Wythe)
- anolis: Revert "anolis: net/smc: don't req_notify until all CQEs drained" (D. Wythe)
- anolis: Revert "anolis: net/smc: sync err info when TCP connection is refused" (D. Wythe)
- anolis: Revert "anolis: net/smc: Only save the original clcsock callback functions" (D. Wythe)
- anolis: Revert "anolis: net/smc: Fix slab-out-of-bounds issue in fallback" (D. Wythe)
- anolis: arm64: drivers/perf: explicitly begin searching for PCI device (Shuai Xue)
- anolis: ACPI / MPAM: Rename the signature of the new MPAM ACPI table (Shawn Wang)
- anolis: ACPI / PPTT: Downgrade the revision requirement in acpi_pptt_get_cpumask_from_cache_id_and_level (Shawn Wang)
- anolis: RDMA/erdma: Update for hardware (Kai)
- anolis: configs: enable CONFIG_FAST_COPY_MM by default (Xu Yu)
- anolis: mm: fcm: introduce fast copy mm interface (Xu Yu)
- anolis: configs: enable DMA_CMA and DMA_PERNUMA_CMA (Liu Song)
- anolis: configs: enable Contiguous Memory Allocator (Liu Song)
- anolis: configs: disable RODATA_FULL_DEFAULT_ENABLED (Liu Song)
- anolis: configs: disable KFENCE by default (Liu Song)
- anolis: configs: arm64: enable ARM64_PSEUDO_NMI (Liu Song)
- anolis: configs: arm64: enable ARCH_RANDOM (Liu Song)
- anolis: configs: arm64: enable ARM64_TAGGED_ADDR_ABI (Liu Song)
- anolis: configs: support more than 8 numa nodes (Liu Song)
- anolis: configs: refresh some default configuration (Liu Song)
- anolis: vfio/mdev: Set dev_iommu for device of mdev_device (Zelin Deng)
- iommu/arm-smmu-v3: Realize support_dirty_log iommu ops (Kunkun Jiang)
- iommu/arm-smmu-v3: Realize clear_dirty_log iommu ops (Kunkun Jiang)
- iommu/arm-smmu-v3: Realize sync_dirty_log iommu ops (Kunkun Jiang)
- iommu/arm-smmu-v3: Realize switch_dirty_log iommu ops (Kunkun Jiang)
- iommu/arm-smmu-v3: Add feature detection for BBML (Kunkun Jiang)
- iommu/arm-smmu-v3: Enable HTTU for stage1 with io-pgtable mapping (Kunkun Jiang)
- iommu/arm-smmu-v3: Add support for Hardware Translation Table Update (Jean-Philippe Brucker)
- iommu/io-pgtable-arm: Add and realize clear_dirty_log ops (Kunkun Jiang)
- iommu/io-pgtable-arm: Add and realize sync_dirty_log ops (Kunkun Jiang)
- iommu/io-pgtable-arm: Add and realize merge_page ops (Kunkun Jiang)
- iommu/io-pgtable-arm: Add and realize split_block ops (Kunkun Jiang)
- iommu/io-pgtable-arm: Add quirk ARM_HD and ARM_BBMLx (Kunkun Jiang)
- iommu: Introduce dirty log tracking framework (Keqian Zhu)
- anolis: MAINTAINERS: Add Yitian Cryptography Complex (YCC) driver maintainer entry (Zelin Deng)
- anolis: crypto/ycc: Add ycc iommu mapping for user space dma (Zelin Deng)
- anolis: crypto/ycc: Add hash algorithm support (Xuchun Shang)
- anolis: crypto/ycc: Add sm2 algorithm support (Xuchun Shang)
- anolis: crypto/ycc: Add rsa algorithm support (Guanjun)
- anolis: crypto/ycc: Add aead algorithm support (Guanjun)
- anolis: crypto/ycc: Add skcipher algorithm support (Guanjun)
- anolis: crypto/ycc: Send uevent when ycc gets fatal errors (Zelin Deng)
- anolis: crypto/ycc: Add device error handling support for ycc hw errors (Zelin Deng)
- anolis: crypto/ycc: Add uio support for ycc user rings (Zelin Deng)
- anolis: crypto/ycc: Add irq support for ycc kernel rings (Zelin Deng)
- anolis: crypto/ycc: Add ycc ring configuration (Zelin Deng)
- anolis: crypto/ycc: Add YCC (Yitian Cryptography Complex) accelerator driver (Zelin Deng)
- anolis: mm: support priority control when swap anonymous pages (Kaihao Bai)
- KVM: x86: Avoid theoretical NULL pointer dereference in kvm_irq_delivery_to_apic_fast() (Vitaly Kuznetsov) {CVE-2022-2153}
- KVM: x86: Check lapic_in_kernel() before attempting to set a SynIC irq (Vitaly Kuznetsov) {CVE-2022-2153}
- KVM: Add infrastructure and macro to mark VM as bugged (Sean Christopherson) {CVE-2022-2153}
- thermal: netlink: Fix parameter type of thermal_genl_cpu_capability_event() stub (Nathan Chancellor)
- thermal: netlink: Add a new event to notify CPU capabilities change (Srinivas Pandruvada)
- thermal: intel: hfi: INTEL_HFI_THERMAL depends on NET (Randy Dunlap)
- thermal: intel: hfi: Notify user space for HFI events (Srinivas Pandruvada)
- thermal: intel: hfi: Enable notification interrupt (Ricardo Neri)
- x86/Documentation: Describe the Intel Hardware Feedback Interface (Ricardo Neri)
- thermal: intel: hfi: Handle CPU hotplug events (Ricardo Neri)
- thermal: intel: hfi: Minimally initialize the Hardware Feedback Interface (Ricardo Neri)
- x86/cpu: Add definitions for the Intel Hardware Feedback Interface (Ricardo Neri)
- thermal: Move therm_throt there from x86/mce (Borislav Petkov)
- x86/mce: Get rid of mcheck_intel_therm_init() (Borislav Petkov)
- anolis: arm_mpam: Limit MB percentage in schemata to multiples of granularity (Shawn Wang)
- anolis: arm_mpam: Fix the inaccurate MB granularity in resctrl info directory (Shawn Wang)
- anolis: arm_mpam: Fix MB default percentage error in schemata (Shawn Wang)
- anolis: arm_mpam: Remove the zero padding for MB percentage showing in schemata (Shawn Wang)
- anolis: arm_mpam: Fix L3 cache size display error in resctrl fs (Shawn Wang)
- anolis: net/hookers: modify hooker_uninstall comment (Gou Hao)
- anolis: mm: coldpgs: configs: enable coldpgs as module (Rongwei Wang)
- anolis: mm: kidled: Do not check the slab age when it disable the slab scan (zhongjiang-ali)
- anolis: mm: coldpgs: release v2.5.0 (Xu Yu)
- anolis: mm: coldpgs: add mlock {dropped, refault} counter (Xu Yu)
- anolis: mm: coldpgs: add reclaim type control interface for memcg (zhongjiang-ali)
- anolis: mm: coldpgs: support reclaiming cold slab (zhongjiang-ali)
- anolis: mm: coldpgs: count the size of page and slab in bytes (zhongjiang-ali)
- anolis: mm: coldpgs: support reclaiming cold pages (Xunlei Pang)
- anolis: mm: kidled: allocate slab age for specified kmem cache (zhongjiang-ali)
- anolis: mm: kidled: refactor cold slab (zhongjiang-ali)
- anolis: mm: kidled: use different variables to store the rounds (zhongjiang-ali)
- anolis: mm: kidled: Make kidled scan both page and slab equally (zhongjiang-ali)
- anolis: mm: kidled: make kidled to support scan slab separately (zhongjiang-ali)
- anolis: mm: kidled: isolate slab scan with page scan (zhongjiang-ali)
- anolis: mm: kidled: make kidled support to identify cold slab (zhongjiang-ali)
- anolis: mm: kidled: provide a unified interface in preparation for scanning slab (zhongjiang-ali)
- dmaengine: idxd: Correct IAX operation code names (Fenghua Yu)
- MAINTAINERS: idxd driver maintainer update (Dave Jiang)
- dmaengine: idxd: Remove unnecessary synchronize_irq() before free_irq() (Minghao Chi)
- dmaengine: idxd: make idxd_register/unregister_dma_channel() static (Dave Jiang)
- dmaengine: idxd: fix lockdep warning on device driver removal (Dave Jiang)
- dmaengine: idxd: fix retry value to be constant for duration of function call (Dave Jiang)
- dmaengine: idxd: match type for retries var in idxd_enqcmds() (Dave Jiang)
- anolis: mm/damon: fix behaviour of init_regions (Rongwei Wang)
- anolis: perf: spe: detimine LLC cache line size at runtime (Shuai Xue)
- netfilter: nf_tables: disallow non-stateful expression in sets earlier (Pablo Neira Ayuso) {CVE-2022-32250}
- anolis: net/netfilter: rename nft_expr_info (Kangjie Xu) {CVE-2022-32250}
- kvm/x86: Fix MMIO data mismatch issue (Dong Xiaocheng)
- vfio: mdev: idxd: fix vdcm_reset() for vidxd device (Hao Xiang)
- Revert "vfio: mdev: idxd: fix soft reset for guest" (Hao Xiang)
- vfio: mdev: idxd: fix setup pasid permission for IMS in guest scalable mode (Hao Xiang)
- vfio: mdev: idxd: fix uninit var usage in vidxd_wq_abort() (Dave Jiang)
- vfio: mdev: idxd: fix uninit var in vidxd_wq_reset() (Dave Jiang)
- vfio: mdev: idxd: fix uninit var usage in vidxd_resume_wq_state() (Dave Jiang)
- vfio: mdev: idxd: fix vidxd_resume_wq_state() unint var (Hao Xiang)
- vfio: mdev: idxd: fix mm ref leak (Dave Jiang)
- dmaengine: idxd: return 0 if wq already enabled instead of error (Dave Jiang)
- dmaengine: idxd: fix wq clear state when called from device disable (Dave Jiang)
- dmaengine: idxd: fix mdev driver ->remove() unregistering mdev device (Dave Jiang)
- vfio: mdev: idxd: put kref when all devices are released (Dave Jiang)
- vfio: mdev: idxd: check idxd_wq_enable() return value (Dave Jiang)
- vfio: mdev: idxd: fix potential null dereference (Dave Jiang)
- vfio: mdev: idxd: make wq_pasid to be an int (Dave Jiang)
- vfio: mdev: idxd: check return value for iommu_unregister_device_fault_handler() (Hao Xiang)
- anolis: arm_mpam: Fix some bugs for MBWU function adaptation on Yitian (Shawn Wang)
- arm_mpam: Misc fixes for issues found by the kbuild robot (James Morse)
- arm_mpam: Make mpam_msmon_read() safe for irq-masked callers (James Morse)
- arm_mpam: Allow MBWU counters to be used, even when not free running (James Morse)
- arm_mpam: Allow the maximum partid to be overridden from the command line (James Morse)
- kobject: Add kset_get_next_obj() to allow a kset to be walked (James Morse)
- arm_mpam: resctrl: Update the rmid reallocation limit (James Morse)
- arm_mpam: resctrl: Call resctrl_exit() in the event of errors (James Morse)
- arm_mpam: resctrl: Tell resctrl about cpu/domain online/offline (James Morse)
- arm64: mpam: Select ARCH_HAS_CPU_RESCTRL (James Morse)
- arm_mpam: resctrl: Add empty definitions for fine-grained enables (James Morse)
- arm_mpam: resctrl: Add empty definitions for pseudo lock (James Morse)
- arm_mpam: resctrl: Add resctrl_arch_rmid_read() and resctrl_arch_reset_rmid() (James Morse)
- arm_mpam: resctrl: Allow resctrl to allocate monitors for CSU (James Morse)
- untested: arm_mpam: resctrl: Add support for mbm local (James Morse)
- untested: arm_mpam: resctrl: Add support for MBA resource (James Morse)
- mm/filemap: fix infinite loop in generic_file_buffered_read() (Kent Overstreet)
- mm/filemap.c: generic_file_buffered_read() now uses find_get_pages_contig (Kent Overstreet)
- mm/filemap/c: break generic_file_buffered_read up into multiple functions (Kent Overstreet)
- anolis: config: Add TXGBE support in anolis defconfig (Jiawen Wu)
- anolis: doc: Add WangXun txgbe driver known issues (Jiawen Wu)
- anolis: net: txgbe: Support sysfs file system (Jiawen Wu)
- anolis: net: txgbe: Support debug filesystem (Jiawen Wu)
- anolis: net: txgbe: Support power management (Jiawen Wu)
- anolis: net: txgbe: Add ethtool support (Jiawen Wu)
- anolis: net: txgbe: Support PTP (Jiawen Wu)
- anolis: net: txgbe: Support flow director (Jiawen Wu)
- anolis: net: txgbe: Support flow control (Jiawen Wu)
- anolis: net: txgbe: Support to receive and tranmit packets (Jiawen Wu)
- anolis: net: txgbe: Add interrupt support (Jiawen Wu)
- anolis: net: txgbe: Add PHY interface support (Jiawen Wu)
- anolis: net: txgbe: Add operations to interact with firmware (Jiawen Wu)
- anolis: net: txgbe: Add hardware initialization (Jiawen Wu)
- net: txgbe: Add build support for txgbe (Jiawen Wu)
- iommu/vt-d: Add present bit check in pasid entry setup helpers (Liu Yi L)
- iommu/vt-d: Use pasid_pte_is_present() helper function (Liu Yi L)
- iommu/vt-d: Allow devices to have more than 32 outstanding PRs (Lu Baolu)
- iommu/vt-d: Fix clearing real DMA device's scalable-mode context entries (Lu Baolu)
- iommu/vt-d: Global devTLB flush when present context entry changed (Sanjay Kumar)
- iommu/vt-d: Fix dereference of pointer info before it is null checked (Colin Ian King)
- iommu/vt-d: Add cache invalidation latency sampling (Lu Baolu)
- iommu/vt-d: Expose latency monitor data through debugfs (Lu Baolu)
- iommu/vt-d: Add common code for dmar latency performance monitors (Lu Baolu)
- iommu/vt-d: Allocate/register iopf queue for sva devices (Lu Baolu)
- iommu/vt-d: Refactor prq_event_thread() (Lu Baolu)
- iommu/vt-d: Use common helper to lookup svm devices (Lu Baolu)
- iommu/vt-d: Select PCI_ATS explicitly (Lu Baolu)
- iommu/vt-d: Tweak the description of a DMA fault (Lu Baolu)
- iommu/vt-d: Check for allocation failure in aux_detach_device() (Dan Carpenter)
- iommu/vt-d: Force to flush iotlb before creating superpage (Longpeng(Mike))
- iommu/vt-d: Avoid unnecessary cache flush in pasid entry teardown (Lu Baolu)
- iommu: Add a page fault handler (Jean-Philippe Brucker)
- iommu/vt-d: Support IOMMU_DEV_FEAT_IOPF (Jean-Philippe Brucker)
- iommu: Separate IOMMU_DEV_FEAT_IOPF from IOMMU_DEV_FEAT_SVA (Jean-Philippe Brucker)
- iommu/vt-d: Make unnecessarily global functions static (Lu Baolu)
- iommu/vt-d: Remove unused function declarations (Lu Baolu)
- iommu/vt-d: Fix lockdep splat in intel_pasid_get_entry() (Lu Baolu)
- iommu/vt-d: Use Real PCI DMA device for IRTE (Jon Derrick)
- iommu/vt-d: Calculate and set flags for handle_mm_fault (Jacob Pan)
- iommu/vt-d: Enable write protect propagation from guest (Jacob Pan)
- iommu/vt-d: Enable write protect for supervisor SVM (Jacob Pan)
- iommu/vt-d: Report more information about invalidation errors (Lu Baolu)
- iommu: Don't use lazy flush for untrusted device (Lu Baolu)
- iommu/vt-d: Add iotlb_sync_map callback (Lu Baolu)
- iommu/vt-d: Do not use flush-queue when caching-mode is on (Nadav Amit)
- iommu: Add iova and size as parameters in iotlb_sync_map (Yong Wu)
- iommu/vt-d: Fix ineffective devTLB invalidation for subdevices (Liu Yi L)
- iommu/vt-d: Cleanup after converting to dma-iommu ops (Lu Baolu)
- anolis: configs: enable CONFIG_X86_SGX_KVM by default (Shirong Hao)
- efi: cper: check section header more appropriately (Shuai Xue)
- efi: cper: fix scnprintf() use in cper_mem_err_location() (Rasmus Villemoes)
- platform/x86/intel/ifs: Add CPU_SUP_INTEL dependency (Borislav Petkov)
- Documentation: In-Field Scan (Tony Luck)
- platform/x86/intel/ifs: add ABI documentation for IFS (Jithu Joseph)
- trace: platform/x86/intel/ifs: Add trace point to track Intel IFS operations (Tony Luck)
- platform/x86/intel/ifs: Add IFS sysfs interface (Jithu Joseph)
- platform/x86/intel/ifs: Add scan test support (Jithu Joseph)
- platform/x86/intel/ifs: Authenticate and copy to secured memory (Jithu Joseph)
- platform/x86/intel/ifs: Check IFS Image sanity (Jithu Joseph)
- platform/x86/intel/ifs: Read IFS firmware image (Jithu Joseph)
- platform/x86/intel/ifs: Add stub driver for In-Field Scan (Tony Luck)
- stop_machine: Add stop_core_cpuslocked() for per-core operations (Peter Zijlstra)
- x86/msr-index: Define INTEGRITY_CAPABILITIES MSR (Tony Luck)
- x86/microcode/intel: Expose collect_cpu_info_early() for IFS (Jithu Joseph)
- anolis: vfio/pci: fix double free when register device fault handler failed (Guanjun)
- anolis: vfio/pci: legacy support for vfio noiommu device (Guanjun)
- arm_mpam: resctrl: Add rmid index helpers (James Morse)
- arm64: mpam: Add helpers to change a tasks and cpu mpam partid/pmg values (James Morse)
- arm_mpam: resctrl: Add CDP emulation (James Morse)
- arm64: mpam: Context switch the MPAM registers (James Morse)
- arm_mpam: resctrl: Implement helpers to update configuration (James Morse)
- arm_mpam: resctrl: Add resctrl_arch_get_config() (James Morse)
- arm_mpam: resctrl: Implement resctrl_arch_reset_resources() (James Morse)
- arm_mpam: resctrl: Pick a value for num_rmid (James Morse)
- arm_mpam: resctrl: Pick the caches we will use as resctrl resources (James Morse)
- arm_mpam: resctrl: Add boilerplate cpuhp and domain allocation (James Morse)
- arm_mpam: Add helper to reset saved mbwu state (James Morse)
- arm_mpam: Track bandwidth counter state for overflow and power management (James Morse)
- arm_mpam: Add mpam_msmon_read() to read monitor value (James Morse)
- arm_mpam: Add helpers to allocate monitors (James Morse)
- arm_mpam: Probe and reset the rest of the features (James Morse)
- arm_mpam: Allow configuration to be applied and restored during cpu online (James Morse)
- arm_mpam: Add static key to indicate when mpam is enabled (James Morse)
- arm_mpam: Register and enable IRQs (James Morse)
- arm_mpam: Extend reset logic to allow devices to be reset any time (James Morse)
- arm_mpam: Add a helper to touch an MSC from any CPU (James Morse)
- arm_mpam: Reset MSC controls from cpu hp callbacks (James Morse)
- arm_mpam: Merge supported features during mpam_enable() into mpam_class (James Morse)
- arm_mpam: Probe the hardware features resctrl supports (James Morse)
- arm_mpam: Probe MSCs to find the supported partid/pmg values (James Morse)
- arm_mpam: Add cpuhp callbacks to probe MSC hardware (James Morse)
- arm_mpam: Add MPAM MSC register layout definitions (James Morse)
- arm_mpam: Add the class and component structures for ris firmware described (James Morse)
- arm_mpam: Add probe/remove for mpam msc driver and kbuild boiler plate (James Morse)
- cacheinfo: Expose the code to generate a cache-id from a device_node (James Morse)
- cacheinfo: Set cache 'id' based on DT data (Rob Herring)
- cacheinfo: Allow for >32-bit cache 'id' (Rob Herring)
- drivers: base: cacheinfo: Add helper to find the cache size from cpu+level (James Morse)
- drivers: base: cacheinfo: Check per_cpu_cacheinfo() is allocated (James Morse)
- dt-bindings: arm: Add MPAM MSC binding (Rob Herring)
- anolis: arm_mpam: Add cmdline parameter to enable and disable MPAM feature (Shawn Wang)
- ACPI / MPAM: Parse the MPAM table (James Morse)
- acpica: Add MPAM table structures (James Morse)
- anolis: staging: mpam: Move the old MPAM ACPI definitions to staging directory (Shawn Wang)
- of: Add of_get_cpu_hwid() to read hardware ID from CPU nodes (Rob Herring)
- software node: balance refcount for managed software nodes (Laurentiu Tudor)
- software node: Provide replacement for device_add_properties() (Heikki Krogerus)
- ACPI / irq: Add acpi_register_partitioned_percpu_gsi() (James Morse)
- ACPI / irq: Allow a compile-time arg0 for acpi_register_gsi()'s fwspec (James Morse)
- anolis: ACPI / PPTT: Add a helper to fill a cpumask from a cache with specific id and level (Shawn Wang)
- ACPI / PPTT: Add a helper to fill a cpumask from a cache_id (James Morse)
- ACPI / PPTT: Add a helper to fill a cpumask from a processor container (James Morse)
- ACPI / PPTT: Find PPTT cache level by ID (James Morse)
- ACPI / PPTT: Add a helper to build a cpumask from a cpu_node (James Morse)
- ACPI / PPTT: Provide a helper to walk processor containers (James Morse)
- anolis: arm64: cpufeature: Refactor the old MPAM configs and related code (Shawn Wang)
- arm64: cpufeature: discover CPU support for MPAM (James Morse)
- Documentation/admin-guide/acpi: Move information out of shell script comments (James Morse)
- ACPICA: ACPI 6.4: PPTT: add new version of subtable type 1 (Erik Kaneda)
- anolis: configs: enable configs related to erofs over fscache feature (Gao Xiang)
- erofs: scan devices from device table (Gao Xiang)
- erofs: add 'fsid' mount option (Gao Xiang)
- erofs: implement fscache-based data read for inline layout (Gao Xiang)
- erofs: implement fscache-based data read for non-inline layout (Gao Xiang)
- erofs: implement fscache-based metadata read (Gao Xiang)
- erofs: register fscache context for extra data blobs (Gao Xiang)
- erofs: register fscache context for primary data blob (Gao Xiang)
- erofs: add anonymous inode caching metadata for data blobs (Gao Xiang)
- erofs: add fscache context helper functions (Gao Xiang)
- erofs: register fscache volume (Gao Xiang)
- erofs: add fscache mode check helper (Gao Xiang)
- erofs: make erofs_map_blocks() generally available (Jeffle Xu)
- anolis: cachefiles: skip check for n_children in on-demand mode (Jeffle Xu)
- cachefiles: enable on-demand read mode (Jeffle Xu)
- cachefiles: implement on-demand read (Jeffle Xu)
- cachefiles: notify the user daemon when withdrawing cookie (Jeffle Xu)
- cachefiles: unbind cachefiles gracefully in on-demand mode (Jeffle Xu)
- cachefiles: notify the user daemon when looking up cookie (Jeffle Xu)
- erofs: use meta buffers for erofs_read_superblock() (Jeffle Xu)
- erofs: use meta buffers for zmap operations (Gao Xiang)
- erofs: use meta buffers for xattr operations (Gao Xiang)
- erofs: use meta buffers for super operations (Gao Xiang)
- erofs: use meta buffers for inode operations (Gao Xiang)
- erofs: introduce meta buffer operations (Gao Xiang)
- erofs: clean up erofs_map_blocks tracepoints (Gao Xiang)
- erofs: add multiple device support (Gao Xiang)
- erofs: decouple basic mount options from fs_context (Gao Xiang)
- erofs: introduce erofs_sb_has_xxx() helpers (Gao Xiang)
- erofs: fix double free of 'copied' (Gao Xiang)
- erofs: support reading chunk-based uncompressed files (Gao Xiang)
- erofs: introduce chunk-based file on-disk format (Gao Xiang)
- erofs: clean up file headers & footers (Gao Xiang)
- erofs: don't use erofs_map_blocks() any more (Yue Hu)
- anolis: userns: add a sysctl to control the max depth (Jiang Liu)
- userns: add a sysctl to disable unprivileged user namespace unsharing (Serge Hallyn)
- anolis: fuse: Introduce fd attach ioctl to retrieve a fuse device (Joseph Qi)
- anolis: fuse: do not passthrough DIO file if unsupported (Peng Tao)
- anolis: fuse: add fd passthrough IO metrics (Peng Tao)
- anolis: fuse: update time properly for fd passthrough IO (Peng Tao)
- anolis: fuse: allow further fs stacking on passthrough fuse (Peng Tao)
- anolis: fuse: add ioctl to support fd passthrough write only (Peng Tao)
- anolis: fuse: add sysctl API to toggle fd passthrough (Peng Tao)
- anolis: fuse: Introduce passthrough for mmap (Alessio Balsini)
- anolis: fuse: Use daemon creds in passthrough mode (Alessio Balsini)
- anolis: fuse: Handle asynchronous read and write in passthrough (Alessio Balsini)
- anolis: fuse: Introduce synchronous read and write for passthrough (Alessio Balsini)
- anolis: fuse: Passthrough initialization and release (Alessio Balsini)
- anolis: fuse: Definitions and ioctl for passthrough (Alessio Balsini)
- anolis: fs: Generic function to convert iocb to rw flags (Alessio Balsini)
- anolis: fuse: add sysfs api to resend processing queue requests (Peng Tao)
- fuse: fix matching of FUSE_DEV_IOC_CLONE command (Alessio Balsini)
- fuse: 32-bit user space ioctl compat for fuse device (Alessio Balsini)
- nvme: fix use after free when disconnecting a reconnecting ctrl (Ruozhu Li)
- nvme: fix write zeroes pi (Klaus Jensen)
- nvme-pci: disable hmb on idle suspend (Keith Busch)
- nvme: allow user toggling hmb usage (Keith Busch)
- nvme-pci: use attribute group for cmb sysfs (Keith Busch)
- nvme-pci: fix controller reset hang when racing with nvme_timeout (Tao Chiu)
- nvme: move the fabrics queue ready check routines to core (Tao Chiu)
- nvme-fabrics: reject I/O to offline device (Victor Gladkov)
- nvme: introduce a nvme_host_path_error helper (Chao Leng)
- blk-mq: introduce blk_mq_set_request_complete (Chao Leng)
- io_uring: fix soft lockup when call __io_remove_buffers (Ye Bin)
- anolis: configs: trace: enable CONFIG_OSNOISE_TRACER and CONFIG_TIMERLAT_TRACER (Qinyun Tan)
- tracing/timerlat: Do not wakeup the thread if the trace stops at the IRQ (Daniel Bristot de Oliveira)
- tracing/timerlat: Print stacktrace in the IRQ handler if needed (Daniel Bristot de Oliveira)
- tracing/timerlat: Notify IRQ new max latency only if stop tracing is set (Daniel Bristot de Oliveira)
- tracing/osnoise: Do not unregister events twice (Daniel Bristot de Oliveira)
- tracing/osnoise: Force quiescent states while tracing (Nicolas Saenz Julienne)
- tracing/osnoise: Make osnoise_main to sleep for microseconds (Daniel Bristot de Oliveira)
- tracing/osnoise: Properly unhook events if start_per_cpu_kthreads() fails (Nikita Yushchenko)
- tracing: Switch to kvfree_rcu() API (Uladzislau Rezki (Sony))
- tracing/osnoise: Make osnoise_instances static (Daniel Bristot de Oliveira)
- tracing/osnoise: Remove PREEMPT_RT ifdefs from inside functions (Daniel Bristot de Oliveira)
- tracing/osnoise: Remove STACKTRACE ifdefs from inside functions (Daniel Bristot de Oliveira)
- tracing/osnoise: Allow multiple instances of the same tracer (Daniel Bristot de Oliveira)
- tracing/osnoise: Remove TIMERLAT ifdefs from inside functions (Daniel Bristot de Oliveira)
- tracing/osnoise: Support a list of trace_array *tr (Daniel Bristot de Oliveira)
- tracing/osnoise: Use start/stop_per_cpu_kthreads() on osnoise_cpus_write() (Daniel Bristot de Oliveira)
- tracing/osnoise: Split workload start from the tracer start (Daniel Bristot de Oliveira)
- tracing/osnoise: Do not follow tracing_cpumask (Daniel Bristot de Oliveira)
- tracing/osnoise: Improve comments about barrier need for NMI callbacks (Daniel Bristot de Oliveira)
- trace/timerlat: Add migrate-disabled field to the timerlat header (Daniel Bristot de Oliveira)
- trace/osnoise: Add migrate-disabled field to the osnoise header (Daniel Bristot de Oliveira)
- trace: Add timerlat tracer (Daniel Bristot de Oliveira)
- trace: Add osnoise tracer (Daniel Bristot de Oliveira)
- trace: Add __print_ns_to_secs() and __print_ns_without_secs() helpers (Steven Rostedt)
- intel_idle: Fix SPR C6 optimization (Artem Bityutskiy)
- intel_idle: Fix the 'preferred_cstates' module parameter (Artem Bityutskiy)
- platform/x86: ISST: use semi-colons instead of commas (Dan Carpenter)
- platform/x86: ISST: Mark mmio_range_devid_0 and mmio_range_devid_1 with static keyword (Zou Wei)
- platform/x86: ISST: Change PCI device macros (Srinivas Pandruvada)
- platform/x86: ISST: Allow configurable offset range (Srinivas Pandruvada)
- platform/x86: ISST: Check for unaligned mmio address (Srinivas Pandruvada)
- powercap: intel_rapl: support new layout of Psys PowerLimit Register on SPR (Zhang Rui)
- x86/sgx: Add check for SGX pages to ghes_do_memory_failure() (Tony Luck)
- x86/sgx: Add hook to error injection address validation (Tony Luck)
- x86/sgx: Hook arch_memory_failure() into mainline code (Tony Luck)
- x86/sgx: Add SGX infrastructure to recover from poison (Tony Luck)
- x86/sgx: Initial poison handling for dirty and free pages (Tony Luck)
- x86/sgx: Add infrastructure to identify SGX EPC pages (Tony Luck)
- x86/sgx: Add new sgx_epc_page flag bit to mark free pages (Tony Luck)
- anolis: uio/uio_pci_generic: Introduce refcnt on open/release (Yao Hongbo)
- vfio: mdev: idxd: fix max batch size for vdcm (Dave Jiang)
- dmaengine: idxd: clear driver_name on wq reset (Dave Jiang)
- dmaengine: idxd: remove trailing white space on input str for driver_name (Dave Jiang)
- dmaengine: idxd: add wq driver name support for accel-config user tool (Dave Jiang)
- arm64: kdump: Do not allocate crash low memory if not needed (Zhen Lei)
- docs: kdump: Update the crashkernel description for arm64 (Zhen Lei)
- of: Support more than one crash kernel regions for kexec -s (Zhen Lei)
- of: fdt: Add memory for devices by DT property "linux,usable-memory-range" (Chen Zhou)
- arm64: kdump: Reimplement crashkernel=X (Chen Zhou)
- arm64: Use insert_resource() to simplify code (Zhen Lei)
- kdump: return -ENOENT if required cmdline option does not exist (Zhen Lei)
- arm64: kdump: Skip kmemleak scan reserved memory for kdump (Chen Wandun)
- anolis: Revert "ck: arm64: Add Crash kernel low reservation" (Kaihao Bai)
- anolis: Revert "ck: arm64: kdump: support reserving crashkernel above 4G" (Kaihao Bai)
- dmaengine: idxd: load wq configuration for max_xfer and max_batch (Dave Jiang)
- dmaengine: idxd: don't clear device context when read-only (Dave Jiang)
- engine: idxd: fix wq reset on RO config (Dave Jiang)
- anolis: configs: arm64: enable ARM64_E0PD (Feng Su)
- iommu/vt-d: Always signal completion for PRQ drain (Jacob Pan)
- iommu/vt-d: Use INVALID response code instead of FAILURE (Lu Baolu)
- x86/resctrl: Make resctrl_arch_rmid_read() safe to call in IRQ context (James Morse)
- x86/resctrl: Make __rmid_read() retry when it is interrupted (James Morse)
- x86/resctrl: Move mbm_state updates to __rmid_read() (James Morse)
- fs/resctrl: Export the closid/rmid to user-space (James Morse)
- fs/resctrl: Add helper to query if counters are free running (James Morse)
- fs/resctrl: Split resctrl_arch_mon_ctx_alloc() to have a no_wait version (James Morse)
- x86/resctrl: Move resctrl to /fs/ (James Morse)
- x86/resctrl: Move the filesystem bits to headers visible to fs/resctrl (James Morse)
- fs/resctrl: Add boiler plate for external resctrl code (James Morse)
- x86/resctrl: Drop __init/__exit on assorted symbols (James Morse)
- x86/resctrl: Describe resctrl's bitmap size assumptions (James Morse)
- x86/resctrl: Claim get_domain_from_cpu() for resctrl (James Morse)
- x86/resctrl: Move get_config_index() to a header (James Morse)
- x86/resctrl: Move thread_throttle_mode_init() to be managed by resctrl (James Morse)
- x86/resctrl: Make resctrl_arch_pseudo_lock_fn() take a plr (James Morse)
- x86/resctrl: Make prefetch_disable_bits belong to the arch code (James Morse)
- x86/resctrl: Allow an architecture to disable pseudo lock (James Morse)
- x86/resctrl: Export the is_mbm_*_enabled() helpers to asm/resctrl.h (James Morse)
- x86/resctrl: Stop using the for_each_*_rdt_resource() walkers (James Morse)
- x86/resctrl: Move max_{name,data}_width into resctrl code (James Morse)
- x86/resctrl: Move monitor init work to a resctrl init call (James Morse)
- x86/resctrl: Add a resctrl helper to reset all the resources (James Morse)
- x86/resctrl: Move resctrl types to a separate header (James Morse)
- x86/resctrl: Split arch and fs resctrl locks (James Morse)
- x86/resctrl: Wrap resctrl_arch_find_domain() around rdt_find_domain() (James Morse)
- x86/resctrl: Add cpu offline callback for resctrl work (James Morse)
- x86/resctrl: Allow overflow/limbo handlers to be scheduled on any-but cpu (James Morse)
- x86/resctrl: Add cpu online callback for resctrl work (James Morse)
- x86/resctrl: Export resctrl fs's init function (James Morse)
- x86/resctrl: Remove rdtgroup from update_cpu_closid_rmid() (James Morse)
- x86/resctrl: Add helper for setting cpu default properties (James Morse)
- x86/resctrl: Move ctrlval string parsing links away from the arch code (James Morse)
- x86/resctrl: Add helpers for system wide mon/alloc capable (James Morse)
- x86/resctrl: Make rdt_enable_key the arch's decision to switch (James Morse)
- x86/resctrl: Move alloc/mon static keys into helpers (James Morse)
- x86/resctrl: Make resctrl_mounted checks explicit (James Morse)
- x86/resctrl: Add a helper to avoid reaching into the arch code resource list (James Morse)
- x86/resctrl: Allow arch to allocate memory needed in resctrl_arch_rmid_read() (James Morse)
- x86/resctrl: Allow resctrl_arch_rmid_read() to sleep (James Morse)
- x86/resctrl: Queue mon_event_read() instead of sending an IPI (James Morse)
- x86/resctrl: Move closid/rmid matching and setting to use helpers (James Morse)
- x86/resctrl: Allow the allocator to check if a closid can allocate clean rmid (James Morse)
- x86/resctrl: Allow rmid allocation to be scoped by closid (James Morse)
- x86/resctrl: Move rmid allocation out of mkdir_rdt_prepare() (James Morse)
- x86/resctrl: Access per-rmid structures by index (James Morse)
- x86/resctrl: Track the closid with the rmid (James Morse)
- x86/resctrl: massage comment in rdtgroup_locksetup_enter() (James Morse)
- x86/resctrl: Only allocate mbps_val[] when it is going to be used (James Morse)
- x86/resctrl: Make resctrl_arch_rmid_read() return values in bytes (James Morse)
- x86/resctrl: Add resctrl_rmid_realloc_limit to abstract x86's boot_cpu_data (James Morse)
- x86/resctrl: Rename and change the units of resctrl_cqm_threshold (James Morse)
- x86/resctrl: Move get_corrected_mbm_count() into resctrl_arch_rmid_read() (James Morse)
- x86/resctrl: Move mbm_overflow_count() into resctrl_arch_rmid_read() (James Morse)
- x86/resctrl: Pass the required parameters into resctrl_arch_rmid_read() (James Morse)
- x86/resctrl: Abstract __rmid_read() (James Morse)
- x86/resctrl: Allow per-rmid arch private storage to be reset (James Morse)
- x86/resctrl: Add per-rmid arch private storage for overflow and chunks (James Morse)
- x86/resctrl: Calculate bandwidth from the previous __mon_event_count() chunks (James Morse)
- x86/resctrl: Allow update_mba_bw() to update controls directly (James Morse)
- x86/resctrl: Remove architecture copy of mbps_val (James Morse)
- x86/resctrl: Switch over to the resctrl mbps_val list (James Morse)
- x86/resctrl: Create mba_sc configuration in the rdt_domain (James Morse)
- x86/resctrl: Abstract and use supports_mba_mbps() (James Morse)
- x86/resctrl: Remove set_mba_sc()s control array re-initialisation (James Morse)
- x86/resctrl: Add domain offline callback for resctrl work (James Morse)
- x86/resctrl: Group struct rdt_hw_domain cleanup (James Morse)
- x86/resctrl: Add domain online callback for resctrl work (James Morse)
- x86/resctrl: Merge mon_capable and mon_enabled (James Morse)
- x86/resctrl: Kill off alloc_enabled (James Morse)
- x86/resctrl: Fix kfree() of the wrong type in domain_add_cpu() (James Morse)
- x86/resctrl: Free the ctrlval arrays when domain_setup_mon_state() fails (James Morse)
- x86/resctrl: Make resctrl_arch_get_config() return its value (James Morse)
- x86/resctrl: Merge the CDP resources (James Morse)
- x86/resctrl: Expand resctrl_arch_update_domains()'s msr_param range (James Morse)
- x86/resctrl: Remove rdt_cdp_peer_get() (James Morse)
- x86/resctrl: Merge the ctrl_val arrays (James Morse)
- x86/resctrl: Calculate the index from the configuration type (James Morse)
- x86/resctrl: Apply offset correction when config is staged (James Morse)
- x86/resctrl: Make ctrlval arrays the same size (James Morse)
- x86/resctrl: Pass configuration type to resctrl_arch_get_config() (James Morse)
- x86/resctrl: Add a helper to read a closid's configuration (James Morse)
- x86/resctrl: Rename update_domains() to resctrl_arch_update_domains() (James Morse)
- x86/resctrl: Allow different CODE/DATA configurations to be staged (James Morse)
- x86/resctrl: Group staged configuration into a separate struct (James Morse)
- x86/resctrl: Move the schemata names into struct resctrl_schema (James Morse)
- x86/resctrl: Add a helper to read/set the CDP configuration (James Morse)
- x86/resctrl: Swizzle rdt_resource and resctrl_schema in pseudo_lock_region (James Morse)
- x86/resctrl: Pass the schema to resctrl filesystem functions (James Morse)
- x86/resctrl: Add resctrl_arch_get_num_closid() (James Morse)
- x86/resctrl: Store the effective num_closid in the schema (James Morse)
- x86/resctrl: Walk the resctrl schema list instead of an arch list (James Morse)
- x86/resctrl: Label the resources with their configuration type (James Morse)
- x86/resctrl: Pass the schema in info dir's private pointer (James Morse)
- x86/resctrl: Add a separate schema list for resctrl (James Morse)
- x86/resctrl: Split struct rdt_domain (James Morse)
- x86/resctrl: Split struct rdt_resource (James Morse)
- x86/resctrl: Fix kernel-doc in internal.h (Fabio M. De Francesco)
- x86/resctrl: Fix kernel-doc in pseudo_lock.c (Fabio M. De Francesco)
- x86/resctrl: Fix init const confusion (Andi Kleen)
- x86/resctrl: Apply READ_ONCE/WRITE_ONCE to task_struct.{rmid,closid} (Valentin Schneider)
- x86/resctrl: Use task_curr() instead of task_struct->on_cpu to prevent unnecessary IPI (Reinette Chatre)
- x86/resctrl: Add printf attribute to log function (Tom Rix)
- x86/resctrl: Clean up unused function parameter in rmdir path (Xiaochen Shen)
- x86/resctrl: Constify kernfs_ops (Rikard Falkeborn)
- anolis: staging: mpam: Fix compile conflicts with kunpeng mpam (Xin Hao)
- tracing: Quiet smp_processor_id() use in preemptable warning in hwlat (banye)
- tracing: Replace deprecated CPU-hotplug functions. (banye)
- tracing: Disable "other" permission bits in the tracefs files (Steven Rostedt (VMware))
- trace/hwlat: Remove printk from sampling loop (Daniel Bristot de Oliveira)
- trace/hwlat: Use trace_min_max_param for width and window params (Daniel Bristot de Oliveira)
- trace: Add a generic function to read/write u64 values from tracefs (Daniel Bristot de Oliveira)
- trace/hwlat: Support hotplug operations (Daniel Bristot de Oliveira)
- trace/hwlat: Protect kdata->kthread with get/put_online_cpus (Daniel Bristot de Oliveira)
- trace/hwlat: Implement the per-cpu mode (Daniel Bristot de Oliveira)
- trace/hwlat: Switch disable_migrate to mode none (Daniel Bristot de Oliveira)
- trace/hwlat: Implement the mode config option (Daniel Bristot de Oliveira)
- tracing: Merge irqflags + preempt counter. (Sebastian Andrzej Siewior)
- anolis: xfs: drop all atomic staging extents when remounting (Gao Xiang)
- anolis: xfs: make atomic write extent reflects block size (Joseph Qi)
- anolis: tpm_tis_spi: workaround for yitian hardware bug (luanshi)
- vfio: mdev: idxd: fix soft reset for guest (Dave Jiang)
- anolis: configs: enable TLB_RANGE (Changsheng Liu)
- anolis: filemap: check compound_head(page)->mapping in generic_file_buffered_read() (Rongwei Wang)
- xfs: validate inode fork size against fork format (Dave Chinner)
- anolis: Revert "virtio_pci: Support surprise removal of virtio pci device" (Guanjun)
- anolis: livepatch: Revert module_enable_ro and module_disable_ro (yinbinbin)
- xfs: revert "xfs: actually bump warning counts when we send warnings" (Eric Sandeen)
- io_uring: fix lack of protection for compl_nr (Hao Xu)
- anolis: net: make tcp_drop noinline (Kangjie Xu)
- io_uring: fix __tctx_task_work() ctx race (Pavel Begunkov)
- io_uring: fix race condition in task_work add and clear (Jens Axboe)
- io_uring: tctx->task_lock should be IRQ safe (Jens Axboe)
- io_uring: don't restrict issue_flags for io_openat (Pavel Begunkov)
- io_uring: defer flushing cached reqs (Pavel Begunkov)
- io_uring: take comp_state from ctx (Pavel Begunkov)
- io_uring: enable req cache for task_work items (Jens Axboe)
- io_uring: provide FIFO ordering for task_work (Jens Axboe)
- io_uring: deduplicate failing task_work_add (Pavel Begunkov)
- io_uring: use persistent request cache (Jens Axboe)
- io_uring: feed reqs back into alloc cache (Pavel Begunkov)
- io_uring: persistent req cache (Pavel Begunkov)
- io_uring: count ctx refs separately from reqs (Pavel Begunkov)
- io_uring: remove fallback_req (Pavel Begunkov)
- io_uring: submit-completion free batching (Pavel Begunkov)
- io_uring: replace list with array for compl batch (Pavel Begunkov)
- io_uring: don't reinit submit state every time (Pavel Begunkov)
- io_uring: remove ctx from comp_state (Pavel Begunkov)
- io_uring: don't keep submit_state on stack (Pavel Begunkov)
- io_uring: don't propagate io_comp_state (Pavel Begunkov)
- io_uring: save atomic dec for inline executed reqs (Pavel Begunkov)
- io_uring: don't flush CQEs deep down the stack (Pavel Begunkov)
- io_uring: help inlining of io_req_complete() (Pavel Begunkov)
- io_uring: make op handlers always take issue flags (Pavel Begunkov)
- io_uring: replace force_nonblock with flags (Pavel Begunkov)
- anolis: xfs: set atomic write sb flag (Gao Xiang)
- anolis: xfs: fix xfs atomic staging registration (Gao Xiang)
- anolis: xfs: take iolock when setting di_flags (Joseph Qi)
- anolis: xfs: fix uninitialized return value (Joseph Qi)
- anolis: xfs: add atomic write sysfs entry (Joseph Qi)
- anolis: xfs: persist atomic write di_flags (Joseph Qi)
- anolis: xfs: add missing cmap->br_state = XFS_EXT_NORM update (Gao Xiang)
- anolis: xfs: fix improper return value (Gao Xiang)
- anolis: xfs: release atomic staging extents when unmounting (Gao Xiang)
- anolis: xfs: staging atomic merging (Gao Xiang)
- anolis: xfs: limit the number of atomic staging CoW (Gao Xiang)
- anolis: xfs: atomic write end_cow sanity check (Gao Xiang)
- anolis: xfs: deal with truncate flow (Gao Xiang)
- anolis: xfs: introduce atomic staging (Gao Xiang)
- anolis: xfs: add ioctl to set DIO_ATOMIC_WRITE flag (Joseph Qi)
- anolis: xfs: introduce DIO atomic write feature (Gao Xiang)
- anolis: configs: enable CONFIG_BPF_UNPRIV_DEFAULT_OFF by default (Qiao Ma)
- bpf: Disallow unprivileged bpf by default (Pawan Gupta)
- bpf/selftests: Test PTR_TO_RDONLY_MEM (Hao Luo) {CVE-2022-0500}
- bpf: Add MEM_RDONLY for helper args that are pointers to rdonly mem. (Hao Luo) {CVE-2022-0500}
- bpf: Make per_cpu_ptr return rdonly PTR_TO_MEM. (Hao Luo) {CVE-2022-0500}
- bpf: Convert PTR_TO_MEM_OR_NULL to composable types. (Hao Luo) {CVE-2022-0500}
- bpf: Introduce MEM_RDONLY flag (Hao Luo) {CVE-2022-0500}
- bpf: Replace PTR_TO_XXX_OR_NULL with PTR_TO_XXX | PTR_MAYBE_NULL (Hao Luo) {CVE-2022-0500}
- bpf: Replace RET_XXX_OR_NULL with RET_XXX | PTR_MAYBE_NULL (Hao Luo) {CVE-2022-0500}
- bpf: Replace ARG_XXX_OR_NULL with ARG_XXX | PTR_MAYBE_NULL (Hao Luo) {CVE-2022-0500}
- bpf: Introduce composable reg, ret and arg types. (Hao Luo) {CVE-2022-0500}
- KVM: arm64: Fix Function ID typo for PTP_KVM service (Zenghui Yu)
- ptp: Don't print an error if ptp_kvm is not supported (Jon Hunter)
- KVM: arm64: Fix table format for PTP documentation (Marc Zyngier)
- ptp: arm/arm64: Enable ptp_kvm for arm/arm64 (Jianyong Wu)
- KVM: arm64: Add support for the KVM PTP service (Jianyong Wu)
- clocksource: Add clocksource id for arm arch counter (Jianyong Wu)
- time: Add mechanism to recognize clocksource in time_get_snapshot (Thomas Gleixner)
- ptp: Reorganize ptp_kvm.c to make it arch-independent (Jianyong Wu)
- KVM: arm64: Advertise KVM UID to guests via SMCCC (Will Deacon)
- arm/arm64: Probe for the presence of KVM hypervisor (Will Deacon)
- anolis: mm: damon: change pr_err() to pr_debug() to avoid too many logs in console (Xin Hao)
- x86/kvm: Add kexec support for SEV Live Migration. (Ashish Kalra)
- x86/kvm: Add guest support for detecting and enabling SEV Live Migration feature. (Ashish Kalra)
- EFI: Introduce the new AMD Memory Encryption GUID. (Ashish Kalra)
- mm: x86: Invoke hypercall when page encryption status is changed (Brijesh Singh)
- x86/kvm: Add AMD SEV specific Hypercall3 (Brijesh Singh)
- KVM: X86: Introduce KVM_HC_MAP_GPA_RANGE hypercall (Ashish Kalra)
- x86/sgx: Silence softlockup detection when releasing large enclaves (Reinette Chatre)
- x86/sgx: Fix free page accounting (Reinette Chatre)
- x86/sgx/virt: implement SGX_IOC_VEPC_REMOVE ioctl (Paolo Bonzini)
- x86/sgx/virt: extract sgx_vepc_remove_page (Paolo Bonzini)
- x86/sgx: use vma_lookup() in sgx_encl_find() (Liam Howlett)
- mm: add vma_lookup(), update find_vma_intersection() comments (Liam Howlett)
- i915_vma: Rename vma_lookup to i915_vma_lookup (Liam Howlett)
- x86/sgx: Add missing xa_destroy() when virtual EPC is destroyed (Kai Huang)
- x86/sgx: Do not update sgx_nr_free_pages in sgx_setup_epc_section() (Jarkko Sakkinen)
- KVM: x86: Add capability to grant VM access to privileged SGX attribute (Sean Christopherson)
- KVM: VMX: Enable SGX virtualization for SGX1, SGX2 and LC (Sean Christopherson)
- KVM: VMX: Add ENCLS[EINIT] handler to support SGX Launch Control (LC) (Sean Christopherson)
- KVM: VMX: Add emulation of SGX Launch Control LE hash MSRs (Sean Christopherson)
- KVM: VMX: Add SGX ENCLS[ECREATE] handler to enforce CPUID restrictions (Sean Christopherson)
- KVM: VMX: Frame in ENCLS handler for SGX virtualization (Sean Christopherson)
- KVM: VMX: Add basic handling of VM-Exit from SGX enclave (Sean Christopherson)
- KVM: x86: Add reverse-CPUID lookup support for scattered SGX features (Sean Christopherson)
- KVM: x86: Add support for reverse CPUID lookup of scattered features (Sean Christopherson)
- KVM: x86: Define new #PF SGX error code bit (Sean Christopherson)
- KVM: x86: Export kvm_mmu_gva_to_gpa_{read,write}() for SGX (VMX) (Sean Christopherson)
- x86/sgx: Move provisioning device creation out of SGX driver (Sean Christopherson)
- x86/sgx: Add helpers to expose ECREATE and EINIT to KVM (Sean Christopherson)
- x86/sgx: Add helper to update SGX_LEPUBKEYHASHn MSRs (Kai Huang)
- x86/sgx: Add encls_faulted() helper (Sean Christopherson)
- x86/sgx: Add SGX2 ENCLS leaf definitions (EAUG, EMODPR and EMODT) (Sean Christopherson)
- x86/sgx: Move ENCLS leaf definitions to sgx.h (Sean Christopherson)
- x86/sgx: Initialize virtual EPC driver even when SGX driver is disabled (Kai Huang)
- x86/cpu/intel: Allow SGX virtualization without Launch Control support (Sean Christopherson)
- x86/sgx: Introduce virtual EPC for use by KVM guests (Sean Christopherson)
- x86/sgx: Add SGX_CHILD_PRESENT hardware error code (Sean Christopherson)
- x86/sgx: Wipe out EREMOVE from sgx_free_epc_page() (Kai Huang)
- x86/cpufeatures: Add SGX1 and SGX2 sub-features (Sean Christopherson)
- anolis: x86/cpufeatures: Remove duplicated definitions of X86_FEATURE_SGX1 and X86_FEATURE_SGX2 (Zhiquan Li)
- x86/cpufeatures: Make SGX_LC feature bit depend on SGX bit (Kai Huang)
- selftests/sgx: Fix Q1 and Q2 calculation in sigstruct.c (Tianjia Zhang)
- selftests/sgx: remove checks for file execute permissions (Dave Hansen)
- selftests/sgx: Refine the test enclave to have storage (Jarkko Sakkinen)
- selftests/sgx: Add EXPECT_EEXIT() macro (Jarkko Sakkinen)
- selftests/sgx: Dump enclave memory map (Jarkko Sakkinen)
- selftests/sgx: Migrate to kselftest harness (Jarkko Sakkinen)
- selftests/sgx: Rename 'eenter' and 'sgx_call_vdso' (Jarkko Sakkinen)
- x86/sgx: Expose SGX architectural definitions to the kernel (Sean Christopherson)
- selftests/sgx: Use getauxval() to simplify test code (Tianjia Zhang)
- selftests/sgx: Improve error detection and messages (Dave Hansen)
- x86/sgx: Remove unnecessary kmap() from sgx_ioc_enclave_init() (Ira Weiny)
- x86/sgx: Add a basic NUMA allocation scheme to sgx_alloc_epc_page() (Jarkko Sakkinen)
- x86/sgx: Replace section->init_laundry_list with sgx_dirty_page_list (Jarkko Sakkinen)
- x86/fpu: KVM: Set the base guest FPU uABI size to sizeof(struct kvm_xsave) (Hao Xiang)
- KVM: x86/cpuid: Clear XFD for component i if the base feature is missing (Hao Xiang)
- anolis: sched: Queue task on wakelist in the same llc if the wakee cpu is idle (Tianchen Ding)
- iommu/amd: Keep track of amd_iommu_irq_remap state (Joerg Roedel)
- iommu/amd: Don't call early_amd_iommu_init() when AMD IOMMU is disabled (Joerg Roedel)
- anolis: kfence: Fix the race condition when disabling KFENCE (Tianchen Ding)
- dmaengine: idxd: fail if exceeds wq->max_batch_size (Xu Yu)
- anolis: PCI/MSI: do not leak masks which is temporary (Xu Yu)
- mm: defer kmemleak object creation of module_alloc() (Kefeng Wang)
- ck: mm: damon: change kmalloc() flags in damon_new_region() (Xin Hao)
- ck: mm: damon: Fixed potential race issues when accessing "damon_regon" (Xin Hao)
- xfs: don't take a spinlock unconditionally in the DIO fastpath (Dave Chinner)
- anolis: configs: enable blk-iocost (Joseph Qi)
- anolis: mm: Add polling/interrupt switch for DMA copy (Jiangbo Wu)
- IDXD: claim device to support HugePage/THP segment size (Jiangbo Wu)
- mm: Add DMA pages copy support for page migration (Jiangbo Wu)
- dmaengine: idxd: Add DMA_MEMCPY_SG transfer support (Jiangbo Wu)
- dmaengine: idxd: Add batch descriptor support (Jiangbo Wu)
- dmaengine: Add DMA_MEMCPY_SG function (Jiangbo Wu)
- anolis: mm: migrate: add interface to enable batch migration (Xu Yu)
- mm: migrate: batch copying pages (Huang Ying)
- mm: migrate: batch flushing TLB (Huang Ying)
- mm: migrate: introduce batch page migration (Xu Yu)
- mm: migrate: don't split THP for misplaced NUMA page (Yang Shi)
- mm: migrate: return -ENOSYS if THP migration is unsupported (Yang Shi)
- mm: migrate: simplify the logic for handling permanent failure (Yang Shi)
- dmaengine: Revert "cookie bypass for out of order completion" (Ben Walker)
- dmaengine: idxd: Support device_tx_status (Ben Walker)
- dmaengine: idxd: idxd_desc.id is now a u16 (Ben Walker)
- dmaengine: Add provider documentation on cookie assignment (Ben Walker)
- dmaengine: Remove dma_set_tx_state (Ben Walker)
- dmaengine: Providers should prefer dma_set_residue over dma_set_tx_state (Ben Walker)
- dmaengine: Remove last, used from dma_tx_state (Ben Walker)
- dmaengine: Remove the last, used parameters in dma_async_is_tx_complete (Ben Walker)
- dmaengine: Move dma_set_tx_state to the provider API header (Ben Walker)
- dmaengine: Remove dma_async_is_complete from client API (Ben Walker)
- dmaengine: idxd: Separate user and kernel pasid enabling (Dave Jiang)
- dmaengine: idxd: Use DMA API for in-kernel DMA with PASID (Jacob Pan)
- iommu: Add PASID support for DMA mapping API users (Jacob Pan)
- iommu/vt-d: Implement domain ops for attach_dev_pasid (Jacob Pan)
- iommu: Add a helper to do PASID lookup from domain (Jacob Pan)
- iommu: Add a per domain PASID for DMA API (Jacob Pan)
- iommu: Add attach/detach_dev_pasid domain ops (Lu Baolu)
- iommu/vt-d: Allow IOMMU bypass for SoC integrated devices (Jacob Pan)
- iommu/vt-d: Enable ATS for the devices in SATC table (Yian Chen)
- iommu/vt-d: Parse SATC reporting structure (Yian Chen)
- iommu/vt-d: Add new enum value and structure for SATC (Yian Chen)
- vfio/type1: Fix a bug in IOMMU dirty bit clearing (Sanjay Kumar)
- vfio/type1: Fix a bug for getting dirty page (Liu Yi L)
- idxd: Fix kernel bug found during DSA DWQ live migration (Yi Sun)
- anolis: configs: Open CONFIG_RT_GROUP_SCHED (Xuchun Shang)
- ck: mm: add an switch to enable in-use hugtlb pages migraiton (Xin Hao)
- ck: mm: damon: add flush once in one sampling period (Xin Hao)
- kvm: Support 512bit operand for emulated instruction (Yi Sun)
- anolis: UAPI: Fix macro definition of KVM_HC_VM_ATTESTATION in include/uapi/linux/kvm_para.h (hanliyang)
- ubsan: remove CONFIG_UBSAN_OBJECT_SIZE (Kees Cook)
- ubsan: remove overflow checks (Andrey Ryabinin)
- ubsan: disable unsigned-overflow check for i386 (Arnd Bergmann)
- ubsan: remove UBSAN_MISC in favor of individual options (Kees Cook)
- ubsan: enable for all*config builds (Kees Cook)
- ubsan: disable UBSAN_TRAP for all*config (Kees Cook)
- ubsan: disable object-size sanitizer under GCC (Kees Cook)
- ubsan: move cc-option tests into Kconfig (Kees Cook)
- ubsan: remove redundant -Wno-maybe-uninitialized (Kees Cook)

* Fri Jul 29 2022 Yuanhe Shu <xiangzao@linux.alibaba.com> [5.10.112-11.2.an8]
- netfilter: nf_tables: disallow non-stateful expression in sets earlier (Pablo Neira Ayuso) {CVE-2022-32250}
- anolis: net/netfilter: rename nft_expr_info (Kangjie Xu) {CVE-2022-32250}

* Thu Jul 14 2022 Qiao Ma<mqaio@linux.alibaba.com> [5.10.112-11.1.an8]
- netfilter: nf_tables: stricter validation of element data (Pablo Neira Ayuso) {CVE-2022-34918}

* Tue May 24 2022 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.112-11.an8]
- net/sched: cls_u32: fix netns refcount changes in u32_change() (Eric Dumazet) {CVE-2022-29581}
- selftests/rseq: Remove useless assignment to cpu variable (Mathieu Desnoyers)
- selftests/rseq: introduce own copy of rseq uapi header (Mathieu Desnoyers)
- selftests/rseq: remove ARRAY_SIZE define from individual tests (Shuah Khan)
- rseq/selftests: Fix MEMBARRIER_CMD_PRIVATE_EXPEDITED_RSEQ build error under other arch. (Xingxing Su)
- Documentation/filesystem/dax: DAX on virtiofs (Jeffle Xu)
- fuse: mark inode DONT_CACHE when per inode DAX hint changes (Jeffle Xu)
- fuse: negotiate per inode DAX in FUSE_INIT (Jeffle Xu)
- fuse: enable per inode DAX (Jeffle Xu)
- fuse: support per inode DAX in fuse protocol (Jeffle Xu)
- fuse: make DAX mount option a tri-state (Jeffle Xu)
- fuse: add fuse_should_enable_dax() helper (Jeffle Xu)
- fuse: extend init flags (Miklos Szeredi)
- fs: Kill DCACHE_DONTCACHE dentry even if DCACHE_REFERENCED is set (Hao Li)
- anolis: arm64: Export generic asm/kvm_para.h to userspace (Shawn Wang)
- anolis: mm: duptext: consider memory nodes allowed when duplicating (Xu Yu)
- anolis: arm64: support TIF_POLLING_NRFLAG (Tianchen Ding)
- x86/microcode: Add an option to reload microcode even if revision is unchanged (Ashok Raj)
- mm/damon/dbgfs: protect targets destructions with kdamond_lock (SeongJae Park)
- anolis: drm/cirrus: fix a NULL vs IS_ERR() checks (Shile Zhang)
- kvm/x86: This commit emulates ENQCMD and ENQCMDS on an EPT exit (Sanjay Kumar)
- kvm/x86: Emulate the MOVDIR64B instruction on an EPT VMexit (Sanjay Kumar)
- KVM: x86: lapic: Rename [GET/SET]_APIC_DEST_FIELD to [GET/SET]_XAPIC_DEST_FIELD (Suravee Suthikulpanit)
- x86/cpufeatures: Introduce x2AVIC CPUID bit (Suravee Suthikulpanit)
- anolis: configs: x86: enable Intel TSX (Jiayu Ni)
- anlios: iommu/arm-smmu-v3: reading queue event info for pcie smmu (Jiankang Chen)
- perf c2c: Support AUX trace (Leo Yan)
- perf c2c: Support memory event PERF_MEM_EVENTS__LOAD_STORE (Leo Yan)
- KVM: SEV: Mask CPUID[0x8000001F].eax according to supported features (Paolo Bonzini)
- x86/cpufeatures: Assign dedicated feature word for CPUID_0x8000001F[EAX] (Sean Christopherson)
- anolis: x86/cpufeatures: fix {REQUIRED,DISABLED}_MASK_BIT_SET definition. (GuoRui.Yu)
- vfio/mdev: idxd: add 1swq mdev type (Dave Jiang)
- vfio/mdev: idxd: add shared wq support for mdev emulation (Dave Jiang)
- vfio/mdev: idxd: add PCI support for PASID (Dave Jiang)
- vfio/mdev: idxd: add error notification from host driver to mediated device (Dave Jiang)
- vfio/mdev: idxd: ims domain setup for the vdcm (Dave Jiang)
- vfio/mdev: idxd: add 1dwq-v1 mdev type (Dave Jiang)
- vfio/mdev: idxd: add basic mdev registration and helper functions (Dave Jiang)
- vfio/mdev: idxd: add new wq state for mdev (Dave Jiang)
- dmaengine: idxd: mdev host setup (Dave Jiang)
- vfio/mdev: idxd: add virtual device emulation support (Dave Jiang)
- vfio: Adding Dynamic trap flag (Sanjay Kumar)
- vfio/mdev: add req_trigger (Dave Jiang)
- KVM: TDX: Skip #MSMI induced #MC handling and handle it as a user #MC (GuoRui.Yu)
- x86: Flush cache of TDX private memory during kexec() (Kai Huang)
- anolis: SEAM/TDX: Load and init TDX-SEAMLDR during boot (GuoRui.Yu)
- KVM: TDX: Add macro framework to wrap TDX SEAMCALLs (Sean Christopherson)
- KVM: TDX: Add SEAMRR related MSRs macro definition (Kai Huang)
- KVM: TDX: Add architectural definitions for structures and values (Sean Christopherson)
- KVM: TDX: Add TDX "architectural" error codes (Sean Christopherson)
- x86/cpu: Move get_builtin_firmware() common code (from microcode only) (Zhang Chen)
- x86/msr-index: Define MSR_IA32_MKTME_KEYID_PART used by TDX (Sean Christopherson)
- x86/cpufeatures: Add synthetic feature flag for TDX (in host) (Sean Christopherson)
- x86/mtrr: mask out keyid bits from variable mtrr mask register (Isaku Yamahata)
- x86/mktme: move out MKTME related constatnts/macro to msr-index.h (Isaku Yamahata)
- vfio: vfio migration stuff (Dave Jiang)
- vfio: vfio fixups for idxd mdev (Dave Jiang)
- vfio/mdev: idxd: add mdev type as a new wq type (Dave Jiang)
- vfio/mdev: Add a member for iommu fault data in mdev_device (Liu Yi L)
- vfio_pci: A fix to ext_irq support (Liu Yi L)
- vfio/pci: Add framework for custom interrupt indices (Eric Auger)
- vfio: Use capability chains to handle device specific irq (Tina Zhang)
- vfio/pci: Allow to mmap the fault queue (Eric Auger)
- vfio/pci: Register an iommu fault handler (Eric Auger)
- vfio/pci: Add VFIO_REGION_TYPE_NESTED region type (Liu Yi L)
- vfio: Add new IRQ for DMA fault reporting (Cathy Zhang)
- vfio/type1: Save domain when attach domain to mdev (Lu Baolu)
- vfio/mdev: Add a member for iommu domain in mdev_device (Lu Baolu)
- dmaengine: idxd: prep for virtual device commands (Dave Jiang)
- dmaengine: idxd: add device support functions in prep for mdev (Dave Jiang)
- dmaengine: idxd: add IMS detection in base driver (Dave Jiang)
- vfio: Provide better generic support for open/release vfio_device_ops (Jason Gunthorpe)
- vfio: Introduce a vfio_uninit_group_dev() API call (Max Gurtovoy)
- vfio/pci: Make vfio_pci_regops->rw() return ssize_t (Yishai Hadas)
- vfio/mdev: Allow the mdev_parent_ops to specify the device driver to bind (Jason Gunthorpe)
- vfio/mdev: Remove CONFIG_VFIO_MDEV_DEVICE (Jason Gunthorpe)
- vfio: centralize module refcount in subsystem layer (Max Gurtovoy)
- vfio/gvt: fix DRM_I915_GVT dependency on VFIO_MDEV (Arnd Bergmann)
- vfio/mdev: Correct the function signatures for the mdev_type_attributes (Jason Gunthorpe)
- vfio/mdev: Remove kobj from mdev_parent_ops->create() (Jason Gunthorpe)
- vfio/gvt: Make DRM_I915_GVT depend on VFIO_MDEV (Jason Gunthorpe)
- vfio/mdev: Fix missing static's on MDEV_TYPE_ATTR's (Jason Gunthorpe)
- vfio/mbochs: Use mdev_get_type_group_id() (Jason Gunthorpe)
- vfio/mdpy: Use mdev_get_type_group_id() (Jason Gunthorpe)
- vfio/mtty: Use mdev_get_type_group_id() (Jason Gunthorpe)
- vfio/gvt: Use mdev_get_type_group_id() (Jason Gunthorpe)
- vfio/mdev: Add mdev/mtype_get_type_group_id() (Jason Gunthorpe)
- vfio/mdev: Remove duplicate storage of parent in mdev_device (Jason Gunthorpe)
- vfio/mdev: Reorganize mdev_device_create() (Jason Gunthorpe)
- vfio/mdev: Add missing reference counting to mdev_type (Jason Gunthorpe)
- vfio/mdev: Expose mdev_get/put_parent to mdev_private.h (Jason Gunthorpe)
- vfio/mdev: Use struct mdev_type in struct mdev_device (Jason Gunthorpe)
- vfio/mdev: Simplify driver registration (Jason Gunthorpe)
- vfio/mdev: Add missing typesafety around mdev_device (Jason Gunthorpe)
- vfio: Remove device_data from the vfio bus driver API (Jason Gunthorpe)
- vfio/pci: Replace uses of vfio_device_data() with container_of (Jason Gunthorpe)
- vfio: Make vfio_device_ops pass a 'struct vfio_device *' instead of 'void *' (Jason Gunthorpe)
- vfio/mdev: Make to_mdev_device() into a static inline (Jason Gunthorpe)
- vfio/mdev: Use vfio_init/register/unregister_group_dev (Jason Gunthorpe)
- vfio/pci: Use vfio_init/register/unregister_group_dev (Jason Gunthorpe)
- vfio/fsl-mc: Use vfio_init/register/unregister_group_dev (Jason Gunthorpe)
- vfio/platform: Use vfio_init/register/unregister_group_dev (Jason Gunthorpe)
- vfio: Split creation of a vfio_device into init and register ops (Jason Gunthorpe)
- vfio: Simplify the lifetime logic for vfio_device (Jason Gunthorpe)
- vfio: Remove extra put/gets around vfio_device->group (Jason Gunthorpe)
- vfio: iommu driver notify callback (Steve Sistare)
- vfio-mdev: Wire in a request handler for mdev parent (Eric Farman)
- iommu/vt-d: Listen to IOASID notifications (Jacob Pan)
- iommu/ioasid: Add a workqueue for cleanup work (Jacob Pan)
- iommu/ioasid: Support mm token type ioasid_set notifications (Jacob Pan)
- iommu/ioasid: Introduce notification APIs (Jacob Pan)
- iommu/ioasid: Introduce ioasid_set private ID (Jacob Pan)
- iommu/ioasid: Add ioasid_set iterator helper functions (Jacob Pan)
- iommu/ioasid: Add free function and states (Jacob Pan)
- iommu/ioasid: Redefine IOASID set and allocation APIs (Jacob Pan)
- iommu/arm-smmu-v3: Implement iommu_sva_bind/unbind() (Jean-Philippe Brucker)
- iommu/ioasid: Support setting system-wide capacity (Jacob Pan)
- iommu/ioasid: Add a separate function for detach data (Jacob Pan)
- iommu/ioasid: Rename ioasid_set_data() (Jacob Pan)
- iommu/vt-d: Convert intel iommu driver to the iommu ops (Tom Murphy)
- iommu: Allow the dma-iommu api to use bounce buffers (Tom Murphy)
- iommu: Add iommu_dma_free_cpu_cached_iovas() (Tom Murphy)
- iommu: Handle freelists when using deferred flushing in iommu drivers (Tom Murphy)
- anolis: iommu/sva: Add ioasid refcount back (Fengqian Gao)
- driver core: Export device_driver_attach() (Jason Gunthorpe)
- driver core: Don't return EPROBE_DEFER to userspace during sysfs bind (Christoph Hellwig)
- driver core: Flow the return code from ->probe() through to sysfs bind (Christoph Hellwig)
- driver core: Better distinguish probe errors in really_probe (Christoph Hellwig)
- driver core: Pull required checks into driver_probe_device() (Jason Gunthorpe)
- driver core: Avoid pointless deferred probe attempts (Saravana Kannan)
- driver core: make driver_probe_device() static (Julian Wiedmann)
- anolis: configs: refresh default configs (Shile Zhang)
- anolis: mm: correct slab meminfo in rich container (Xu Yu)
- msi: dynamic dev_msi (Megha Dey)
- IMS:add aux data callback (Megha Dey)
- MSI: export msi_domain_alloc_irqs() symbol (Dave Jiang)
- PCI/MSI: Free newly allocated vectors if MSI-X allocation fails (Megha Dey)
- PCI/MSI: Free MSI-X interrupt with a given IRQ number (Megha Dey)
- PCI/MSI: Enable dynamic allocation of MSI-X vectors (Megha Dey)
- genirq/msi: Iterate msi_list starting from a given desc (Megha Dey)
- PCI/MSI: Add bitmap to track allocated MSI-X vectors (Megha Dey)
- PCI/MSI: Add mutex to serialize MSI-X interrupt allocation (Megha Dey)
- PCI/MSI: Separate one time setup functionalities (Megha Dey)
- PCI/MSI: Store MSI-X table address in struct pci_dev (Megha Dey)
- genirq/msi: Provide helpers to return Linux IRQ/dev_msi hw IRQ number (Dave Jiang)
- irqchip: Add IMS (Interrupt Message Store) driver (Megha Dey)
- platform-msi: Add platform check for subdevice irq domain (Lu Baolu)
- iommu: Add capability IOMMU_CAP_VIOMMU_HINT (Megha Dey)
- iommu/vt-d: Add DEV-MSI support (Megha Dey)
- genirq: Set auxiliary data for an interrupt (Megha Dey)
- irqdomain/msi: Provide msi_alloc/free_store() callbacks (Thomas Gleixner)
- platform-msi: Add device MSI infrastructure (Thomas Gleixner)
- genirq/msi: Provide and use msi_domain_set_default_info_flags() (Thomas Gleixner)
- PCI/MSI: Handle msi_populate_sysfs() errors correctly (Wang Hai)
- genirq/msi: Move MSI sysfs handling from PCI to MSI core (Barry Song)
- PCI/MSI: Consolidate error handling in msi_capability_init() (Thomas Gleixner)
- PCI/sysfs: Use sysfs_emit() and sysfs_emit_at() in "show" functions (Krzysztof Wilczyński)
- PCI/sysfs: Rely on lengths from scnprintf(), dsm_label_utf16s_to_utf8s() (Krzysztof Wilczyński)
- PCI/sysfs: Rename device_has_dsm() to device_has_acpi_name() (Krzysztof Wilczyński)
- genirq/proc: Take buslock on affinity write (Thomas Gleixner)
- genirq: Export affinity setter for modules (Thomas Gleixner)
- platform-msi: Provide default irq_chip:: Ack (Thomas Gleixner)
- x86/msi: Rename and rework pci_msi_prepare() to cover non-PCI MSI (Thomas Gleixner)
- x86/irq: Add DEV_MSI allocation type (Thomas Gleixner)
- crypto: ccp: Use the stack and common buffer for INIT command (Sean Christopherson)
- crypto: ccp: Use the stack and common buffer for status commands (Sean Christopherson)
- crypto: ccp: Use the stack for small SEV command buffers (Sean Christopherson)
- crypto: ccp: Play nice with vmalloc'd memory for SEV command structs (Sean Christopherson)
- crypto: ccp: Reject SEV commands with mismatching command buffer (Sean Christopherson)
- crypto: ccp - Don't initialize SEV support without the SEV feature (Tom Lendacky)
- vfio/iommu_type1: Optimize dirty bitmap population based on iommu HWDBM (jiangkunkun)
- vfio/iommu_type1: Add HWDBM status maintanance (jiangkunkun)
- iommu/vtd: Merge small pages back to big page (Yi Sun)
- iommu/vtd: Split super page to 4KB when enabling FL/SL AD (Yi Sun)
- iommu/vt-d: Fix a bug in the loop iterator during clearing the SL A/D bits (Sanjay Kumar)
- iommu/vt-d: Fix bugs in __domain_sync_dirty_log() (Sanjay Kumar)
- iommu/vt-d: Pass the correct domain to intel_pasid_setup_slade() (Sanjay Kumar)
- iommu/vt-d: Clear dirty log for FLT before getting it (Yi Sun)
- iommu/vt-d: Add HWDBM device feature reporting (Yi Sun)
- iommu: Add HWDBM device feature reporting (jiangkunkun)
- iommu/vt-d: Add interface to clear dirty log (Yi Sun)
- iommu: Clear dirty log according to bitmap (jiangkunkun)
- iommu/vt-d: Add interface to sync dirty log (Yi Sun)
- iommu: Add interface to sync dirty log (jiangkunkun)
- iommu/vt-d: Add interface to set HWDBM (Yi Sun)
- iommu: Add interface to set HWDBM (Yi Sun)
- iommu/vt-d: Add feature detection for SLADS (Yi Sun)
- iommu/vt-d: Add an option force using SL for IOVA (Kumar Sanjay K)
- iommu/vt-d: Fix general protection fault in aux_detach_device() (Liu Yi L)

* Tue May 10 2022 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.112-11_rc2.an8]
- EDAC/amd64: Add new register offset support and related changes (Yazen Ghannam)
- EDAC/amd64: Set memory type per DIMM (Yazen Ghannam)
- EDAC/amd64: Add support for AMD Family 19h Models 10h-1Fh and A0h-AFh (Yazen Ghannam)
- EDAC: Add RDDR5 and LRDDR5 memory types (Yazen Ghannam)
- anolis: ACPI: add detection for core die topology (Guanghui Feng)
- anolis: mm: disable proactive compaction by default (Xu Yu)
- mm, slub: use prefetchw instead of prefetch (Hyeonggon Yoo)
- hwmon: (k10temp) Support up to 12 CCDs on AMD Family of processors (Babu Moger)
- hwmon: (k10temp) Add support for AMD Family 19h Models 10h-1Fh and A0h-AFh (Babu Moger)
- hwmon: (k10temp) Remove unused definitions (Babu Moger)
- x86/amd_nb: Add AMD Family 19h Models (10h-1Fh) and (A0h-AFh) PCI IDs (Yazen Ghannam)
- hwmon: (k10temp) Remove residues of current and voltage (suma hegde)
- hwmon: (k10temp) Add support for yellow carp (Mario Limonciello)
- hwmon: (k10temp) Rework the temperature offset calculation (Mario Limonciello)
- hwmon: (k10temp) Don't show Tdie for all Zen/Zen2/Zen3 CPU/APU (Mario Limonciello)
- hwmon: (k10temp) Add additional missing Zen2 and Zen3 APUs (Mario Limonciello)
- hwmon: (k10temp) support Zen3 APUs (David Bartley)
- x86/amd_nb: Add AMD family 19h model 50h PCI ids (David Bartley)
- hwmon: (k10temp) Zen3 Ryzen Desktop CPUs support (Gabriel Craciunescu)
- anolis: mm, kidled: optimize kidled_set_page_age() call (Gang Deng)
- perf test: Remove now useless failing sub test "BPF relocation checker" (Thomas Richter)
- perf script: Fix PERF_SAMPLE_WEIGHT_STRUCT support (Kan Liang)
- perf tools: Update topdown documentation for Sapphire Rapids (Kan Liang)
- perf stat: Support L2 Topdown events (Kan Liang)
- perf test: Support PERF_SAMPLE_WEIGHT_STRUCT (Kan Liang)
- perf report: Support instruction latency (Kan Liang)
- perf tools: Support PERF_SAMPLE_WEIGHT_STRUCT (Kan Liang)
- perf c2c: Support data block and addr block (Kan Liang)
- perf tools: Support data block and addr block (Kan Liang)
- perf tools: Support the auxiliary event (Kan Liang)
- tools headers uapi: Update tools's copy of linux/perf_event.h (Kan Liang)
- tools headers UAPI: Update tools's copy of linux/perf_event.h (Kan Liang)
- perf/core: Add support for PERF_SAMPLE_CODE_PAGE_SIZE (Stephane Eranian)
- perf/core: Add PERF_SAMPLE_DATA_PAGE_SIZE (Kan Liang)
- anolis: sched: Make membarrier_mm_sync_core_before_usermode() empty on ARM (Tianchen Ding)
- hinic: fix bug of wq out of bound access (XiangZao)
- perf mem: Support ARM SPE events (Leo Yan)
- perf mem: Support AUX trace (Leo Yan)
- perf auxtrace: Add itrace option '-M' for memory events (Leo Yan)
- perf mem: Only initialize memory event for recording (Leo Yan)
- perf mem: Support new memory event PERF_MEM_EVENTS__LOAD_STORE (Leo Yan)
- perf mem: Introduce weak function perf_mem_events__ptr() (Leo Yan)
- perf mem: Search event name with more flexible path (Leo Yan)
- perf mem2node: Improve warning if detected no memory nodes (Leo Yan)
- ccp: ccp - add support for Green Sardine (Devaraj Rangasamy)
- crypto: ccp - Add support for new CCP/PSP device ID (John Allen)
- anolis: RDMA/erdma: Fix bug in destroy qp (Kai)
- anolis: RDMA/erdma: ERDMA driver bugfix (Kai)
- anolis: RDMA/erdma: Update for hardware and SMC-R (Kai)
- anolis: RDMA/erdma: fix SPDX header for erdma (Kai)
- anolis: RDMA/erdma: remove useless print info (Kai)
- anolis: RDMA/erdma: add irq affinity based on numa (Kai)
- anolis: RDMA/erdma: fix mr count bug in ib_alloc_mr (Kai)
- anolis: RDMA/erdma: Add ERDMA driver (Kai)
- anolis: net/smc: Fix slab-out-of-bounds issue in fallback (Wen Gu)
- anolis: net/smc: Only save the original clcsock callback functions (Wen Gu)
- anolis: net/smc: sync err info when TCP connection is refused (Tony Lu)
- anolis: net/smc: don't req_notify until all CQEs drained (Wen Gu)
- anolis: net/smc: Introduce link dimension req & comp debug info (Wen Gu)
- anolis: net/smc: Introduce rtoken validity check before sending (Wen Gu)
- anolis: net/smc: Introduce a sysctl to disable {a}symmetric link group (Wen Gu)
- anolis: net/smc: Keep first contact clcsock (Tony Lu)
- anolis: net/smc: Multiple CQs per IB devices (Tony Lu)
- anolis: net/smc: Introduce smc_ib_cq to bind link and cq (Tony Lu)
- anolis: net/smc: Introduce link-related proc file (Guangguan Wang)
- anolis: net/smc: Support rq flow control in smc-r link layer (Guangguan Wang)
- anolis: net/smc: Add sysctl conrtol for handshake limiation (D. Wythe)
- anolis: net/smc: Avoid unmapping bufs from unused links (Wen Gu)
- anolis: net/smc: allow different subnet communication (Tony Lu)
- anolis: net/smc: don't call ib_req_notify_cq in the send routine (Dust Li)
- anolis: net/smc: Add TX and RX diagnosis information (Tony Lu)
- anolis: net/smc: Introduce TCP to SMC replacement netlink commands (Tony Lu)
- anolis: net/smc: Introduce SMC-R-related proc files (Tony Lu)
- anolis: net/smc: Introduce sysctl tcp2smc (Tony Lu)
- anolis: net/smc: Expose SMCPROTO_SMC and SMCPROTO_SMC6 to userspace (Tony Lu)
- anolis: net/smc: Introduce tunable sysctls for sndbuf and RMB size (Wen Gu)
- anolis: net/smc: Supplement for SMC-R iWARP support (Wen Gu)
- anolis: net/smc: Introduce iWARP device support (Wen Gu)
- anolis: net/smc: Introduce iWARP extended information in struct smc_link (Wen Gu)
- net/smc: Fix sock leak when release after smc_shutdown() (Tony Lu)
- net/smc: Fix af_ops of child socket pointing to released memory (Karsten Graul)
- net/smc: Fix NULL pointer dereference in smc_pnet_find_ib() (Karsten Graul)
- net/smc: use memcpy instead of snprintf to avoid out of bounds read (Karsten Graul)
- net/smc: Send out the remaining data in sndbuf before close (Wen Gu)
- net/smc: fix a memory leak in smc_sysctl_net_exit() (Eric Dumazet)
- net/smc: fix -Wmissing-prototypes warning when CONFIG_SYSCTL not set (Dust Li)
- net/smc: fix compile warning for smc_sysctl (Dust Li)
- Revert "net/smc: don't req_notify until all CQEs drained" (Dust Li)
- net/smc: fix unexpected SMC_CLC_DECL_ERR_REGRMB error cause by server (D. Wythe)
- net/smc: fix unexpected SMC_CLC_DECL_ERR_REGRMB error generated by client (D. Wythe)
- net: smc: fix different types in min() (Jakub Kicinski)
- net/smc: don't send in the BH context if sock_owned_by_user (Dust Li)
- net/smc: don't req_notify until all CQEs drained (Dust Li)
- net/smc: correct settings of RMB window update limit (Dust Li)
- net/smc: send directly on setting TCP_NODELAY (Dust Li)
- net/smc: add sysctl for autocorking (Dust Li)
- net/smc: add autocorking support (Dust Li)
- net/smc: add sysctl interface for SMC (Dust Li)
- net/smc: Call trace_smc_tx_sendmsg when data corked (Tony Lu)
- net/smc: Fix cleanup when register ULP fails (Tony Lu)
- net/smc: fix connection leak (D. Wythe)
- net/smc: Use a mutex for locking "struct smc_pnettable" (Fabio M. De Francesco)
- net/smc: unlock on error paths in __smc_setsockopt() (Dan Carpenter)
- net/smc: return ETIMEDOUT when smc_connect_clc() timeout (D. Wythe)
- net/smc: Add comment for smc_tx_pending (Tony Lu)
- net/smc: Add global configure for handshake limitation by netlink (D. Wythe)
- net/smc: Dynamic control handshake limitation by socket options (D. Wythe)
- net/smc: Limit SMC visits when handshake workqueue congested (D. Wythe)
- net/smc: Limit backlog connections (D. Wythe)
- net/smc: Make smc_tcp_listen_work() independent (D. Wythe)
- net/smc: Avoid overwriting the copies of clcsock callback functions (Wen Gu)
- net/smc: Cork when sendpage with MSG_SENDPAGE_NOTLAST flag (Tony Lu)
- net/smc: Remove corked dealyed work (Tony Lu)
- net/smc: Send directly when TCP_CORK is cleared (Tony Lu)
- net/smc: Forward wakeup to smc socket waitqueue after fallback (Wen Gu)
- net/smc: Transitional solution for clcsock race issue (Wen Gu)
- net/smc: Fix hung_task when removing SMC-R devices (Wen Gu)
- net/smc: Remove unused function declaration (Wen Gu)
- net/smc: Resolve the race between SMC-R link access and clear (Wen Gu)
- net/smc: Introduce a new conn->lgr validity check helper (Wen Gu)
- net/smc: Resolve the race between link group access and termination (Wen Gu)
- net/smc: Reset conn->lgr when link group registration fails (Wen Gu)
- net/smc: add comments for smc_link_{usable|sendable} (Dust Li)
- net/smc: remove redundant re-assignment of pointer link (Colin Ian King)
- net/smc: Introduce TCP ULP support (Tony Lu)
- net/smc: Introduce net namespace support for linkgroup (Tony Lu)
- net/smc: Use the bitmap API when applicable (Christophe JAILLET)
- net/smc: fix kernel panic caused by race of smc_sock (Dust Li)
- net/smc: don't send CDC/LLC message if link not ready (Dust Li)
- net/smc: fix using of uninitialized completions (Karsten Graul)
- net/smc: Prevent smc_release() from long blocking (D. Wythe)
- net/smc: Clear memory when release and reuse buffer (Tony Lu)
- net/smc: Keep smc_close_final rc during active close (Tony Lu)
- net/smc: fix wrong list_del in smc_lgr_cleanup_early (Dust Li)
- net/smc: Don't call clcsock shutdown twice when smc shutdown (Tony Lu)
- net/smc: Fix loop in smc_listen (Guo DaXing)
- net/smc: Fix NULL pointer dereferencing in smc_vlan_by_tcpsk() (Karsten Graul)
- net/smc: Ensure the active closing peer first closes clcsock (Tony Lu)
- net/smc: Clean up local struct sock variables (Tony Lu)
- net/smc: Avoid warning of possible recursive locking (Wen Gu)
- net/smc: Make sure the link_id is unique (Wen Gu)
- net/smc: Transfer remaining wait queue entries during fallback (Wen Gu)
- net/smc: fix sk_refcnt underflow on linkdown and fallback (Dust Li)
- net/smc: Print function name in smcr_link_down tracepoint (Tony Lu)
- net/smc: Introduce tracepoint for smcr link down (Tony Lu)
- net/smc: Introduce tracepoints for tx and rx msg (Tony Lu)
- net/smc: Introduce tracepoint for fallback (Tony Lu)
- net/smc: Correct spelling mistake to TCPF_SYN_RECV (Wen Gu)
- net/smc: Fix smc_link->llc_testlink_time overflow (Tony Lu)
- net/smc: stop links when their GID is removed (Karsten Graul)
- net/smc: add netlink support for SMC-Rv2 (Karsten Graul)
- net/smc: extend LLC layer for SMC-Rv2 (Karsten Graul)
- net/smc: add v2 support to the work request layer (Karsten Graul)
- net/smc: retrieve v2 gid from IB device (Karsten Graul)
- net/smc: add v2 format of CLC decline message (Karsten Graul)
- net/smc: add listen processing for SMC-Rv2 (Karsten Graul)
- net/smc: add SMC-Rv2 connection establishment (Karsten Graul)
- net/smc: prepare for SMC-Rv2 connection (Karsten Graul)
- net/smc: save stack space and allocate smc_init_info (Karsten Graul)
- net/smc: improved fix wait on already cleared link (Karsten Graul)
- net/smc: fix 'workqueue leaked lock' in smc_conn_abort_work (Karsten Graul)
- net/smc: add missing error check in smc_clc_prfx_set() (Karsten Graul)
- net/smc: add generic netlink support for system EID (Karsten Graul)
- net/smc: keep static copy of system EID (Karsten Graul)
- net/smc: add support for user defined EIDs (Karsten Graul)
- net/smc: Allow SMC-D 1MB DMB allocations (Stefan Raspl)
- net/smc: Correct smc link connection counter in case of smc client (Guvenc Gulce)
- net/smc: fix wait on already cleared link (Karsten Graul)
- net/smc: Ensure correct state of the socket in send path (Guvenc Gulce)
- net/smc: Fix ENODATA tests in smc_nl_get_fback_stats() (Dan Carpenter)
- net/smc: Make SMC statistics network namespace aware (Guvenc Gulce)
- net/smc: Add netlink support for SMC fallback statistics (Guvenc Gulce)
- net/smc: Add netlink support for SMC statistics (Guvenc Gulce)
- net/smc: Add SMC statistics support (Guvenc Gulce)
- net/smc: no need to flush smcd_dev's event_wq before destroying it (Julian Wiedmann)
- net/smc: avoid possible duplicate dmb unregistration (Karsten Graul)
- net/smc: remove device from smcd_dev_list after failed device_add() (Julian Wiedmann)
- net/smc: properly handle workqueue allocation failure (Anirudh Rayabharam)
- Revert "net/smc: fix a NULL pointer dereference" (Greg Kroah-Hartman)
- smc: disallow TCP_ULP in smc_setsockopt() (Cong Wang)
- net/smc: Remove redundant assignment to rc (Jiapeng Chong)
- net: smc: Remove repeated struct declaration (Wan Jiabing)
- net/smc: use memcpy instead of snprintf to avoid out of bounds read (Guvenc Gulce)
- smc: fix out of bound access in smc_nl_get_sys_info() (Jakub Kicinski)
- net/smc: fix access to parent of an ib device (Karsten Graul)
- net/smc: Add support for obtaining SMCR device list (Guvenc Gulce)
- net/smc: Add support for obtaining SMCD device list (Guvenc Gulce)
- net/smc: Add SMC-D Linkgroup diagnostic support (Guvenc Gulce)
- net/smc: Introduce SMCR get link command (Guvenc Gulce)
- net/smc: Introduce SMCR get linkgroup command (Guvenc Gulce)
- net/smc: Add support for obtaining system information (Guvenc Gulce)
- net/smc: Introduce generic netlink interface for diagnostic purposes (Guvenc Gulce)
- net/smc: Refactor smc ism v2 capability handling (Guvenc Gulce)
- net/smc: Add diagnostic information to link structure (Guvenc Gulce)
- net/smc: Add diagnostic information to smc ib-device (Guvenc Gulce)
- net/smc: Add link counters for IB device ports (Guvenc Gulce)
- net/smc: Add connection counters for links (Guvenc Gulce)
- net/smc: Use active link of the connection (Guvenc Gulce)
- net/smc: use helper smc_conn_abort() in listen processing (Karsten Graul)
- net/smc: fix direct access to ib_gid_addr->ndev in smc_ib_determine_gid() (Karsten Graul)
- net/smc: fix matching of existing link groups (Karsten Graul)
- net: smc: convert tasklets to use new tasklet_setup() API (Allen Pais)
- net/smc: improve return codes for SMC-Dv2 (Karsten Graul)
- Revert "ck: RDMA/erdma: Add ERDMA driver" (Tony Lu)
- Revert "net/smc: fix matching of existing link groups" (Tony Lu)
- anolis: mm: duptext: skip PG_mlocked operations on slave pages (Xu Yu)
- configs: enable AMD pstate (Jiayu Ni)
- MAINTAINERS: Add AMD P-State driver maintainer entry (Huang Rui)
- Documentation: amd-pstate: Add AMD P-State driver introduction (Huang Rui)
- cpufreq: amd-pstate: Add AMD P-State performance attributes (Huang Rui)
- cpufreq: amd-pstate: Add AMD P-State frequencies attributes (Huang Rui)
- cpufreq: amd-pstate: Add boost mode support for AMD P-State (Huang Rui)
- cpufreq: amd-pstate: Add trace for AMD P-State module (Huang Rui)
- cpufreq: amd-pstate: Introduce the support for the processors with shared memory solution (Huang Rui)
- cpufreq: amd-pstate: Add fast switch function for AMD P-State (Huang Rui)
- cpufreq: amd-pstate: Introduce a new AMD P-State driver to support future processors (Huang Rui)
- ACPI: CPPC: Add CPPC enable register function (Jinzhou Su)
- ACPI: CPPC: Implement support for SystemIO registers (Steven Noonan)
- x86/msr: Add AMD CPPC MSR definitions (Huang Rui)
- x86/cpufeatures: Add AMD Collaborative Processor Performance Control feature flag (Huang Rui)
- cpufreq: Add special-purpose fast-switching callback for drivers (Rafael J. Wysocki)
- cpufreq: schedutil: Add util to struct sg_cpu (Rafael J. Wysocki)
- x86, sched: Fix the AMD CPPC maximum performance value on certain AMD Ryzen generations (Huang Rui)
- x86, sched: Calculate frequency invariance for AMD systems (Nathan Fontenot)
- cppc_cpufreq: replace per-cpu data array with a list (Ionela Voinescu)
- cppc_cpufreq: clarify support for coordination types (Ionela Voinescu)
- cppc_cpufreq: use policy->cpu as driver of frequency setting (Ionela Voinescu)
- cppc_cpufreq: clean up cpu, cpu_num and cpunum variable use (Ionela Voinescu)
- x86/fpu/xstate: Consolidate size calculations (Thomas Gleixner)
- x86/fpu/xstate: Handle supervisor states in XSTATE permissions (Thomas Gleixner)
- x86/fpu/xsave: Handle compacted offsets correctly with supervisor states (Thomas Gleixner)
- x86/fpu: Cache xfeature flags from CPUID (Thomas Gleixner)
- x86/fpu/xsave: Initialize offset/size cache early (Thomas Gleixner)
- x86/fpu: Remove unused supervisor only offsets (Thomas Gleixner)
- x86/fpu: Remove redundant XCOMP_BV initialization (Thomas Gleixner)
- x86/cpufeatures: Put the AMX macros in the word 18 block (Jim Mattson)
- x86/ptrace: Fix xfpregs_set()'s incorrect xmm clearing (Andy Lutomirski)
- signal: Skip the altstack update when not needed (Chang S. Bae)
- Documentation/x86: Update documentation for SVA (Shared Virtual Addressing) (Fenghua Yu)
- tools/objtool: Check for use of the ENQCMD instruction in the kernel (Fenghua Yu)
- x86/cpufeatures: Re-enable ENQCMD (Fenghua Yu)
- x86/traps: Demand-populate PASID MSR via #GP (Fenghua Yu)
- sched: Define and initialize a flag to identify valid PASID in the task (Peter Zijlstra)
- x86/fpu: Clear PASID when copying fpstate (Fenghua Yu)
- iommu/sva: Assign a PASID to mm on PASID allocation and free it on mm exit (Xiaochen Shen)
- kernel/fork: Initialize mm's PASID (Fenghua Yu)
- iommu/ioasid: Introduce a helper to check for valid PASIDs (Fenghua Yu)
- mm: Change CONFIG option for mm->pasid field (Fenghua Yu)
- iommu/sva: Rename CONFIG_IOMMU_SVA_LIB to CONFIG_IOMMU_SVA (Xiaochen Shen)
- iommu/vt-d: Clean up unused PASID updating functions (Xiaochen Shen)
- iommu/vt-d: Fix PASID leak in intel_svm_unbind_mm() (Fenghua Yu)
- iommu/vt-d: Fix PASID reference leak (Fenghua Yu)
- iommu/vt-d: Use iommu_sva_alloc(free)_pasid() helpers (Lu Baolu)
- iommu/vt-d: Add pasid private data helpers (Lu Baolu)
- iommu/vt-d: Remove SVM_FLAG_PRIVATE_PASID (Lu Baolu)
- iommu/vt-d: Remove svm_dev_ops (Lu Baolu)
- iommu/vt-d: Disable SVM when ATS/PRI/PASID are not enabled in the device (Kyung Min Park)
- iommu/sva: Add PASID helpers (Jean-Philippe Brucker)
- iommu/ioasid: Add ioasid references (Jean-Philippe Brucker)
- dmaengine: idxd: update IAA definitions for user header (Dave Jiang)
- dmaengine: idxd: remove trailing white space on input str for wq name (Dave Jiang)
- dmaengine: idxd: don't load pasid config until needed (Dave Jiang)
- dmaengine: idxd: fix device cleanup on disable (Dave Jiang)
- dmaengine: idxd: Remove useless DMA-32 fallback configuration (Christophe JAILLET)
- dmaengine: idxd: restore traffic class defaults after wq reset (Dave Jiang)
- dmaengine: idxd: deprecate token sysfs attributes for read buffers (Dave Jiang)
- dmaengine: idxd: change bandwidth token to read buffers (Dave Jiang)
- dmaengine: idxd: fix wq settings post wq disable (Dave Jiang)
- dmaengine: idxd: change MSIX allocation based on per wq activation (Dave Jiang)
- dmaengine: idxd: fix descriptor flushing locking (Dave Jiang)
- dmaengine: idxd: embed irq_entry in idxd_wq struct (Dave Jiang)
- dmaengine: idxd: add knob for enqcmds retries (Dave Jiang)
- dmaengine: idxd: set defaults for wq configs (Dave Jiang)
- dmaengine: idxd: handle interrupt handle revoked event (Dave Jiang)
- dmaengine: idxd: handle invalid interrupt handle descriptors (Dave Jiang)
- dmaengine: idxd: create locked version of idxd_quiesce() call (Dave Jiang)
- dmaengine: idxd: add helper for per interrupt handle drain (Dave Jiang)
- dmaengine: idxd: move interrupt handle assignment (Dave Jiang)
- dmaengine: idxd: int handle management refactoring (Dave Jiang)
- dmaengine: idxd: rework descriptor free path on failure (Dave Jiang)
- dmaengine: idxd: fix missed completion on abort path (Dave Jiang)
- dmaengine: idxd: fix calling wq quiesce inside spinlock (Dave Jiang)
- configs: enable AMD ptdma module (Jiayu Ni)
- dmaengine: ptdma: remove PT_OFFSET to avoid redefnition (Sanjay R Mehta)
- dmaengine: ptdma: Add debugfs entries for PTDMA (Sanjay R Mehta)
- dmaengine: ptdma: register PTDMA controller as a DMA resource (Sanjay R Mehta)
- dmaengine: ptdma: Initial driver for the AMD PTDMA (Sanjay R Mehta)
- anolis: x86/pkey: Re-fix undefined behaviour with PKRU_WD_BIT (Guanjun)

* Wed Apr 20 2022 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.112-11_rc1.an8]
- configs: refresh the ANCK default configs (Shile Zhang)
- bump kernel to 5.10.1112 (Shile Zhang)
- cpuidle: intel_idle: Drop redundant backslash at line end (Rafael J. Wysocki)
- cpuidle: intel_idle: Update intel_idle() kerneldoc comment (Rafael J. Wysocki)
- intel_idle: add core C6 optimization for SPR (Artem Bityutskiy)
- intel_idle: add 'preferred_cstates' module argument (Artem Bityutskiy)
- intel_idle: add SPR support (Artem Bityutskiy)
- intel_idle: enable interrupts before C1 on Xeons (Artem Bityutskiy)
- intel_idle: Adjust the SKX C6 parameters if PC6 is disabled (Chen Yu)
- intel_idle: add Iclelake-D support (Artem Bityutskiy)
- intel_idle: update ICX C6 data (Artem Bityutskiy)
- intel_idle: remove definition of DEBUG (Tom Rix)
- intel_idle: add SnowRidge C-state table (Artem Bityutskiy)
- vfio/type1: Add vfio_group_iommu_domain() (Lu Baolu)
- anolis: mm: introduce ability to reserve page cache on system wide (zhongjiang-ali)
- arm64: Track no early_pgtable_alloc() for kmemleak (Qian Cai)
- EDAC/i10nm: Add detection of memory levels for ICX/SPR servers (Qiuxu Zhuo)
- EDAC/skx_common: Add new ADXL components for 2-level memory (Qiuxu Zhuo)
- mm/hwpoison: fix error page recovered but reported "not recovered" (Naoya Horiguchi)
- x86/mce: Reduce number of machine checks taken during recovery (Youquan Song)
- userfaultfd/selftests: fix calculation of expected ioctls (Axel Rasmussen)
- userfaultfd/selftests: fix feature support detection (Axel Rasmussen)
- userfaultfd/selftests: fixup supported features in userfaultfd_open (Xu Yu)
- mm/userfaultfd: fail uffd-wp registration if not supported (Peter Xu)
- memcg: do not tweak node in alloc_mem_cgroup_per_node_info (Wei Yang)
- mm: make free_area_init_node aware of memory less nodes (Michal Hocko)
- mm, memory_hotplug: reorganize new pgdat initialization (Michal Hocko)
- mm, memory_hotplug: drop arch_free_nodedata (Michal Hocko)
- mm: handle uninitialized numa nodes gracefully (Michal Hocko)
- mm, memory_hotplug: make arch_alloc_nodedata independent on CONFIG_MEMORY_HOTPLUG (Michal Hocko)
- anolis: mm, kidled: add some missed comments (Gang Deng)
- anolis: sched: Credit clarification for BVT and its related work (Cruz Zhao)
- anolis: mm/damon: Solve the bug of NUMA statistics about threads (Xin Hao)
- anolis: mm: fix suspicious RCU usage in mem_cgroup_account_oom_skip (Gu Mi)
- anolis: kfence: improve performance about kfence_ksize() (Tianchen Ding)
- anolis: Revert "mm: remove pfn_valid_within() and CONFIG_HOLES_IN_ZONE" (zhongjiang-ali)
- anolis: Revert "mm: memory_hotplug: cleanup after removal of pfn_valid_within()" (zhongjiang-ali)
- x86/fpu: Add guest support to xfd_enable_feature() (Thomas Gleixner)
- KVM: x86: Revert "KVM: x86: Mark GPRs dirty when written" (Sean Christopherson)
- KVM: SVM: Unconditionally sync GPRs to GHCB on VMRUN of SEV-ES guest (Sean Christopherson)
- KVM: x86: __kvm_vcpu_halt can be static (Paolo Bonzini)
- KVM: SVM: Add support for booting APs in an SEV-ES guest (Tom Lendacky)
- KVM: SVM: fix 32-bit compilation (Paolo Bonzini)
- KVM: SVM: Add AP_JUMP_TABLE support in prep for AP booting (Tom Lendacky)
- KVM: SVM: Provide support to launch and run an SEV-ES guest (Tom Lendacky)
- KVM: SVM: Provide an updated VMRUN invocation for SEV-ES guests (Tom Lendacky)
- KVM: SVM: Provide support for SEV-ES vCPU loading (Tom Lendacky)
- KVM: SVM: Provide support for SEV-ES vCPU creation/loading (Tom Lendacky)
- KVM: SVM: Update ASID allocation to support SEV-ES guests (Tom Lendacky)
- KVM: SVM: Set the encryption mask for the SVM host save area (Tom Lendacky)
- KVM: SVM: Add NMI support for an SEV-ES guest (Tom Lendacky)
- x86/kvm: Convert FPU handling to a single swap buffer (Thomas Gleixner)
- x86/fpu: Provide infrastructure for KVM FPU cleanup (Thomas Gleixner)
- x86/fpu: Prepare for sanitizing KVM FPU code (Thomas Gleixner)
- KVM: SVM: Do not report support for SMM for an SEV-ES guest (Tom Lendacky)
- KVM: x86: Update __get_sregs() / __set_sregs() to support SEV-ES (Tom Lendacky)
- KVM: SVM: Add support for CR8 write traps for an SEV-ES guest (Tom Lendacky)
- KVM: SVM: Add support for CR4 write traps for an SEV-ES guest (Tom Lendacky)
- KVM: SVM: Add support for CR0 write traps for an SEV-ES guest (Tom Lendacky)
- KVM: SVM: Add support for EFER write traps for an SEV-ES guest (Tom Lendacky)
- KVM: SVM: Support string IO operations for an SEV-ES guest (Tom Lendacky)
- KVM: SVM: Support MMIO for an SEV-ES guest (Tom Lendacky)
- KVM: SVM: Create trace events for VMGEXIT MSR protocol processing (Tom Lendacky)
- KVM: SVM: Create trace events for VMGEXIT processing (Tom Lendacky)
- KVM: SVM: Add support for SEV-ES GHCB MSR protocol function 0x100 (Tom Lendacky)
- KVM: SVM: Add support for SEV-ES GHCB MSR protocol function 0x004 (Tom Lendacky)
- KVM: SVM: Add support for SEV-ES GHCB MSR protocol function 0x002 (Tom Lendacky)
- KVM: SVM: Add initial support for a VMGEXIT VMEXIT (Tom Lendacky)
- KVM: SVM: Prepare for SEV-ES exit handling in the sev.c file (Tom Lendacky)
- KVM: SVM: Cannot re-initialize the VMCB after shutdown with SEV-ES (Tom Lendacky)
- KVM: SVM: Do not allow instruction emulation under SEV-ES (Tom Lendacky)
- KVM: SVM: Prevent debugging under SEV-ES (Tom Lendacky)
- KVM: SVM: Add required changes to support intercepts under SEV-ES (Tom Lendacky)
- KVM: x86: introduce complete_emulated_msr callback (Paolo Bonzini)
- KVM: x86: use kvm_complete_insn_gp in emulating RDMSR/WRMSR (Paolo Bonzini)
- KVM: x86: remove bogus #GP injection (Paolo Bonzini)
- KVM: x86: Mark GPRs dirty when written (Tom Lendacky)
- KVM: SVM: Add support for the SEV-ES VMSA (Tom Lendacky)
- KVM: SVM: Add GHCB accessor functions for retrieving fields (Tom Lendacky)
- KVM: SVM: Add support for SEV-ES capability in KVM (Tom Lendacky)
- x86/cpu: Add VM page flush MSR availablility as a CPUID feature (Tom Lendacky)
- anolis: mm/damon: Fix NUMA statistics bug (Xin Hao)
- anolis: mm: add ability to restrict swapout in global reclaim (Xu Yu)
- sock: remove one redundant SKB_FRAG_PAGE_ORDER macro (Yunsheng Lin)
- anolis: configs: Disable group identity by default (Cruz Zhao)
- anolis: Kconfig: Disable group identity by default (Cruz Zhao)
- anolis: arm64: drivers/perf: add HN-F PMU filter events support (Shuai Xue)
- anolis: configs: arm64: open the coresight defconfig (Jay Chen)
- anolis: pci: Add a quirk for ALIBABA yitian710 to recover fatal aer (Yao Hongbo)
- anolis: mm, memcg: use css_tryget_online() instead of css_get() (Gang Deng)
- anolis: mm: duptext: cond_resched when truncating duplicated pages (Xu Yu)
- writeback, cgroup: do not reparent dax inodes (Roman Gushchin)
- writeback, cgroup: remove wb from offline list before releasing refcnt (Roman Gushchin)
- writeback, cgroup: release dying cgwbs by switching attached inodes (Roman Gushchin)
- writeback, cgroup: support switching multiple inodes at once (Roman Gushchin)
- writeback, cgroup: split out the functional part of inode_switch_wbs_work_fn() (Roman Gushchin)
- writeback, cgroup: keep list of inodes attached to bdi_writeback (Roman Gushchin)
- writeback, cgroup: switch to rcu_work API in inode_switch_wbs() (Roman Gushchin)
- writeback, cgroup: add smp_mb() to cgroup_writeback_umount() (Roman Gushchin)
- writeback, cgroup: do not switch inodes with I_WILL_FREE flag (Roman Gushchin)
- anolis: mm: duptext: early bail out when mlock slave page (Xu Yu)
- f2fs: fix to do sanity check on inode type during garbage collection (Chao Yu)
- f2fs: remove false alarm on iget failure during GC (Jaegeuk Kim)
- anolis: kfence: fit is_kfence_address() to KASAN at any path (Tianchen Ding)
- sched/pelt: Relax the sync of load_sum with load_avg (Vincent Guittot)
- sched/pelt: Relax the sync of runnable_sum with runnable_avg (Vincent Guittot)
- sched/pelt: Continue to relax the sync of util_sum with util_avg (Vincent Guittot)
- selftests/x86/amx: Update the ARCH_REQ_XCOMP_PERM test (Yang Zhong)
- x86/arch_prctl: Fix the ARCH_REQ_XCOMP_PERM implementation (Yang Zhong)
- anolis: configs: enable config for mm/damon (Xin Hao)
- anolis: kfence: wake up allocation timer when disabling (Tianchen Ding)
- anolis: docs/kfence: update document for per-slab switch (Tianchen Ding)
- ABI: sysfs-kernel-slab: use a wildcard for the cache name (Mauro Carvalho Chehab)
- anolis: kfence: add order0 page switch (Tianchen Ding)
- anolis: kfence: add per-slab switch to sysfs (Tianchen Ding)
- anolis: x86/fpu: Export xstate_request_perm symbol (Hao Xiang)
- anolis: efi: cper: print error type string of raw data info (Shuai Xue)
- anolis: efi: cper: print raw data info of estatus for Yitian SoC (Shuai Xue)
- anolis: scsi: add a proc interface to cancel scsi eh reset (Zhenghua Jia)
- arm64: ipi_nmi: Add support for NMI backtrace (Sumit Garg)
- nmi: backtrace: Allow runtime arch specific override (Sumit Garg)
- arm64: smp: Assign and setup an IPI as NMI (Sumit Garg)
- irqchip/gic-v3: Enable support for SGIs to act as NMIs (Sumit Garg)
- arm64: Add framework to turn IPI as NMI (Sumit Garg)
- KVM: x86: Use ERR_PTR_USR() to return -EFAULT as a __user pointer (Sean Christopherson)
- KVM: x86: add system attribute to retrieve full set of supported xsave states (Paolo Bonzini)
- KVM: x86: Add a helper to retrieve userspace address from kvm_device_attr (Sean Christopherson)
- selftest: kvm: Add amx selftest (Yang Zhong)
- selftest: kvm: Move struct kvm_x86_state to header (Yang Zhong)
- selftest: kvm: Reorder vcpu_load_state steps for AMX (Paolo Bonzini)
- KVM: x86/cpuid: Exclude unpermitted xfeatures sizes at KVM_GET_SUPPORTED_CPUID (Like Xu)
- anolis: x86/fpu: Export some symbols to kvm (Hao Xiang)
- x86/fpu: Fix inline prefix warnings (Yang Zhong)
- kvm: x86: Disable interception for IA32_XFD on demand (Kevin Tian)
- x86/fpu: Provide fpu_sync_guest_vmexit_xfd_state() (Thomas Gleixner)
- kvm: x86: Add support for getting/setting expanded xstate buffer (Guang Zeng)
- x86/fpu: Add uabi_size to guest_fpu (Thomas Gleixner)
- kvm: x86: Add CPUID support for Intel AMX (Jing Liu)
- kvm: x86: Add XCR0 support for Intel AMX (Jing Liu)
- kvm: x86: Disable RDMSR interception of IA32_XFD_ERR (Jing Liu)
- kvm: x86: Emulate IA32_XFD_ERR for guest (Jing Liu)
- kvm: x86: Intercept #NM for saving IA32_XFD_ERR (Jing Liu)
- x86/fpu: Prepare xfd_err in struct fpu_guest (Jing Liu)
- kvm: x86: Add emulation for IA32_XFD (Jing Liu)
- x86/fpu: Provide fpu_update_guest_xfd() for IA32_XFD emulation (Kevin Tian)
- kvm: x86: Enable dynamic xfeatures at KVM_SET_CPUID2 (Jing Liu)
- x86/fpu: Provide fpu_enable_guest_xfd_features() for KVM (Sean Christopherson)
- x86/fpu: Add guest support to xfd_enable_feature() (Thomas Gleixner)
- x86/fpu: Make XFD initialization in __fpstate_reset() a function argument (Jing Liu)
- kvm: x86: Exclude unpermitted xfeatures at KVM_GET_SUPPORTED_CPUID (Jing Liu)
- kvm: x86: Fix xstate_required_size() to follow XSTATE alignment rule (Jing Liu)
- x86/fpu: Prepare guest FPU for dynamically enabled FPU features (Thomas Gleixner)
- x86/fpu: Extend fpu_xstate_prctl() with guest permissions (Thomas Gleixner)
- anolis: arm64:ipi: add crash nmi ipi support with PSEUDO_NMI (Jay Chen)
- anolis: mm: kidled: show the kidled age layout in page flags (zhongjiang-ali)
- anolis: kfence: fix bug when switching sampling mode (Tianchen Ding)
- anolis: configs: add config for memfd_secret syscall (Yan Yan)
- mm/secretmem: avoid letting secretmem_users drop to zero (Kees Cook)
- secretmem: Prevent secretmem_users from wrapping to zero (Matthew Wilcox (Oracle))
- mm/secretmem: fix NULL page->mapping dereference in page_is_secretmem() (Sean Christopherson)
- mm/secretmem: wire up ->set_page_dirty (Mike Rapoport)
- tools headers UAPI: Sync files changed by the memfd_secret new syscall (Arnaldo Carvalho de Melo)
- secretmem: test: add basic selftest for memfd_secret(2) (Mike Rapoport)
- arch, mm: wire up memfd_secret system call where relevant (Mike Rapoport)
- PM: hibernate: disable when there are active secretmem users (Mike Rapoport)
- mm: introduce memfd_secret system call to create "secret" memory areas (Mike Rapoport)
- set_memory: allow querying whether set_direct_map_*() is actually enabled (Mike Rapoport)
- riscv/Kconfig: make direct map manipulation options depend on MMU (Mike Rapoport)
- mmap: make mlock_future_check() global (Mike Rapoport)
- anolis: agdi: open the agdi config for bmc nmi (Jay Chen)
- ACPI: AGDI: Add support for Arm Generic Diagnostic Dump and Reset device (Ilkka Koskinen)
- ACPI: tables: Add AGDI to the list of known table signatures (Ilkka Koskinen)
- ACPICA: iASL: Add suppport for AGDI table (Ilkka Koskinen)
- x86/feat_ctl: Add new VMX feature, Tertiary VM-Execution control (Robert Hoo)
- io_uring: don't hold uring_lock when calling io_run_task_work* (Hao Xu)
- ACPI: APEI: rename ghes_init with an "acpi_" prefix (Shuai Xue)
- ACPI: APEI: explicit init HEST and GHES in apci_init (Shuai Xue)
- anolis: configs: arm64: enable CONFIG_THEAD_M1_PMU as module (Shuai Xue)
- anolis: arm64: drivers/perf: support PCIe PMU counter multiplexing (Shuai Xue)
- anolis: arm64: drivers/perf: enable 64 bit time based analysis counter (Shuai Xue)
- anolis: arm64: drivers/perf: add PCIe PMU driver for Yitian SoC (Shuai Xue)
- anolis: arm64: drivers/perf: change DDRSS PMU irq_flag to IRQF_SHARED (Shuai Xue)
- anolis: arm64: drivers/perf: add cycle event to calculate bandwidth (Shuai Xue)
- anolis: arm64: driver/perf: disable DDRSS interrupt before free irq (Shuai Xue)
- anolis: arm64: drivers/perf: add DDR Sub-System PMU driver for Yitian SoC (Shuai Xue)
- x86/fpu/signal: Initialize sw_bytes in save_xstate_epilog() (Marco Elver)
- x86/fpu: Optimize out sigframe xfeatures when in init state (Dave Hansen)
- Documentation/x86: Add documentation for using dynamic XSTATE features (Chang S. Bae)
- selftests/x86/amx: Add context switch test (Chang S. Bae)
- selftests/x86/amx: Add test cases for AMX state management (Chang S. Bae)
- x86/fpu/amx: Enable the AMX feature in 64-bit mode (Chang S. Bae)
- x86/fpu: Add XFD handling for dynamic states (Chang S. Bae)
- x86/fpu: Calculate the default sizes independently (Chang S. Bae)
- x86/fpu/amx: Define AMX state components and have it used for boot-time checks (Chang S. Bae)
- x86/fpu/xstate: Prepare XSAVE feature table for gaps in state component numbers (Chang S. Bae)
- x86/fpu/xstate: Add fpstate_realloc()/free() (Chang S. Bae)
- x86/fpu/xstate: Add XFD #NM handler (Chang S. Bae)
- x86/fpu: Update XFD state where required (Chang S. Bae)
- x86/fpu: Add sanity checks for XFD (Thomas Gleixner)
- x86/fpu: Add XFD state to fpstate (Chang S. Bae)
- x86/msr-index: Add MSRs for XFD (Chang S. Bae)
- x86/cpufeatures: Add eXtended Feature Disabling (XFD) feature bit (Chang S. Bae)
- x86/fpu: Reset permission and fpstate on exec() (Chang S. Bae)
- x86/fpu: Prepare fpu_clone() for dynamically enabled features (Thomas Gleixner)
- x86/fpu/signal: Prepare for variable sigframe length (Chang S. Bae)
- x86/signal: Use fpu::__state_user_size for sigalt stack validation (Thomas Gleixner)
- x86/fpu: Add basic helpers for dynamically enabled features (Thomas Gleixner)
- x86/arch_prctl: Add controls for dynamic XSTATE components (Chang S. Bae)
- x86/fpu: Add fpu_state_config::legacy_features (Thomas Gleixner)
- x86/fpu: Add members to struct fpu to cache permission information (Thomas Gleixner)
- x86/fpu/xstate: Provide xstate_calculate_size() (Chang S. Bae)
- x86/signal: Implement sigaltstack size validation (Thomas Gleixner)
- signal: Add an optional check for altstack size (Thomas Gleixner)
- x86/fpu: Remove old KVM FPU interface (Thomas Gleixner)
- x86/kvm: Convert FPU handling to a single swap buffer (Thomas Gleixner)
- x86/fpu: Provide infrastructure for KVM FPU cleanup (Thomas Gleixner)
- x86/fpu: Prepare for sanitizing KVM FPU code (Thomas Gleixner)
- x86/fpu/xstate: Move remaining xfeature helpers to core (Thomas Gleixner)
- x86/fpu: Rework restore_regs_from_fpstate() (Thomas Gleixner)
- x86/fpu: Mop up xfeatures_mask_uabi() (Thomas Gleixner)
- x86/fpu: Move xstate feature masks to fpu_*_cfg (Thomas Gleixner)
- x86/fpu: Move xstate size to fpu_*_cfg (Thomas Gleixner)
- x86/fpu/xstate: Cleanup size calculations (Thomas Gleixner)
- x86/fpu: Cleanup fpu__init_system_xstate_size_legacy() (Thomas Gleixner)
- x86/fpu: Provide struct fpu_config (Thomas Gleixner)
- x86/fpu/signal: Use fpstate for size and features (Thomas Gleixner)
- x86/fpu/xstate: Use fpstate for copy_uabi_to_xstate() (Thomas Gleixner)
- x86/fpu: Use fpstate in __copy_xstate_to_uabi_buf() (Thomas Gleixner)
- x86/fpu: Use fpstate in fpu_copy_kvm_uabi_to_fpstate() (Thomas Gleixner)
- x86/fpu/xstate: Use fpstate for xsave_to_user_sigframe() (Thomas Gleixner)
- x86/fpu/xstate: Use fpstate for os_xsave() (Thomas Gleixner)
- x86/fpu: Use fpstate::size (Thomas Gleixner)
- x86/fpu: Add size and mask information to fpstate (Thomas Gleixner)
- x86/process: Move arch_thread_struct_whitelist() out of line (Thomas Gleixner)
- x86/fpu: Do not leak fpstate pointer on fork (Thomas Gleixner)
- x86/fpu: Remove fpu::state (Thomas Gleixner)
- x86/math-emu: Convert to fpstate (Thomas Gleixner)
- x86/fpu/core: Convert to fpstate (Thomas Gleixner)
- x86/fpu/signal: Convert to fpstate (Thomas Gleixner)
- x86/fpu/regset: Convert to fpstate (Thomas Gleixner)
- x86/fpu: Convert tracing to fpstate (Thomas Gleixner)
- x86/KVM: Convert to fpstate (Thomas Gleixner)
- x86/fpu: Replace KVMs xstate component clearing (Thomas Gleixner)
- x86/fpu: Convert restore_fpregs_from_fpstate() to struct fpstate (Thomas Gleixner)
- x86/fpu: Convert fpstate_init() to struct fpstate (Thomas Gleixner)
- x86/fpu: Provide struct fpstate (Thomas Gleixner)
- x86/fpu: Replace KVMs home brewed FPU copy to user (Thomas Gleixner)
- x86/fpu: Provide a proper function for ex_handler_fprestore() (Thomas Gleixner)
- x86/fpu: Replace the includes of fpu/internal.h (Thomas Gleixner)
- x86/fpu: Mop up the internal.h leftovers (Thomas Gleixner)
- x86/sev: Include fpu/xcr.h (Thomas Gleixner)
- x86/fpu: Remove internal.h dependency from fpu/signal.h (Thomas Gleixner)
- x86/fpu: Move fpstate functions to api.h (Thomas Gleixner)
- x86/fpu: Move mxcsr related code to core (Thomas Gleixner)
- x86/fpu: Move fpregs_restore_userregs() to core (Thomas Gleixner)
- x86/fpu: Make WARN_ON_FPU() private (Thomas Gleixner)
- x86/fpu: Move legacy ASM wrappers to core (Thomas Gleixner)
- x86/fpu: Move os_xsave() and os_xrstor() to core (Thomas Gleixner)
- x86/fpu: Make os_xrstor_booting() private (Thomas Gleixner)
- x86/fpu: Clean up CPU feature tests (Thomas Gleixner)
- x86/fpu: Move context switch and exit to user inlines into sched.h (Thomas Gleixner)
- x86/fpu: Mark fpu__init_prepare_fx_sw_frame() as __init (Thomas Gleixner)
- x86/fpu: Rework copy_xstate_to_uabi_buf() (Thomas Gleixner)
- x86/fpu: Replace KVMs home brewed FPU copy from user (Thomas Gleixner)
- x86/fpu: Move KVMs FPU swapping to FPU core (Thomas Gleixner)
- x86/fpu/xstate: Mark all init only functions __init (Thomas Gleixner)
- x86/fpu/xstate: Provide and use for_each_xfeature() (Thomas Gleixner)
- x86/fpu: Cleanup xstate xcomp_bv initialization (Thomas Gleixner)
- x86/fpu: Do not inherit FPU context for kernel and IO worker threads (Thomas Gleixner)
- x86/process: Clone FPU in copy_thread() (Thomas Gleixner)
- x86/fpu: Remove pointless memset in fpu_clone() (Thomas Gleixner)
- x86/fpu: Cleanup the on_boot_cpu clutter (Thomas Gleixner)
- x86/fpu: Restrict xsaves()/xrstors() to independent states (Thomas Gleixner)
- x86/pkru: Remove useless include (Thomas Gleixner)
- x86/fpu: Update stale comments (Thomas Gleixner)
- x86/fpu: Remove pointless argument from switch_fpu_finish() (Thomas Gleixner)
- anolis: arm64: drivers/perf: fix event_sel width of XP node (Shuai Xue)
- anolis: arm64: drivers/perf: overwrite the ccla_rni node with ccla (Shuai Xue)
- anolis: arm64: drivers/perf: overwrite the devid of ccla_rni node (Shuai Xue)
- anolis: arm64: drivers/perf: remove the haps workaround (Shuai Xue)
- anolis: arm64: drivers/perf: add CMN-700 PMU driver for Yitian SoC (Shuai Xue)
- x86/fpu/signal: Fix missed conversion to correct boolean retval in save_xstate_epilog() (Anders Roxell)
- x86/fpu/signal: Change return code of restore_fpregs_from_user() to boolean (Thomas Gleixner)
- x86/fpu/signal: Change return code of check_xstate_in_sigframe() to boolean (Thomas Gleixner)
- x86/fpu/signal: Change return type of __fpu_restore_sig() to boolean (Thomas Gleixner)
- x86/fpu/signal: Change return type of fpu__restore_sig() to boolean (Thomas Gleixner)
- x86/signal: Change return type of restore_sigcontext() to boolean (Thomas Gleixner)
- x86/fpu/signal: Change return type of copy_fpregs_to_sigframe() helpers to boolean (Thomas Gleixner)
- x86/fpu/signal: Change return type of copy_fpstate_to_sigframe() to boolean (Thomas Gleixner)
- x86/fpu/signal: Move xstate clearing out of copy_fpregs_to_sigframe() (Thomas Gleixner)
- x86/fpu/signal: Move header zeroing out of xsave_to_user_sigframe() (Thomas Gleixner)
- x86/fpu/signal: Clarify exception handling in restore_fpregs_from_user() (Thomas Gleixner)
- x86/extable: Remove EX_TYPE_FAULT from MCE safe fixups (Thomas Gleixner)
- x86/fpu: Use EX_TYPE_FAULT_MCE_SAFE for exception fixups (Thomas Gleixner)
- x86/copy_mc: Use EX_TYPE_DEFAULT_MCE_SAFE for exception fixups (Thomas Gleixner)
- x86/extable: Provide EX_TYPE_DEFAULT_MCE_SAFE and EX_TYPE_FAULT_MCE_SAFE (Thomas Gleixner)
- x86/extable: Rework the exception table mechanics (Thomas Gleixner)
- x86/mce: Get rid of stray semicolons (Thomas Gleixner)
- x86/mce: Deduplicate exception handling (Thomas Gleixner)
- x86/extable: Get rid of redundant macros (Thomas Gleixner)
- x86/extable: Tidy up redundant handler functions (Thomas Gleixner)
- x86/fpu: Mask out the invalid MXCSR bits properly (Borislav Petkov)
- x86/fpu: Restore the masking out of reserved MXCSR bits (Borislav Petkov)
- x86/fpu/xstate: Clear xstate header in copy_xstate_to_uabi_buf() again (Thomas Gleixner)
- x86/fpu/signal: Let xrstor handle the features to init (Thomas Gleixner)
- x86/fpu/signal: Handle #PF in the direct restore path (Thomas Gleixner)
- anolis: Revert "ck: Revert "x86/fpu: Return proper error codes from user access functions"" (Lin Wang)
- x86/fpu/signal: Split out the direct restore code (Thomas Gleixner)
- x86/fpu/signal: Sanitize copy_user_to_fpregs_zeroing() (Thomas Gleixner)
- x86/fpu/signal: Sanitize the xstate check on sigframe (Thomas Gleixner)
- x86/fpu/signal: Remove the legacy alignment check (Thomas Gleixner)
- x86/fpu/signal: Move initial checks into fpu__restore_sig() (Thomas Gleixner)
- x86/fpu: Mark init_fpstate __ro_after_init (Thomas Gleixner)
- x86/pkru: Remove xstate fiddling from write_pkru() (Thomas Gleixner)
- x86/fpu: Don't store PKRU in xstate in fpu_reset_fpstate() (Thomas Gleixner)
- x86/fpu: Remove PKRU handling from switch_fpu_finish() (Thomas Gleixner)
- x86/fpu: Mask PKRU from kernel XRSTOR[S] operations (Thomas Gleixner)
- x86/fpu: Hook up PKRU into ptrace() (Dave Hansen)
- x86/fpu: Add PKRU storage outside of task XSAVE buffer (Dave Hansen)
- x86/fpu: Dont restore PKRU in fpregs_restore_userspace() (Aubrey Li)
- x86/fpu: Rename xfeatures_mask_user() to xfeatures_mask_uabi() (Thomas Gleixner)
- x86/fpu: Move FXSAVE_LEAK quirk info __copy_kernel_to_fpregs() (Thomas Gleixner)
- x86/fpu: Rename __fpregs_load_activate() to fpregs_restore_userregs() (Thomas Gleixner)
- x86/fpu: Clean up the fpu__clear() variants (Andy Lutomirski)
- x86/fpu: Rename fpu__clear_all() to fpu_flush_thread() (Thomas Gleixner)
- x86/fpu: Use pkru_write_default() in copy_init_fpstate_to_fpregs() (Thomas Gleixner)
- x86/cpu: Write the default PKRU value when enabling PKE (Thomas Gleixner)
- x86/pkru: Provide pkru_write_default() (Thomas Gleixner)
- x86/pkru: Provide pkru_get_init_value() (Thomas Gleixner)
- x86/cpu: Sanitize X86_FEATURE_OSPKE (Thomas Gleixner)
- x86/fpu: Rename and sanitize fpu__save/copy() (Thomas Gleixner)
- x86/pkeys: Move read_pkru() and write_pkru() (Dave Hansen)
- x86/fpu/xstate: Sanitize handling of independent features (Thomas Gleixner)
- x86/fpu: Rename "dynamic" XSTATEs to "independent" (Andy Lutomirski)
- x86/fpu: Rename initstate copy functions (Thomas Gleixner)
- x86/fpu: Rename copy_kernel_to_fpregs() to restore_fpregs_from_fpstate() (Thomas Gleixner)
- x86/fpu: Get rid of the FNSAVE optimization (Thomas Gleixner)
- x86/fpu: Rename copy_fpregs_to_fpstate() to save_fpregs_to_fpstate() (Thomas Gleixner)
- x86/fpu: Deduplicate copy_uabi_from_user/kernel_to_xstate() (Thomas Gleixner)
- x86/fpu: Rename xstate copy functions which are related to UABI (Thomas Gleixner)
- x86/fpu: Rename fregs-related copy functions (Thomas Gleixner)
- x86/math-emu: Rename frstor() (Thomas Gleixner)
- x86/fpu: Rename fxregs-related copy functions (Thomas Gleixner)
- x86/fpu: Rename copy_user_to_xregs() and copy_xregs_to_user() (Thomas Gleixner)
- x86/fpu: Rename copy_xregs_to_kernel() and copy_kernel_to_xregs() (Thomas Gleixner)
- x86/fpu: Get rid of copy_supervisor_to_kernel() (Thomas Gleixner)
- x86/fpu: Cleanup arch_set_user_pkey_access() (Thomas Gleixner)
- x86/kvm: Avoid looking up PKRU in XSAVE buffer (Dave Hansen)
- x86/fpu: Get rid of using_compacted_format() (Thomas Gleixner)
- x86/fpu: Move fpu__write_begin() to regset (Thomas Gleixner)
- x86/fpu/regset: Move fpu__read_begin() into regset (Thomas Gleixner)
- x86/fpu: Remove fpstate_sanitize_xstate() (Thomas Gleixner)
- x86/fpu: Use copy_xstate_to_uabi_buf() in fpregs_get() (Thomas Gleixner)
- x86/fpu: Use copy_xstate_to_uabi_buf() in xfpregs_get() (Thomas Gleixner)
- x86/fpu: Make copy_xstate_to_kernel() usable for [x]fpregs_get() (Thomas Gleixner)
- x86/fpu: Clean up fpregs_set() (Andy Lutomirski)
- x86/fpu: Fail ptrace() requests that try to set invalid MXCSR values (Andy Lutomirski)
- x86/fpu: Rewrite xfpregs_set() (Andy Lutomirski)
- x86/fpu: Simplify PTRACE_GETREGS code (Dave Hansen)
- x86/fpu: Reject invalid MXCSR values in copy_kernel_to_xstate() (Thomas Gleixner)
- x86/fpu: Sanitize xstateregs_set() (Thomas Gleixner)
- anolis: Revert "ck: Revert "x86/fpu: Limit xstate copy size in xstateregs_set()"" (Lin Wang)
- x86/fpu: Move inlines where they belong (Thomas Gleixner)
- x86/fpu: Remove unused get_xsave_field_ptr() (Thomas Gleixner)
- x86/fpu: Get rid of fpu__get_supported_xfeatures_mask() (Thomas Gleixner)
- x86/fpu: Make xfeatures_mask_all __ro_after_init (Thomas Gleixner)
- x86/fpu: Mark various FPU state variables __ro_after_init (Thomas Gleixner)
- x86/pkeys: Revert a5eff7259790 ("x86/pkeys: Add PKRU value to init_fpstate") (Thomas Gleixner)
- anolis: Revert "ck: Revert "x86/fpu: Fix copy_xstate_to_kernel() gap handling"" (Lin Wang)
- selftests/x86: Test signal frame XSTATE header corruption handling (Andy Lutomirski)
- x86/fpu: Add address range checks to copy_user_to_xstate() (Andy Lutomirski)
- anolis: Revert "Interim v4: x86: Support Intel Advanced Matrix Extensions" (Aubrey Li)
- ftrace: Have architectures opt-in for mcount build time sorting (Yinan Liu)
- script/sorttable: Fix some initialization problems (Yinan Liu)
- scripts: ftrace - move the sort-processing in ftrace_init (Yinan Liu)
- kfence: fix memory leak when cat kfence objects (Baokun Li)
- arm64: mm: don't use CON and BLK mapping if KFENCE is enabled (Jisheng Zhang)
- anolis: docs/kfence: update document for KFENCE (Tianchen Ding)
- anolis: kfence: introduce pool_mode and auto distribute pools to nodes (Tianchen Ding)
- anolis: kfence: support recover TLB to pud huge in x86_64 (Tianchen Ding)
- anolis: kfence, x86: provide API to recover TLB to pud huge (Tianchen Ding)
- anolis: kfence: automatically free unused areas after disabled (Tianchen Ding)
- anolis: kfence: support re-enabling KFENCE (Tianchen Ding)
- anolis: kfence: support dynamically enabling after system startup (Tianchen Ding)
- anolis: kfence: split memblock alloc area into 1GB when system booting (Tianchen Ding)
- anolis: kfence: show more info in stats (Tianchen Ding)
- anolis: kfence: introduce pool per area instead of pool per node (Tianchen Ding)
- anolis: kfence: perform code refactor to prepare for enabling dynamically (Tianchen Ding)
- anolis: kfence: add PG_kfence to recognize kfence address in fast path (Tianchen Ding)
- virtio-mem: fix sleeping in RCU read side section in virtio_mem_online_page_cb() (David Hildenbrand)
- mm/page_alloc: count CMA pages per zone and print them in /proc/zoneinfo (David Hildenbrand)
- mm/memory_hotplug: improved dynamic memory group aware "auto-movable" online policy (David Hildenbrand)
- mm/memory_hotplug: memory group aware "auto-movable" online policy (David Hildenbrand)
- virtio-mem: use a single dynamic memory group for a single virtio-mem device (David Hildenbrand)
- mm/memory_hotplug: MEMHP_MERGE_RESOURCE -> MHP_MERGE_RESOURCE (David Hildenbrand)
- dax/kmem: use a single static memory group for a single probed unit (David Hildenbrand)
- ACPI: memhotplug: use a single static memory group for a single memory device (David Hildenbrand)
- mm/memory_hotplug: track present pages in memory groups (David Hildenbrand)
- drivers/base/memory: introduce "memory groups" to logically group memory blocks (David Hildenbrand)
- mm/memory_hotplug: introduce "auto-movable" online policy (David Hildenbrand)
- mm: track present early pages per zone (David Hildenbrand)
- ACPI: memhotplug: memory resources cannot be enabled yet (David Hildenbrand)
- mm/memory_hotplug: remove nid parameter from remove_memory() and friends (David Hildenbrand)
- mm/memory_hotplug: remove nid parameter from arch_remove_memory() (David Hildenbrand)
- mm: memory_hotplug: cleanup after removal of pfn_valid_within() (Mike Rapoport)
- mm: remove pfn_valid_within() and CONFIG_HOLES_IN_ZONE (Mike Rapoport)
- memory-hotplug.rst: complete admin-guide overhaul (David Hildenbrand)
- memory-hotplug.rst: remove locking details from admin-guide (David Hildenbrand)
- arm64/Kconfig: introduce ARCH_MHP_MEMMAP_ON_MEMORY_ENABLE (Oscar Salvador)
- x86/Kconfig: introduce ARCH_MHP_MEMMAP_ON_MEMORY_ENABLE (Oscar Salvador)
- mm,memory_hotplug: add kernel boot option to enable memmap_on_memory (Oscar Salvador)
- acpi,memhotplug: enable MHP_MEMMAP_ON_MEMORY when supported (Oscar Salvador)
- mm,memory_hotplug: allocate memmap from the added memory range (Oscar Salvador)
- mm,memory_hotplug: factor out adjusting present pages into adjust_present_page_count() (David Hildenbrand)
- mm,memory_hotplug: relax fully spanned sections check (Oscar Salvador)
- drivers/base/memory: introduce memory_block_{online,offline} (Oscar Salvador)
- virtio-mem: prioritize unplug from ZONE_MOVABLE in Big Block Mode (David Hildenbrand)
- virtio-mem: simplify high-level unplug handling in Big Block Mode (David Hildenbrand)
- virtio-mem: prioritize unplug from ZONE_MOVABLE in Sub Block Mode (David Hildenbrand)
- virtio-mem: simplify high-level unplug handling in Sub Block Mode (David Hildenbrand)
- virtio-mem: simplify high-level plug handling in Sub Block Mode (David Hildenbrand)
- virtio-mem: use page_zonenum() in virtio_mem_fake_offline() (David Hildenbrand)
- virtio-mem: don't read big block size in Sub Block Mode (David Hildenbrand)
- fs/proc/kcore: use page_offline_(freeze|thaw) (David Hildenbrand)
- virtio-mem: use page_offline_(start|end) when setting PageOffline() (David Hildenbrand)
- mm: introduce page_offline_(begin|end|freeze|thaw) to synchronize setting PageOffline() (David Hildenbrand)
- fs/proc/kcore: don't read offline sections, logically offline pages and hwpoisoned pages (David Hildenbrand)
- fs/proc/kcore: pfn_is_ram check only applies to KCORE_RAM (David Hildenbrand)
- fs/proc/kcore: drop KCORE_REMAP and KCORE_OTHER (David Hildenbrand)
- mm,page_alloc: drop unnecessary checks from pfn_range_valid_contig (Oscar Salvador)
- mm: make alloc_contig_range handle in-use hugetlb pages (Oscar Salvador)
- mm: make alloc_contig_range handle free hugetlb pages (Oscar Salvador)
- mm,hugetlb: split prep_new_huge_page functionality (Oscar Salvador)
- mm,compaction: let isolate_migratepages_{range,block} return error codes (Oscar Salvador)
- mm,page_alloc: bail out earlier on -ENOMEM in alloc_contig_migrate_range (Oscar Salvador)
- hugetlb: create remove_hugetlb_page() to separate functionality (Mike Kravetz)
- virtio-mem: Big Block Mode (BBM) - safe memory hotunplug (David Hildenbrand)
- virtio-mem: Big Block Mode (BBM) - basic memory hotunplug (David Hildenbrand)
- mm/memory_hotplug: extend offline_and_remove_memory() to handle more than one memory block (David Hildenbrand)
- virtio-mem: allow to force Big Block Mode (BBM) and set the big block size (David Hildenbrand)
- virtio-mem: Big Block Mode (BBM) memory hotplug (David Hildenbrand)
- virtio-mem: factor out adding/removing memory from Linux (David Hildenbrand)
- virtio-mem: memory notifier callbacks are specific to Sub Block Mode (SBM) (David Hildenbrand)
- virito-mem: existing (un)plug functions are specific to Sub Block Mode (SBM) (David Hildenbrand)
- virtio-mem: memory block ids are specific to Sub Block Mode (SBM) (David Hildenbrand)
- virtio-mem: nb_sb_per_mb and subblock_size are specific to Sub Block Mode (SBM) (David Hildenbrand)
- virito-mem: subblock states are specific to Sub Block Mode (SBM) (David Hildenbrand)
- virtio-mem: memory block states are specific to Sub Block Mode (SBM) (David Hildenbrand)
- virito-mem: document Sub Block Mode (SBM) (David Hildenbrand)
- virtio-mem: generalize handling when memory is getting onlined deferred (David Hildenbrand)
- virtio-mem: don't always trigger the workqueue when offlining memory (David Hildenbrand)
- virtio-mem: drop last_mb_id (David Hildenbrand)
- virtio-mem: generalize virtio_mem_overlaps_range() (David Hildenbrand)
- virtio-mem: generalize virtio_mem_owned_mb() (David Hildenbrand)
- virtio-mem: generalize check for added memory (David Hildenbrand)
- virtio-mem: retry fake-offlining via alloc_contig_range() on ZONE_MOVABLE (David Hildenbrand)
- virtio-mem: factor out handling of fake-offline pages in memory notifier (David Hildenbrand)
- virtio-mem: factor out fake-offlining into virtio_mem_fake_offline() (David Hildenbrand)
- virtio-mem: print debug messages from virtio_mem_send_*_request() (David Hildenbrand)
- virtio-mem: factor out calculation of the bit number within the subblock bitmap (David Hildenbrand)
- virtio-mem: use "unsigned long" for nr_pages when fake onlining/offlining (David Hildenbrand)
- virtio-mem: drop rc2 in virtio_mem_mb_plug_and_add() (David Hildenbrand)
- virtio-mem: simplify MAX_ORDER - 1 / pageblock_order handling (David Hildenbrand)
- virtio-mem: more precise calculation in virtio_mem_mb_state_prepare_next_mb() (David Hildenbrand)
- virtio-mem: determine nid only once using memory_add_physaddr_to_nid() (David Hildenbrand)
- anolis: PCI: Waiting command completed in get_port_device_capability() (Yao Hongbo)
- UBUNTU: SAUCE: bpf: prevent helper argument PTR_TO_ALLOC_MEM to have offset other than 0 (Thadeu Lima de Souza Cascardo)
- anolis: ACPI / APEI: restore interrupt before panic in sdei flow (Liguang Zhang)
- anolis: coresight: change tmc from misc device to cdev device (Jay Chen)
- anolis: coresight etm4x add the new pid for yitian (Jay Chen)
- coresight: etm4x: Add ETM PID for Cortex-A78 (Sai Prakash Ranjan)
- coresight: core: Add support for dedicated percpu sinks (Anshuman Khandual)
- coresight: etm-perf: Handle stale output handles (Suzuki K Poulose)
- coresight: ete: Add support for ETE tracing (Suzuki K Poulose)
- coresight: ete: Add support for ETE sysreg access (Suzuki K Poulose)
- coresight: etm4x: Add support for PE OS lock (Suzuki K Poulose)
- coresight: etm-perf: Allow an event to use different sinks (Suzuki K Poulose)
- coresight: etm4x: Move ETM to prohibited region for disable (Suzuki K Poulose)
- arm64: Add TRBE definitions (Anshuman Khandual)
- arm64: Add support for trace synchronization barrier (Suzuki K Poulose)
- perf: aux: Add CoreSight PMU buffer formats (Suzuki K Poulose)
- perf: aux: Add flags for the buffer format (Suzuki K Poulose)
- coresight: etm-perf: Fix define build issue when built as module (Mike Leach)
- coresight: etm-perf: Support PID tracing for kernel at EL2 (Suzuki K Poulose)
- coresight: etm-perf: Clarify comment on perf options (Leo Yan)
- coresight: etm4x: Handle accesses to TRCSTALLCTLR (Suzuki K Poulose)
- coresight: Add support for v8.4 SelfHosted tracing (Jonathan Zhou)
- coresight: etm4x: Add support for sysreg only devices (Suzuki K Poulose)
- coresight: etm4x: Run arch feature detection on the CPU (Suzuki K Poulose)
- coresight: etm4x: Refactor probing routine (Suzuki K Poulose)
- coresight: etm4x: Detect system instructions support (Suzuki K Poulose)
- coresight: etm4x: Add necessary synchronization for sysreg access (Suzuki K Poulose)
- coresight: etm4x: Expose trcdevarch via sysfs (Suzuki K Poulose)
- coresight: etm4x: Use TRCDEVARCH for component discovery (Suzuki K Poulose)
- coresight: etm4x: Detect access early on the target CPU (Suzuki K Poulose)
- coresight: etm4x: Handle ETM architecture version (Suzuki K Poulose)
- coresight: etm4x: Clean up exception level masks (Suzuki K Poulose)
- coresight: etm4x: Cleanup secure exception level masks (Suzuki K Poulose)
- coresight: etm4x: Check for Software Lock (Suzuki K Poulose)
- coresight: etm4x: Define DEVARCH register fields (Suzuki K Poulose)
- coresight: etm4x: Hide sysfs attributes for unavailable registers (Suzuki K Poulose)
- coresight: etm4x: Add sysreg access helpers (Suzuki K Poulose)
- coresight: etm4x: Add commentary on the registers (Suzuki K Poulose)
- coresight: etm4x: Make offset available for sysfs attributes (Suzuki K Poulose)
- coresight: etm4x: Convert all register accesses (Suzuki K Poulose)
- anolis: revert the LTS etm4x patch changed by maintianer (Jay Chen)
- coresight: etm4x: Always read the registers on the host CPU (Suzuki K Poulose)
- coresight: Convert claim/disclaim operations to use access wrappers (Suzuki K Poulose)
- coresight: cti: Reduce scope for the variable 'cs_fwnode' in cti_plat_create_connection() (Markus Elfring)
- coresight: Convert coresight_timeout to use access abstraction (Suzuki K Poulose)
- coresight: tpiu: Prepare for using coresight device access abstraction (Suzuki K Poulose)
- coresight: Introduce device access abstraction (Suzuki K Poulose)
- coresight: etm4x: Handle access to TRCSSPCICRn (Suzuki K Poulose)
- coresight: etm4x: add AMBA id for Cortex-A55 and Cortex-A75 (Chunyan Zhang)
- coresight: etm4x: Modify core-commit to avoid HiSilicon ETM overflow (Qi Liu)
- coresight: etm4x: Update TRCIDR3.NUMPROCS handling to match v4.2 (Suzuki K Poulose)
- arm64: Add TRFCR_ELx definitions (Jonathan Zhou)
- anolis: perf: smmuv3_pmu: enable the global filter for yitian (Jay Chen)
- anolis: arm64:mpam: Move KunPeng mpam relative codes to staging dir (Xin Hao)
- anolis: config:arm64: Enable STAGING default (Xin Hao)
- anolis: configs: x86: add a config for User Interrupts feature (Hao Xiang)
- x86/fpu/xstate: Enumerate User Interrupts supervisor state (Sohil Mehta)
- x86/cpu: Enumerate User Interrupts support (Sohil Mehta)

* Tue Apr 12 2022 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.84-10.4.an8]
- fuse: fix pipe buffer lifetime for direct_io (Miklos Szeredi) {CVE-2022-1011}
- misc: fastrpc: avoid double fput() on failed usercopy (Mathias Krause)
- netfilter: nf_tables: initialize registers in nft_do_chain() (Pablo Neira Ayuso) {CVE-2022-1016}
- watch_queue: Make comment about setting ->defunct more accurate (David Howells) {CVE-2022-0995}
- watch_queue: Fix lack of barrier/sync/lock between post and read (David Howells) {CVE-2022-0995}
- watch_queue: Free the alloc bitmap when the watch_queue is torn down (David Howells) {CVE-2022-0995}
- watch_queue: Fix the alloc bitmap size to reflect notes allocated (David Howells) {CVE-2022-0995}
- watch_queue: Fix to always request a pow-of-2 pipe ring size (David Howells) {CVE-2022-0995}
- watch_queue: Fix to release page in ->release() (David Howells) {CVE-2022-0995}
- watch_queue, pipe: Free watchqueue state after clearing pipe ring (David Howells) {CVE-2022-0995}
- watch_queue: Fix filter limit check (David Howells) {CVE-2022-0995}
- net/packet: rx_owner_map depends on pg_vec (Willem de Bruijn) {CVE-2021-22600}
- esp: Fix possible buffer overflow in ESP transformation (Steffen Klassert) {CVE-2022-27666}
- sock: remove one redundant SKB_FRAG_PAGE_ORDER macro (Yunsheng Lin) {CVE-2022-27666}
- netfilter: nf_tables_offload: incorrect flow offload action array size (Pablo Neira Ayuso) {CVE-2022-25636}
- ipv4: avoid using shared IP generator for connected sockets (Eric Dumazet) {CVE-2020-36516}
- ipv4: tcp: send zero IPID in SYNACK messages (Eric Dumazet) {CVE-2020-36516}
- f2fs: fix to do sanity check on inode type during garbage collection (Chao Yu) {CVE-2021-44879}
- f2fs: remove false alarm on iget failure during GC (Jaegeuk Kim) {CVE-2021-44879}
- bpf, selftests: Add test case trying to taint map value pointer (Daniel Borkmann) {CVE-2021-45402}
- bpf: Make 32->64 bounds propagation slightly more robust (Daniel Borkmann) {CVE-2021-45402}
- bpf: Fix signed bounds propagation after mov32 (Daniel Borkmann) {CVE-2021-45402}
- NFS: LOOKUP_DIRECTORY is also ok with symlinks (Trond Myklebust) {CVE-2022-24448}
- NFSv4: nfs_atomic_open() can race when looking up a non-regular file (Trond Myklebust) {CVE-2022-24448}
- NFSv4: Handle case where the lookup of a directory fails (Trond Myklebust) {CVE-2022-24448}
- UBUNTU: SAUCE: bpf: prevent helper argument PTR_TO_ALLOC_MEM to have offset other than 0 (Thadeu Lima de Souza Cascardo) {CVE-2021-4204}
- bpf: Fix out of bounds access from invalid *_or_null type verification (Daniel Borkmann) {CVE-2022-23222}
- netdevsim: Zero-initialize memory for new map's value in function nsim_bpf_map_alloc (Haimin Zhang) {CVE-2021-4135}

* Wed Mar 09 2022 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.84-10.3.an8]
- lib/iov_iter: initialize "flags" in new pipe_buffer (Max Kellermann) {CVE-2022-0847}
- tipc: improve size validations for received domain records (Jon Maloy) {CVE-2022-0435}

* Tue Feb 15 2022 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.84-10.2.an8]
- cgroup-v1: Require capabilities to set release_agent (Eric W. Biederman) {CVE-2022-0492}
- selftests: cgroup: Test open-time cgroup namespace usage for migration checks (Tejun Heo) {CVE-2021-4197}
- selftests: cgroup: Test open-time credential usage for migration checks (Tejun Heo) {CVE-2021-4197}
- selftests: cgroup: Make cg_create() use 0755 for permission instead of 0644 (Tejun Heo) {CVE-2021-4197}
- cgroup: Use open-time cgroup namespace for process migration perm checks (Tejun Heo) {CVE-2021-4197}
- cgroup: Allocate cgroup_file_ctx for kernfs_open_file->priv (Tejun Heo) {CVE-2021-4197}
- cgroup: Use open-time credentials for process migraton perm checks (Tejun Heo) {CVE-2021-4197}

* Mon Feb 07 2022 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.84-10.1.an8]
- drm/i915: Flush TLBs before releasing backing store (Tvrtko Ursulin) {CVE-2022-0330}
- drm/vmwgfx: Fix stale file descriptors on failed usercopy (Mathias Krause) {CVE-2022-22942}
- vfs: fs_context: fix up param length parsing in legacy_parse_param (Jamie Hill-Daniel) {CVE-2022-0185}

* Tue Jan 25 2022 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.84-10.an8]
- xfs: map unwritten blocks in XFS_IOC_{ALLOC,FREE}SP just like fallocate (Darrick J. Wong)
- anolis: Revert "anolis: net/smc: Add SMC-R link-down counters" (Wen Gu)
- srcu: Take early exit on memory-allocation failure (Paul E. McKenney)
- mm/compaction: fix 'limit' in fast_isolate_freepages (Wonhyuk Yang)
- mm: memcontrol: set the correct memcg swappiness restriction (Baolin Wang)
- perf/x86/intel/uncore: Fix IIO event constraints for Snowridge (Alexander Antonov)
- anolis: kprobes: fix ftrace conflict check for aarch64 (Yihao Wu)
- Revert "ck: ext4: don't submit unwritten extent while holding active jbd2 handle" (Joseph Qi)
- io_uring: allow non-fixed files with SQPOLL (Jens Axboe)
- io_uring: remove redundant initialization of variable ret (Colin Ian King)
- block: disable iopoll for split bio (Jeffle Xu)
- io_uring: Use WRITE_ONCE() when writing to sq_flags (Nadav Amit)
- ACPI: APEI: EINJ: Relax platform response timeout to 1 second (Shuai Xue)
- anolis: SPI: support Yitian spi (yaoxing)
- anolis: I2C: designware i2c support (yaoxing)
- anolis: RDMA/erdma: Fix bug in destroy qp (Kai)
- mm: cma: use pr_err_ratelimited for CMA warning (Baolin Wang)
- anolis: net/smc: support auto-cork with nagle algorithm (Dust Li)
- net/smc: Reduce overflow of smc clcsock listen queue (D. Wythe)
- anolis: net/smc: Forward wakeup to smc socket wait queue when fallback (Wen Gu)
- anolis: net/smc: Avoid setting clcsock options after clcsock released (Wen Gu)
- anolis: net/smc: Resolve the race between SMC-R link access and clear (Wen Gu)
- anolis: net/smc: Resolve the race between link group access and termination (Wen Gu)
- anolis: net/smc: Reset conn->lgr when link group registration failed (Wen Gu)
- anolis: net/smc: Avoid unmapping bufs from unused links (Wen Gu)
- anolis: Revert "net/smc: Transfer remaining wait queue entries during fallback" (Tony Lu)
- anolis: Revert "net/smc: Avoid warning of possible recursive locking" (Tony Lu)
- anolis: net/smc: allow different subnet communication (Tony Lu)
- anolis: net/smc: don't call ib_req_notify_cq in the send routine (Dust Li)
- anolis: net/smc: Add SMC-R link-down counters (Tony Lu)
- anolis: net/smc: Add TX and RX diagnosis information (Tony Lu)
- anolis: net/smc: Introduce TCP to SMC replacement netlink commands (Tony Lu)
- anolis: net/smc: Introduce SMC-R-related proc files (Tony Lu)
- anolis: net/smc: Introduce sysctl tcp2smc (Tony Lu)
- anolis: net/smc: Expose SMCPROTO_SMC and SMCPROTO_SMC6 to userspace (Tony Lu)
- anolis: net/smc: Introduce tunable sysctls for sndbuf and RMB size (Tony Lu)
- net/smc: add comments for smc_link_{usable|sendable} (Dust Li)
- net/smc: remove redundant re-assignment of pointer link (Colin Ian King)
- net/smc: Introduce TCP ULP support (Tony Lu)
- net/smc: Introduce net namespace support for linkgroup (Tony Lu)
- net/smc: Use the bitmap API when applicable (Christophe JAILLET)
- net/smc: fix kernel panic caused by race of smc_sock (Dust Li)
- net/smc: don't send CDC/LLC message if link not ready (Dust Li)
- net/smc: fix using of uninitialized completions (Karsten Graul)
- net/smc: Prevent smc_release() from long blocking (D. Wythe)
- anolis: Revert "anolis: net/smc: Introduce tunable sysctls for sndbuf and RMB size" (Tony Lu)
- anolis: Revert "anolis: net/smc: Expose SMCPROTO_SMC and SMCPROTO_SMC6 to userspace" (Tony Lu)
- anolis: Revert "anolis: net/smc: Introduce sysctl tcp2smc" (Tony Lu)
- anolis: Revert "anolis: net/smc: Introduce SMC-R-related proc files" (Tony Lu)
- anolis: Revert "anolis: net/smc: Introduce TCP to SMC replacement netlink commands" (Tony Lu)
- anolis: Revert "anolis: net/smc: Add TX and RX diagnosis information" (Tony Lu)
- anolis: Revert "anolis: net/smc: Add SMC-R link-down counters" (Tony Lu)
- anolis: Revert "anolis: net/smc: don't call ib_req_notify_cq in the send routine" (Tony Lu)
- anolis: Revert "anolis: net/smc: support auto-cork with nagle algorithm" (Tony Lu)
- anolis: Revert "anolis: net/smc: allow different subnet communication" (Tony Lu)
- anolis: Revert "anolis: Revert "net/smc: Avoid warning of possible recursive locking"" (Tony Lu)
- anolis: Revert "anolis: Revert "net/smc: Transfer remaining wait queue entries during fallback"" (Tony Lu)
- anolis: Revert "anolis: net/smc: Forward wake-up to smc socket wait queue when fallback" (Tony Lu)
- tpm_tis_spi: set default probe function if device id not match (Liguang Zhang)
- anolis: RDMA/erdma: ERDMA driver bugfix (Kai)
- fget: clarify and improve __fget_files() implementation (Linus Torvalds)
- anolis: sched: Fix the bug that break the loop before update idle when select idle core (Cruz Zhao)
- mm, vmscan: guarantee drop_slab_node() termination (Vlastimil Babka)

* Tue Jan 04 2022 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.84-10_rc2.an8]
- irqchip/gic-v3-its: Drop the setting of PTZ altogether (Shenming Lu)
- irqchip/gic-v3-its: Add a cache invalidation right after vPE unmapping (Marc Zyngier)
- irqchip/gic-v4.1: Reduce the delay when polling GICR_VPENDBASER.Dirty (Shenming Lu)
- KVM: arm64: Delay the polling of the GICR_VPENDBASER.Dirty bit (Shenming Lu)
- platform/x86: ISST: Use numa node id for cpu pci dev mapping (Srinivas Pandruvada)
- platform/x86: ISST: Optimize CPU to PCI device mapping (Srinivas Pandruvada)
- anolis: mm: set some slab function always inline (Tianchen Ding)
- iommu/amd: Stop irq_remapping_select() matching when remapping is disabled (David Woodhouse)
- iommu/amd: Set iommu->int_enabled consistently when interrupts are set up (David Woodhouse)
- iommu/amd: Fix IOMMU interrupt generation in X2APIC mode (David Woodhouse)
- iommu/amd: Don't register interrupt remapping irqdomain when IR is disabled (David Woodhouse)
- iommu/amd: Fix union of bitfields in intcapxt support (David Woodhouse)
- x86/ioapic: Correct the PCI/ISA trigger type selection (Thomas Gleixner)
- i2c: designware: Get right data length (Liguang Zhang)
- anolis: task_work: cleanup notification modes (Joseph Qi)
- anolis: Kconfig: RICH_CONTAINER should select SCHED_SLI. (Erwei Deng)
- jump_label: Fix usage in module __init (Peter Zijlstra)

* Mon Dec 20 2021 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.84-10_rc1.an8]
- bump kernel to 5.10.84
- anolis: configs: refresh the defconfig (Shile Zhang)
- anolis: kfence: fix bug and improve performance while freeing pages (Tianchen Ding)
- anolis: KVM: x86: Support VM_ATTESTATION hypercall (hanliyang)
- KVM/SVM: add support for SEV attestation command (Brijesh Singh)
- anolis: crypto: ccp: Don't check tee support on Hygon platform (hanliyang)
- ck: crypto: ccp: Add Hygon CSV support (Hao Feng)
- anolis: RDMA/erdma: Update for hardware and SMC-R (Kai)
- mm/vmalloc: improve allocation failure error messages (Nicholas Piggin)
- mm/vmalloc: remove unmap_kernel_range (Nicholas Piggin)
- powerpc/xive: remove unnecessary unmap_kernel_range (Nicholas Piggin)
- kernel/dma: remove unnecessary unmap_kernel_range (Nicholas Piggin)
- mm/vmalloc: remove map_kernel_range (Nicholas Piggin)
- mm/vmalloc: hugepage vmalloc mappings (Nicholas Piggin)
- mm/vmalloc: add vmap_range_noflush variant (Nicholas Piggin)
- mm: move vmap_range from mm/ioremap.c to mm/vmalloc.c (Nicholas Piggin)
- mm/vmalloc: provide fallback arch huge vmap support functions (Nicholas Piggin)
- x86: inline huge vmap supported functions (Nicholas Piggin)
- arm64: inline huge vmap supported functions (Nicholas Piggin)
- powerpc: inline huge vmap supported functions (Nicholas Piggin)
- mm: HUGE_VMAP arch support cleanup (Nicholas Piggin)
- mm/ioremap: rename ioremap_*_range to vmap_*_range (Nicholas Piggin)
- mm/vmalloc: rename vmap_*_range vmap_pages_*_range (Nicholas Piggin)
- mm: apply_to_pte_range warn and fail if a large pte is encountered (Nicholas Piggin)
- mm/vmalloc: fix HUGE_VMAP regression by enabling huge pages in vmalloc_to_page (Nicholas Piggin)
- ARM: mm: add missing pud_page define to 2-level page tables (Nicholas Piggin)
- x86/ioapic: Use I/O-APIC ID for finding irqdomain, not index (David Woodhouse)
- x86/kvm: Enable 15-bit extension when KVM_FEATURE_MSI_EXT_DEST_ID detected (David Woodhouse)
- iommu/hyper-v: Disable IRQ pseudo-remapping if 15 bit APIC IDs are available (David Woodhouse)
- x86/apic: Support 15 bits of APIC ID in MSI where available (David Woodhouse)
- x86/ioapic: Handle Extended Destination ID field in RTE (David Woodhouse)
- iommu/vt-d: Simplify intel_irq_remapping_select() (David Woodhouse)
- x86: Kill all traces of irq_remapping_get_irq_domain() (David Woodhouse)
- x86/ioapic: Use irq_find_matching_fwspec() to find remapping irqdomain (David Woodhouse)
- x86/hpet: Use irq_find_matching_fwspec() to find remapping irqdomain (David Woodhouse)
- iommu/hyper-v: Implement select() method on remapping irqdomain (David Woodhouse)
- iommu/vt-d: Implement select() method on remapping irqdomain (David Woodhouse)
- iommu/amd: Implement select() method on remapping irqdomain (David Woodhouse)
- x86/apic: Add select() method on vector irqdomain (David Woodhouse)
- genirq/irqdomain: Implement get_name() method on irqchip fwnodes (David Woodhouse)
- x86/ioapic: Generate RTE directly from parent irqchip's MSI message (David Woodhouse)
- x86/ioapic: Cleanup IO/APIC route entry structs (Thomas Gleixner)
- x86/io_apic: Cleanup trigger/polarity helpers (Thomas Gleixner)
- x86/msi: Remove msidef.h (Thomas Gleixner)
- x86/pci/xen: Use msi_msg shadow structs (Thomas Gleixner)
- x86/kvm: Use msi_msg shadow structs (Thomas Gleixner)
- PCI: vmd: Use msi_msg shadow structs (Thomas Gleixner)
- iommu/amd: Use msi_msg shadow structs (Thomas Gleixner)
- iommu/intel: Use msi_msg shadow structs (Thomas Gleixner)
- x86/msi: Provide msi message shadow structs (Thomas Gleixner)
- genirq/msi: Allow shadow declarations of msi_msg:: $member (Thomas Gleixner)
- x86/hpet: Move MSI support into hpet.c (David Woodhouse)
- x86/apic: Always provide irq_compose_msi_msg() method for vector domain (David Woodhouse)
- x86/apic: Cleanup destination mode (Thomas Gleixner)
- x86/apic: Get rid of apic:: Dest_logical (Thomas Gleixner)
- x86/apic: Replace pointless apic:: Dest_logical usage (Thomas Gleixner)
- x86/apic: Cleanup delivery mode defines (Thomas Gleixner)
- x86/devicetree: Fix the ioapic interrupt type table (Thomas Gleixner)
- x86/apic/uv: Fix inconsistent destination mode (Thomas Gleixner)
- x86/msi: Only use high bits of MSI address for DMAR unit (David Woodhouse)
- anolis: sched: fix some warnings with allnoconfig (Cruz Zhao)
- anolis: configs: enable config for duptext (Xu Yu)
- anolis: mm: duptext: truncate all duplicated pages when disable duptext (Xu Yu)
- anolis: mm: duptext: do not mlock slave duplicated page (Xu Yu)
- anolis: mm: duptext: stop migration when suitable for duptext (Xu Yu)
- anolis: mm: duptext: keep mapped duplicated pages when drop caches (Xu Yu)
- anolis: mm: duptext: avoid writes to file with duplicated pages (Xu Yu)
- anolis: mm: duptext: remove duplicated pages when split file THP (Xu Yu)
- anolis: mm: duptext: remove duplicated pages when collapse file THP (Xu Yu)
- anolis: mm: duptext: remove duplicated pages when removed from page cache (Xu Yu)
- anolis: mm: duptext: create duplicated page for remote access (Xu Yu)
- anolis: mm: duptext: add core implementation (Xu Yu)
- anolis: mm: duptext: add PG_dup flag (Xu Yu)
- anolis: net/smc: Supplement for SMC-R iWARP support (Wen Gu)
- anolis: net/smc: Introduce iWARP device support (Wen Gu)
- anolis: net/smc: Forward wake-up to smc socket wait queue when fallback (Wen Gu)
- anolis: Revert "net/smc: Transfer remaining wait queue entries during fallback" (Tony Lu)
- anolis: Revert "net/smc: Avoid warning of possible recursive locking" (Tony Lu)
- anolis: net/smc: allow different subnet communication (Tony Lu)
- anolis: net/smc: support auto-cork with nagle algorithm (Dust Li)
- anolis: net/smc: don't call ib_req_notify_cq in the send routine (Dust Li)
- anolis: net/smc: Add SMC-R link-down counters (Tony Lu)
- anolis: net/smc: Add TX and RX diagnosis information (Tony Lu)
- anolis: net/smc: Introduce TCP to SMC replacement netlink commands (Tony Lu)
- anolis: net/smc: Introduce SMC-R-related proc files (Tony Lu)
- anolis: net/smc: Introduce sysctl tcp2smc (Tony Lu)
- anolis: net/smc: Expose SMCPROTO_SMC and SMCPROTO_SMC6 to userspace (Tony Lu)
- anolis: net/smc: Introduce tunable sysctls for sndbuf and RMB size (Tony Lu)
- net/smc: Clear memory when release and reuse buffer (Tony Lu)
- net/smc: Clean up local struct sock variables (Tony Lu)
- net/smc: Print function name in smcr_link_down tracepoint (Tony Lu)
- net/smc: Introduce tracepoint for smcr link down (Tony Lu)
- net/smc: Introduce tracepoints for tx and rx msg (Tony Lu)
- net/smc: Introduce tracepoint for fallback (Tony Lu)
- net/smc: Fix smc_link->llc_testlink_time overflow (Tony Lu)
- net/smc: stop links when their GID is removed (Karsten Graul)
- net/smc: add netlink support for SMC-Rv2 (Karsten Graul)
- net/smc: extend LLC layer for SMC-Rv2 (Karsten Graul)
- net/smc: add v2 support to the work request layer (Karsten Graul)
- net/smc: retrieve v2 gid from IB device (Karsten Graul)
- net/smc: add v2 format of CLC decline message (Karsten Graul)
- net/smc: add listen processing for SMC-Rv2 (Karsten Graul)
- net/smc: add SMC-Rv2 connection establishment (Karsten Graul)
- net/smc: prepare for SMC-Rv2 connection (Karsten Graul)
- net/smc: save stack space and allocate smc_init_info (Karsten Graul)
- net/smc: improved fix wait on already cleared link (Karsten Graul)
- net/smc: add generic netlink support for system EID (Karsten Graul)
- net/smc: keep static copy of system EID (Karsten Graul)
- net/smc: add support for user defined EIDs (Karsten Graul)
- net/smc: Allow SMC-D 1MB DMB allocations (Stefan Raspl)
- net/smc: Correct smc link connection counter in case of smc client (Guvenc Gulce)
- net: smc: convert tasklets to use new tasklet_setup() API (Allen Pais)
- net/smc: improve return codes for SMC-Dv2 (Karsten Graul)
- anolis: Revert "ck: net/smc: Introduce tunable sysctls for sndbuf and RMB size" (Tony Lu)
- anolis: Revert "ck: net/smc: Fix smc_link->llc_testlink_time overflow" (Tony Lu)
- anolis: Revert "ck: net/smc: Expose SMCPROTO_SMC and SMCPROTO_SMC6 to userspace" (Tony Lu)
- anolis: Revert "ck: net/smc: Introduce sysctl tcp2smc" (Tony Lu)
- anolis: Revert "ck: net/smc: Remove hardcoded RoCE" (Tony Lu)
- anolis: Revert "ck: net/smc: Introduce iWARP device support" (Tony Lu)
- anolis: Revert "ck: net/smc: Introduce SMC-R-related proc files" (Tony Lu)
- anolis: Revert "ck: net/smc: Introduce TCP to SMC replacement netlink commands" (Tony Lu)
- anolis: Revert "ck: net/smc: Add TX and RX diagnosis information" (Tony Lu)
- anolis: Revert "ck: net/smc: Add SMC-R link-down counters" (Tony Lu)
- anolis: configs: enable config for hugetext (Xu Yu)
- anolis: mm, thp: hugetext: disable file THP when THP is configured as always (Xu Yu)
- anolis: mm, thp: hugetext: replace hugetext_enabled with hugetext_file{anon}_enabled (Rongwei Wang)
- anolis: mm, thp: hugetext: redesign fine-grained hugetext_enabled (Rongwei Wang)
- anolis: mm, thp: hugetext: accelerate khugepaged for hugetext vma (Xu Yu)
- anolis: mm, thp: hugetext: add anonymous and executable vma into hugetext (Rongwei Wang)
- anolis: mm, thp: hugetext: make PIC binary mapping address THP align (Rongwei Wang)
- anolis: mm, thp: hugetext: make executable file mapping address THP align (Rongwei Wang)
- anolis: mm, thp: introduce hugetext framework (Rongwei Wang)
- anolis: mm, thp: check page mapping when truncating page cache (Rongwei Wang)
- mm/thp: decrease nr_thps in file's mapping on THP split (Marek Szyprowski)
- mm, thp: fix incorrect unmap behavior for private pages (Rongwei Wang)
- mm, thp: relax the VM_DENYWRITE constraint on file-backed THPs (Collin Fijalkovich)
- mm: memcontrol: add file_thp, shmem_thp to memory.stat (Johannes Weiner)
- anolis: kprobes: Check function patched by module without kprobe (Yihao Wu)
- anolis: sched: fix the bug that cfs_rq->nr_tasks incorrect (Cruz Zhao)
- dmaengine: idxd: fix resource leak on dmaengine driver disable (Dave Jiang)
- dmaengine: idxd: cleanup completion record allocation (Dave Jiang)
- dmaengine: idxd: reconfig device after device reset command (Dave Jiang)
- dmaengine: idxd: add halt interrupt support (Dave Jiang)
- dmaengine: idxd: Use list_move_tail instead of list_del/list_add_tail (Bixuan Cui)
- dmanegine: idxd: fix resource free ordering on driver removal (Dave Jiang)
- dmaengine: idxd: remove kernel wq type set when load configuration (Dave Jiang)
- dmaengine: idxd: remove gen cap field per spec 1.2 update (Dave Jiang)
- dmaengine: idxd: check GENCAP config support for gencfg register (Dave Jiang)
- dmaengine: idxd: move out percpu_ref_exit() to ensure it's outside submission (Dave Jiang)
- dmaengine: idxd: remove interrupt disable for dev_lock (Dave Jiang)
- dmaengine: idxd: remove interrupt disable for cmd_lock (Dave Jiang)
- dmaengine: idxd: fix setting up priv mode for dwq (Dave Jiang)
- dmaengine: idxd: set descriptor allocation size to threshold for swq (Dave Jiang)
- dmaengine: idxd: make submit failure path consistent on desc freeing (Dave Jiang)
- dmaengine: idxd: remove interrupt flag for completion list spinlock (Dave Jiang)
- dmaengine: idxd: make I/O interrupt handler one shot (Dave Jiang)
- dmaengine: idxd: clear block on fault flag when clear wq (Dave Jiang)
- dmaengine: idxd: add capability check for 'block on fault' attribute (Dave Jiang)
- dmaengine: idxd: Remove unused status variable in irq_process_work_list() (Nathan Chancellor)
- dmaengine: idxd: Fix a possible NULL pointer dereference (Christophe JAILLET)
- dmaengine: idxd: fix abort status check (Dave Jiang)
- dmanegine: idxd: add software command status (Dave Jiang)
- dmaengine: idxd: rotate portal address for better performance (Dave Jiang)
- dmaengine: idxd: fix uninit var for alt_drv (Dave Jiang)
- dmaengine: idxd: Set defaults for GRPCFG traffic class (Dave Jiang)
- dmaengine: idxd: remove fault processing code (Dave Jiang)
- dmaengine: idxd: move dsa_drv support to compatible mode (Dave Jiang)
- dmaengine: dsa: move dsa_bus_type out of idxd driver to standalone (Dave Jiang)
- dmaengine: idxd: create user driver for wq 'device' (Dave Jiang)
- dmaengine: idxd: create dmaengine driver for wq 'device' (Dave Jiang)
- dmaengine: idxd: create idxd_device sub-driver (Dave Jiang)
- dmaengine: idxd: add type to driver in order to allow device matching (Dave Jiang)
- dmanegine: idxd: open code the dsa_drv registration (Dave Jiang)
- dmaengine: idxd: idxd: move remove() bits for idxd 'struct device' to device.c (Dave Jiang)
- dmaengine: idxd: move probe() bits for idxd 'struct device' to device.c (Dave Jiang)
- dmaengine: idxd: fix bus_probe() and bus_remove() for dsa_bus (Dave Jiang)
- dmaengine: idxd: remove iax_bus_type prototype (Dave Jiang)
- dmaengine: idxd: remove bus shutdown (Dave Jiang)
- dmaengine: idxd: move wq_disable() to device.c (Dave Jiang)
- dmaengine: idxd: move wq_enable() to device.c (Dave Jiang)
- dmaengine: idxd: remove IDXD_DEV_CONF_READY (Dave Jiang)
- dmaengine: idxd: add 'struct idxd_dev' as wrapper for conf_dev (Dave Jiang)
- dmaengine: idxd: add driver name (Dave Jiang)
- dmaengine: idxd: add driver register helper (Dave Jiang)
- dmaengine: idxd: have command status always set (Dave Jiang)
- dmaengine: idxd: Add wq occupancy information to sysfs attribute (Dave Jiang)
- dmanegine: idxd: cleanup all device related bits after disabling device (Dave Jiang)
- dmaengine: idxd: Simplify code and axe the use of a deprecated API (Christophe JAILLET)
- dmaengine: idxd: fix submission race window (Dave Jiang)
- dmaengine: idxd: fix sequence for pci driver remove() and shutdown() (Dave Jiang)
- dmaengine: idxd: fix setup sequence for MSIXPERM table (Dave Jiang)
- dmaengine: idxd: assign MSIX vectors to each WQ rather than roundrobin (Dave Jiang)
- dmaengine: idxd: fix array index when int_handles are being used (Dave Jiang)
- dmaengine: idxd: fix desc->vector that isn't being updated (Dave Jiang)
- dmaengine: idxd: add missing percpu ref put on failure (Dave Jiang)
- dmaengine: idxd: Change license on idxd.h to LGPL (Tony Luck)
- dmaengine: idxd: remove devm allocation for idxd->int_handles (Dave Jiang)
- dmaengine: idxd: Remove redundant variable cdev_ctx (Jiapeng Chong)
- dmaengine: idxd: Fix missing error code in idxd_cdev_open() (Jiapeng Chong)
- dmaengine: idxd: Add missing cleanup for early error out in probe call (Dave Jiang)
- dmaengine: idxd: add engine 'struct device' missing bus type assignment (Dave Jiang)
- dmaengine: idxd: Enable IDXD performance monitor support (Tom Zanussi)
- dmaengine: idxd: Add IDXD performance monitor support (Tom Zanussi)
- dmaengine: idxd: remove MSIX masking for interrupt handlers (Dave Jiang)
- dmaengine: idxd: device cmd should use dedicated lock (Dave Jiang)
- dmaengine: idxd: support reporting of halt interrupt (Dave Jiang)
- dmaengine: idxd: enable SVA feature for IOMMU (Dave Jiang)
- dmaengine: idxd: convert sprintf() to sysfs_emit() for all usages (Dave Jiang)
- dmaengine: idxd: add interrupt handle request and release support (Dave Jiang)
- dmaengine: idxd: add support for readonly config mode (Dave Jiang)
- dmaengine: idxd: add percpu_ref to descriptor submission path (Dave Jiang)
- dmaengine: idxd: remove detection of device type (Dave Jiang)
- dmaengine: idxd: iax bus removal (Dave Jiang)
- dmaengine: idxd: fix group conf_dev lifetime (Dave Jiang)
- dmaengine: idxd: fix engine conf_dev lifetime (Dave Jiang)
- dmaengine: idxd: fix wq conf_dev 'struct device' lifetime (Dave Jiang)
- dmaengine: idxd: fix idxd conf_dev 'struct device' lifetime (Dave Jiang)
- dmaengine: idxd: use ida for device instance enumeration (Dave Jiang)
- dmaengine: idxd: removal of pcim managed mmio mapping (Dave Jiang)
- dmaengine: idxd: cleanup pci interrupt vector allocation management (Dave Jiang)
- dmaengine: idxd: Update calculation of group offset to be more readable (Dave Jiang)
- platform/x86: intel_pmt_telemetry: Ignore zero sized entries (David E. Box)
- platform/x86: intel_pmt_crashlog: Constify static attribute_group struct (Rikard Falkeborn)
- platform/x86: intel_pmt_crashlog: Fix incorrect macros (David E. Box)
- platform/x86: intel_pmt_class: Initial resource to 0 (David E. Box)
- mfd: intel_pmt: Add support for DG1 (David E. Box)
- mfd: intel_pmt: Fix nuisance messages and handling of disabled capabilities (David E. Box)
- platform/x86: intel_pmt_crashlog: Add dependency on MFD_INTEL_PMT (David E. Box)
- platform/x86: intel_pmt_telemetry: Add dependency on MFD_INTEL_PMT (David E. Box)
- platform/x86: intel_pmt: Make INTEL_PMT_CLASS non-user-selectable (David E. Box)
- platform/x86: pmt: Fix a potential Oops on error in probe (Dan Carpenter)
- platform/x86: Intel PMT Crashlog capability driver (Alexander Duyck)
- platform/x86: Intel PMT Telemetry capability driver (Alexander Duyck)
- platform/x86: Intel PMT class driver (Alexander Duyck)
- mfd: Intel Platform Monitoring Technology support (David E. Box)
- PCI: Add defines for Designated Vendor-Specific Extended Capability (David E. Box)
- x86/mce: Drop copyin special case for #MC (Tony Luck)
- generic_perform_write()/iomap_write_actor(): saner logics for short copy (Al Viro)
- x86/mce: Change to not send SIGBUS error during copy from user (Tony Luck)
- anolis: kfence: adjust KFENCE page alloc to fit debug kernel (Tianchen Ding)
- anolis: kfence: improve performance on is_kfence_address() (Tianchen Ding)
- khugepaged: selftests: remove debug_cow (Nanyong Sun)
- anolis: kfence: fix bug about reading cache line size (Tianchen Ding)
- fs/buffer.c: add checking buffer head stat before clear (Yang Guo)
- anolis: sched: Add a stub function in task's exiting (Erwei Deng)
- dmaengine: idxd: Use cpu_feature_enabled() (Borislav Petkov)
- anolis: hooker: fix a crash when loading the hooker module (Wen Yang)
- anolis: xsk: fugfix for xsk_pool_pgs_delay_unpin argument type when no XDP SOCKET (Xuan Zhuo)
- anolis: mm: coldpgs: replace PageTranshuge because not hold PageLock (Rongwei Wang)
- arm64: Import updated version of Cortex Strings' strlen (Sam Tebbs)
- arm64: Import latest version of Cortex Strings' strncmp (Sam Tebbs)
- arm64: Import latest version of Cortex Strings' strcmp (Sam Tebbs)
- arm64: Import latest version of Cortex Strings' memcmp (Sam Tebbs)
- arm64: Import latest memcpy()/memmove() implementation (Robin Murphy)
- arm64: Better optimised memchr() (Robin Murphy)
- arm64: Add assembly annotations for weak-PI-alias madness (Robin Murphy)
- anolis: virtio_net: change xsk_budget default to 64 (Xuan Zhuo)
- anolis: virtio-net: support unpin xsk pages (Xuan Zhuo)
- anolis: virtio_net: add __netif_tx_lock_bh before access sq->xsk (Xuan Zhuo)
- anolis: virtio-net: bugfix for xsk free the xsk.hdr with race (Xuan Zhuo)
- anolis: virtio-net: add barrier for bit option (Xuan Zhuo)
- anolis: virtio-net: xsk remove the timer cancel option (Xuan Zhuo)
- anolis: virtio-net: fix kcalloc with GFP_ATOMIC inside xsk enable (Xuan Zhuo)
- anolis: virtio-net, xsk: add stats from xsk (Xuan Zhuo)
- anolis: virtio-net, xsk: virtio-net support xsk (Xuan Zhuo)
- anolis: virtio-net: prepare for support xsk (Xuan Zhuo)
- anolis: virtio-net: distinguish XDP_TX and XSK XMIT ctx (Xuan Zhuo)
- anolis: xsk: remove dma check (Xuan Zhuo)
- anolis: xsk: support xsk_pool_get_page (Xuan Zhuo)
- anolis: xsk: support unpin page delay (Xuan Zhuo)
- anolis: sched: Fix cg_nr_iowait race condition (Yihao Wu)
- anolis: mm,memcg: export memory.{min,low,high} to v1 (yaoxing)
- anolis: sched: add reserve paddings for some structures in scheudler (Erwei Deng)
- anolis: kfence: add order-0 page test cases (Tianchen Ding)
- anolis: kfence: support order-0 page check (Tianchen Ding)
- anolis: cgroup: add fast path for cgroup_mkdir() (Yi Tao)
- anolis: cgroup: add user space interface for cgroup pool (Yi Tao)
- anolis: mm: use seq_file to show add_pid file (Gang Deng)
- anolis: memcg: Account throttled time due to memory.wmark_min_adj (Xunlei Pang)
- anolis: memcg: Introduce memory.wmark_min_adj (Xunlei Pang)
- anolis: io_uring: rename sqthread's task name (Hao Xu)
- anolis: io_uring: lift nice value of sqthread (Hao Xu)
- anolis: io_uring: allow sqthread to inherit cpuacct from its creator (Hao Xu)
- anolis: io_uring: submit sqes in the original context when waking up sqthread (Hao Xu)
- anolis: io_uring: add support for us granularity of io_sq_thread_idle (Hao Xu)
- anolis: sched: fix load average always > 1 (Tianchen Ding)
- anolis: kfence: improve KFENCE performance (Tianchen Ding)
- anolis: kfence: unlimit alloc interval when num_objects_pernode > 65535 (Tianchen Ding)
- anolis: kfence: add per cpu freelist (Tianchen Ding)
- anolis: kfence: support kfence pool per numa node (Tianchen Ding)
- anolis: kfence: add cmdline parameter to change num_objects on boot (Tianchen Ding)
- ext4: drop unnecessary journal handle in delalloc write (Zhang Yi)
- ext4: factor out write end code of inline file (Zhang Yi)
- anolis: perf: add spe-c2c help command (Shuai Xue)
- anolis: perf: fix c2c report failure (Shuai Xue)
- anolis: perf: adjust histogram for spe c2c (Shuai Xue)
- anolis: perf: speed up the process of perf sample (Shuai Xue)
- anolis: perf: add progress ui for finding false sharing (Shuai Xue)
- anolis: perf: accelerate perf spe-c2c report (Shuai Xue)
- openEuler: perf tools: add support for ARM spe-c2c (Shuai Xue)
- anolis: configs: set config for Intel Data Accelerators support (Guanjun)
- anolis: Revert "x86/cpufeatures: Force disable X86_FEATURE_ENQCMD and remove update_pasid()" (Guanjun)
- anolis: bond: broadcast ARP or ND messages to all slaves (Tony Lu)
- napi: fix race inside napi_enable (Xuan Zhuo)
- Intel: EDAC/i10nm: Retrieve and print retry_rd_err_log registers (Youquan Song)
- Intel: Fix backport issue for MCA recovery (Youquan Song)
- Intel: perf/x86/intel/uncore: Support IMC free-running counters on Sapphire Rapids server (Kan Liang)
- Intel: perf/x86/intel/uncore: Support IIO free-running counters on Sapphire Rapids server (Kan Liang)
- Intel: perf/x86/intel/uncore: Factor out snr_uncore_mmio_map() (Kan Liang)
- Intel: perf/x86/intel/uncore: Add alias PMU name (Kan Liang)
- Intel: perf/x86/intel/uncore: Add Sapphire Rapids server MDF support (Kan Liang)
- Intel: perf/x86/intel/uncore: Add Sapphire Rapids server M3UPI support (Kan Liang)
- Intel: perf/x86/intel/uncore: Add Sapphire Rapids server UPI support (Kan Liang)
- Intel: perf/x86/intel/uncore: Add Sapphire Rapids server M2M support (Kan Liang)
- Intel: perf/x86/intel/uncore: Add Sapphire Rapids server IMC support (Kan Liang)
- Intel: perf/x86/intel/uncore: Add Sapphire Rapids server PCU support (Kan Liang)
- Intel: perf/x86/intel/uncore: Add Sapphire Rapids server M2PCIe support (Kan Liang)
- Intel: perf/x86/intel/uncore: Add Sapphire Rapids server IRP support (Kan Liang)
- Intel: perf/x86/intel/uncore: Add Sapphire Rapids server IIO support (Kan Liang)
- Intel: perf/x86/intel/uncore: Add Sapphire Rapids server CHA support (Kan Liang)
- Intel: perf/x86/intel/uncore: Add Sapphire Rapids server framework (Kan Liang)
- Intel: x86/cpu: Fix core name for Sapphire Rapids (Andi Kleen)
- Intel: x86/cpu: Resort and comment Intel models (Peter Zijlstra)
- net/tls: getsockopt supports complete algorithm list (Tianjia Zhang)
- net/tls: tls_crypto_context add supported algorithms context (Tianjia Zhang)
- selftests/tls: add SM4 algorithm dependency for tls selftests (Tianjia Zhang)
- selftests/tls: add SM4 GCM/CCM to tls selftests (Tianjia Zhang)
- net/tls: support SM4 CCM algorithm (Tianjia Zhang)
- net/tls: support SM4 GCM/CCM algorithm (Tianjia Zhang)
- selftests: tls: fix chacha+bidir tests (Jakub Kicinski)
- selftests: tls: clean up uninitialized warnings (Jakub Kicinski)
- selftests/tls: Add {} to avoid static checker warning (Kees Cook)
- selftests/tls: fix selftest with CHACHA20-POLY1305 (Vadim Fedorenko)
- selftests/tls: fix selftests after adding ChaCha20-Poly1305 (Vadim Fedorenko)
- selftests/tls: add CHACHA20-POLY1305 to tls selftests (Vadim Fedorenko)
- net/tls: add CHACHA20-POLY1305 configuration (Vadim Fedorenko)
- net/tls: add CHACHA20-POLY1305 specific behavior (Vadim Fedorenko)
- net/tls: add CHACHA20-POLY1305 specific defines and structures (Vadim Fedorenko)
- net/tls: make inline helpers protocol-aware (Vadim Fedorenko)
- crypto: x86/sm4 - Fix invalid section entry size (Tianjia Zhang)
- crypto: x86/sm4 - Fix frame pointer stack corruption (Josh Poimboeuf)
- crypto: sm4 - Do not change section of ck and sbox (Nathan Chancellor)
- Documentation/x86: Rename resctrl_ui.rst and add two errata to the file (Fenghua Yu)
- x86/resctrl: Correct MBM total and local values (Fenghua Yu)
- anolis: sched: introduce ID_EXPELLER_SHARE_CORE feature (Michael Wang)
- anolis: arm64: Add CPU freqency information for /proc/cpuinfo (Baolin Wang)
- anolis: arm64: Expose address bits (physical/virtual) via cpuinfo (Baolin Wang)
- kfence: show cpu and timestamp in alloc/free info (Marco Elver)
- kfence: fix is_kfence_address() for addresses below KFENCE_POOL_SIZE (Marco Elver)
- kfence, x86: only define helpers if !MODULE (Marco Elver)
- kfence: skip all GFP_ZONEMASK allocations (Alexander Potapenko)
- kfence: move the size check to the beginning of __kfence_alloc() (Alexander Potapenko)
- kfence: defer kfence_test_init to ensure that kunit debugfs is created (Weizhao Ouyang)
- kfence: unconditionally use unbound work queue (Marco Elver)
- kfence: use TASK_IDLE when awaiting allocation (Marco Elver)
- kfence: use power-efficient work queue to run delayed work (Marco Elver)
- kfence: maximize allocation wait timeout duration (Marco Elver)
- kfence: await for allocation using wait_event (Marco Elver)
- kfence: zero guard page after out-of-bounds access (Marco Elver)
- kfence, x86: fix preemptible warning on KPTI-enabled systems (Marco Elver)
- kfence: make compatible with kmemleak (Marco Elver)
- kfence: fix reports if constant function prefixes exist (Marco Elver)
- kfence, slab: fix cache_alloc_debugcheck_after() for bulk allocations (Marco Elver)
- kfence: fix printk format for ptrdiff_t (Marco Elver)
- anolis: config: set config for kfence (Tianchen Ding)
- kfence: add test suite (Marco Elver)
- kfence, Documentation: add KFENCE documentation (Marco Elver)
- kfence, kasan: make KFENCE compatible with KASAN (Alexander Potapenko)
- mm, kfence: insert KFENCE hooks for SLUB (Alexander Potapenko)
- mm, kfence: insert KFENCE hooks for SLAB (Alexander Potapenko)
- kfence: use pt_regs to generate stack trace on faults (Marco Elver)
- arm64, kfence: enable KFENCE for ARM64 (Marco Elver)
- x86, kfence: enable KFENCE for x86 (Alexander Potapenko)
- mm: add Kernel Electric-Fence infrastructure (Alexander Potapenko)
- anolis: sched: set WRITE_ONCE on calculating load_sum when updating cfs rq load (Tianchen Ding)
- anolis: mm, kidled: fix race when free idle age (Gang Deng)
- anolis: mm, kidled: skip node which has none memory (Gang Deng)
- anolis: sched: fix the bug that nr_expel_immune underflow (CruzZhao)
- anolis: sched: Fix the bug that skip judge whether se is null before sub nr_running when thottle cfs_rq (CruzZhao)
- anolis: sched: Ensure the logic of check_preempt_tick with CONFIG_GROUP_IDENTITY off (Peng Wang)
- anolis: Revert "ck: sched: keep task's min runtime without group_identity" (Cruz Zhao)
- ck: x86/kvm: Initially vcpu fpu state points to __default_state (Guanjun)
- ck: sched: keep task's min runtime without group_identity (Peng Wang)
- configs: x86: enable CONFIG_CRYPTO_SM4_AESNI_AVX_X86_64 and CONFIG_CRYPTO_SM4_AESNI_AVX2_X86_64 (YiLin.Li)
- crypto: x86/sm4 - add AES-NI/AVX2/x86_64 implementation (Tianjia Zhang)
- crypto: x86/sm4 - export reusable AESNI/AVX functions (Tianjia Zhang)
- crypto: tcrypt - add GCM/CCM mode test for SM4 algorithm (Tianjia Zhang)
- crypto: testmgr - Add GCM/CCM mode test of SM4 algorithm (Tianjia Zhang)
- crypto: tcrypt - Fix missing return value check (Tianjia Zhang)
- crypto: tcrypt - add the asynchronous speed test for SM4 (Tianjia Zhang)
- crypto: x86/sm4 - add AES-NI/AVX/x86_64 implementation (Tianjia Zhang)
- crypto: arm64/sm4-ce - Make dependent on sm4 library instead of sm4-generic (Tianjia Zhang)
- crypto: sm4 - create SM4 library based on sm4 generic code (Tianjia Zhang)
- crypto: tcrypt - include 1420 byte blocks in aead and skcipher benchmarks (Ard Biesheuvel)
- io_uring: refactor sendmsg/recvmsg iov managing (Pavel Begunkov)
- io_uring: clean iov usage for recvmsg buf select (Pavel Begunkov)
- io_uring: set msg_name on msg fixup (Pavel Begunkov)
- ck: meminfo: Clean warnings in meminfo_proc_show() (Xiang Zheng)
- ck: mm: Pin code section of process in memory (Xunlei Pang)
- x86/bus_lock: Set rate limit for bus lock (Fenghua Yu)
- Documentation/x86: Add ratelimit in buslock.rst (Fenghua Yu)
- Documentation/admin-guide: Add bus lock ratelimit (Fenghua Yu)
- Documentation/x86: Add buslock.rst (Fenghua Yu)
- Documentation/admin-guide: Change doc for split_lock_detect parameter (Fenghua Yu)
- x86/traps: Handle #DB for bus lock (Fenghua Yu)
- x86/cpufeatures: Enumerate #DB for bus lock detection (Fenghua Yu)
- ck: Intel: selftest/x86/amx: Replace __attribute__((aligned(size))) with __aligned(size) (Guanjun)
- Intel: x86/fpu/xstate: Introduce boot-parameters to control state component support (Chang S. Bae)
- Intel: x86/fpu/xstate: Support dynamic user state in the signal handling path (Chang S. Bae)
- Intel: selftest/x86/amx: Include test cases for the AMX state management (Chang S. Bae)
- Intel: x86/fpu/amx: Enable the AMX feature in 64-bit mode (Chang S. Bae)
- Intel: x86/fpu/amx: Define AMX state components and have it used for boot-time checks (Chang S. Bae)
- Intel: x86/cpufeatures/amx: Enumerate Advanced Matrix Extension (AMX) feature bits (Chang S. Bae)
- Intel: x86/fpu/xstate: Extend the table to map state components with features (Chang S. Bae)
- Intel: x86/fpu/xstate: Support ptracer-induced xstate buffer expansion (Chang S. Bae)
- Intel: x86/fpu/xstate: Expand the xstate buffer on the first use of dynamic user state (Chang S. Bae)
- Intel: x86/fpu/xstate: Update the xstate context copy function to support dynamic states (Chang S. Bae)
- Intel: x86/fpu/xstate: Update the xstate buffer address finder to support dynamic states (Chang S. Bae)
- Intel: x86/fpu/xstate: Update the xstate save function to support dynamic states (Chang S. Bae)
- Intel: x86/fpu/xstate: Define the scope of the initial xstate data (Chang S. Bae)
- Intel: x86/fpu/xstate: Introduce helpers to manage the xstate buffer dynamically (Chang S. Bae)
- Intel: x86/fpu/xstate: Convert the struct fpu 'state' field to a pointer (Chang S. Bae)
- Intel: x86/fpu/xstate: Calculate and remember dynamic xstate buffer sizes (Chang S. Bae)
- Intel: x86/fpu/xstate: Add new variables to indicate dynamic xstate buffer size (Chang S. Bae)
- Intel: x86/fpu/xstate: Add a new variable to indicate dynamic user states (Chang S. Bae)
- Intel: x86/fpu/xstate: Modify the context restore helper to handle both static and dynamic buffers (Chang S. Bae)
- Intel: x86/fpu/xstate: Modify address finders to handle both static and dynamic buffers (Chang S. Bae)
- Intel: x86/fpu/xstate: Modify state copy helpers to handle both static and dynamic buffers (Chang S. Bae)
- Intel: x86/fpu/xstate: Modify the initialization helper to handle both static and dynamic buffers (Chang S. Bae)
- ck: Revert "x86/fpu: Limit xstate copy size in xstateregs_set()" (Guanjun)
- ck: Revert "x86/fpu: Fix copy_xstate_to_kernel() gap handling" (Guanjun)
- ck: Revert "x86/fpu: Return proper error codes from user access functions" (Guanjun)
- PCI/RCEC: Fix RCiEP device to RCEC association (Qiuxu Zhuo)
- PCI/AER: Add RCEC AER error injection support (Qiuxu Zhuo)
- PCI/PME: Add pcie_walk_rcec() to RCEC PME handling (Sean V Kelley)
- PCI/AER: Add pcie_walk_rcec() to RCEC AER handling (Sean V Kelley)
- PCI/ERR: Recover from RCiEP AER errors (Qiuxu Zhuo)
- PCI/ERR: Add pcie_link_rcec() to associate RCiEPs (Sean V Kelley)
- PCI/ERR: Recover from RCEC AER errors (Sean V Kelley)
- PCI/AER: Write AER Capability only when we control it (Sean V Kelley)
- PCI/ERR: Clear AER status only when we control AER (Sean V Kelley)
- PCI/ERR: Add pci_walk_bridge() to pcie_do_recovery() (Sean V Kelley)
- PCI/ERR: Avoid negated conditional for clarity (Sean V Kelley)
- PCI/ERR: Use "bridge" for clarity in pcie_do_recovery() (Sean V Kelley)
- PCI/ERR: Simplify by computing pci_pcie_type() once (Sean V Kelley)
- PCI/ERR: Simplify by using pci_upstream_bridge() (Sean V Kelley)
- PCI/ERR: Rename reset_link() to reset_subordinates() (Sean V Kelley)
- PCI/ERR: Cache RCEC EA Capability offset in pci_init_capabilities() (Sean V Kelley)
- PCI/ERR: Bind RCEC devices to the Root Port driver (Qiuxu Zhuo)

* Mon Sep 06 2021 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.60-9.an8]
- bump kernel to 5.10.60
- blk-mq: fix is_flush_rq (Ming Lei)
- ck: mm/swap: consider max pages in iomap_swapfile_add_extent (Xu Yu)
- ck:mm:break in readahead when multiple threads detected (Tianchen Ding)
- ck: Revert "ck: kernel: Reduce tasklist_lock contention at fork and exit" (Su Lifan)
- configs: set config for Intel Data Accelerators support (Guanjun)
- Intel: AVX VNNI: x86: Enumerate AVX Vector Neural Network instructions (Kyung Min Park)
- selftest/x86/signal: Include test cases for validating sigaltstack (Chang S. Bae)
- selftest/sigaltstack: Use the AT_MINSIGSTKSZ aux vector if available (Chang S. Bae)
- x86/elf: Support a new ELF aux vector AT_MINSIGSTKSZ (Chang S. Bae)
- x86/signal: Introduce helpers to get the maximum signal frame size (Chang S. Bae)
- uapi/auxvec: Define the aux vector AT_MINSIGSTKSZ (Chang S. Bae)
- ck: scripts: sign-file - support the sm2-with-sm3 signature based on openssl version less than 3.x (YiLin.Li)
- ck: cpuacct: fix enumeration sequence error for sched sli latency (Erwei Deng)
- blk-mq: fix kernel panic during iterating over flush request (Ming Lei)
- ck: livepatch: fix stack check when disabling patch (Xu Yu)
- ck: arm64: kdump: support reserving crashkernel above 4G (Xin Hao)
- ck: arm64: Add Crash kernel low reservation (Xin Hao)
- x86,swiotlb: Adjust SWIOTLB bounce buffer size for SEV guests (Ashish Kalra)

* Mon Aug 09 2021 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.50-8.al8]
- ck: cpuacct: fix cgroup usage overflow when sirq grows (Yihao Wu)
- ck: net/smc: Add SMC-R link-down counters (Wen Gu)
- ck: net/smc: Fix kernel panic caused by QP creation failure. (Wen Gu)
- ck: cgroup: Fix possible migration exception in cgroup_migrate_execute() (hongnanli)
- io_uring: fix -EAGAIN retry with IOPOLL (Jens Axboe)
- ck: net/smc: Add TX and RX diagnosis information (Wen Gu)
- ck: net/smc: Introduce TCP to SMC replacement netlink commands (Wen Gu)
- ck: net/smc: Introduce SMC-R-related proc files (Wen Gu)
- arm64: vdso: Avoid ISB after reading from cntvct_el0 (Will Deacon)
- ck: blk-throttle: fix race bug that loses wakeup event (Xiaoguang Wang)
- ck: selftests: openat2: Fix testing failure for O_LARGEFILE flag (Baolin Wang)
- mm,hwpoison: send SIGBUS with error virutal address (Naoya Horiguchi)
- mm/hwpoison: do not lock page again when me_huge_page() successfully recovers (Naoya Horiguchi)
- mm,hwpoison: return -EHWPOISON to denote that the page has already been poisoned (Aili Yao)
- mm/memory-failure: use a mutex to avoid memory_failure() races (Tony Luck)
- ck: arm64: Fix the CPPC ioremap failure (Baolin Wang)
- ck: RDMA/erdma: fix SPDX header for erdma (Kai)
- ck: RDMA/erdma: remove useless print info (Kai)
- ck: RDMA/erdma: add irq affinity based on numa (Kai)
- ck: net: enable PingTrace feature by default (Qiao Ma)
- ck: RDMA/erdma: fix mr count bug in ib_alloc_mr (Kai)
- seq_file: disallow extremely large seq buffer allocations (Eric Sandeen)
- configs: update for code rebase (Shile Zhang)
- ck: sched: make ID_LOOSE_EXPEL defined with CONFIG_GROUP_IDENTITY on (Cruz Zhao)

* Mon Jul 19 2021 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.50-8.rc1.al8]
- bump kernel to 5.10.50
- sched: Limit the amount of NUMA imbalance that can exist at fork time (Mel Gorman)
- sched/numa: Allow a floating imbalance between NUMA nodes (Mel Gorman)
- sched/numa: Rename nr_running and break out the magic number (Mel Gorman)
- ck: MAINTAINERS: Add maintainers for ERDMA (Kai)
- ck: configs: set configs for ERDMA driver (Kai)
- ck: RDMA/erdma: Add ERDMA driver (Kai)
- ck: net/smc: Introduce iWARP device support (Wen Gu)
- ck: net/smc: Remove hardcoded RoCE (Wen Gu)
- ck: net/smc: Introduce iWARP extended information in struct smc_link (Wen Gu)
- ck: net/smc: Introduce sysctl tcp2smc (Wen Gu)
- ck: net/smc: Expose SMCPROTO_SMC and SMCPROTO_SMC6 to userspace (Wen Gu)
- ck: net/smc: Fix smc_link->llc_testlink_time overflow (Tony Lu)
- ck: Revert "net/smc: don't wait for send buffer space when data was already sent" (Tony Lu)
- ck: net/smc: Introduce tunable sysctls for sndbuf and RMB size (Tony Lu)
- configs: enable configs for ICMP PingTrace support (Qiao Ma)
- ck: net: add PingTrace feature support (Qiao Ma)
- ck: UKFEF: unified kernel fault event framework (Meng Shen)
- ck: configs: ARM64: add pv qspinlock feature support (Xin Hao)
- ck: KVM: arm64: add pv_qspinlock select in menuconfig (Xin Hao)
- ck: KVM: arm64: init pv_qspinlock (Xin Hao)
- ck: KVM: arm64: Add interface to support PV qspinlock (Xin Hao)
- ck: KVM: arm64: Add SMCCC PV-qspinlock to kick cpu (Xin Hao)
- ck: KVM: arm64: Add SMCCC PV-qspinlock uid (Xin Hao)
- net/smc: Ensure correct state of the socket in send path (Guvenc Gulce)
- net/smc: Fix ENODATA tests in smc_nl_get_fback_stats() (Dan Carpenter)
- net/smc: Make SMC statistics network namespace aware (Guvenc Gulce)
- net/smc: Add netlink support for SMC fallback statistics (Guvenc Gulce)
- net/smc: Add netlink support for SMC statistics (Guvenc Gulce)
- net/smc: Add SMC statistics support (Guvenc Gulce)
- net/smc: no need to flush smcd_dev's event_wq before destroying it (Julian Wiedmann)
- net/smc: avoid possible duplicate dmb unregistration (Karsten Graul)
- net/smc: Remove redundant assignment to rc (Jiapeng Chong)
- net: smc: Remove repeated struct declaration (Wan Jiabing)
- net/smc: use memcpy instead of snprintf to avoid out of bounds read (Guvenc Gulce)
- smc: fix out of bound access in smc_nl_get_sys_info() (Jakub Kicinski)
- net/smc: fix access to parent of an ib device (Karsten Graul)
- net/smc: Add support for obtaining SMCR device list (Guvenc Gulce)
- net/smc: Add support for obtaining SMCD device list (Guvenc Gulce)
- net/smc: Add SMC-D Linkgroup diagnostic support (Guvenc Gulce)
- net/smc: Introduce SMCR get link command (Guvenc Gulce)
- net/smc: Introduce SMCR get linkgroup command (Guvenc Gulce)
- net/smc: Add support for obtaining system information (Guvenc Gulce)
- net/smc: Introduce generic netlink interface for diagnostic purposes (Guvenc Gulce)
- net/smc: Refactor smc ism v2 capability handling (Guvenc Gulce)
- net/smc: Add diagnostic information to link structure (Guvenc Gulce)
- net/smc: Add diagnostic information to smc ib-device (Guvenc Gulce)
- net/smc: Add link counters for IB device ports (Guvenc Gulce)
- net/smc: Add connection counters for links (Guvenc Gulce)
- net/smc: Use active link of the connection (Guvenc Gulce)
- net/smc: use helper smc_conn_abort() in listen processing (Karsten Graul)
- ck: virio-net: add lro option (Xuan Zhuo)
- xdp, net: Fix use-after-free in bpf_xdp_link_release (Xuan Zhuo)
- bpf, test: fix NULL pointer dereference on invalid expected_attach_type (Xuan Zhuo)
- ck: X.509: Support parsing certificate using SM2 algorithm (Tianjia Zhang)
- x509: Detect sm2 keys by their parameters OID (Stefan Berger)
- ck: configs: Enable SM2 asymmetric algorithm (Tianjia Zhang)
- ck: pkcs7: make parser enable SM2 and SM3 algorithms combination (Tianjia Zhang)
- ck: configs: Enable LSM Instrumentation with BPF (Tianjia Zhang)
- bpf: Fix BPF_LSM kconfig symbol dependency (Daniel Borkmann)
- bpf: Avoid old-style declaration warnings (Arnd Bergmann)
- bpf: Drop disabled LSM hooks from the sleepable set (Mikko Ylinen)
- bpf: Local storage helpers should check nullness of owner ptr passed (KP Singh)
- bpf: Add a selftest for bpf_ima_inode_hash (KP Singh)
- bpf: Add a BPF helper for getting the IMA hash of an inode (KP Singh)
- ima: Implement ima_inode_hash (KP Singh)
- bpf: Add bpf_ktime_get_coarse_ns helper (Dmitrii Banshchikov)
- bpf: Add tests for bpf_bprm_opts_set helper (KP Singh)
- bpf: Add bpf_bprm_opts_set helper (KP Singh)
- bpf: Expose bpf_d_path helper to sleepable LSM hooks (KP Singh)
- bpf: Augment the set of sleepable LSM hooks (KP Singh)
- bpf: Fix NULL dereference in bpf_task_storage (Martin KaFai Lau)
- bpf: Exercise syscall operations for inode and sk storage (KP Singh)
- bpf: Add tests for task_local_storage (KP Singh)
- bpf: Update selftests for local_storage to use vmlinux.h (KP Singh)
- bpf: Implement get_current_task_btf and RET_PTR_TO_BTF_ID (KP Singh)
- bpftool: Add support for task local storage (KP Singh)
- libbpf: Add support for task local storage (KP Singh)
- bpf: Implement task local storage (KP Singh)
- bpf: Allow LSM programs to use bpf spin locks (KP Singh)
- ck: configs: Enable the memory hotplug for ARM architecture (Baolin Wang)
- arm64/mm/hotplug: Ensure early memory sections are all online (Anshuman Khandual)
- arm64/mm/hotplug: Enable MEM_OFFLINE event handling (Anshuman Khandual)
- arm64/mm/hotplug: Register boot memory hot remove notifier earlier (Anshuman Khandual)
- virtio-net: fix for skb_over_panic inside big mode (Xuan Zhuo)
- virtio_net: get build_skb() buf by data ptr (Xuan Zhuo)
- virtio-net: fix for unable to handle page fault for address (Xuan Zhuo)
- virtio-net: fix use-after-free in skb_gro_receive (Xuan Zhuo)
- virtio-net: fix use-after-free in page_to_skb() (Eric Dumazet)
- virtio-net: restrict build_skb() use to some arches (Eric Dumazet)
- virtio-net: page_to_skb() use build_skb when there's sufficient tailroom (Xuan Zhuo)
- ck: sched: fix the bug that sysctl_sched_bvt_place_epsilon doesn't work (Cruz Zhao)
- configs: set CONFIG_X86_SGX to default value (GuoRui.Yu)
- x86/sgx: Maintain encl->refcount for each encl->mm_list entry (GuoRui.Yu)
- MAINTAINERS: Add Dave Hansen as reviewer for INTEL SGX (GuoRui.Yu)
- x86/sgx: Drop racy follow_pfn() check (GuoRui.Yu)
- MAINTAINERS: Fix the tree location for INTEL SGX patches (GuoRui.Yu)
- x86/sgx: Fix the return type of sgx_init() (GuoRui.Yu)
- x86/sgx: Return -EINVAL on a zero length buffer in sgx_ioc_enclave_add_pages() (GuoRui.Yu)
- x86/sgx: Fix a typo in kernel-doc markup (GuoRui.Yu)
- x86/sgx: Fix sgx_ioc_enclave_provision() kernel-doc comment (GuoRui.Yu)
- x86/sgx: Return -ERESTARTSYS in sgx_ioc_enclave_add_pages() (GuoRui.Yu)
- selftests/sgx: Use a statically generated 3072-bit RSA key (GuoRui.Yu)
- x86/sgx: Clarify 'laundry_list' locking (GuoRui.Yu)
- x86/sgx: Update MAINTAINERS (GuoRui.Yu)
- Documentation/x86: Document SGX kernel architecture (GuoRui.Yu)
- x86/sgx: Add ptrace() support for the SGX driver (GuoRui.Yu)
- x86/sgx: Add a page reclaimer (GuoRui.Yu)
- selftests/x86: Add a selftest for SGX (GuoRui.Yu)
- x86/vdso: Implement a vDSO for Intel SGX enclave call (GuoRui.Yu)
- x86/traps: Attempt to fixup exceptions in vDSO before signaling (GuoRui.Yu)
- x86/fault: Add a helper function to sanitize error code (GuoRui.Yu)
- x86/vdso: Add support for exception fixup in vDSO functions (GuoRui.Yu)
- x86/sgx: Add SGX_IOC_ENCLAVE_PROVISION (GuoRui.Yu)
- x86/sgx: Add SGX_IOC_ENCLAVE_INIT (GuoRui.Yu)
- x86/sgx: Add SGX_IOC_ENCLAVE_ADD_PAGES (GuoRui.Yu)
- x86/sgx: Add SGX_IOC_ENCLAVE_CREATE (GuoRui.Yu)
- x86/sgx: Add an SGX misc driver interface (GuoRui.Yu)
- mm: Add 'mprotect' hook to struct vm_operations_struct (GuoRui.Yu)
- x86/sgx: Add SGX page allocator functions (GuoRui.Yu)
- x86/cpu/intel: Add a nosgx kernel parameter (GuoRui.Yu)
- x86/cpu/intel: Detect SGX support (GuoRui.Yu)
- x86/mm: Signal SIGSEGV with PF_SGX (GuoRui.Yu)
- x86/sgx: Initialize metadata for Enclave Page Cache (EPC) sections (GuoRui.Yu)
- x86/{cpufeatures,msr}: Add Intel SGX Launch Control hardware bits (GuoRui.Yu)
- x86/cpufeatures: Add Intel SGX hardware bits (GuoRui.Yu)
- x86/sgx: Add wrappers for ENCLS functions (GuoRui.Yu)
- x86/sgx: Add SGX architectural data structures (GuoRui.Yu)
- ck: mm: Fix minus "Cached" in meminfo (Xunlei Pang)
- ck: memcg: Restrict memcg zombie scan interval (Xunlei Pang)
- ck: sched: fix the bug that crash caused by __update_identity() (Cruz Zhao)

* Thu Jun 24 2021 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.46-7.al8]
- bump kernel to 5.10.46
- configs: update default configs for code rebase (Shile Zhang)
- configs: x86: build in VIRTIO_BLK and EXT4_FS for quick boot (Shile Zhang)
- configs: x86: disable TSX by default (Su Lifan)
- ck: sched: fix the bug that compile faid without CONFIG_GROUP_IDENTITY (Cruz Zhao)
- ck: sched: make up nr_high/under_running for cfs bandwidth (Michael Wang)
- ck: arm64: shield warn msgs when halt-poll is not supproted (Xin Hao)
- x86/cpufeatures: x86/msr: Add Intel SGX hardware bits (GuoRui.Yu)
- configs: cleanup default configs (Shile Zhang)
- Intel: dmaengine: idxd: clear MSIX permission entry on shutdown (Dave Jiang)
- Intel: dmaengine: idxd: add module parameter to force disable of SVA (Dave Jiang)
- Intel: x86/asm: Add a missing __iomem annotation in enqcmds() (Dave Jiang)
- Intel: x86/asm: Annotate movdir64b()'s dst argument with __iomem (Dave Jiang)
- Intel: dma: idxd: use DEFINE_MUTEX() for mutex lock (Zheng Yongjun)
- Intel: dmaengine: idxd: Fix list corruption in description completion (Dave Jiang)
- Intel: dmaengine: idxd: add IAX configuration support in the IDXD driver (Dave Jiang)
- Intel: dmaengine: idxd: add ATS disable knob for work queues (Dave Jiang)
- Intel: dmaengine: idxd: define table offset multiplier (Dave Jiang)
- Intel: dmaengine: idxd: Add ABI documentation for shared wq (Dave Jiang)
- Intel: dmaengine: idxd: Clean up descriptors with fault error (Dave Jiang)
- Intel: dmaengine: idxd: Add shared workqueue support (Dave Jiang)
- ck: sched: enable group identity (Cruz Zhao)
- ck: sched: fix the performence regression caused by update_rq_on_expel() (Cruz Zhao)
- ck: sched: Introduce sched_feat ID_LAST_HIGHCLASS_STAY (Cruz Zhao)
- ck: sched: fix the bug that nr_high_running underflow (Cruz Zhao)
- ck: sched: rescue the expellee on migration (Michael Wang)
- ck: sched: introduce 'idle seeker' and ID_IDLE_AVG (Michael Wang)
- ck: sched: introduce group identity 'idle saver' (Michael Wang)
- ck: sched: introduce group identity 'smt expeller' (Michael Wang)
- ck: sched: introduce per-cgroup identity (Michael Wang)
- uprobes: (Re)add missing get_uprobe() in __find_uprobe() (Sven Schnelle)
- ck: cpuinfo: Add cpuinfo support of cpu quota and cpu shard (Xunlei Pang)
- ck: make the rich container support k8s. (Xunlei Pang)
- ck: sysctl: set different default value for rich container. (Erwei Deng)
- ck: configs: Add RICH_CONTAINER_CG_SWITCH and SCHEDSTATS_HOST (Erwei Deng)
- ck: sched: Introduce load 1/5/15 for running tasks (Xunlei Pang)
- ck: sched: get_sched_lat_count_idx optimization (Erwei Deng)
- ck: sched: Finer grain of sched latency (Yihao Wu)
- ck: sched: Add "nr" to sched latency histogram (Yihao Wu)
- ck: sched: Add cgroup's scheduling latency histograms (Yihao Wu)
- ck: sched: Add cgroup-level blocked time histograms (Yihao Wu)
- ck: Introduce cfs scheduling latency histograms (Yihao Wu)
- ck: cpuacct: make cpuacct record nr_migrations (Yihao Wu)
- ck: sched/fair: Add sched_cfs_statistics to export some (Xunlei Pang)
- ck: sched/fair: Add parent_wait_contrib statistics (Xunlei Pang)
- ck: meminfo: Add meminfo support for rich container (Xunlei Pang)
- ck: Revert "sched/debug: Use task_pid_nr_ns in /proc/$pid/sched" (Xunlei Pang)
- ck: sched: introduce asynchronous cgroup load calculation. (Michael Wang)
- ck: sched: Add SLI switch for cpuacct (Yihao Wu)
- ck: fs,quota: Restrict privileged hardlimit in rich container (Xunlei Pang)
- ck: pidstat: Add task uptime support for rich container (Xunlei Pang)
- ck: proc/uptime: Add uptime support for rich container (Xunlei Pang)
- ck: proc/loadavg: Add load support for rich containr (Xunlei Pang)
- ck: proc/stat: Add top support for rich container (Xunlei Pang)
- ck: pidns: Support rich container switch on/off (Xunlei Pang)
- ck: sysfs/cpu: Add online cpus support for rich container (Xunlei Pang)
- ck: arm64: cpuinfo: Add cpuinfo support for rich container (zou cao)
- ck: x86: cpuinfo: Add cpuinfo support for rich (Xunlei Pang)
- ck: configs: Enable rich container (Xunlei Pang)
- ck: sched: Introduce per-cgroup iowait accounting (Yihao Wu)
- ck: sched: Introduce per-cgroup steal accounting (Yihao Wu)
- ck: cpuset: fix frame size longer than 2048 in update_cpumasks_hier (Erwei Deng)
- ck: sched: Introduce per-cgroup idle accounting (Yihao Wu)
- ck: cpuacct: Export nr_running & nr_uninterruptible (Yihao Wu)
- ck: sched/cputime: Fix guest cputime of cpuacct.proc_stat (Shanpei Chen)
- ck: cpuacct/proc_stat: Consider isolcpus (Xunlei Pang)
- ck: cpuacct: export cpuacct.proc_stat interface (Xunlei Pang)
- ck: sched: Maintain "nr_uninterruptible" in runqueue (Xunlei Pang)
- ck: configs: enable CONFIG_SCHED_SLI (Yihao Wu)
- ck: sched: add kconfig SCHED_SLI (Yihao Wu)
- ck: mm, memcg: introduce per memcg switch to reap zombie background (Xu Yu)
- ck: mm, memcg: Provide users the ability to reap zombie memcgs (Xunlei Pang)
- ck: configs: Support kidled (Gavin Shan)
- ck: kidled: introduce local and accumulated idle_page_stats (Xu Yu)
- ck: mm: Support kidled (Gavin Shan)
- ck: arm64: fix numa distance for HiSilicon chips (Rongwei Wang)
- ck :config: arm64: enable SDEI and NMI_WATCHDOG (Xin Hao)
- sdei_watchdog: avoid possible false hardlockup (Xiongfeng Wang)
- openEuler: kprobes/arm64: Blacklist sdei watchdog callback functions (Xiongfeng Wang)
- openEuler: sdei_watchdog: set secure timer period base on 'watchdog_thresh' (Xiongfeng Wang)
- openEuler: sdei_watchdog: clear EOI of the secure timer before kdump (Xiongfeng Wang)
- openEuler: sdei_watchdog: refresh 'last_timestamp' when enabling nmi_watchdog (Xiongfeng Wang)
- openEuler: watchdog: add nmi_watchdog support for arm64 based on SDEI (Xiongfeng Wang)
- openEuler: lockup_detector: init lockup detector after all the init_calls (Xiongfeng Wang)
- openEuler: firmware: arm_sdei: make 'sdei_api_event_disable/enable' public (Xiongfeng Wang)
- openEuler: firmware: arm_sdei: add interrupt binding api (Xiongfeng Wang)
- openEuler: watchdog: make hardlockup detect code public (Xiongfeng Wang)
- perf/x86/intel/uncore: Generic support for the MMIO type of uncore blocks (Kan Liang)
- perf/x86/intel/uncore: Generic support for the PCI type of uncore blocks (Kan Liang)
- perf/x86/intel/uncore: Rename uncore_notifier to uncore_pci_sub_notifier (Kan Liang)
- perf/x86/intel/uncore: Generic support for the MSR type of uncore blocks (Kan Liang)
- perf/x86/intel/uncore: Parse uncore discovery tables (Kan Liang)
- perf/x86/intel/uncore: With > 8 nodes, get pci bus die id from NUMA info (Steve Wahl)
- perf/x86/intel/uncore: Store the logical die id instead of the physical die id. (Steve Wahl)
- rbtree, timerqueue: Use rb_add_cached() (Peter Zijlstra)
- rbtree, rtmutex: Use rb_add_cached() (Peter Zijlstra)
- rbtree, uprobes: Use rbtree helpers (Peter Zijlstra)
- rbtree, perf: Use new rbtree helpers (Peter Zijlstra)
- rbtree, sched/deadline: Use rb_add_cached() (Peter Zijlstra)
- rbtree, sched/fair: Use rb_add_cached() (Peter Zijlstra)
- rbtree: Add generic add and find helpers (Peter Zijlstra)
- ck: mm: memcontrol: enable oom.group on cgroup-v1 (Wenwei Tao)
- ck: mm: memcontrol: introduce memcg priority oom (Wenwei Tao)
- ck: kernel: cgroup: account number of tasks in the css and its descendants (Wenwei Tao)
- ck: EDAC/mce_amd: Print !SMCA processor warning for 0x19 family (Zelin Deng)
- ck: configs: arm64: mpam: enable mpam func default (Xin Hao)
- openEuler: arm64/mpam: Backport MPAM supports  (Xin Hao)
- ck: io_uring: keep IORING_SETUP_SQPOLL_PERCPU consistent with liburing (Joseph Qi)
- virtio-net: support XDP when not more queues (Xuan Zhuo)
- ck: virtio: add module option to force_xdp (Xuan Zhuo)

* Mon May 10 2021 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.23-6.al8]
- ck: jbd2: jbd2_seq_stats_next should increase position index (Xiaoguang Wang)
- ck: arm64: Pass an appropriate invalidate physical address to KVM (Baolin Wang)
- locking/qrwlock: Fix ordering in queued_write_lock_slowpath() (Ali Saidi)
- Revert "ck: workaround for ev lost inside epoll" (Xuan Zhuo)
- cpupower: Add cpuid cap flag for MSR_AMD_HWCR support (Nathan Fontenot)
- cpupower: Remove family arg to decode_pstates() (Nathan Fontenot)
- cpupower: Condense pstate enabled bit checks in decode_pstates() (Nathan Fontenot)
- cpupower: Update family checks when decoding HW pstates (Nathan Fontenot)
- cpupower: Remove unused pscur variable. (Nathan Fontenot)
- cpupower: Add CPUPOWER_CAP_AMD_HW_PSTATE cpuid caps flag (Nathan Fontenot)
- cpupower: Correct macro name for CPB caps flag (Robert Richter)
- cpupower: Update msr_pstate union struct naming (Nathan Fontenot)

* Fri Apr 16 2021 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.23-5.al8]
- configs: x86: resize log buffer (Shile Zhang)

* Mon Apr 12 2021 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.23-4.al8]
- ck: Revert "virtio_net: enable napi_tx by default" (Su Lifan)
- ck: livepatch: Add atomic replace with stop machine model (xiejingfeng)

* Thu Apr 01 2021 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.23-4.rc2.al8]
- ck: workaround for ev lost inside epoll (Xuan Zhuo)
- ck: blk: fix wrong usage about __rq_for_each_bio() (Xiaoguang Wang)
- ck: livepatch: support forced patching with stop machine model (xiejingfeng)
- ck: livepatch: adjust some printing level (xiejingfeng)

* Fri Mar 12 2021 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.23-4.rc1.al8]
- bump kernel to 5.10.23
- ck: locking/qspinlock/x86: Fix performance regression caused by virt_spin_lock (Yihao Wu)
- ck: blk: add iohang check function (Xiaoguang Wang)
- ck: block: add counter to track io request's d2c time (Xiaoguang Wang)
- ck:livepatch: remove the pre_patch_callback/post_unpatch_callback from the stop_machine context (xiejingfeng)
- ck: livepatch: introduce stop machine consistency model (xiejingfeng)
- Intel: 5G ISA: x86: Enumerate AVX512 FP16 CPUID feature flag (Luming Yu)
- Intel: perf/x86/intel: Support CPUID 10.ECX to disable fixed counters (Kan Liang)
- Intel: perf/x86/intel: Add perf core PMU support for Sapphire Rapids (Kan Liang)
- Intel: perf/x86/intel: Filter unsupported Topdown metrics event (Kan Liang)
- Intel: perf/x86/intel: Factor out intel_update_topdown_event() (Kan Liang)
- Intel: perf/core: Add PERF_SAMPLE_WEIGHT_STRUCT (Kan Liang)
- Intel: perf/intel: Remove Perfmon-v4 counter_freezing support (Peter Zijlstra)
- Intel EDAC/i10nm: Add Intel Sapphire Rapids server support (Qiuxu Zhuo)
- Intel: EDAC: Add DDR5 new memory type (Qiuxu Zhuo)
- ck: configs: livepatch/core: introduce consistency model macro (xiejingfeng)
- ck: livepatch/core: introduce consistency model macro (xiejingfeng)
- ck: kernel: Reduce tasklist_lock contention at fork and exit (Xunlei Pang)
- ck: cpuidle-haltpoll: Switch on cpuidle-haltpoll by default (Yihao Wu)
- ck: configs: arm64: enable reliable stacktraces (Xin Hao)
- ck: arm64: add reliable stacktraces support (Xin Hao)
- ck: sched/fair: Introduce init buffer into CFS burst (Huaixin Chang)
- ck: sched/fair: Add document for burstable CFS bandwidth control (Huaixin Chang)
- ck: sched/fair: Add cfs bandwidth burst statistics (Huaixin Chang)
- ck: sched/fair: Make CFS bandwidth controller burstable (Huaixin Chang)
- ck: sched/fair: Introduce primitives for CFS bandwidth burst (Huaixin Chang)
- ck: panic: change the default value of crash_kexec_post_notifiers to true (Shile Zhang)
- ck: sched: disable auto group by default (Shanpei Chen)
- configs: net: Enable mptcp (Xuan Zhuo)
- ck: arm64: Support the VCPU preemption check (Jerry Yao)
- ck: arm64: Add interface to support VCPU preempted check (Jerry Yao)
- ck: arm64: Re-define the hypervisor call of pv poll control (Baolin Wang)
- ck: arm64: Fix the incorrect base address (Baolin Wang)
- configs: arm64: Enable the halt polling on ARM64 architecture (Baolin Wang)
- ck: arm64: Add new configs to support halt polling (Baolin Wang)
- ck: cpuidle: Allow enabling the halt polling on ARM64 (Baolin Wang)
- ck: arm64: Add new parameter to control halt polling (Baolin Wang)
- ck: arm64: Introduce cpuidle_haltpoll.h header file (Baolin Wang)
- ck: arm64: Add kvm_para.h header file (Baolin Wang)
- ck: arm64: Introduce default_idle() interface (Baolin Wang)
- ck: nitro_enclaves: Split mmio region and increase EIF_LOAD_OFFSET (Jason Cai (Xiang Feng))
- ck: io_uring: support multiple rings to share same poll thread by specifying same cpu (Xiaoguang Wang)
- io_uring: refactor io_sq_thread() handling (Xiaoguang Wang)

* Fri Feb 26 2021 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.18-3.al8]
- bump kernel to 5.10.18
- configs: x86: change preempt model to none for preformance tuning (Shile Zhang)
- ck: arm64: Introduce command line parameter to disable CNP (Vladimir Murzin)
- configs: enable kexec signature (Shile Zhang)
- configs: arm64: enable pvpanic (Shile Zhang)
- configs: arm64: enable gpio keys (Shile Zhang)
- configs: x86: shrink log buffer for memory saving (Shile Zhang)
- configs: update config to add PCIE EDR support (Zelin Deng)
- ck: pci/iohub-sriov: fix iohub-sriov VF probe issue (shanghui.lsh)
- ck: introduce deferred_meminit boot parameter (chenxiangzuo)
- ck: random: speed up the initialization of module (Xingjun Liu)
- ck: random: introduce the initialization seed (Xingjun Liu)
- ck: arm64: Support PV poll control feature (Baolin Wang)
- configs: enable config for TCP_RT (Xuan Zhuo)
- ck: tcp_rt module: add tcp_rt module (Xuan Zhuo)
- ck: add tcprt framework to kernel (Xuan Zhuo)
- ck: mm: add an interface to adjust the penalty time dynamically (zhongjiang-ali)
- ck: mm: support swap.high for cgroup v1 (zhongjiang-ali)
- ck: psi: Support PSI under cgroup v1 (Xunlei Pang)
- ck: mm,memcg: record latency of memcg wmark reclaim (Xu Yu)
- ck: mm: count the memory pressure when wmark meets. (zhongjiang-ali)
- ck: mm: memcontrol: make distance between wmark_low and wmark_high configurable (Yang Shi)
- ck: mm: vmscan: make memcg kswapd set memcg state to dirty or writeback (Yang Shi)
- ck: mm: memcontrol: treat memcg wmark reclaim work as kswapd (Yang Shi)
- ck: mm: memcontrol: add background reclaim support for cgroupv2 (Yang Shi)
- ck: mm: memcontrol: support background async page reclaim (Yang Shi)
- configs: arm64: enable tk_core cacheline aligned (Xin Hao)
- ck: arm64: adjust tk_core memory layout (Xin Hao)
- ck: mm, memcg: make direct swapin latency more accurate (Xu Yu)
- ck: mm, memcg: optimize eventfds with rculist (Xu Yu)
- ck: mm, memcg: introduce memsli monitor (Xu Yu)
- configs: enable CONFIG_MEMSLI (Xu Yu)
- ck: mm, memcg: record memory stall latency in every memcg (Xu Yu)
- ck: net: track the pid who created socks (Tony Lu)
- ck: virtio_net: introduce TX timeout dev_watchdog handler (Tony Lu)
- configs: bump kernel to 5.10.12 (Shile Zhang)
- configs: enable CONFIG_DEBUG_INFO_BTF config (Qiao Ma)
- configs: enable SMC as a module (Dust Li)
- ck: udp: Add udp sockopt UDP_USE_WRITE_QUEUE (Cambda Zhu)
- ck: udp: add uhash4 for established connection (Cambda Zhu)
- configs: enable CONFIG_TCP_SYNACK_TIMEOUT_PROC (Qiao Ma)
- ck: tcp: Add sysctl for tcp syn/synack retrans timeout (Cambda Zhu)
- configs: enable CONFIG_TCP_INIT_CWND_PROC (Qiao Ma)
- ck: tcp: Add sysctl for TCP_INIT_CWND (Cambda Zhu)
- ck: net/sched: Output Tx queue state on timeout (Gavin Shan)
- ck: tcp: Add feature support for tunable tcp delayed ack (George Zhang)
- ck: tcp: add tcp_tw_timeout_inherit system wide option (George Zhang)
- ck: net/tcp: Support tunable tcp timeout value in TIME-WAIT state (George Zhang)

* Mon Jan 11 2021 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.6-2.al8]
- bump kernel to 5.10.6
- configs: arm64: disable CONFIG_ARM64_TAGGED_ADDR_ABI for performance tuning (Shile Zhang)
- configs: arm64: disable ARMv8.3/4/5 features support for performance tuning (Shile Zhang)
- configs: arm64: disable CONFIG_ARM64_PSEUDO_NMI for performance tuning (Shile Zhang)
- configs: arm64: Apply the Alinux customized configs (Shile Zhang)
- ck: hotfix: Add Kernel hotfix enhancement (xiejingfeng)
- ck: hookers: add arm64 dependency (Zou Cao)
- ck: Hookers: add arm64 support (Zou Cao)
- ck: net/hookers: fix link error with ipv6 disabled (kbuild test robot)
- ck: net: kernel hookers service for toa module (George Zhang)
- ck: arm64: use __kernel_text_address to tell unwind (Zou Cao)
- ck: arm64: add livepatch support (Zou Cao)
- ck: Revert "kallsyms: unexport kallsyms_lookup_name() and kallsyms_on_each_symbol()" (Xin Hao)
- ck: mm: add ratelimit printk to prevent softlockup (zhongjiang-ali)
- ck: mm: limit the print message frequency when memcg oom triggers (zhongjiang-ali)
- ck: mm: restrict the print message frequency further when memcg oom triggers. (zhongjiang-ali)
- ck: block: replace reserved field with extended bio_flags (zhongjiang-ali)
- ck: block: fix inflight statistics of part0 (Jeffle Xu)
- io_uring: add timeout support for io_uring_enter() (Hao Xu)
- io_uring: only wake up sq thread while current task is in io worker context (Xiaoguang Wang)
- ck: fuse: add sysfs api to flush processing queue requests (Ma Jie Yue)
- ck: block-throttle: only do io statistics if needed (Hao Xu)
- ck: blk-throttle: limit bios to fix amount of pages entering writeback prematurely (Xiaoguang Wang)
- ck: block-throttle: add counters for completed io (Jiufei Xue)
- ck: block-throttle: code cleanup (Jiufei Xue)
- ck: blk-throttle: add throttled io/bytes counter (Joseph Qi)
- ck: blk-throttle: fix tg NULL pointer dereference (Joseph Qi)
- ck: blk-throttle: support io delay stats (Joseph Qi)
- ck: fs: record page or bio info while process is waitting on it (Xiaoguang Wang)
- ck: io_uring: revert io_file_supports_async() (Hao Xu)
- dm: add support for DM_TARGET_NOWAIT for various targets (Jeffle Xu)
- ck: io_uring: support ioctl (Hao Xu)
- ck: configs: add and enable CONFIG_PCI_IOHUB_SRIOV (Hao Xu)
- ck: pci/iohub-sriov: Support for Alibaba PCIe IOHub SRIOV (liushanghui)
- ck: mm: add proc interface to control context readahead (Xiaoguang Wang)
- ck: jbd2: fix build errors (Xiaoguang Wang)
- ck: jbd2: fix build warnings (Joseph Qi)
- ck: jbd2: track slow handle which is preventing transaction committing (Xiaoguang Wang)
- ck: iocost: fix format mismatch build warning (Joseph Qi)
- ck: jbd2: fix build warning on i386 (Joseph Qi)
- ck: jbd2/doc: fix new kernel-doc warning (Joseph Qi)
- ck: iocost: add ioc_gq stat (Jiufei Xue)
- ck: nvme-pci: Disable dicard zero-out functionality on Intel's P3600 NVMe disk drive (Wenwei Tao)
- ck: memcg: Point wb to root memcg/blkcg when offlining to avoid zombie (Xunlei Pang)
- ck: jbd2: add proc entry to control whether doing buffer copy-out (Hao Xu)
- ck: ext4: don't submit unwritten extent while holding active jbd2 handle (Xiaoguang Wang)
- ck: fs,ext4: remove projid limit when create hard link (zhangliguang)
- ck: jbd2: add new "stats" proc file (Xiaoguang Wang)
- ck: jbd2: create jbd2-ckpt thread for journal checkpoint (Joseph Qi)
- ck: iocost: add legacy interface file (Jiufei Xue)
- ck: virtio_ring: Support using kernel booting parameter when compiled as module (Shannon Zhao)
- ck: writeback: memcg_blkcg_tree_lock can be static (kbuild test robot)
- ck: fs/writeback: wrap cgroup writeback v1 logic (Hao Xu)
- ck: writeback: introduce cgwb_v1 boot param (Jiufei Xue)
- ck: fs/writeback: Attach inode's wb to root if needed (luanshi)
- ck: fs/writeback: fix double free of blkcg_css (Jiufei Xue)
- ck: writeback: add debug info for memcg-blkcg link (Jiufei Xue)
- ck: writeback: add memcg_blkcg_link tree (Jiufei Xue)
- ck: kconfig: Disable x86 clocksource watchdog (Jiufei Xue)
- ck: drivers/virtio: add vring_force_dma_api boot param (Eryu Guan)
- ck: block-throttle: enable hierarchical throttling even on traditional hierarchy (Joseph Qi)

* Tue Dec 15 2020 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.1-1.al8]
- bump kernel to 5.10.1

* Mon Dec 07 2020 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.0-rc7.al8]
- bump kernel to 5.10.0-rc7

* Mon Nov 30 2020 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.0-rc6.al8]
- bump kernel to 5.10.0-rc6

* Mon Nov 23 2020 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.0-rc5.al8]
- bump kernel to 5.10.0-rc5

* Mon Nov 16 2020 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.0-rc4.al8]
- bump kernel to 5.10.0-rc4

* Mon Nov 09 2020 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.0-rc3.al8]
- bump kernel to 5.10.0-rc3

* Tue Nov 03 2020 Shile Zhang <shile.zhang@linux.alibaba.com> [5.10.0-rc2.al8]
- bump kernel to 5.10.0-rc2
- initial kernel for Alibaba Cloud Linux 3
