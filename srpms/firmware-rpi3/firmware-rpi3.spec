%global debug_package %{nil}

Name:         firmware-rpi3
License:      MulanPSLv2 
Version:      20180522
Release:      1.0.3%{?dist}
Summary:      firmware for raspberry pi
Group:        FIXME
Source0:      firmware-1.%{version}.tar.gz
Source1:      config.txt
Source2:      cmdline.txt
Source3:      bcm2837-rpi-3-b.dtb
Source4:      bcm2837-rpi-3-b-plus.dtb

%description
firmware for raspberry pi3


%package boot
Summary:      firmware for raspberry pi3 -- bootfiles
Group:        FIXME

%description boot
firmware for raspberry pi3 -- bootfiles

%prep
%setup -q -n firmware-1.%{version}

%build
# nothing -- this is pre-built binary stuff ...

%install
# /boot
mkdir -p %{buildroot}/boot/efi/broadcom
(cd boot; cp -av LICENCE.broadcom		\
	bootcode.bin fixup*.dat start*.elf	\
	%{buildroot}/boot/efi)
cp -v %{SOURCE1} %{SOURCE2} %{buildroot}/boot/efi
cp -v %{SOURCE3} %{SOURCE4} %{buildroot}/boot/efi/broadcom

%files boot
/boot/efi/LICENCE.broadcom
/boot/efi/bootcode.bin
/boot/efi/fixup*.dat
/boot/efi/start*.elf
/boot/efi/broadcom/bcm2837-rpi-3-b.dtb
/boot/efi/broadcom/bcm2837-rpi-3-b-plus.dtb
%config(noreplace) /boot/efi/*.txt

%changelog
* Mon Oct 03 2022 Willard Hu <mingkunhu2000@gmail.com> - 20221003-1.0.3 
- Initial build
