# 项目信息

## 项目名称： 将Vscode包集成到龙蜥生态
## 方案描述： 
目标是为Anolis OS8构建vscode的rpm包。首先是编写spec，利用mock工具完成本地构建；其次对构建过程中需要联网下载的内容进行预先处理，通过不同的形式提供给koji，完成离线构建；最终对构建好的rpm包进行安装测试。

## 时间规划：
|日期|计划|完成情况|
|--|--|--|
|7月1日-8月1日|学习rpm工具，梳理vscode依赖，打包部分依赖|:heavy_check_mark:|
|8月1日-9月5日|计划完成x86_64和aarch64架构的vscode打包|:heavy_check_mark:|
|9月15日-9月30日|结项报告&&文档整理|:heavy_check_mark:|

# 项目总结
## 项目产出
完成vscode、yarnpkg的集成工作，编写spec，完成包构建与测试

| 名称 | PR |
| ---- | ---- |
| vscode  |  https://gitee.com/src-anolis-os/vscode/pulls/1   |
| yarnpkg |  https://gitee.com/src-anolis-sig/yarnpkg/pulls/2  |
|   文档   |https://gitee.com/aa1hshh/ospp2022|

vscode的rpm包已经合入epao
|架构|链接|
|--|--|
|x86_64|https://mirrors.openanolis.cn/epao/8/x86_64/Packages/vscode-1.68.0-1.an8.x86_64.rpm|
|aarch64|https://mirrors.openanolis.cn/epao/8/aarch64/Packages/vscode-1.68.0-1.an8.aarch64.rpm|

## 方案进度
已完成x86_64和aarch64架构的vscode包构建
## 问题及解决方案
详见开发记录里边`遇到的问题`以及`开发记录&&问题整理`部分[链接](https://gitee.com/aa1hshh/ospp2022#%E9%81%87%E5%88%B0%E7%9A%84%E9%97%AE%E9%A2%98)
## 项目完成质量

项目开发和文档编写工作均已完成，已经高质量完成了本次项目。

## 与导师沟通情况
与导师沟通顺畅，重点开发阶段一天2-3次沟通，平时一周1-2次沟通。





# 活动总结

之前在openEuler社区参与过RISC-V架构rpm包的测试工作，但对具体的spec编写，排错的方法并不了解。看到开源之夏-龙蜥社区OpenAnolis发布了相关的项目，就想着借助项目开发学习下如何将一款上游软件集成到Linux发行版的方法。

初期根据导师推荐的一些电子资源，动手实践学习rpm打包hello系的源代码；随后参考上游文档，源码本地编译运行并编写spec对vscode进行初步的打包验证。

发现社区koji需要离线构建之后，经过很多实验和验证(踩坑)，确定了以下方案：需要在线下载的包，比如yarn进行提前打包，直接写入BuildRequires；支持缓存的则在构建时将Source解压到对应的目录下；不支持缓存则修改源码，从指定目录读取。

之后利用mock工具创建独立环境分别为x86_64和aarch64架构构建vscode，最后在社区koji完成构建，相关rpm被合入epao。


# 展望

目前已经按照项目要求完成了将Vscode包集成到龙蜥生态的任务。根据上游社区的说明，开源版本的vscode不提供扩展支持，而我认为扩展才是vscode编辑器的灵魂。目前上游社区的建议是手动下载扩展并安装，但这样比较麻烦，目前想法是以rpm的形式打包若干常见的扩展，方便用户使用。

# 致谢

通过本次活动，我动手实践将vscode合入了龙蜥社区仓库，参与了一个开源软件集成到Linux发行版的全过程，终于为开源社区合入了一份代码。我要特别感谢我的项目导师葛立伟，导师在我的开发过程中给予了我很多鼓励和帮助，没有导师的指导我很难有这样一个收获满满的夏天。最后感谢开源之夏活动，提供了这样一个从实践中学习的机会。



