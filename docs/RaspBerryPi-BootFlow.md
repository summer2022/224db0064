# 启动流程

## 第一阶段

- 上电后启动 GPU，执行保存在 ROM 中的代码，此时 CPU 处于复位状态，并没有启动

> 为何先启动 GPU？
>
> - Broadcom 想拥有主导权
>   - Broadcom VideoCore IV 具有代码处理能力
> - GPU 先启动让开机渲染画面快一些



- GPU 将 `bootcode.bin` 读取到 `L2 Cache`（128KB）中



## 第二阶段

- 执行 `bootcode.bin`
  - 初始化 RAM
  - 将 `start*.elf` 加载到RAM中



## 第三阶段

- `start*.elf` 负责加载SD card中的 `config.txt`

> `config.txt` 记录了配置信息，如CPU，GPU和RAM的频率



- `start*.elf` 将RAM分为两部分：CPU访问空间和GPU访问空间

> `config.txt` 在内存地址空间分配完成后才加载，因此，不可以在 `config.txt` 中更改内存地址的配置



- `start*.elf` 从SD card的第一个分区中加载 `cmdline.txt`

> `cmdline.txt` 保存的是启动kernel的参数



最后，`start.elf` 把 `kernel.img`，`ramdisk`，`dtb` 加载到内存的预定地址，然后向 CPU 发出重启信号。之后，CPU 就可以从内存的预定地址执行 kernel 的代码，进入了软件定义的系统启动流程。

## 细节补充

### 芯片型号

- 树莓派 3/3B --> BCM2837-RP3/3B
- 树莓派 4      --> BCM2835-RP4



### 树莓派4与前代产品的区别

- 树莓派 4 使用置于 EEPROM 的 `bootcode.bin`
- 早期产品使用置于文件系统 `/boot` 目录下的 `bootcode.bin`



### /boot目录下的内容

#### bootcode.bin

作为bootloader，执行基本设置后加载 `start*.elf`。



#### start*.elf

二进制固件，已经加载在 Broadcom 的 GPU（VideoCore）中，接管后面的启动过程。

> `start.elf` 是基本固件，`start_x.elf` 包括相机驱动程序和编解码器，`start_db.elf` 是固件的调试版本，`start_cd.elf` 是简化版本，不支持编解码器和3D之类的硬件块，并且在 `gpu_mem=16` 中指定时使用 `config.txt`。



####   fixup* .dat

链接器文件，并且与 `start*.elf` 上一节中列出的文件配对。



#### cmdline.txt

指定输出特定级别内核调试信息。



####  config.txt

配置参数，如显示参数和超频选项。



#### issue.txt

分发的日期和 git commit ID。



#### ssh.txt

如果存在此文件，则将在启动时启用 SSH。



#### kernel*.img

内核镜像



#### bcm*.dtb

包含 Raspberry Pi 各种模型的硬件定义，并在引导时根据检测到的启动模式设置内核。



#### overlays

在 `overlays` 子文件夹中包含设备树覆盖，这些用于配置可能连接到系统的各种硬件设备。
