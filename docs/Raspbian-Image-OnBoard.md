# RaspBerry Pi 3B 启动 Raspbian

## 烧录

### 工具

- Raspberry Pi Imager（推荐）
- Win32DiskImager



### 流程

以下按照树莓派官方提供的烧录工具 Raspberry Pi Imager 进行讲解



1. 将 SD 卡格式化为 FAT32 

   ![image-20220626224653010](./images/raspbian0.jpg)

2. 选择自己需要的镜像烧录

![image-20220626224820271](./images/raspbian1.jpg)

3. 高级设置（ 若后续使用 ssh，则需要进行相关设置 ）
   - 选中 ssh 选项，并设置相关密码
   - 配置 Wifi 选项



### 检查结果

若红灯常亮，绿灯闪烁，即为正常运行；否则，需要进一步定位错误

## 修改 rootfs

由于较新版本的 Raspbian 默认不开启 ssh 服务，所以需要进行一些修改

- 将 SD 卡插入电脑，打开 `boot`分区

- 添加空文件 `ssh`（ 无文件后缀 ）

  

## ssh 连接

通过 ssh 方式连接树莓派，有以下两种方式，本文使用较为简单的第一种

- 客户端与树莓派处于同一局域网
- 树莓派拥有外网 IP

### 工具

- MobaXterm（推荐）
- Xshell

- PuTTY



### 流程

1. 将树莓派通过网线连接至电脑
2. 将 Wifi 共享于树莓派
3. 通过 ssh 工具建立 Session
4. 默认登陆账户为 `pi`，密码为 `raspberry`



### 验证结果

- 使用 `ping` 命令，查看是否连通
- ssh 建立 Session 后，查看是否可以正常登入和使用



## Debug

### 查找树莓派 IP 地址

若上述的方法有误，则尝试在烧录工具的高级选项中选择 `raspberrypi.local`；然后使用 `ping` 命令测试连通性；最终，使用其显示的 *IPv6* 地址

### 烧录

烧录工具报错 `raspberry pi imager unable to customize, 'config.txt' does not existed`，可能为烧录工具的bug，可尝试以下解决方法：

- 尝试修改/减少高级选项
- 使用最新的 `rpi-imager`
- 查看 Github 上 `rpi-imager` 官方仓库的 issue
  - [Issue #289](https://github.com/raspberrypi/rpi-imager/issues/289)
  - [Issue #171](https://github.com/raspberrypi/rpi-imager/issues/171)





