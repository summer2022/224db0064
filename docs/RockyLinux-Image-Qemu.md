# 在 qemu 上运行 RockyRpi

## 环境

- Ubuntu 20.04 LTS

- qemu 6.0.0

- RockyRpi 8.5

  

## 步骤

### 安装qemu

- 下载 `wget https://download.qemu.org/qemu-6.0.0.tar.xz`

  

- 解压 `tar xvJf qemu-6.0.0.tar.xz`

  

- 配置 `./configure --target-list=arm-softmmu,aarch64-softmmu --enable-debug --enable-sdl`

  > `./configure --help` 查看配置相关选项和帮助信息
  >
  > 可能需要安装依赖包 `SDL2-devel` 和 `meson`

  

- 编译 `make`

  

- 安装 `make install`

  

- 查看版本 `qemu-system-aarch64 --version`

  

### 准备镜像

- 下载镜像，镜像目录为`IMG_DIR`



- 解压镜像 `xz -d IMG_NAME.img.xz`



- 提取 `dtb` 和 `kernel8.img`

  - 使用 losetup 将镜像文件虚拟成块设备 `losetup -f --show IMG_NAME.img`

  - 使用 kpartx 创建分区表 /dev/loop0的设备映射 `kpartx -va /dev/loop0`

  - 挂载镜像的 boot 分区 `mount /dev/mapper/loop0p1 BOOT_DIR`

  - 提取 dtb 和 kernel8.img 到 IMG_DIR

    ```bash
    cd IMG_DIR
    cp BOOT_DIR/kernel8.img ./
    cp BOOT_DIR/*.dtb ./
    ```



### 启动

- 查看可模拟的设备 `qemu-system-aarch64 -M help`，此处使用`raspi3`



- 启动

  ```bash
  cd (IMG_DIR)
  qemu-system-aarch64 -M raspi3 -cpu cortex-a53 -m 1024 -kernel kernel8.img -dtb bcm2710-rpi-3-b.dtb -sd (IMG_NAME.img)  -net user,hostfwd=tcp::6022-:22 -append "rw earlyprintk loglevl=8 console=ttyAMA0,115200 dwc_otg.lpm_enable=0 root=/dev/mmcblk0p3 rootwait  panic=1" -no-reboot -nographic
  ```

  

### 进入系统

- 启动较慢，耐心等待

- 默认账户

  > 用户名：rocky
  >
  > 密码：rockylinux



## 细节

### Debug

- **qemu 4.xx **无法启动，换成**6.xx** 解决问题
- **rockyrpi8.4** 失败，**rockyrpi8.5** 成功,尽可能使用新版本
- 构建和启动用 **sudo**
- rockyrpi8.5的镜像大小是 **3.54G**，启动前需要对齐到 **4G**



