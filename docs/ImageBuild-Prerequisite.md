# 镜像制作前置知识

## appliance-tools

appliance-creator, creates pre-installed appliance images and meta-data.



**appliance-creator** creates pre-installed appliance images from kickstart files. Kickstart files contain the information about packages and configurations that are built into the image.

## Kickstart 脚本

kickstart 是一个利用 Anaconda 工具实现服务器自动化安装的方法



### 通过 Kickstart 安装的过程

1. 创建一个 kickstart 文件

2. 创建有 kickstart 文件的引导介质或者使这个文件在网络上可用；

3. 筹备一个安装树

4. 简要过程：anconda 自身启动 =>选取 ks 安装模式 => 从 ks 文件读取配置 => 最后安装
5. 开始安装时，由光盘镜像上的 bootloader 引导加载内核，第二阶段的 bootloader 会去读取配置文件并把其中的参数传递给内核，内核再传递给 anaconda 程序



### 文件格式

#### 命令段

- 作用：指定各种安装前的配置选项，如键盘类型等

- 选项类型：必备命令+可选命令



- 必备命令：
  - authconfig：认证方式配置
  - bootloader：定义 bootloader 的安装位置及相关配置
  - keyboard：设置键盘类型
  - lang：设置语言类型
  - part：分区布局
  - clearpart：清除分区
  - volgroup：创建卷组
  - logvol：创建逻辑卷
  - rootpw：设置管理员密码
  - timezone：时区



- 可选命令：
  - install 或 upgrade：安装或升级
  - text：安装界面类型，text 为 TUI，默认为 GUI
  - network：配置网络接口
  - firewall：防火墙
  - halt、poweroff 或 reboot：定义安装完成之后的行为，如关机、重启
  - user：安装完成后为系统创建新用户
  - repo：指明安装时使用的 repository（安装源）
  - url：指明安装时使用的 repository（安装源），但为 url 格式





#### 程序包段

- 作用：指定要安装的程序包、包组以及不安装的程序包

- 表示方式：

%packages	程序包段的开始
@group_name	要安装的包组
package	要安装的程序包
-package	不安装的程序包
%end	程序包段的结束



|   关键字    |      含义      |
| :---------: | :------------: |
|  %packages  | 程序包段的开始 |
| @group_name |  要安装的包组  |
|   package   | 要安装的程序包 |
|  -package   | 不安装的程序包 |
|    %end     | 程序包段的结束 |


一个程序包被其他指定要安装的程序包所依赖，那么就算指明该程序不安装，系统也会自动安装这个程序包。



#### 脚本段

包括安装前要执行的脚本和安装后要执行的脚本



1. 安装前要执行的脚本：%pre

   ``` 
   %pre
   ...
   %end
   
   //运行环境：运行于安装介质上的微型Linux系统环境
   ```





2. 安装后要执行的脚本：%post

   ```
   %post
   ...
   %end
   
   //运行环境：安装完成后的系统
   ```



## SPEC 文件

- Spec 文件用于告诉 rpmbuild 如何构建 RPM 包

- Spec 文件包含 preamble 和 body 两部分，preamble 部分主要包含一些包的元数据，body 部分主要用于打包，安装等



### preamble

![img](./images/spec0.jpg)

构建后的包名为N-V-R（NAME-VERSION-RELEASE）



### body

![img](./images/spec1.jpg)




