# 知识补充

## SRPM 和 RPM 的区别

- RPM 的全名是 Red Hat Package Manager，原本是由 Red Hat 公司所发展的，流传甚广；
- RPM 类型的套件中，所含有的套件是经过编译后的 binary file ，所以可以直接安装在使用者端( Client )的系统上，不过，也由于如此，所以 RPM 对于安装者的环境要求相当严格；
- RPM 除了将套件安装至使用者的系统上之外，还会将该套件的版本、名称、档案与目录配置、系统需求等等均记录于数据库( /var/lib/rpm )当中，方便未来的查询与升级、移除；
- RPM 可针对不同的硬件等级来加以编译，制作出来的档案可于附档名( i386, i586, i686 )来分辨；
- RPM 最大的问题为套件之间的相依性问题；
- SRPM 为 Source RPM ，内含的档案为 Source code 而非为 binary file ，所以安装 SRPM 时还需要经过 compile ，不过，SRPM 最大的优点就是可以让使用者自行修改设定参数( makefile/configure 的参数 )，以符合使用者自己的 Linux 环境；



## scp 命令拷贝镜像

- scp 在本地和远程主机之间复制文件

## /etc/yum.repo.d/ 中 repo 文件相关支持

repo 文件是 Fedora 中 yum 源（软件仓库）的配置文件，通常一个 repo 文件定义了一个或者多个软件仓库的细节内容，例如我们将从哪里下载需要安装或者升级的软件包，repo 文件中的设置内容将被 yum 读取和应用



- `serverid`

  > 其中 serverid 是用于区别各个不同的 repository，必须有一个独一无二的名称

- `name`

  > 是对 repository 的描述，支持 *releasever* *basearch* 这样的变量

- `baseurl=url://path/to/repository/`

  > baseurl 是服务器设置中最重要的部分，只有设置正确，才能从上面获取软件。
  >
  > 它的格式是：
  > baseurl=url://server1/path/to/repository/
  > url://server2/path/to/repository/
  > url://server3/path/to/repository/
  >
  > 
  >
  > 其中 url 支持的协议有 http:// ftp:// file://三种。
  >
  > baseurl 后可以跟多个 url，你可以自己改为速度比较快的镜像站，但 baseurl 只能有一个，也就是说不能像如下格式：
  > baseurl=url://server1/path/to/repository/
  > baseurl=url://server2/path/to/repository/
  > baseurl=url://server3/path/to/repository/
  >
  > 其中 url 指向的目录必须是这个repository header目录的上一级，它也支持 *releasever* *basearch* 这样的变量。

- `mirrorlist`

  > 指定一个镜像服务器的地址列表，将 releasever 和 releasever 和 releasever 和 basearch 替换成自己对应的版本和架构，例如 10 和 i386，在浏览器中打开，我们就能看到一长串镜可用的镜像服务器地址列表。

- `enabled=[1 or 0]`

  > - 当某个软件仓库被配置成 enabled=0 时，yum 在安装或升级软件包时不会将该仓库做为软件包提供源。使用这个选项，可以启用或禁用软件仓库。
  >- 通过 yum 的 --enablerepo=[repo_name] 和 --disablerepo=[repo_name] 选项，或者通过 PackageKit 的"添加/删除软件"工具，也能够方便地启用和禁用指定的软件仓库

## spec & kickstart

### spec

- 告诉 rpmbuild 如何构建 RPM 或者 SRPM 包



### kickstart

- 配置磁盘分区
- 设定 repo
- 配置信息
- 配置 rpm
- 设置初始 boot 命令 （cmdline.txt）

- 设置内核
- 制作rootfs



## raspberrypi2 & RockyRpi

### repo 1

- 通过 spec 构建 RPM包
- 构建 rpi 内核



### repo 2

- 通过ks制作镜像
- 可以使用上述构建的 rpi 内核



### 总结

- 通过 patch 和 spec 制作适配 Anolis 的 RpiKernel
- 通过 ks 生成最终的 Anolis 镜像
