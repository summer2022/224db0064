# appliance-tools 调试

## 环境配置

### 软件

#### OS

- LSB Version: core-4.1-aarch64:core-4.1-noarch
- Distributor ID: AlibabaCloud
- Description: Alibaba Cloud Linux release 3 (Soaring Falcon)
- Release: 3
- Codename: SoaringFalcon
- Kernel: 5.10.84-10.4.al8.aarch64



#### 工具包

- yum: 4.4.2
- appliance-tools: 011.1-4.el8.noarch
- EPEL enabled

### 硬件

- Architecture: aarch64
- CPU: Neoverse-N1 (80 cores)
- RAM: 256GB

## Error Log

```
Backup stored at /var/lib/authselect/backups/2022-06-29-13-57-22.X4N6rg
Profile "sssd" was selected.
The following nsswitch maps are overwritten by the profile:
- passwd
- group
- netgroup
- automount
- services

Make sure that SSSD service is configured and enabled. See SSSD documentation for more information.

Adding port '22/tcp' to default zone.
success
Locking password for user root.
passwd: Success
WARNING! No bootloader found.
Changing password for user rocky.
passwd: all authentication tokens updated successfully.
400 files removed
cmdline.txt looks like this, please review:
console=ttyAMA0,115200 console=tty1 root=PARTUUID=2de685ad-03 rootfstype=ext4 elevator=deadline rootwait
mkswap: /dev/disk/by-uuid/0151e950-1dff-4131-8c43-29baf05fc71b: warning: wiping old swap signature.
Setting up swapspace version 1, size = 488 MiB (511700992 bytes)
LABEL=_swap, UUID=0151e950-1dff-4131-8c43-29baf05fc71b
/ 100.0%

Unmounting directory /var/tmp/imgcreate-z33fku3n/install_root/boot
Unmounting directory /var/tmp/imgcreate-z33fku3n/install_root/
Unable to create appliance : Failed to unmap partitions for '/dev/loop1'
Traceback (most recent call last):
  File "/bin/appliance-creator", line 156, in main
    creator.unmount()
  File "/usr/lib/python3.6/site-packages/imgcreate/creator.py", line 587, in unmount
    self._unmount_instroot()
  File "/usr/lib/python3.6/site-packages/appcreate/appliance.py", line 646, in _unmount_instroot
    self.__instloop.cleanup()
  File "/usr/lib/python3.6/site-packages/appcreate/partitionedfs.py", line 257, in cleanup
    self.__unmap_partitions()
  File "/usr/lib/python3.6/site-packages/appcreate/partitionedfs.py", line 226, in __unmap_partitions
    d['disk'].device)
imgcreate.errors.MountError: Failed to unmap partitions for '/dev/loop1'

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "/bin/appliance-creator", line 193, in <module>
    sys.exit(main())
  File "/bin/appliance-creator", line 160, in main
    creator.cleanup()
  File "/usr/lib/python3.6/site-packages/imgcreate/creator.py", line 613, in cleanup
    self.unmount()
  File "/usr/lib/python3.6/site-packages/imgcreate/creator.py", line 587, in unmount
    self._unmount_instroot()
  File "/usr/lib/python3.6/site-packages/appcreate/appliance.py", line 646, in _unmount_instroot
    self.__instloop.cleanup()
  File "/usr/lib/python3.6/site-packages/appcreate/partitionedfs.py", line 257, in cleanup
    self.__unmap_partitions()
  File "/usr/lib/python3.6/site-packages/appcreate/partitionedfs.py", line 226, in __unmap_partitions
    d['disk'].device)
imgcreate.errors.MountError: Failed to unmap partitions for '/dev/loop1'
Exception ignored in: <bound method ImageCreator.__del__ of <appcreate.appliance.ApplianceImageCreator object at 0xffff9335df98>>
Traceback (most recent call last):
  File "/usr/lib/python3.6/site-packages/imgcreate/creator.py", line 114, in __del__
    self.cleanup()
  File "/usr/lib/python3.6/site-packages/imgcreate/creator.py", line 613, in cleanup
    self.unmount()
  File "/usr/lib/python3.6/site-packages/imgcreate/creator.py", line 587, in unmount
    self._unmount_instroot()
  File "/usr/lib/python3.6/site-packages/appcreate/appliance.py", line 646, in _unmount_instroot
    self.__instloop.cleanup()
  File "/usr/lib/python3.6/site-packages/appcreate/partitionedfs.py", line 257, in cleanup
    self.__unmap_partitions()
  File "/usr/lib/python3.6/site-packages/appcreate/partitionedfs.py", line 226, in __unmap_partitions
    d['disk'].device)
imgcreate.errors.MountError: Failed to unmap partitions for '/dev/loop1'
```





## Full Log

```
[root@iZbp1ch91lmmx5kqvnwybmZ RockyRpi]# sudo ./Rocky8_Rpi4_mkimage.sh /home/AnolisRpi/reference/RockyImage
WARNING! grub package not found.
Extending sparse file /var/tmp/imgcreate-z33fku3n/tmp-msz00wro/RockyRpi-sda.raw to 3787456512
Losetup add /dev/loop1 mapping to /var/tmp/imgcreate-z33fku3n/tmp-msz00wro/RockyRpi-sda.raw
Formating ext4 filesystem on /dev/loop13
Formating args: ['mkfs.ext4', '-F', '-L', '_/', '-m', '1', '-b', '4096', '/dev/loop13']
Tuning filesystem on /dev/loop13
Mounting /dev/loop13 at /var/tmp/imgcreate-z33fku3n/install_root/
Formating vfat filesystem on /dev/loop11
Formating args: ['mkfs.vfat', '/dev/loop11']
Creating mount point /var/tmp/imgcreate-z33fku3n/install_root/boot
Mounting /dev/loop11 at /var/tmp/imgcreate-z33fku3n/install_root/boot
Setting up swapspace version 1, size = 488 MiB (511700992 bytes)
LABEL=_swap, UUID=0151e950-1dff-4131-8c43-29baf05fc71b
Skipping (/sys/fs/selinux,/sys/fs/selinux) because source doesn't exist.
rockyrpi                                                             36 kB/s | 124 kB     00:03
rockyextras                                                         206 kB/s | 324 kB     00:01
BaseOS                                                              1.4 MB/s | 2.1 MB     00:01
AppStream                                                           3.5 MB/s | 7.3 MB     00:02
PowerTools                                                          1.5 MB/s | 2.1 MB     00:01
selected group: core
selected package: 'net-tools'
selected package: 'cloud-utils-growpart'
selected package: 'raspberrypi2-kernel4'
selected package: 'vim'
selected package: 'raspberrypi2-firmware'
selected package: 'nano'
selected package: 'chrony'
selected package: 'rocky-release-rpi'
selected package: 'bash-completion'
selected package: 'NetworkManager-wifi'
(1/358): rocky-release-rpi-8.0-1.el8.noarch.rpm                      18 kB/s | 9.3 kB     00:00
(2/358): NetworkManager-1.36.0-4.el8.aarch64.rpm                    1.6 MB/s | 2.1 MB     00:01
(3/358): NetworkManager-libnm-1.36.0-4.el8.aarch64.rpm              3.6 MB/s | 1.8 MB     00:00
(4/358): raspberrypi2-firmware-5.15.34-v8.1.el8.aarch64.rpm         4.2 MB/s |  10 MB     00:02
(5/358): NetworkManager-team-1.36.0-4.el8.aarch64.rpm               584 kB/s | 149 kB     00:00
(6/358): NetworkManager-wifi-1.36.0-4.el8.aarch64.rpm               751 kB/s | 188 kB     00:00
(7/358): acl-2.2.53-1.el8.1.aarch64.rpm                             350 kB/s |  79 kB     00:00
(8/358): audit-3.0.7-2.el8.2.aarch64.rpm                            975 kB/s | 254 kB     00:00
(9/358): NetworkManager-tui-1.36.0-4.el8.aarch64.rpm                339 kB/s | 331 kB     00:00
(10/358): audit-libs-3.0.7-2.el8.2.aarch64.rpm                      469 kB/s | 117 kB     00:00
(11/358): authselect-1.2.2-3.el8.aarch64.rpm                        488 kB/s | 132 kB     00:00
(12/358): authselect-libs-1.2.2-3.el8.aarch64.rpm                   852 kB/s | 219 kB     00:00
(13/358): basesystem-11-5.el8.noarch.rpm                             40 kB/s | 9.3 kB     00:00
(14/358): bash-4.4.20-3.el8.aarch64.rpm                             3.9 MB/s | 1.5 MB     00:00
(15/358): bash-completion-2.7-5.el8.noarch.rpm                      815 kB/s | 272 kB     00:00
(16/358): brotli-1.0.6-3.el8.aarch64.rpm                            1.2 MB/s | 313 kB     00:00
(17/358): bzip2-libs-1.0.6-26.el8.aarch64.rpm                       192 kB/s |  47 kB     00:00
(18/358): c-ares-1.13.0-6.el8.aarch64.rpm                           364 kB/s |  91 kB     00:00
(19/358): ca-certificates-2021.2.50-80.0.el8_4.noarch.rpm           1.2 MB/s | 389 kB     00:00
(20/358): raspberrypi2-kernel4-5.15.34-v8.1.el8.aarch64.rpm         4.9 MB/s |  24 MB     00:04
(21/358): chkconfig-1.19.1-1.el8.aarch64.rpm                        729 kB/s | 196 kB     00:00
(22/358): chrony-4.1-1.el8.aarch64.rpm                              1.0 MB/s | 314 kB     00:00
(23/358): cpio-2.12-11.el8.aarch64.rpm                              972 kB/s | 259 kB     00:00
(24/358): coreutils-common-8.30-12.el8.aarch64.rpm                  4.5 MB/s | 2.0 MB     00:00
(25/358): cracklib-2.9.6-15.el8.aarch64.rpm                         361 kB/s |  92 kB     00:00
(26/358): crda-3.18_2020.04.29-1.el8.noarch.rpm                      94 kB/s |  23 kB     00:00
(27/358): cracklib-dicts-2.9.6-15.el8.aarch64.rpm                   5.8 MB/s | 4.0 MB     00:00
(28/358): cronie-1.5.2-6.el8.aarch64.rpm                            459 kB/s | 116 kB     00:00
(29/358): coreutils-8.30-12.el8.aarch64.rpm                         1.0 MB/s | 1.2 MB     00:01
(30/358): cronie-anacron-1.5.2-6.el8.aarch64.rpm                    173 kB/s |  41 kB     00:00
(31/358): crypto-policies-20211116-1.gitae470d6.el8.noarch.rpm      291 kB/s |  63 kB     00:00
(32/358): crontabs-1.11-17.20190603git.el8.noarch.rpm               102 kB/s |  24 kB     00:00
(33/358): crypto-policies-scripts-20211116-1.gitae470d6.el8.noarch. 344 kB/s |  82 kB     00:00
(34/358): cryptsetup-libs-2.3.7-2.el8.aarch64.rpm                   1.8 MB/s | 471 kB     00:00
(35/358): curl-7.61.1-22.el8.aarch64.rpm                            1.3 MB/s | 346 kB     00:00
(36/358): cyrus-sasl-lib-2.1.27-6.el8_5.aarch64.rpm                 517 kB/s | 121 kB     00:00
(37/358): dbus-1.12.8-18.el8.aarch64.rpm                            174 kB/s |  40 kB     00:00
(38/358): dbus-common-1.12.8-18.el8.noarch.rpm                      189 kB/s |  45 kB     00:00
(39/358): dbus-daemon-1.12.8-18.el8.aarch64.rpm                     982 kB/s | 232 kB     00:00
(40/358): dbus-glib-0.110-2.el8.aarch64.rpm                         540 kB/s | 121 kB     00:00
(41/358): dbus-libs-1.12.8-18.el8.aarch64.rpm                       686 kB/s | 175 kB     00:00
(42/358): dbus-tools-1.12.8-18.el8.aarch64.rpm                      357 kB/s |  83 kB     00:00
(43/358): device-mapper-1.02.181-3.el8.aarch64.rpm                  1.4 MB/s | 372 kB     00:00
(44/358): device-mapper-libs-1.02.181-3.el8.aarch64.rpm             1.3 MB/s | 396 kB     00:00
(45/358): diffutils-3.6-6.el8.aarch64.rpm                           1.4 MB/s | 351 kB     00:00
(46/358): dmidecode-3.3-4.el8.aarch64.rpm                           341 kB/s |  72 kB     00:00
(47/358): dnf-plugins-core-4.0.21-11.el8.noarch.rpm                 334 kB/s |  70 kB     00:00
(48/358): dnf-data-4.7.0-8.el8.noarch.rpm                           647 kB/s | 154 kB     00:00
(49/358): dnf-4.7.0-8.el8.noarch.rpm                                1.5 MB/s | 540 kB     00:00
(50/358): dracut-config-rescue-049-201.git20220131.el8.aarch64.rpm  262 kB/s |  60 kB     00:00
(51/358): dracut-049-201.git20220131.el8.aarch64.rpm                1.5 MB/s | 372 kB     00:00
(52/358): dracut-network-049-201.git20220131.el8.aarch64.rpm        406 kB/s | 108 kB     00:00
(53/358): dracut-squash-049-201.git20220131.el8.aarch64.rpm         269 kB/s |  61 kB     00:00
(54/358): e2fsprogs-1.45.6-4.el8.aarch64.rpm                        3.3 MB/s | 1.0 MB     00:00
(55/358): e2fsprogs-libs-1.45.6-4.el8.aarch64.rpm                   781 kB/s | 226 kB     00:00
(56/358): elfutils-debuginfod-client-0.186-1.el8.aarch64.rpm        313 kB/s |  69 kB     00:00
(57/358): elfutils-default-yama-scope-0.186-1.el8.noarch.rpm        212 kB/s |  49 kB     00:00
(58/358): elfutils-libelf-0.186-1.el8.aarch64.rpm                   805 kB/s | 226 kB     00:00
(59/358): elfutils-libs-0.186-1.el8.aarch64.rpm                     955 kB/s | 285 kB     00:00
(60/358): ethtool-5.13-1.el8.aarch64.rpm                            841 kB/s | 210 kB     00:00
(61/358): expat-2.2.5-8.el8.aarch64.rpm                             354 kB/s | 102 kB     00:00
(62/358): file-libs-5.33-20.el8.aarch64.rpm                         1.9 MB/s | 539 kB     00:00
(63/358): filesystem-3.8-6.el8.aarch64.rpm                          3.1 MB/s | 1.1 MB     00:00
(64/358): findutils-4.6.0-20.el8.aarch64.rpm                        1.5 MB/s | 523 kB     00:00
(65/358): firewalld-0.9.3-13.el8.noarch.rpm                         1.9 MB/s | 502 kB     00:00
(66/358): firewalld-filesystem-0.9.3-13.el8.noarch.rpm              325 kB/s |  77 kB     00:00
(67/358): fuse-libs-2.9.7-15.el8.aarch64.rpm                        364 kB/s |  96 kB     00:00
(68/358): glibc-gconv-extra-2.28-189.1.el8.aarch64.rpm              3.7 MB/s | 1.8 MB     00:00
(69/358): gawk-4.2.1-4.el8.aarch64.rpm                              2.7 MB/s | 1.1 MB     00:00
(70/358): gdbm-1.18-1.el8.aarch64.rpm                               484 kB/s | 126 kB     00:00
(71/358): gdbm-libs-1.18-1.el8.aarch64.rpm                          231 kB/s |  58 kB     00:00
(72/358): glibc-2.28-189.1.el8.aarch64.rpm                          2.8 MB/s | 1.8 MB     00:00
(73/358): glib2-2.56.4-158.el8.aarch64.rpm                          3.8 MB/s | 2.4 MB     00:00
(74/358): gpm-libs-1.20.7-17.el8.aarch64.rpm                        175 kB/s |  38 kB     00:00
(75/358): glibc-common-2.28-189.1.el8.aarch64.rpm                   2.6 MB/s | 1.2 MB     00:00
(76/358): gmp-6.1.2-10.el8.aarch64.rpm                              1.0 MB/s | 262 kB     00:00
(77/358): gnupg2-smime-2.2.20-2.el8.aarch64.rpm                     1.1 MB/s | 267 kB     00:00
(78/358): gnutls-3.6.16-4.el8.aarch64.rpm                           3.2 MB/s | 938 kB     00:00
(79/358): gnupg2-2.2.20-2.el8.aarch64.rpm                           3.7 MB/s | 2.4 MB     00:00
(80/358): gobject-introspection-1.56.1-1.el8.aarch64.rpm            1.0 MB/s | 247 kB     00:00
(81/358): gpgme-1.13.1-11.el8.aarch64.rpm                           1.2 MB/s | 323 kB     00:00
(82/358): grep-3.1-6.el8.aarch64.rpm                                1.0 MB/s | 267 kB     00:00
(83/358): gzip-1.9-13.el8_5.aarch64.rpm                             659 kB/s | 164 kB     00:00
(84/358): groff-base-1.22.3-18.el8.aarch64.rpm                      2.7 MB/s | 989 kB     00:00
(85/358): hardlink-1.3-6.el8.aarch64.rpm                            126 kB/s |  28 kB     00:00
(86/358): hdparm-9.54-4.el8.aarch64.rpm                             395 kB/s |  96 kB     00:00
(87/358): glibc-all-langpacks-2.28-189.1.el8.aarch64.rpm            9.6 MB/s |  25 MB     00:02
(88/358): hostname-3.20-6.el8.aarch64.rpm                           135 kB/s |  31 kB     00:00
(89/358): ima-evm-utils-1.3.2-12.el8.aarch64.rpm                    270 kB/s |  62 kB     00:00
(90/358): unbound-libs-1.7.3-17.el8.aarch64.rpm                     1.6 MB/s | 470 kB     00:00
(91/358): hwdata-0.314-8.12.el8.noarch.rpm                          3.5 MB/s | 1.7 MB     00:00
(92/358): info-6.5-7.el8.aarch64.rpm                                709 kB/s | 190 kB     00:00
(93/358): initscripts-10.00.17-1.el8.aarch64.rpm                    1.3 MB/s | 339 kB     00:00
(94/358): iproute-5.15.0-4.el8.aarch64.rpm                          2.0 MB/s | 792 kB     00:00
(95/358): iprutils-2.4.19-1.el8.aarch64.rpm                         1.1 MB/s | 251 kB     00:00
(96/358): ipset-7.1-1.el8.aarch64.rpm                               155 kB/s |  44 kB     00:00
(97/358): ipset-libs-7.1-1.el8.aarch64.rpm                          277 kB/s |  67 kB     00:00
(98/358): iptables-1.8.4-22.el8.aarch64.rpm                         2.2 MB/s | 581 kB     00:00
(99/358): iptables-ebtables-1.8.4-22.el8.aarch64.rpm                320 kB/s |  71 kB     00:00
(100/358): iptables-libs-1.8.4-22.el8.aarch64.rpm                   396 kB/s | 104 kB     00:00
(101/358): iputils-20180629-9.el8.aarch64.rpm                       602 kB/s | 146 kB     00:00
(102/358): irqbalance-1.4.0-6.el8.aarch64.rpm                       253 kB/s |  59 kB     00:00
(103/358): iw-4.14-5.el8.aarch64.rpm                                328 kB/s |  87 kB     00:00
(104/358): iwl100-firmware-39.31.5.1-107.el8.1.noarch.rpm           618 kB/s | 173 kB     00:00
(105/358): iwl1000-firmware-39.31.5.1-107.el8.1.noarch.rpm          1.0 MB/s | 236 kB     00:00
(106/358): iwl2000-firmware-18.168.6.1-107.el8.1.noarch.rpm         1.1 MB/s | 260 kB     00:00
(107/358): iwl135-firmware-18.168.6.1-107.el8.1.noarch.rpm          907 kB/s | 266 kB     00:00
(108/358): iwl105-firmware-18.168.6.1-107.el8.1.noarch.rpm          713 kB/s | 257 kB     00:00
(109/358): iwl2030-firmware-18.168.6.1-107.el8.1.noarch.rpm         929 kB/s | 269 kB     00:00
(110/358): iwl5000-firmware-8.83.5.1_1-107.el8.1.noarch.rpm         1.0 MB/s | 317 kB     00:00
(111/358): iwl5150-firmware-8.24.2.2-107.el8.1.noarch.rpm           686 kB/s | 170 kB     00:00
(112/358): iwl3160-firmware-25.30.13.0-107.el8.1.noarch.rpm         3.3 MB/s | 1.7 MB     00:00
(113/358): iwl6000-firmware-9.221.4.1-107.el8.1.noarch.rpm          692 kB/s | 190 kB     00:00
(114/358): iwl6000g2a-firmware-18.168.6.1-107.el8.1.noarch.rpm      1.2 MB/s | 333 kB     00:00
(115/358): iwl6050-firmware-41.28.5.1-107.el8.1.noarch.rpm          854 kB/s | 266 kB     00:00
(116/358): jansson-2.14-1.el8.aarch64.rpm                           181 kB/s |  46 kB     00:00
(117/358): json-c-0.13.1-3.el8.aarch64.rpm                              111 kB/s |  39 kB     00:00
(118/358): vim-enhanced-8.0.1763-16.el8_5.13.aarch64.rpm                          2.8 MB/s | 1.3 MB     00:00
(119/358): vim-common-8.0.1763-16.el8_5.13.aarch64.rpm                            6.3 MB/s | 6.3 MB     00:01
(120/358): vim-filesystem-8.0.1763-16.el8_5.13.noarch.rpm                         193 kB/s |  50 kB     00:00
(121/358): kbd-2.0.4-10.el8.aarch64.rpm                                           1.5 MB/s | 380 kB     00:00
(122/358): kbd-legacy-2.0.4-10.el8.noarch.rpm                                     1.6 MB/s | 480 kB     00:00
(123/358): kbd-misc-2.0.4-10.el8.noarch.rpm                                       3.6 MB/s | 1.5 MB     00:00
(124/358): kernel-tools-4.18.0-372.9.1.el8.aarch64.rpm                            5.0 MB/s | 8.1 MB     00:01
(125/358): kernel-tools-libs-4.18.0-372.9.1.el8.aarch64.rpm                       4.8 MB/s | 8.0 MB     00:01
(126/358): kexec-tools-2.0.20-68.el8.aarch64.rpm                                  1.8 MB/s | 479 kB     00:00
(127/358): iwl7260-firmware-25.30.13.0-107.el8.1.noarch.rpm                       6.2 MB/s |  23 MB     00:03
(128/358): keyutils-libs-1.5.10-9.el8.aarch64.rpm                                 136 kB/s |  33 kB     00:00
(129/358): kmod-25-19.el8.aarch64.rpm                                             497 kB/s | 122 kB     00:00
(130/358): kmod-libs-25-19.el8.aarch64.rpm                                        277 kB/s |  65 kB     00:00
(131/358): kpartx-0.8.4-22.el8.aarch64.rpm                                        443 kB/s | 112 kB     00:00
(132/358): less-530-1.el8.aarch64.rpm                                             679 kB/s | 160 kB     00:00
(133/358): krb5-libs-1.18.2-14.el8.aarch64.rpm                                    2.5 MB/s | 814 kB     00:00
(134/358): libacl-2.2.53-1.el8.1.aarch64.rpm                                      131 kB/s |  33 kB     00:00
(135/358): libarchive-3.3.3-3.el8_5.aarch64.rpm                                   1.3 MB/s | 339 kB     00:00
(136/358): libassuan-2.5.1-3.el8.aarch64.rpm                                      354 kB/s |  79 kB     00:00
(137/358): libattr-2.4.48-3.el8.aarch64.rpm                                        99 kB/s |  26 kB     00:00
(138/358): libbasicobjects-0.1.1-39.el8.aarch64.rpm                               138 kB/s |  30 kB     00:00
(139/358): libblkid-2.32.1-35.el8.aarch64.rpm                                     847 kB/s | 211 kB     00:00
(140/358): libbpf-0.4.0-3.el8.aarch64.rpm                                         479 kB/s | 119 kB     00:00
(141/358): libcap-2.48-2.el8.aarch64.rpm                                          311 kB/s |  72 kB     00:00
(142/358): libcap-ng-0.7.11-1.el8.aarch64.rpm                                     139 kB/s |  32 kB     00:00
(143/358): xkeyboard-config-2.28-1.el8.noarch.rpm                                 2.1 MB/s | 781 kB     00:00
(144/358): libcollection-0.7.0-39.el8.aarch64.rpm                                 191 kB/s |  46 kB     00:00
(145/358): libcom_err-1.45.6-4.el8.aarch64.rpm                                    202 kB/s |  48 kB     00:00
(146/358): libcomps-0.1.18-1.el8.aarch64.rpm                                      314 kB/s |  77 kB     00:00
(147/358): libcurl-7.61.1-22.el8.aarch64.rpm                                      1.0 MB/s | 281 kB     00:00
(148/358): libdaemon-0.14-15.el8.aarch64.rpm                                      149 kB/s |  35 kB     00:00
(149/358): libdb-utils-5.3.28-42.el8_4.aarch64.rpm                                532 kB/s | 147 kB     00:00
(150/358): libdb-5.3.28-42.el8_4.aarch64.rpm                                      1.6 MB/s | 686 kB     00:00
(151/358): libdhash-0.5.0-39.el8.aarch64.rpm                                      138 kB/s |  33 kB     00:00
(152/358): libedit-3.1-23.20170329cvs.el8.aarch64.rpm                             412 kB/s |  98 kB     00:00
(153/358): libevent-2.1.8-5.el8.aarch64.rpm                                       905 kB/s | 237 kB     00:00
(154/358): libdnf-0.63.0-8.el8.aarch64.rpm                                        1.3 MB/s | 622 kB     00:00
(155/358): libfdisk-2.32.1-35.el8.aarch64.rpm                                     1.0 MB/s | 241 kB     00:00
(156/358): libffi-3.1-23.el8.aarch64.rpm                                          145 kB/s |  35 kB     00:00
(157/358): libgcc-8.5.0-10.el8.aarch64.rpm                                        282 kB/s |  72 kB     00:00
(158/358): libgcrypt-1.8.5-6.el8.aarch64.rpm                                      1.5 MB/s | 390 kB     00:00
(159/358): libgpg-error-1.31-1.el8.aarch64.rpm                                    940 kB/s | 238 kB     00:00
(160/358): libidn2-2.2.0-1.el8.aarch64.rpm                                        440 kB/s |  92 kB     00:00
(161/358): libibverbs-37.2-1.el8.aarch64.rpm                                      1.4 MB/s | 361 kB     00:00
(162/358): libini_config-1.3.1-39.el8.aarch64.rpm                                 294 kB/s |  67 kB     00:00
(163/358): libkcapi-1.2.0-2.el8.aarch64.rpm                                       227 kB/s |  46 kB     00:00
(164/358): libkcapi-hmaccalc-1.2.0-2.el8.aarch64.rpm                              127 kB/s |  30 kB     00:00
(165/358): libksba-1.3.5-7.el8.aarch64.rpm                                        557 kB/s | 128 kB     00:00
(166/358): libldb-2.4.1-1.el8.aarch64.rpm                                         776 kB/s | 182 kB     00:00
(167/358): libmnl-1.0.4-6.el8.aarch64.rpm                                         131 kB/s |  29 kB     00:00
(168/358): libmodulemd-2.13.0-1.el8.aarch64.rpm                                   846 kB/s | 201 kB     00:00
(169/358): libmount-2.32.1-35.el8.aarch64.rpm                                     919 kB/s | 227 kB     00:00
(170/358): libndp-1.7-6.el8.aarch64.rpm                                           157 kB/s |  39 kB     00:00
(171/358): libnetfilter_conntrack-1.0.6-5.el8.aarch64.rpm                         274 kB/s |  60 kB     00:00
(172/358): libnfnetlink-1.0.1-13.el8.aarch64.rpm                                  142 kB/s |  31 kB     00:00
(173/358): libnfsidmap-2.3.3-51.el8.aarch64.rpm                                   436 kB/s | 118 kB     00:00
(174/358): libnftnl-1.1.5-5.el8.aarch64.rpm                                       320 kB/s |  79 kB     00:00
(175/358): libnghttp2-1.33.0-3.el8_3.1.aarch64.rpm                                322 kB/s |  74 kB     00:00
(176/358): libnl3-cli-3.5.0-1.el8.aarch64.rpm                                     741 kB/s | 192 kB     00:00
(177/358): libnl3-3.5.0-1.el8.aarch64.rpm                                         1.0 MB/s | 301 kB     00:00
(178/358): libnsl2-1.2.0-2.20180605git4a062cf.el8.aarch64.rpm                     237 kB/s |  54 kB     00:00
(179/358): libpath_utils-0.2.1-39.el8.aarch64.rpm                                 150 kB/s |  33 kB     00:00
(180/358): libpcap-1.9.1-5.el8.aarch64.rpm                                        653 kB/s | 160 kB     00:00
(181/358): libpipeline-1.5.0-2.el8.aarch64.rpm                                    236 kB/s |  52 kB     00:00
(182/358): libpkgconf-1.4.2-1.el8.aarch64.rpm                                     147 kB/s |  33 kB     00:00
(183/358): libpsl-0.20.2-6.el8.aarch64.rpm                                        255 kB/s |  60 kB     00:00
(184/358): libpwquality-1.4.4-3.el8.aarch64.rpm                                   437 kB/s | 105 kB     00:00
(185/358): libref_array-0.1.5-39.el8.aarch64.rpm                                  135 kB/s |  31 kB     00:00
(186/358): librepo-1.14.2-1.el8.aarch64.rpm                                       335 kB/s |  87 kB     00:00
(187/358): libreport-filesystem-2.9.5-15.el8.rocky.6.3.aarch64.rpm                 81 kB/s |  20 kB     00:00
(188/358): libseccomp-2.5.2-1.el8.aarch64.rpm                                     279 kB/s |  69 kB     00:00
(189/358): libsecret-0.18.6-1.el8.0.2.aarch64.rpm                                 589 kB/s | 158 kB     00:00
(190/358): libselinux-2.9-5.el8.aarch64.rpm                                       647 kB/s | 161 kB     00:00
(191/358): libselinux-utils-2.9-5.el8.aarch64.rpm                                 798 kB/s | 240 kB     00:00
(192/358): libsemanage-2.9-8.el8.aarch64.rpm                                      600 kB/s | 162 kB     00:00
(193/358): libsepol-2.9-3.el8.aarch64.rpm                                         1.0 MB/s | 320 kB     00:00
(194/358): libsigsegv-2.11-5.el8.aarch64.rpm                                      124 kB/s |  29 kB     00:00
(195/358): libsmartcols-2.32.1-35.el8.aarch64.rpm                                 656 kB/s | 172 kB     00:00
(196/358): libsolv-0.7.20-1.el8.aarch64.rpm                                       1.2 MB/s | 340 kB     00:00
(197/358): libss-1.45.6-4.el8.aarch64.rpm                                         221 kB/s |  52 kB     00:00
(198/358): libssh-0.9.6-3.el8.aarch64.rpm                                         800 kB/s | 206 kB     00:00
(199/358): libssh-config-0.9.6-3.el8.noarch.rpm                                    86 kB/s |  18 kB     00:00
(200/358): libsss_autofs-2.6.2-4.el8_6.aarch64.rpm                                468 kB/s | 120 kB     00:00
(201/358): libsss_certmap-2.6.2-4.el8_6.aarch64.rpm                               647 kB/s | 162 kB     00:00
(202/358): libsss_idmap-2.6.2-4.el8_6.aarch64.rpm                                 492 kB/s | 122 kB     00:00
(203/358): libsss_nss_idmap-2.6.2-4.el8_6.aarch64.rpm                             518 kB/s | 128 kB     00:00
(204/358): libsss_sudo-2.6.2-4.el8_6.aarch64.rpm                                  456 kB/s | 118 kB     00:00
(205/358): libstdc++-8.5.0-10.el8.aarch64.rpm                                     1.3 MB/s | 423 kB     00:00
(206/358): libsysfs-2.1.0-25.el8.aarch64.rpm                                      190 kB/s |  52 kB     00:00
(207/358): libtalloc-2.3.3-1.el8.aarch64.rpm                                      201 kB/s |  45 kB     00:00
(208/358): libtasn1-4.13-3.el8.aarch64.rpm                                        352 kB/s |  74 kB     00:00
(209/358): libtdb-1.4.4-1.el8.aarch64.rpm                                         232 kB/s |  57 kB     00:00
(210/358): libteam-1.31-2.el8.aarch64.rpm                                         268 kB/s |  62 kB     00:00
(211/358): libtevent-0.11.0-0.el8.aarch64.rpm                                     222 kB/s |  47 kB     00:00
(212/358): libtirpc-1.1.4-6.el8.aarch64.rpm                                       450 kB/s | 108 kB     00:00
(213/358): libusbx-1.0.23-4.el8.aarch64.rpm                                       318 kB/s |  72 kB     00:00
(214/358): libunistring-0.9.9-3.el8.aarch64.rpm                                   1.5 MB/s | 408 kB     00:00
(215/358): libutempter-1.1.6-14.el8.aarch64.rpm                                   149 kB/s |  31 kB     00:00
(216/358): libuser-0.62-24.el8.aarch64.rpm                                        1.2 MB/s | 408 kB     00:00
(217/358): libuuid-2.32.1-35.el8.aarch64.rpm                                      377 kB/s |  95 kB     00:00
(218/358): libverto-0.3.0-5.el8.aarch64.rpm                                        96 kB/s |  23 kB     00:00
(219/358): libxcrypt-4.1.1-6.el8.aarch64.rpm                                      310 kB/s |  72 kB     00:00
(220/358): libxml2-2.9.7-13.el8.aarch64.rpm                                       2.1 MB/s | 652 kB     00:00
(221/358): libyaml-0.1.7-5.el8.aarch64.rpm                                        269 kB/s |  56 kB     00:00
(222/358): libzstd-1.4.4-1.el8.aarch64.rpm                                        1.0 MB/s | 239 kB     00:00
(223/358): lmdb-libs-0.9.24-1.el8.aarch64.rpm                                     268 kB/s |  56 kB     00:00
(224/358): logrotate-3.14.0-4.el8.aarch64.rpm                                     369 kB/s |  81 kB     00:00
(225/358): lshw-B.02.19.2-6.el8.aarch64.rpm                                       1.2 MB/s | 299 kB     00:00
(226/358): lsscsi-0.32-3.el8.aarch64.rpm                                          315 kB/s |  68 kB     00:00
(227/358): lua-libs-5.3.4-12.el8.aarch64.rpm                                      486 kB/s | 111 kB     00:00
(228/358): lz4-libs-1.8.3-3.el8_4.aarch64.rpm                                     279 kB/s |  62 kB     00:00
(229/358): lzo-2.08-14.el8.aarch64.rpm                                            269 kB/s |  63 kB     00:00
(230/358): man-db-2.7.6.1-18.el8.aarch64.rpm                                      3.0 MB/s | 875 kB     00:00
(231/358): memstrack-0.1.11-1.el8.aarch64.rpm                                     201 kB/s |  46 kB     00:00
(232/358): mpfr-3.1.6-1.el8.aarch64.rpm                                           890 kB/s | 213 kB     00:00
(233/358): nano-2.9.8-1.el8.aarch64.rpm                                           1.8 MB/s | 578 kB     00:00
(234/358): mozjs60-60.9.0-4.el8.aarch64.rpm                                       7.4 MB/s | 6.0 MB     00:00
(235/358): ncurses-base-6.1-9.20180224.el8.noarch.rpm                             310 kB/s |  80 kB     00:00
(236/358): ncurses-6.1-9.20180224.el8.aarch64.rpm                                 964 kB/s | 382 kB     00:00
(237/358): ncurses-libs-6.1-9.20180224.el8.aarch64.rpm                            1.2 MB/s | 309 kB     00:00
(238/358): net-tools-2.0-0.52.20160912git.el8.aarch64.rpm                         1.3 MB/s | 319 kB     00:00
(239/358): nettle-3.4.1-7.el8.aarch64.rpm                                         1.2 MB/s | 306 kB     00:00
(240/358): newt-0.52.20-11.el8.aarch64.rpm                                        481 kB/s | 117 kB     00:00
(241/358): npth-1.5-4.el8.aarch64.rpm                                             108 kB/s |  25 kB     00:00
(242/358): nftables-0.9.3-25.el8.aarch64.rpm                                      1.2 MB/s | 310 kB     00:00
(243/358): numactl-libs-2.0.12-13.el8.aarch64.rpm                                 155 kB/s |  35 kB     00:00
(244/358): openldap-2.4.46-18.el8.aarch64.rpm                                     1.3 MB/s | 338 kB     00:00
(245/358): openssh-8.0p1-13.el8.aarch64.rpm                                       1.7 MB/s | 487 kB     00:00
(246/358): openssh-clients-8.0p1-13.el8.aarch64.rpm                               2.4 MB/s | 624 kB     00:00
(247/358): openssh-server-8.0p1-13.el8.aarch64.rpm                                1.6 MB/s | 473 kB     00:00
(248/358): openssl-1.1.1k-6.el8_5.aarch64.rpm                                     2.0 MB/s | 689 kB     00:00
(249/358): openssl-pkcs11-0.4.10-2.el8.aarch64.rpm                                282 kB/s |  64 kB     00:00
(250/358): p11-kit-0.23.22-1.el8.aarch64.rpm                                      1.2 MB/s | 305 kB     00:00
(251/358): openssl-libs-1.1.1k-6.el8_5.aarch64.rpm                                2.1 MB/s | 1.3 MB     00:00
(252/358): p11-kit-trust-0.23.22-1.el8.aarch64.rpm                                560 kB/s | 133 kB     00:00
(253/358): pam-1.3.1-16.el8.aarch64.rpm                                           2.6 MB/s | 731 kB     00:00
(254/358): parted-3.2-39.el8.aarch64.rpm                                          1.7 MB/s | 544 kB     00:00
(255/358): passwd-0.80-4.el8.aarch64.rpm                                          511 kB/s | 113 kB     00:00
(256/358): pciutils-libs-3.7.0-1.el8.aarch64.rpm                                  213 kB/s |  51 kB     00:00
(257/358): pcre-8.42-6.el8.aarch64.rpm                                            735 kB/s | 186 kB     00:00
(258/358): pinentry-1.1.0-2.el8.aarch64.rpm                                       474 kB/s |  97 kB     00:00
(259/358): pcre2-10.32-2.el8.aarch64.rpm                                          695 kB/s | 218 kB     00:00
(260/358): pigz-2.4-4.el8.aarch64.rpm                                             301 kB/s |  71 kB     00:00
(261/358): pkgconf-1.4.2-1.el8.aarch64.rpm                                        147 kB/s |  36 kB     00:00
(262/358): pkgconf-m4-1.4.2-1.el8.noarch.rpm                                       80 kB/s |  16 kB     00:00
(263/358): pkgconf-pkg-config-1.4.2-1.el8.aarch64.rpm                              59 kB/s |  14 kB     00:00
(264/358): platform-python-3.6.8-45.el8.rocky.0.aarch64.rpm                       401 kB/s |  85 kB     00:00
(265/358): platform-python-pip-9.0.3-22.el8.rocky.0.noarch.rpm                    4.7 MB/s | 1.6 MB     00:00
(266/358): platform-python-setuptools-39.2.0-6.el8.noarch.rpm                     1.6 MB/s | 630 kB     00:00
(267/358): policycoreutils-2.9-19.el8.aarch64.rpm                                 1.2 MB/s | 373 kB     00:00
(268/358): polkit-0.115-13.el8_5.2.aarch64.rpm                                    576 kB/s | 149 kB     00:00
(269/358): polkit-libs-0.115-13.el8_5.2.aarch64.rpm                               311 kB/s |  73 kB     00:00
(270/358): polkit-pkla-compat-0.1-12.el8.aarch64.rpm                              187 kB/s |  44 kB     00:00
(271/358): popt-1.18-1.el8.aarch64.rpm                                            251 kB/s |  59 kB     00:00
(272/358): prefixdevname-0.1.0-6.el8.aarch64.rpm                                  1.4 MB/s | 399 kB     00:00
(273/358): procps-ng-3.3.15-6.el8.aarch64.rpm                                     1.1 MB/s | 327 kB     00:00
(274/358): psmisc-23.1-5.el8.aarch64.rpm                                          599 kB/s | 142 kB     00:00
(275/358): publicsuffix-list-dafsa-20180723-1.el8.noarch.rpm                      241 kB/s |  55 kB     00:00
(276/358): plymouth-0.9.4-11.20200615git1e36e30.el8.aarch64.rpm                   481 kB/s | 123 kB     00:00
(277/358): plymouth-core-libs-0.9.4-11.20200615git1e36e30.el8.aarch64.rpm         487 kB/s | 117 kB     00:00
(278/358): python3-dateutil-2.6.1-6.el8.noarch.rpm                                1.0 MB/s | 250 kB     00:00
(279/358): python3-dbus-1.2.4-15.el8.aarch64.rpm                                  526 kB/s | 131 kB     00:00
(280/358): python3-decorator-4.2.1-2.el8.noarch.rpm                               113 kB/s |  26 kB     00:00
(281/358): python3-dnf-4.7.0-8.el8.noarch.rpm                                     2.0 MB/s | 544 kB     00:00
(282/358): plymouth-scripts-0.9.4-11.20200615git1e36e30.el8.aarch64.rpm           176 kB/s |  43 kB     00:00
(283/358): python3-dnf-plugins-core-4.0.21-11.el8.noarch.rpm                      901 kB/s | 239 kB     00:00
(284/358): python3-firewall-0.9.3-13.el8.noarch.rpm                               1.4 MB/s | 433 kB     00:00
(285/358): python3-gobject-base-3.28.3-2.el8.aarch64.rpm                          1.0 MB/s | 307 kB     00:00
(286/358): python3-gpg-1.13.1-11.el8.aarch64.rpm                                  945 kB/s | 236 kB     00:00
(287/358): python3-hawkey-0.63.0-8.el8.aarch64.rpm                                455 kB/s | 107 kB     00:00
(288/358): python3-libcomps-0.1.18-1.el8.aarch64.rpm                              203 kB/s |  50 kB     00:00
(289/358): python3-libdnf-0.63.0-8.el8.aarch64.rpm                                2.5 MB/s | 704 kB     00:00
(290/358): python3-libselinux-2.9-5.el8.aarch64.rpm                               1.1 MB/s | 271 kB     00:00
(291/358): python3-linux-procfs-0.7.0-1.el8.noarch.rpm                            179 kB/s |  41 kB     00:00
(292/358): python3-nftables-0.9.3-25.el8.aarch64.rpm                              135 kB/s |  29 kB     00:00
(293/358): python3-libs-3.6.8-45.el8.rocky.0.aarch64.rpm                          5.8 MB/s | 7.7 MB     00:01
(294/358): python3-pip-wheel-9.0.3-22.el8.rocky.0.noarch.rpm                      2.6 MB/s | 894 kB     00:00
(295/358): python3-pyudev-0.21.0-7.el8.noarch.rpm                                 404 kB/s |  83 kB     00:00
(296/358): python3-rpm-4.14.3-23.el8.aarch64.rpm                                  689 kB/s | 154 kB     00:00
(297/358): python3-setuptools-wheel-39.2.0-6.el8.noarch.rpm                       1.2 MB/s | 286 kB     00:00
(298/358): python3-six-1.11.0-8.el8.noarch.rpm                                    185 kB/s |  37 kB     00:00
(299/358): python3-perf-4.18.0-372.9.1.el8.aarch64.rpm                            4.6 MB/s | 8.2 MB     00:01
(300/358): python3-slip-0.6.4-11.el8.noarch.rpm                                   183 kB/s |  37 kB     00:00
(301/358): python3-slip-dbus-0.6.4-11.el8.noarch.rpm                              162 kB/s |  38 kB     00:00
(302/358): python3-syspurpose-1.28.29-3.el8.aarch64.rpm                           1.2 MB/s | 326 kB     00:00
(303/358): readline-7.0-10.el8.aarch64.rpm                                        729 kB/s | 192 kB     00:00
(304/358): rocky-gpg-keys-8.6-3.el8.noarch.rpm                                     54 kB/s |  12 kB     00:00
(305/358): rocky-release-8.6-3.el8.noarch.rpm                                      87 kB/s |  21 kB     00:00
(306/358): rocky-repos-8.6-3.el8.noarch.rpm                                        56 kB/s |  14 kB     00:00
(307/358): rootfiles-8.1-22.el8.noarch.rpm                                         49 kB/s |  12 kB     00:00
(308/358): rpm-4.14.3-23.el8.aarch64.rpm                                          1.6 MB/s | 542 kB     00:00
(309/358): rpm-build-libs-4.14.3-23.el8.aarch64.rpm                               547 kB/s | 150 kB     00:00
(310/358): rpm-libs-4.14.3-23.el8.aarch64.rpm                                     1.2 MB/s | 327 kB     00:00
(311/358): rpm-plugin-selinux-4.14.3-23.el8.aarch64.rpm                           292 kB/s |  77 kB     00:00
(312/358): rpm-plugin-systemd-inhibit-4.14.3-23.el8.aarch64.rpm                   335 kB/s |  78 kB     00:00
(313/358): sed-4.5-5.el8.aarch64.rpm                                              1.0 MB/s | 294 kB     00:00
(314/358): selinux-policy-3.14.3-95.el8.noarch.rpm                                2.1 MB/s | 644 kB     00:00
(315/358): libestr-0.1.10-1.el8.aarch64.rpm                                       122 kB/s |  26 kB     00:00
(316/358): setup-2.12.2-6.el8.noarch.rpm                                          808 kB/s | 180 kB     00:00
(317/358): sg3_utils-1.44-5.el8.aarch64.rpm                                       1.6 MB/s | 905 kB     00:00
(318/358): sg3_utils-libs-1.44-5.el8.aarch64.rpm                                  392 kB/s |  92 kB     00:00
(319/358): shadow-utils-4.6-16.el8.aarch64.rpm                                    2.0 MB/s | 1.2 MB     00:00
(320/358): shared-mime-info-1.9-3.el8.aarch64.rpm                                 995 kB/s | 327 kB     00:00
(321/358): libfastjson-0.99.9-1.el8.aarch64.rpm                                   171 kB/s |  36 kB     00:00
(322/358): slang-2.3.2-3.el8.aarch64.rpm                                          912 kB/s | 346 kB     00:00
(323/358): snappy-1.1.8-3.el8.aarch64.rpm                                         168 kB/s |  35 kB     00:00
(324/358): sqlite-3.26.0-15.el8.aarch64.rpm                                       1.6 MB/s | 630 kB     00:00
(325/358): sqlite-libs-3.26.0-15.el8.aarch64.rpm                                  1.4 MB/s | 549 kB     00:00
(326/358): selinux-policy-targeted-3.14.3-95.el8.noarch.rpm                       3.8 MB/s |  15 MB     00:03
(327/358): squashfs-tools-4.3-20.el8.aarch64.rpm                                  647 kB/s | 159 kB     00:00
(328/358): sssd-client-2.6.2-4.el8_6.aarch64.rpm                                  797 kB/s | 221 kB     00:00
(329/358): sssd-kcm-2.6.2-4.el8_6.aarch64.rpm                                     843 kB/s | 242 kB     00:00
(330/358): sssd-common-2.6.2-4.el8_6.aarch64.rpm                                  2.6 MB/s | 1.6 MB     00:00
(331/358): sssd-nfs-idmap-2.6.2-4.el8_6.aarch64.rpm                               479 kB/s | 119 kB     00:00
(332/358): sudo-1.8.29-8.el8.aarch64.rpm                                          2.1 MB/s | 889 kB     00:00
(333/358): systemd-libs-239-58.el8.aarch64.rpm                                    2.5 MB/s | 1.0 MB     00:00
(334/358): systemd-239-58.el8.aarch64.rpm                                         3.1 MB/s | 3.3 MB     00:01
(335/358): systemd-pam-239-58.el8.aarch64.rpm                                     1.5 MB/s | 445 kB     00:00
(336/358): teamd-1.31-2.el8.aarch64.rpm                                           557 kB/s | 126 kB     00:00
(337/358): timedatex-0.5-3.el8.aarch64.rpm                                        144 kB/s |  31 kB     00:00
(338/358): systemd-udev-239-58.el8.aarch64.rpm                                    2.6 MB/s | 1.5 MB     00:00
(339/358): tpm2-tss-2.3.2-4.el8.aarch64.rpm                                       810 kB/s | 239 kB     00:00
(340/358): trousers-0.3.15-1.el8.aarch64.rpm                                      579 kB/s | 146 kB     00:00
(341/358): trousers-lib-0.3.15-1.el8.aarch64.rpm                                  602 kB/s | 157 kB     00:00
(342/358): tuned-2.18.0-2.el8.noarch.rpm                                          1.1 MB/s | 315 kB     00:00
(343/358): tzdata-2022a-1.el8.noarch.rpm                                          1.6 MB/s | 473 kB     00:00
(344/358): vim-minimal-8.0.1763-16.el8_5.13.aarch64.rpm                           1.8 MB/s | 543 kB     00:00
(345/358): virt-what-1.18-13.el8.aarch64.rpm                                      167 kB/s |  35 kB     00:00
(346/358): util-linux-2.32.1-35.el8.aarch64.rpm                                   2.8 MB/s | 2.5 MB     00:00
(347/358): which-2.21-17.el8.aarch64.rpm                                          215 kB/s |  47 kB     00:00
(348/358): xfsprogs-5.0.0-10.el8.aarch64.rpm                                      2.6 MB/s | 1.1 MB     00:00
(349/358): xz-5.2.4-4.el8_6.aarch64.rpm                                           639 kB/s | 152 kB     00:00
(350/358): wpa_supplicant-2.10-1.el8.aarch64.rpm                                  2.3 MB/s | 2.0 MB     00:00
(351/358): xz-libs-5.2.4-4.el8_6.aarch64.rpm                                      372 kB/s |  89 kB     00:00
(352/358): yum-4.7.0-8.el8.noarch.rpm                                             743 kB/s | 205 kB     00:00
(353/358): zlib-1.2.11-18.el8_5.aarch64.rpm                                       433 kB/s |  99 kB     00:00
(354/358): python3-unbound-1.7.3-17.el8.aarch64.rpm                               510 kB/s | 117 kB     00:00
(355/358): libxkbcommon-0.9.1-1.el8.aarch64.rpm                                   448 kB/s | 110 kB     00:00
(356/358): cloud-utils-growpart-0.31-3.el8.noarch.rpm                             121 kB/s |  31 kB     00:00
(357/358): rsyslog-8.2102.0-7.el8_6.1.aarch64.rpm                                 1.7 MB/s | 737 kB     00:00
(358/358): linux-firmware-20220210-107.git6342082c.el8.noarch.rpm                 7.6 MB/s | 196 MB     00:25
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Running scriptlet: filesystem-3.8-6.el8.aarch64                                                             1/1
  Preparing        :                                                                                          1/1
  Installing       : libgcc-8.5.0-10.el8.aarch64                                                            1/358
  Running scriptlet: libgcc-8.5.0-10.el8.aarch64                                                            1/358
  Installing       : tzdata-2022a-1.el8.noarch                                                              2/358
  Installing       : python3-setuptools-wheel-39.2.0-6.el8.noarch                                           3/358
  Installing       : python3-pip-wheel-9.0.3-22.el8.rocky.0.noarch                                          4/358
  Installing       : xkeyboard-config-2.28-1.el8.noarch                                                     5/358
  Installing       : vim-filesystem-2:8.0.1763-16.el8_5.13.noarch                                           6/358
  Installing       : rocky-gpg-keys-8.6-3.el8.noarch                                                        7/358
  Installing       : rocky-release-8.6-3.el8.noarch                                                         8/358
  Installing       : rocky-repos-8.6-3.el8.noarch                                                           9/358
  Installing       : setup-2.12.2-6.el8.noarch                                                             10/358
  Running scriptlet: setup-2.12.2-6.el8.noarch                                                             10/358
  Installing       : filesystem-3.8-6.el8.aarch64                                                          11/358
warning: group mail does not exist - using root

  Installing       : basesystem-11-5.el8.noarch                                                            12/358
  Installing       : publicsuffix-list-dafsa-20180723-1.el8.noarch                                         13/358
  Installing       : pkgconf-m4-1.4.2-1.el8.noarch                                                         14/358
  Installing       : ncurses-base-6.1-9.20180224.el8.noarch                                                15/358
  Installing       : pcre2-10.32-2.el8.aarch64                                                             16/358
  Installing       : libselinux-2.9-5.el8.aarch64                                                          17/358
  Installing       : ncurses-libs-6.1-9.20180224.el8.aarch64                                               18/358
  Installing       : glibc-all-langpacks-2.28-189.1.el8.aarch64                                            19/358
  Installing       : glibc-common-2.28-189.1.el8.aarch64                                                   20/358
  Installing       : glibc-gconv-extra-2.28-189.1.el8.aarch64                                              21/358
  Running scriptlet: glibc-gconv-extra-2.28-189.1.el8.aarch64                                              21/358
  Running scriptlet: glibc-2.28-189.1.el8.aarch64                                                          22/358
  Installing       : glibc-2.28-189.1.el8.aarch64                                                          22/358
  Running scriptlet: glibc-2.28-189.1.el8.aarch64                                                          22/358
  Installing       : bash-4.4.20-3.el8.aarch64                                                             23/358
  Running scriptlet: bash-4.4.20-3.el8.aarch64                                                             23/358
  Installing       : libsepol-2.9-3.el8.aarch64                                                            24/358
  Running scriptlet: libsepol-2.9-3.el8.aarch64                                                            24/358
  Installing       : zlib-1.2.11-18.el8_5.aarch64                                                          25/358
  Installing       : xz-libs-5.2.4-4.el8_6.aarch64                                                         26/358
  Installing       : popt-1.18-1.el8.aarch64                                                               27/358
  Installing       : bzip2-libs-1.0.6-26.el8.aarch64                                                       28/358
  Installing       : libcap-2.48-2.el8.aarch64                                                             29/358
  Installing       : sqlite-libs-3.26.0-15.el8.aarch64                                                     30/358
  Installing       : libcom_err-1.45.6-4.el8.aarch64                                                       31/358
  Running scriptlet: libcom_err-1.45.6-4.el8.aarch64                                                       31/358
  Installing       : libgpg-error-1.31-1.el8.aarch64                                                       32/358
  Installing       : elfutils-libelf-0.186-1.el8.aarch64                                                   33/358
  Installing       : info-6.5-7.el8.aarch64                                                                34/358
  Installing       : readline-7.0-10.el8.aarch64                                                           35/358
  Running scriptlet: readline-7.0-10.el8.aarch64                                                           35/358
  Installing       : libuuid-2.32.1-35.el8.aarch64                                                         36/358
  Running scriptlet: libuuid-2.32.1-35.el8.aarch64                                                         36/358
  Installing       : libxcrypt-4.1.1-6.el8.aarch64                                                         37/358
  Installing       : libzstd-1.4.4-1.el8.aarch64                                                           38/358
  Installing       : expat-2.2.5-8.el8.aarch64                                                             39/358
  Installing       : libstdc++-8.5.0-10.el8.aarch64                                                        40/358
  Running scriptlet: libstdc++-8.5.0-10.el8.aarch64                                                        40/358
  Installing       : libmnl-1.0.4-6.el8.aarch64                                                            41/358
  Running scriptlet: libmnl-1.0.4-6.el8.aarch64                                                            41/358
  Installing       : libnl3-3.5.0-1.el8.aarch64                                                            42/358
  Running scriptlet: libnl3-3.5.0-1.el8.aarch64                                                            42/358
  Installing       : lua-libs-5.3.4-12.el8.aarch64                                                         43/358
  Installing       : chkconfig-1.19.1-1.el8.aarch64                                                        44/358
  Installing       : libxml2-2.9.7-13.el8.aarch64                                                          45/358
  Installing       : gmp-1:6.1.2-10.el8.aarch64                                                            46/358
  Running scriptlet: gmp-1:6.1.2-10.el8.aarch64                                                            46/358
  Installing       : libunistring-0.9.9-3.el8.aarch64                                                      47/358
  Installing       : libidn2-2.2.0-1.el8.aarch64                                                           48/358
  Installing       : libgcrypt-1.8.5-6.el8.aarch64                                                         49/358
  Running scriptlet: libgcrypt-1.8.5-6.el8.aarch64                                                         49/358
  Installing       : libcap-ng-0.7.11-1.el8.aarch64                                                        50/358
  Installing       : audit-libs-3.0.7-2.el8.2.aarch64                                                      51/358
  Installing       : libffi-3.1-23.el8.aarch64                                                             52/358
  Installing       : p11-kit-0.23.22-1.el8.aarch64                                                         53/358
  Installing       : libtalloc-2.3.3-1.el8.aarch64                                                         54/358
  Installing       : findutils-1:4.6.0-20.el8.aarch64                                                      55/358
  Running scriptlet: findutils-1:4.6.0-20.el8.aarch64                                                      55/358
  Installing       : libassuan-2.5.1-3.el8.aarch64                                                         56/358
  Installing       : file-libs-5.33-20.el8.aarch64                                                         57/358
  Installing       : json-c-0.13.1-3.el8.aarch64                                                           58/358
  Installing       : libattr-2.4.48-3.el8.aarch64                                                          59/358
  Installing       : libacl-2.2.53-1.el8.1.aarch64                                                         60/358
  Installing       : sed-4.5-5.el8.aarch64                                                                 61/358
  Running scriptlet: sed-4.5-5.el8.aarch64                                                                 61/358
  Installing       : libsmartcols-2.32.1-35.el8.aarch64                                                    62/358
  Running scriptlet: libsmartcols-2.32.1-35.el8.aarch64                                                    62/358
  Installing       : lz4-libs-1.8.3-3.el8_4.aarch64                                                        63/358
  Installing       : libtevent-0.11.0-0.el8.aarch64                                                        64/358
  Installing       : libsemanage-2.9-8.el8.aarch64                                                         65/358
  Installing       : gdbm-libs-1:1.18-1.el8.aarch64                                                        66/358
  Installing       : jansson-2.14-1.el8.aarch64                                                            67/358
  Installing       : keyutils-libs-1.5.10-9.el8.aarch64                                                    68/358
  Installing       : libbasicobjects-0.1.1-39.el8.aarch64                                                  69/358
  Installing       : libcollection-0.7.0-39.el8.aarch64                                                    70/358
  Installing       : libref_array-0.1.5-39.el8.aarch64                                                     71/358
  Installing       : libtdb-1.4.4-1.el8.aarch64                                                            72/358
  Installing       : nettle-3.4.1-7.el8.aarch64                                                            73/358
  Running scriptlet: nettle-3.4.1-7.el8.aarch64                                                            73/358
  Installing       : libnl3-cli-3.5.0-1.el8.aarch64                                                        74/358
  Running scriptlet: libnl3-cli-3.5.0-1.el8.aarch64                                                        74/358
  Installing       : ethtool-2:5.13-1.el8.aarch64                                                          75/358
  Installing       : libnftnl-1.1.5-5.el8.aarch64                                                          76/358
  Running scriptlet: libnftnl-1.1.5-5.el8.aarch64                                                          76/358
  Installing       : diffutils-3.6-6.el8.aarch64                                                           77/358
  Running scriptlet: diffutils-3.6-6.el8.aarch64                                                           77/358
  Installing       : libksba-1.3.5-7.el8.aarch64                                                           78/358
  Installing       : cpio-2.12-11.el8.aarch64                                                              79/358
  Installing       : dmidecode-1:3.3-4.el8.aarch64                                                         80/358
  Installing       : libdhash-0.5.0-39.el8.aarch64                                                         81/358
  Installing       : libedit-3.1-23.20170329cvs.el8.aarch64                                                82/358
  Installing       : libnfnetlink-1.0.1-13.el8.aarch64                                                     83/358
  Running scriptlet: libnfnetlink-1.0.1-13.el8.aarch64                                                     83/358
  Installing       : libseccomp-2.5.2-1.el8.aarch64                                                        84/358
  Running scriptlet: libseccomp-2.5.2-1.el8.aarch64                                                        84/358
  Installing       : libsss_idmap-2.6.2-4.el8_6.aarch64                                                    85/358
  Running scriptlet: libsss_idmap-2.6.2-4.el8_6.aarch64                                                    85/358
  Installing       : libtasn1-4.13-3.el8.aarch64                                                           86/358
  Running scriptlet: libtasn1-4.13-3.el8.aarch64                                                           86/358
  Installing       : p11-kit-trust-0.23.22-1.el8.aarch64                                                   87/358
  Running scriptlet: p11-kit-trust-0.23.22-1.el8.aarch64                                                   87/358
  Installing       : lzo-2.08-14.el8.aarch64                                                               88/358
  Installing       : numactl-libs-2.0.12-13.el8.aarch64                                                    89/358
  Running scriptlet: numactl-libs-2.0.12-13.el8.aarch64                                                    89/358
  Installing       : pcre-8.42-6.el8.aarch64                                                               90/358
  Installing       : grep-3.1-6.el8.aarch64                                                                91/358
  Running scriptlet: grep-3.1-6.el8.aarch64                                                                91/358
  Installing       : xz-5.2.4-4.el8_6.aarch64                                                              92/358
  Installing       : squashfs-tools-4.3-20.el8.aarch64                                                     93/358
  Installing       : libnetfilter_conntrack-1.0.6-5.el8.aarch64                                            94/358
  Running scriptlet: libnetfilter_conntrack-1.0.6-5.el8.aarch64                                            94/358
  Installing       : libteam-1.31-2.el8.aarch64                                                            95/358
  Running scriptlet: libteam-1.31-2.el8.aarch64                                                            95/358
  Installing       : gdbm-1:1.18-1.el8.aarch64                                                             96/358
  Installing       : groff-base-1.22.3-18.el8.aarch64                                                      97/358
  Installing       : acl-2.2.53-1.el8.1.aarch64                                                            98/358
  Installing       : vim-minimal-2:8.0.1763-16.el8_5.13.aarch64                                            99/358
  Installing       : libpsl-0.20.2-6.el8.aarch64                                                          100/358
  Installing       : mpfr-3.1.6-1.el8.aarch64                                                             101/358
  Running scriptlet: mpfr-3.1.6-1.el8.aarch64                                                             101/358
  Installing       : libcomps-0.1.18-1.el8.aarch64                                                        102/358
  Installing       : iw-4.14-5.el8.aarch64                                                                103/358
  Installing       : libibverbs-37.2-1.el8.aarch64                                                        104/358
  Running scriptlet: libibverbs-37.2-1.el8.aarch64                                                        104/358
  Installing       : libpcap-14:1.9.1-5.el8.aarch64                                                       105/358
  Installing       : iptables-libs-1.8.4-22.el8.aarch64                                                   106/358
  Running scriptlet: iptables-1.8.4-22.el8.aarch64                                                        107/358
  Installing       : iptables-1.8.4-22.el8.aarch64                                                        107/358
  Running scriptlet: iptables-1.8.4-22.el8.aarch64                                                        107/358
  Installing       : iptables-ebtables-1.8.4-22.el8.aarch64                                               108/358
  Running scriptlet: iptables-ebtables-1.8.4-22.el8.aarch64                                               108/358
  Installing       : nftables-1:0.9.3-25.el8.aarch64                                                      109/358
  Running scriptlet: nftables-1:0.9.3-25.el8.aarch64                                                      109/358
  Installing       : ipset-libs-7.1-1.el8.aarch64                                                         110/358
  Running scriptlet: ipset-libs-7.1-1.el8.aarch64                                                         110/358
  Installing       : ipset-7.1-1.el8.aarch64                                                              111/358
  Installing       : mozjs60-60.9.0-4.el8.aarch64                                                         112/358
  Installing       : snappy-1.1.8-3.el8.aarch64                                                           113/358
  Installing       : sqlite-3.26.0-15.el8.aarch64                                                         114/358
  Installing       : coreutils-common-8.30-12.el8.aarch64                                                 115/358
  Running scriptlet: coreutils-common-8.30-12.el8.aarch64                                                 115/358
  Installing       : libbpf-0.4.0-3.el8.aarch64                                                           116/358
  Installing       : e2fsprogs-libs-1.45.6-4.el8.aarch64                                                  117/358
  Running scriptlet: e2fsprogs-libs-1.45.6-4.el8.aarch64                                                  117/358
  Installing       : libss-1.45.6-4.el8.aarch64                                                           118/358
  Running scriptlet: libss-1.45.6-4.el8.aarch64                                                           118/358
  Installing       : pigz-2.4-4.el8.aarch64                                                               119/358
  Installing       : libselinux-utils-2.9-5.el8.aarch64                                                   120/358
  Installing       : kernel-tools-libs-4.18.0-372.9.1.el8.aarch64                                         121/358
  Running scriptlet: kernel-tools-libs-4.18.0-372.9.1.el8.aarch64                                         121/358
  Installing       : less-530-1.el8.aarch64                                                               122/358
  Installing       : vim-common-2:8.0.1763-16.el8_5.13.aarch64                                            123/358
  Installing       : brotli-1.0.6-3.el8.aarch64                                                           124/358
  Installing       : c-ares-1.13.0-6.el8.aarch64                                                          125/358
  Running scriptlet: c-ares-1.13.0-6.el8.aarch64                                                          125/358
  Installing       : fuse-libs-2.9.7-15.el8.aarch64                                                       126/358
  Running scriptlet: fuse-libs-2.9.7-15.el8.aarch64                                                       126/358
  Installing       : hardlink-1:1.3-6.el8.aarch64                                                         127/358
  Installing       : hdparm-9.54-4.el8.aarch64                                                            128/358
  Installing       : libdaemon-0.14-15.el8.aarch64                                                        129/358
  Installing       : libndp-1.7-6.el8.aarch64                                                             130/358
  Running scriptlet: libndp-1.7-6.el8.aarch64                                                             130/358
  Installing       : libnghttp2-1.33.0-3.el8_3.1.aarch64                                                  131/358
  Installing       : libpath_utils-0.2.1-39.el8.aarch64                                                   132/358
  Installing       : libini_config-1.3.1-39.el8.aarch64                                                   133/358
  Installing       : libpipeline-1.5.0-2.el8.aarch64                                                      134/358
  Running scriptlet: libpipeline-1.5.0-2.el8.aarch64                                                      134/358
  Installing       : libpkgconf-1.4.2-1.el8.aarch64                                                       135/358
  Installing       : pkgconf-1.4.2-1.el8.aarch64                                                          136/358
  Installing       : pkgconf-pkg-config-1.4.2-1.el8.aarch64                                               137/358
  Installing       : libsigsegv-2.11-5.el8.aarch64                                                        138/358
  Installing       : gawk-4.2.1-4.el8.aarch64                                                             139/358
  Installing       : libsss_autofs-2.6.2-4.el8_6.aarch64                                                  140/358
  Installing       : libsss_nss_idmap-2.6.2-4.el8_6.aarch64                                               141/358
  Running scriptlet: libsss_nss_idmap-2.6.2-4.el8_6.aarch64                                               141/358
  Installing       : libsss_sudo-2.6.2-4.el8_6.aarch64                                                    142/358
  Running scriptlet: libsss_sudo-2.6.2-4.el8_6.aarch64                                                    142/358
  Installing       : libverto-0.3.0-5.el8.aarch64                                                         143/358
  Installing       : libyaml-0.1.7-5.el8.aarch64                                                          144/358
  Installing       : lmdb-libs-0.9.24-1.el8.aarch64                                                       145/358
  Installing       : memstrack-0.1.11-1.el8.aarch64                                                       146/358
  Installing       : ncurses-6.1-9.20180224.el8.aarch64                                                   147/358
  Installing       : krb5-libs-1.18.2-14.el8.aarch64                                                      148/358
  Installing       : libtirpc-1.1.4-6.el8.aarch64                                                         149/358
  Running scriptlet: libtirpc-1.1.4-6.el8.aarch64                                                         149/358
  Installing       : libnsl2-1.2.0-2.20180605git4a062cf.el8.aarch64                                       150/358
  Running scriptlet: libnsl2-1.2.0-2.20180605git4a062cf.el8.aarch64                                       150/358
  Installing       : platform-python-pip-9.0.3-22.el8.rocky.0.noarch                                      151/358
  Installing       : platform-python-setuptools-39.2.0-6.el8.noarch                                       152/358
  Installing       : python3-libs-3.6.8-45.el8.rocky.0.aarch64                                            153/358
  Installing       : platform-python-3.6.8-45.el8.rocky.0.aarch64                                         154/358
  Running scriptlet: platform-python-3.6.8-45.el8.rocky.0.aarch64                                         154/358
  Installing       : openssl-1:1.1.1k-6.el8_5.aarch64                                                     155/358
  Installing       : crypto-policies-scripts-20211116-1.gitae470d6.el8.noarch                             156/358
  Installing       : crypto-policies-20211116-1.gitae470d6.el8.noarch                                     157/358
  Running scriptlet: crypto-policies-20211116-1.gitae470d6.el8.noarch                                     157/358
  Installing       : openssl-pkcs11-0.4.10-2.el8.aarch64                                                  158/358
  Installing       : openssl-libs-1:1.1.1k-6.el8_5.aarch64                                                159/358
  Running scriptlet: openssl-libs-1:1.1.1k-6.el8_5.aarch64                                                159/358
  Installing       : coreutils-8.30-12.el8.aarch64                                                        160/358
  Running scriptlet: ca-certificates-2021.2.50-80.0.el8_4.noarch                                          161/358
  Installing       : ca-certificates-2021.2.50-80.0.el8_4.noarch                                          161/358
  Running scriptlet: ca-certificates-2021.2.50-80.0.el8_4.noarch                                          161/358
  Installing       : shadow-utils-2:4.6-16.el8.aarch64                                                    162/358
  Installing       : libdb-5.3.28-42.el8_4.aarch64                                                        163/358
  Running scriptlet: libdb-5.3.28-42.el8_4.aarch64                                                        163/358
  Installing       : libblkid-2.32.1-35.el8.aarch64                                                       164/358
  Running scriptlet: libblkid-2.32.1-35.el8.aarch64                                                       164/358
  Installing       : libmount-2.32.1-35.el8.aarch64                                                       165/358
  Running scriptlet: libmount-2.32.1-35.el8.aarch64                                                       165/358
  Installing       : systemd-libs-239-58.el8.aarch64                                                      166/358
  Running scriptlet: systemd-libs-239-58.el8.aarch64                                                      166/358
  Installing       : dbus-libs-1:1.12.8-18.el8.aarch64                                                    167/358
  Running scriptlet: dbus-libs-1:1.12.8-18.el8.aarch64                                                    167/358
  Installing       : gzip-1.9-13.el8_5.aarch64                                                            168/358
  Running scriptlet: gzip-1.9-13.el8_5.aarch64                                                            168/358
  Installing       : python3-six-1.11.0-8.el8.noarch                                                      169/358
  Installing       : cracklib-2.9.6-15.el8.aarch64                                                        170/358
  Installing       : kmod-25-19.el8.aarch64                                                               171/358
  Installing       : kmod-libs-25-19.el8.aarch64                                                          172/358
  Running scriptlet: kmod-libs-25-19.el8.aarch64                                                          172/358
  Installing       : cracklib-dicts-2.9.6-15.el8.aarch64                                                  173/358
  Installing       : procps-ng-3.3.15-6.el8.aarch64                                                       174/358
  Running scriptlet: logrotate-3.14.0-4.el8.aarch64                                                       175/358
  Installing       : logrotate-3.14.0-4.el8.aarch64                                                       175/358
  Installing       : which-2.21-17.el8.aarch64                                                            176/358
  Installing       : libevent-2.1.8-5.el8.aarch64                                                         177/358
  Installing       : python3-decorator-4.2.1-2.el8.noarch                                                 178/358
  Installing       : libpwquality-1.4.4-3.el8.aarch64                                                     179/358
  Installing       : pam-1.3.1-16.el8.aarch64                                                             180/358
  Running scriptlet: pam-1.3.1-16.el8.aarch64                                                             180/358
  Installing       : sssd-client-2.6.2-4.el8_6.aarch64                                                    181/358
  Running scriptlet: sssd-client-2.6.2-4.el8_6.aarch64                                                    181/358
  Installing       : python3-dateutil-1:2.6.1-6.el8.noarch                                                182/358
  Installing       : python3-linux-procfs-0.7.0-1.el8.noarch                                              183/358
  Installing       : python3-pyudev-0.21.0-7.el8.noarch                                                   184/358
  Installing       : dbus-tools-1:1.12.8-18.el8.aarch64                                                   185/358
  Installing       : teamd-1.31-2.el8.aarch64                                                             186/358
  Installing       : libusbx-1.0.23-4.el8.aarch64                                                         187/358
  Installing       : plymouth-core-libs-0.9.4-11.20200615git1e36e30.el8.aarch64                           188/358
  Running scriptlet: plymouth-core-libs-0.9.4-11.20200615git1e36e30.el8.aarch64                           188/358
  Installing       : libfdisk-2.32.1-35.el8.aarch64                                                       189/358
  Running scriptlet: libfdisk-2.32.1-35.el8.aarch64                                                       189/358
  Installing       : cyrus-sasl-lib-2.1.27-6.el8_5.aarch64                                                190/358
  Running scriptlet: cyrus-sasl-lib-2.1.27-6.el8_5.aarch64                                                190/358
  Installing       : openldap-2.4.46-18.el8.aarch64                                                       191/358
  Installing       : libldb-2.4.1-1.el8.aarch64                                                           192/358
  Installing       : libnfsidmap-1:2.3.3-51.el8.aarch64                                                   193/358
  Installing       : sssd-nfs-idmap-2.6.2-4.el8_6.aarch64                                                 194/358
  Installing       : libdb-utils-5.3.28-42.el8_4.aarch64                                                  195/358
  Running scriptlet: libutempter-1.1.6-14.el8.aarch64                                                     196/358
  Installing       : libutempter-1.1.6-14.el8.aarch64                                                     196/358
warning: group utempter does not exist - using root
warning: group utmp does not exist - using root

  Installing       : util-linux-2.32.1-35.el8.aarch64                                                     197/358
warning: group tty does not exist - using root

  Running scriptlet: util-linux-2.32.1-35.el8.aarch64                                                     197/358
  Running scriptlet: openssh-8.0p1-13.el8.aarch64                                                         198/358
  Installing       : openssh-8.0p1-13.el8.aarch64                                                         198/358
warning: group ssh_keys does not exist - using root

  Installing       : virt-what-1.18-13.el8.aarch64                                                        199/358
  Running scriptlet: tpm2-tss-2.3.2-4.el8.aarch64                                                         200/358
  Installing       : tpm2-tss-2.3.2-4.el8.aarch64                                                         200/358
  Running scriptlet: tpm2-tss-2.3.2-4.el8.aarch64                                                         200/358
  Installing       : ima-evm-utils-1.3.2-12.el8.aarch64                                                   201/358
  Installing       : raspberrypi2-kernel4-5.15.34-v8.1.el8.aarch64                                        202/358
  Installing       : libarchive-3.3.3-3.el8_5.aarch64                                                     203/358
  Installing       : libsss_certmap-2.6.2-4.el8_6.aarch64                                                 204/358
  Running scriptlet: libsss_certmap-2.6.2-4.el8_6.aarch64                                                 204/358
  Installing       : trousers-lib-0.3.15-1.el8.aarch64                                                    205/358
  Running scriptlet: trousers-lib-0.3.15-1.el8.aarch64                                                    205/358
  Installing       : python3-libcomps-0.1.18-1.el8.aarch64                                                206/358
  Installing       : python3-libselinux-2.9-5.el8.aarch64                                                 207/358
  Installing       : python3-slip-0.6.4-11.el8.noarch                                                     208/358
  Installing       : python3-nftables-1:0.9.3-25.el8.aarch64                                              209/358
  Installing       : python3-perf-4.18.0-372.9.1.el8.aarch64                                              210/358
  Installing       : python3-syspurpose-1.28.29-3.el8.aarch64                                             211/358
  Installing       : npth-1.5-4.el8.aarch64                                                               212/358
  Installing       : pciutils-libs-3.7.0-1.el8.aarch64                                                    213/358
  Running scriptlet: pciutils-libs-3.7.0-1.el8.aarch64                                                    213/358
  Installing       : kernel-tools-4.18.0-372.9.1.el8.aarch64                                              214/358
  Installing       : psmisc-23.1-5.el8.aarch64                                                            215/358
  Installing       : iproute-5.15.0-4.el8.aarch64                                                         216/358
  Installing       : sg3_utils-libs-1.44-5.el8.aarch64                                                    217/358
  Running scriptlet: sg3_utils-libs-1.44-5.el8.aarch64                                                    217/358
  Installing       : slang-2.3.2-3.el8.aarch64                                                            218/358
  Installing       : newt-0.52.20-11.el8.aarch64                                                          219/358
  Installing       : gpm-libs-1.20.7-17.el8.aarch64                                                       220/358
  Running scriptlet: gpm-libs-1.20.7-17.el8.aarch64                                                       220/358
  Installing       : libestr-0.1.10-1.el8.aarch64                                                         221/358
  Running scriptlet: libestr-0.1.10-1.el8.aarch64                                                         221/358
  Installing       : libfastjson-0.99.9-1.el8.aarch64                                                     222/358
  Running scriptlet: libfastjson-0.99.9-1.el8.aarch64                                                     222/358
  Installing       : libxkbcommon-0.9.1-1.el8.aarch64                                                     223/358
  Installing       : libssh-config-0.9.6-3.el8.noarch                                                     224/358
  Installing       : libssh-0.9.6-3.el8.aarch64                                                           225/358
  Installing       : libcurl-7.61.1-22.el8.aarch64                                                        226/358
  Installing       : curl-7.61.1-22.el8.aarch64                                                           227/358
  Installing       : rpm-4.14.3-23.el8.aarch64                                                            228/358
  Installing       : rpm-libs-4.14.3-23.el8.aarch64                                                       229/358
  Running scriptlet: rpm-libs-4.14.3-23.el8.aarch64                                                       229/358
  Installing       : libsolv-0.7.20-1.el8.aarch64                                                         230/358
  Installing       : policycoreutils-2.9-19.el8.aarch64                                                   231/358
  Running scriptlet: policycoreutils-2.9-19.el8.aarch64                                                   231/358
  Installing       : rpm-plugin-selinux-4.14.3-23.el8.aarch64                                             232/358
  Installing       : selinux-policy-3.14.3-95.el8.noarch                                                  233/358
  Running scriptlet: selinux-policy-3.14.3-95.el8.noarch                                                  233/358
  Running scriptlet: selinux-policy-targeted-3.14.3-95.el8.noarch                                         234/358
  Installing       : selinux-policy-targeted-3.14.3-95.el8.noarch                                         234/358
  Running scriptlet: selinux-policy-targeted-3.14.3-95.el8.noarch                                         234/358
  Installing       : rpm-plugin-systemd-inhibit-4.14.3-23.el8.aarch64                                     235/358
  Installing       : device-mapper-8:1.02.181-3.el8.aarch64                                               236/358
  Installing       : device-mapper-libs-8:1.02.181-3.el8.aarch64                                          237/358
  Installing       : cryptsetup-libs-2.3.7-2.el8.aarch64                                                  238/358
  Running scriptlet: cryptsetup-libs-2.3.7-2.el8.aarch64                                                  238/358
  Installing       : elfutils-debuginfod-client-0.186-1.el8.aarch64                                       239/358
  Installing       : elfutils-default-yama-scope-0.186-1.el8.noarch                                       240/358
  Running scriptlet: elfutils-default-yama-scope-0.186-1.el8.noarch                                       240/358
  Installing       : elfutils-libs-0.186-1.el8.aarch64                                                    241/358
  Installing       : dbus-common-1:1.12.8-18.el8.noarch                                                   242/358
  Running scriptlet: dbus-daemon-1:1.12.8-18.el8.aarch64                                                  243/358
  Installing       : dbus-daemon-1:1.12.8-18.el8.aarch64                                                  243/358
warning: group dbus does not exist - using root

  Running scriptlet: dbus-daemon-1:1.12.8-18.el8.aarch64                                                  243/358
  Installing       : systemd-pam-239-58.el8.aarch64                                                       244/358
  Installing       : gnutls-3.6.16-4.el8.aarch64                                                          245/358
  Running scriptlet: systemd-239-58.el8.aarch64                                                           246/358
  Installing       : systemd-239-58.el8.aarch64                                                           246/358
  Running scriptlet: systemd-239-58.el8.aarch64                                                           246/358
  Installing       : dbus-1:1.12.8-18.el8.aarch64                                                         247/358
  Running scriptlet: trousers-0.3.15-1.el8.aarch64                                                        248/358
  Installing       : trousers-0.3.15-1.el8.aarch64                                                        248/358
warning: group tss does not exist - using root
warning: group tss does not exist - using root

  Running scriptlet: trousers-0.3.15-1.el8.aarch64                                                        248/358
  Installing       : glib2-2.56.4-158.el8.aarch64                                                         249/358
  Installing       : shared-mime-info-1.9-3.el8.aarch64                                                   250/358
  Running scriptlet: shared-mime-info-1.9-3.el8.aarch64                                                   250/358
  Installing       : libmodulemd-2.13.0-1.el8.aarch64                                                     251/358
  Installing       : NetworkManager-libnm-1:1.36.0-4.el8.aarch64                                          252/358
  Running scriptlet: NetworkManager-libnm-1:1.36.0-4.el8.aarch64                                          252/358
  Installing       : polkit-libs-0.115-13.el8_5.2.aarch64                                                 253/358
  Running scriptlet: polkit-libs-0.115-13.el8_5.2.aarch64                                                 253/358
  Running scriptlet: polkit-0.115-13.el8_5.2.aarch64                                                      254/358
  Installing       : polkit-0.115-13.el8_5.2.aarch64                                                      254/358
  Running scriptlet: polkit-0.115-13.el8_5.2.aarch64                                                      254/358
  Installing       : polkit-pkla-compat-0.1-12.el8.aarch64                                                255/358
warning: group polkitd does not exist - using root
warning: group polkitd does not exist - using root

  Installing       : cronie-anacron-1.5.2-6.el8.aarch64                                                   256/358
  Running scriptlet: cronie-anacron-1.5.2-6.el8.aarch64                                                   256/358
  Installing       : cronie-1.5.2-6.el8.aarch64                                                           257/358
  Running scriptlet: cronie-1.5.2-6.el8.aarch64                                                           257/358
  Installing       : crontabs-1.11-17.20190603git.el8.noarch                                              258/358
  Installing       : timedatex-0.5-3.el8.aarch64                                                          259/358
  Running scriptlet: timedatex-0.5-3.el8.aarch64                                                          259/358
  Installing       : dbus-glib-0.110-2.el8.aarch64                                                        260/358
  Running scriptlet: dbus-glib-0.110-2.el8.aarch64                                                        260/358
  Installing       : python3-dbus-1.2.4-15.el8.aarch64                                                    261/358
  Installing       : python3-slip-dbus-0.6.4-11.el8.noarch                                                262/358
  Installing       : gobject-introspection-1.56.1-1.el8.aarch64                                           263/358
  Installing       : python3-gobject-base-3.28.3-2.el8.aarch64                                            264/358
  Installing       : python3-firewall-0.9.3-13.el8.noarch                                                 265/358
  Installing       : initscripts-10.00.17-1.el8.aarch64                                                   266/358
  Running scriptlet: initscripts-10.00.17-1.el8.aarch64                                                   266/358
  Installing       : libsecret-0.18.6-1.el8.0.2.aarch64                                                   267/358
  Installing       : pinentry-1.1.0-2.el8.aarch64                                                         268/358
  Running scriptlet: pinentry-1.1.0-2.el8.aarch64                                                         268/358
  Installing       : gnupg2-smime-2.2.20-2.el8.aarch64                                                    269/358
  Installing       : gnupg2-2.2.20-2.el8.aarch64                                                          270/358
  Installing       : gpgme-1.13.1-11.el8.aarch64                                                          271/358
  Installing       : librepo-1.14.2-1.el8.aarch64                                                         272/358
  Installing       : libdnf-0.63.0-8.el8.aarch64                                                          273/358
  Installing       : python3-libdnf-0.63.0-8.el8.aarch64                                                  274/358
  Installing       : python3-hawkey-0.63.0-8.el8.aarch64                                                  275/358
  Installing       : python3-gpg-1.13.1-11.el8.aarch64                                                    276/358
  Installing       : rpm-build-libs-4.14.3-23.el8.aarch64                                                 277/358
  Running scriptlet: rpm-build-libs-4.14.3-23.el8.aarch64                                                 277/358
  Installing       : python3-rpm-4.14.3-23.el8.aarch64                                                    278/358
  Installing       : libuser-0.62-24.el8.aarch64                                                          279/358
  Running scriptlet: libuser-0.62-24.el8.aarch64                                                          279/358
  Running scriptlet: authselect-libs-1.2.2-3.el8.aarch64                                                  280/358
  Installing       : authselect-libs-1.2.2-3.el8.aarch64                                                  280/358
  Installing       : iputils-20180629-9.el8.aarch64                                                       281/358
  Running scriptlet: iputils-20180629-9.el8.aarch64                                                       281/358
  Installing       : libkcapi-1.2.0-2.el8.aarch64                                                         282/358
  Installing       : libkcapi-hmaccalc-1.2.0-2.el8.aarch64                                                283/358
  Running scriptlet: sssd-common-2.6.2-4.el8_6.aarch64                                                    284/358
  Installing       : sssd-common-2.6.2-4.el8_6.aarch64                                                    284/358
warning: group sssd does not exist - using root
warning: group sssd does not exist - using root
warning: group sssd does not exist - using root
warning: group sssd does not exist - using root
warning: group sssd does not exist - using root
warning: group sssd does not exist - using root
warning: group sssd does not exist - using root
warning: group sssd does not exist - using root

  Running scriptlet: sssd-common-2.6.2-4.el8_6.aarch64                                                    284/358
  Installing       : wpa_supplicant-1:2.10-1.el8.aarch64                                                  285/358
  Running scriptlet: wpa_supplicant-1:2.10-1.el8.aarch64                                                  285/358
  Running scriptlet: unbound-libs-1.7.3-17.el8.aarch64                                                    286/358
  Installing       : unbound-libs-1.7.3-17.el8.aarch64                                                    286/358
warning: group unbound does not exist - using root
warning: group unbound does not exist - using root

  Running scriptlet: unbound-libs-1.7.3-17.el8.aarch64                                                    286/358
  Installing       : python3-unbound-1.7.3-17.el8.aarch64                                                 287/358
  Installing       : kpartx-0.8.4-22.el8.aarch64                                                          288/358
  Installing       : libreport-filesystem-2.9.5-15.el8.rocky.6.3.aarch64                                  289/358
  Installing       : dnf-data-4.7.0-8.el8.noarch                                                          290/358
  Installing       : python3-dnf-4.7.0-8.el8.noarch                                                       291/358
  Installing       : dnf-4.7.0-8.el8.noarch                                                               292/358
  Running scriptlet: dnf-4.7.0-8.el8.noarch                                                               292/358
  Installing       : python3-dnf-plugins-core-4.0.21-11.el8.noarch                                        293/358
  Installing       : kbd-misc-2.0.4-10.el8.noarch                                                         294/358
  Installing       : kbd-legacy-2.0.4-10.el8.noarch                                                       295/358
  Installing       : kbd-2.0.4-10.el8.aarch64                                                             296/358
  Installing       : systemd-udev-239-58.el8.aarch64                                                      297/358
  Running scriptlet: systemd-udev-239-58.el8.aarch64                                                      297/358
  Installing       : dracut-049-201.git20220131.el8.aarch64                                               298/358
  Running scriptlet: NetworkManager-1:1.36.0-4.el8.aarch64                                                299/358
  Installing       : NetworkManager-1:1.36.0-4.el8.aarch64                                                299/358
  Running scriptlet: NetworkManager-1:1.36.0-4.el8.aarch64                                                299/358
  Installing       : dracut-network-049-201.git20220131.el8.aarch64                                       300/358
  Installing       : dracut-squash-049-201.git20220131.el8.aarch64                                        301/358
  Installing       : plymouth-scripts-0.9.4-11.20200615git1e36e30.el8.aarch64                             302/358
  Installing       : plymouth-0.9.4-11.20200615git1e36e30.el8.aarch64                                     303/358
  Installing       : crda-3.18_2020.04.29-1.el8.noarch                                                    304/358
  Installing       : hwdata-0.314-8.12.el8.noarch                                                         305/358
  Installing       : firewalld-filesystem-0.9.3-13.el8.noarch                                             306/358
  Installing       : firewalld-0.9.3-13.el8.noarch                                                        307/358
  Running scriptlet: firewalld-0.9.3-13.el8.noarch                                                        307/358
  Installing       : lshw-B.02.19.2-6.el8.aarch64                                                         308/358
  Installing       : NetworkManager-wifi-1:1.36.0-4.el8.aarch64                                           309/358
  Installing       : kexec-tools-2.0.20-68.el8.aarch64                                                    310/358
  Running scriptlet: kexec-tools-2.0.20-68.el8.aarch64                                                    310/358
  Installing       : NetworkManager-team-1:1.36.0-4.el8.aarch64                                           311/358
  Installing       : NetworkManager-tui-1:1.36.0-4.el8.aarch64                                            312/358
  Installing       : dracut-config-rescue-049-201.git20220131.el8.aarch64                                 313/358
  Installing       : dnf-plugins-core-4.0.21-11.el8.noarch                                                314/358
  Installing       : yum-4.7.0-8.el8.noarch                                                               315/358
  Installing       : sssd-kcm-2.6.2-4.el8_6.aarch64                                                       316/358
  Running scriptlet: sssd-kcm-2.6.2-4.el8_6.aarch64                                                       316/358
  Installing       : authselect-1.2.2-3.el8.aarch64                                                       317/358
  Installing       : passwd-0.80-4.el8.aarch64                                                            318/358
  Installing       : audit-3.0.7-2.el8.2.aarch64                                                          319/358
  Running scriptlet: audit-3.0.7-2.el8.2.aarch64                                                          319/358
  Installing       : tuned-2.18.0-2.el8.noarch                                                            320/358
  Running scriptlet: tuned-2.18.0-2.el8.noarch                                                            320/358
  Running scriptlet: chrony-4.1-1.el8.aarch64                                                             321/358
  Installing       : chrony-4.1-1.el8.aarch64                                                             321/358
warning: group chrony does not exist - using root
warning: group chrony does not exist - using root
warning: group chrony does not exist - using root

  Running scriptlet: chrony-4.1-1.el8.aarch64                                                             321/358
Running in chroot, ignoring request: daemon-reload

  Installing       : irqbalance-2:1.4.0-6.el8.aarch64                                                     322/358
  Running scriptlet: irqbalance-2:1.4.0-6.el8.aarch64                                                     322/358
  Installing       : rocky-release-rpi-8.0-1.el8.noarch                                                   323/358
  Running scriptlet: rocky-release-rpi-8.0-1.el8.noarch                                                   323/358
Good

  Installing       : net-tools-2.0-0.52.20160912git.el8.aarch64                                           324/358
  Running scriptlet: net-tools-2.0-0.52.20160912git.el8.aarch64                                           324/358
  Running scriptlet: openssh-server-8.0p1-13.el8.aarch64                                                  325/358
  Installing       : openssh-server-8.0p1-13.el8.aarch64                                                  325/358
  Running scriptlet: openssh-server-8.0p1-13.el8.aarch64                                                  325/358
  Installing       : rsyslog-8.2102.0-7.el8_6.1.aarch64                                                   326/358
  Running scriptlet: rsyslog-8.2102.0-7.el8_6.1.aarch64                                                   326/358
  Installing       : parted-3.2-39.el8.aarch64                                                            327/358
  Running scriptlet: parted-3.2-39.el8.aarch64                                                            327/358
  Installing       : xfsprogs-5.0.0-10.el8.aarch64                                                        328/358
  Running scriptlet: xfsprogs-5.0.0-10.el8.aarch64                                                        328/358
  Installing       : vim-enhanced-2:8.0.1763-16.el8_5.13.aarch64                                          329/358
  Installing       : sg3_utils-1.44-5.el8.aarch64                                                         330/358
  Installing       : openssh-clients-8.0p1-13.el8.aarch64                                                 331/358
  Installing       : cloud-utils-growpart-0.31-3.el8.noarch                                               332/358
  Installing       : sudo-1.8.29-8.el8.aarch64                                                            333/358
  Running scriptlet: sudo-1.8.29-8.el8.aarch64                                                            333/358
  Running scriptlet: man-db-2.7.6.1-18.el8.aarch64                                                        334/358
  Installing       : man-db-2.7.6.1-18.el8.aarch64                                                        334/358
  Running scriptlet: man-db-2.7.6.1-18.el8.aarch64                                                        334/358
  Installing       : prefixdevname-0.1.0-6.el8.aarch64                                                    335/358
  Installing       : e2fsprogs-1.45.6-4.el8.aarch64                                                       336/358
  Installing       : bash-completion-1:2.7-5.el8.noarch                                                   337/358
  Installing       : nano-2.9.8-1.el8.aarch64                                                             338/358
  Running scriptlet: nano-2.9.8-1.el8.aarch64                                                             338/358
  Installing       : iprutils-2.4.19-1.el8.aarch64                                                        339/358
  Running scriptlet: iprutils-2.4.19-1.el8.aarch64                                                        339/358
  Installing       : hostname-3.20-6.el8.aarch64                                                          340/358
  Running scriptlet: hostname-3.20-6.el8.aarch64                                                          340/358
  Installing       : libsysfs-2.1.0-25.el8.aarch64                                                        341/358
  Running scriptlet: libsysfs-2.1.0-25.el8.aarch64                                                        341/358
  Installing       : lsscsi-0.32-3.el8.aarch64                                                            342/358
  Installing       : rootfiles-8.1-22.el8.noarch                                                          343/358
  Installing       : linux-firmware-20220210-107.git6342082c.el8.noarch                                   344/358
  Installing       : iwl7260-firmware-1:25.30.13.0-107.el8.1.noarch                                       345/358
  Installing       : iwl6050-firmware-41.28.5.1-107.el8.1.noarch                                          346/358
  Installing       : iwl6000g2a-firmware-18.168.6.1-107.el8.1.noarch                                      347/358
  Installing       : iwl6000-firmware-9.221.4.1-107.el8.1.noarch                                          348/358
  Installing       : iwl5150-firmware-8.24.2.2-107.el8.1.noarch                                           349/358
  Installing       : iwl5000-firmware-8.83.5.1_1-107.el8.1.noarch                                         350/358
  Installing       : iwl3160-firmware-1:25.30.13.0-107.el8.1.noarch                                       351/358
  Installing       : iwl2030-firmware-18.168.6.1-107.el8.1.noarch                                         352/358
  Installing       : iwl2000-firmware-18.168.6.1-107.el8.1.noarch                                         353/358
  Installing       : iwl135-firmware-18.168.6.1-107.el8.1.noarch                                          354/358
  Installing       : iwl105-firmware-18.168.6.1-107.el8.1.noarch                                          355/358
  Installing       : iwl1000-firmware-1:39.31.5.1-107.el8.1.noarch                                        356/358
  Installing       : iwl100-firmware-39.31.5.1-107.el8.1.noarch                                           357/358
  Installing       : raspberrypi2-firmware-5.15.34-v8.1.el8.aarch64                                       358/358
  Running scriptlet: filesystem-3.8-6.el8.aarch64                                                         358/358
  Running scriptlet: glibc-all-langpacks-2.28-189.1.el8.aarch64                                           358/358
  Running scriptlet: crypto-policies-scripts-20211116-1.gitae470d6.el8.noarch                             358/358
  Running scriptlet: ca-certificates-2021.2.50-80.0.el8_4.noarch                                          358/358
  Running scriptlet: raspberrypi2-kernel4-5.15.34-v8.1.el8.aarch64                                        358/358
dracut: No '/dev/log' or 'logger' included for syslog logging

  Running scriptlet: authselect-libs-1.2.2-3.el8.aarch64                                                  358/358
  Running scriptlet: sssd-common-2.6.2-4.el8_6.aarch64                                                    358/358
  Running scriptlet: tuned-2.18.0-2.el8.noarch                                                            358/358
  Running scriptlet: rootfiles-8.1-22.el8.noarch                                                          358/358
  Running scriptlet: glibc-common-2.28-189.1.el8.aarch64                                                  358/358
  Running scriptlet: info-6.5-7.el8.aarch64                                                               358/358
  Running scriptlet: vim-common-2:8.0.1763-16.el8_5.13.aarch64                                            358/358
  Running scriptlet: systemd-239-58.el8.aarch64                                                           358/358
  Running scriptlet: glib2-2.56.4-158.el8.aarch64                                                         358/358
  Running scriptlet: shared-mime-info-1.9-3.el8.aarch64                                                   358/358
  Running scriptlet: systemd-udev-239-58.el8.aarch64                                                      358/358
  Running scriptlet: man-db-2.7.6.1-18.el8.aarch64                                                        358/358
  Verifying        : raspberrypi2-firmware-5.15.34-v8.1.el8.aarch64                                         1/358
  Verifying        : raspberrypi2-kernel4-5.15.34-v8.1.el8.aarch64                                          2/358
  Verifying        : rocky-release-rpi-8.0-1.el8.noarch                                                     3/358
  Verifying        : NetworkManager-1:1.36.0-4.el8.aarch64                                                  4/358
  Verifying        : NetworkManager-libnm-1:1.36.0-4.el8.aarch64                                            5/358
  Verifying        : NetworkManager-team-1:1.36.0-4.el8.aarch64                                             6/358
  Verifying        : NetworkManager-tui-1:1.36.0-4.el8.aarch64                                              7/358
  Verifying        : NetworkManager-wifi-1:1.36.0-4.el8.aarch64                                             8/358
  Verifying        : acl-2.2.53-1.el8.1.aarch64                                                             9/358
  Verifying        : audit-3.0.7-2.el8.2.aarch64                                                           10/358
  Verifying        : audit-libs-3.0.7-2.el8.2.aarch64                                                      11/358
  Verifying        : authselect-1.2.2-3.el8.aarch64                                                        12/358
  Verifying        : authselect-libs-1.2.2-3.el8.aarch64                                                   13/358
  Verifying        : basesystem-11-5.el8.noarch                                                            14/358
  Verifying        : bash-4.4.20-3.el8.aarch64                                                             15/358
  Verifying        : bash-completion-1:2.7-5.el8.noarch                                                    16/358
  Verifying        : brotli-1.0.6-3.el8.aarch64                                                            17/358
  Verifying        : bzip2-libs-1.0.6-26.el8.aarch64                                                       18/358
  Verifying        : c-ares-1.13.0-6.el8.aarch64                                                           19/358
  Verifying        : ca-certificates-2021.2.50-80.0.el8_4.noarch                                           20/358
  Verifying        : chkconfig-1.19.1-1.el8.aarch64                                                        21/358
  Verifying        : chrony-4.1-1.el8.aarch64                                                              22/358
  Verifying        : coreutils-8.30-12.el8.aarch64                                                         23/358
  Verifying        : coreutils-common-8.30-12.el8.aarch64                                                  24/358
  Verifying        : cpio-2.12-11.el8.aarch64                                                              25/358
  Verifying        : cracklib-2.9.6-15.el8.aarch64                                                         26/358
  Verifying        : cracklib-dicts-2.9.6-15.el8.aarch64                                                   27/358
  Verifying        : crda-3.18_2020.04.29-1.el8.noarch                                                     28/358
  Verifying        : cronie-1.5.2-6.el8.aarch64                                                            29/358
  Verifying        : cronie-anacron-1.5.2-6.el8.aarch64                                                    30/358
  Verifying        : crontabs-1.11-17.20190603git.el8.noarch                                               31/358
  Verifying        : crypto-policies-20211116-1.gitae470d6.el8.noarch                                      32/358
  Verifying        : crypto-policies-scripts-20211116-1.gitae470d6.el8.noarch                              33/358
  Verifying        : cryptsetup-libs-2.3.7-2.el8.aarch64                                                   34/358
  Verifying        : curl-7.61.1-22.el8.aarch64                                                            35/358
  Verifying        : cyrus-sasl-lib-2.1.27-6.el8_5.aarch64                                                 36/358
  Verifying        : dbus-1:1.12.8-18.el8.aarch64                                                          37/358
  Verifying        : dbus-common-1:1.12.8-18.el8.noarch                                                    38/358
  Verifying        : dbus-daemon-1:1.12.8-18.el8.aarch64                                                   39/358
  Verifying        : dbus-glib-0.110-2.el8.aarch64                                                         40/358
  Verifying        : dbus-libs-1:1.12.8-18.el8.aarch64                                                     41/358
  Verifying        : dbus-tools-1:1.12.8-18.el8.aarch64                                                    42/358
  Verifying        : device-mapper-8:1.02.181-3.el8.aarch64                                                43/358
  Verifying        : device-mapper-libs-8:1.02.181-3.el8.aarch64                                           44/358
  Verifying        : diffutils-3.6-6.el8.aarch64                                                           45/358
  Verifying        : dmidecode-1:3.3-4.el8.aarch64                                                         46/358
  Verifying        : dnf-4.7.0-8.el8.noarch                                                                47/358
  Verifying        : dnf-data-4.7.0-8.el8.noarch                                                           48/358
  Verifying        : dnf-plugins-core-4.0.21-11.el8.noarch                                                 49/358
  Verifying        : dracut-049-201.git20220131.el8.aarch64                                                50/358
  Verifying        : dracut-config-rescue-049-201.git20220131.el8.aarch64                                  51/358
  Verifying        : dracut-network-049-201.git20220131.el8.aarch64                                        52/358
  Verifying        : dracut-squash-049-201.git20220131.el8.aarch64                                         53/358
  Verifying        : e2fsprogs-1.45.6-4.el8.aarch64                                                        54/358
  Verifying        : e2fsprogs-libs-1.45.6-4.el8.aarch64                                                   55/358
  Verifying        : elfutils-debuginfod-client-0.186-1.el8.aarch64                                        56/358
  Verifying        : elfutils-default-yama-scope-0.186-1.el8.noarch                                        57/358
  Verifying        : elfutils-libelf-0.186-1.el8.aarch64                                                   58/358
  Verifying        : elfutils-libs-0.186-1.el8.aarch64                                                     59/358
  Verifying        : ethtool-2:5.13-1.el8.aarch64                                                          60/358
  Verifying        : expat-2.2.5-8.el8.aarch64                                                             61/358
  Verifying        : file-libs-5.33-20.el8.aarch64                                                         62/358
  Verifying        : filesystem-3.8-6.el8.aarch64                                                          63/358
  Verifying        : findutils-1:4.6.0-20.el8.aarch64                                                      64/358
  Verifying        : firewalld-0.9.3-13.el8.noarch                                                         65/358
  Verifying        : firewalld-filesystem-0.9.3-13.el8.noarch                                              66/358
  Verifying        : fuse-libs-2.9.7-15.el8.aarch64                                                        67/358
  Verifying        : gawk-4.2.1-4.el8.aarch64                                                              68/358
  Verifying        : gdbm-1:1.18-1.el8.aarch64                                                             69/358
  Verifying        : gdbm-libs-1:1.18-1.el8.aarch64                                                        70/358
  Verifying        : glib2-2.56.4-158.el8.aarch64                                                          71/358
  Verifying        : glibc-2.28-189.1.el8.aarch64                                                          72/358
  Verifying        : glibc-all-langpacks-2.28-189.1.el8.aarch64                                            73/358
  Verifying        : glibc-common-2.28-189.1.el8.aarch64                                                   74/358
  Verifying        : gmp-1:6.1.2-10.el8.aarch64                                                            75/358
  Verifying        : gnupg2-2.2.20-2.el8.aarch64                                                           76/358
  Verifying        : gnupg2-smime-2.2.20-2.el8.aarch64                                                     77/358
  Verifying        : gnutls-3.6.16-4.el8.aarch64                                                           78/358
  Verifying        : gobject-introspection-1.56.1-1.el8.aarch64                                            79/358
  Verifying        : gpgme-1.13.1-11.el8.aarch64                                                           80/358
  Verifying        : grep-3.1-6.el8.aarch64                                                                81/358
  Verifying        : groff-base-1.22.3-18.el8.aarch64                                                      82/358
  Verifying        : gzip-1.9-13.el8_5.aarch64                                                             83/358
  Verifying        : hardlink-1:1.3-6.el8.aarch64                                                          84/358
  Verifying        : hdparm-9.54-4.el8.aarch64                                                             85/358
  Verifying        : hostname-3.20-6.el8.aarch64                                                           86/358
  Verifying        : hwdata-0.314-8.12.el8.noarch                                                          87/358
  Verifying        : ima-evm-utils-1.3.2-12.el8.aarch64                                                    88/358
  Verifying        : info-6.5-7.el8.aarch64                                                                89/358
  Verifying        : initscripts-10.00.17-1.el8.aarch64                                                    90/358
  Verifying        : iproute-5.15.0-4.el8.aarch64                                                          91/358
  Verifying        : iprutils-2.4.19-1.el8.aarch64                                                         92/358
  Verifying        : ipset-7.1-1.el8.aarch64                                                               93/358
  Verifying        : ipset-libs-7.1-1.el8.aarch64                                                          94/358
  Verifying        : iptables-1.8.4-22.el8.aarch64                                                         95/358
  Verifying        : iptables-ebtables-1.8.4-22.el8.aarch64                                                96/358
  Verifying        : iptables-libs-1.8.4-22.el8.aarch64                                                    97/358
  Verifying        : iputils-20180629-9.el8.aarch64                                                        98/358
  Verifying        : irqbalance-2:1.4.0-6.el8.aarch64                                                      99/358
  Verifying        : iw-4.14-5.el8.aarch64                                                                100/358
  Verifying        : iwl100-firmware-39.31.5.1-107.el8.1.noarch                                           101/358
  Verifying        : iwl1000-firmware-1:39.31.5.1-107.el8.1.noarch                                        102/358
  Verifying        : iwl105-firmware-18.168.6.1-107.el8.1.noarch                                          103/358
  Verifying        : iwl135-firmware-18.168.6.1-107.el8.1.noarch                                          104/358
  Verifying        : iwl2000-firmware-18.168.6.1-107.el8.1.noarch                                         105/358
  Verifying        : iwl2030-firmware-18.168.6.1-107.el8.1.noarch                                         106/358
  Verifying        : iwl3160-firmware-1:25.30.13.0-107.el8.1.noarch                                       107/358
  Verifying        : iwl5000-firmware-8.83.5.1_1-107.el8.1.noarch                                         108/358
  Verifying        : iwl5150-firmware-8.24.2.2-107.el8.1.noarch                                           109/358
  Verifying        : iwl6000-firmware-9.221.4.1-107.el8.1.noarch                                          110/358
  Verifying        : iwl6000g2a-firmware-18.168.6.1-107.el8.1.noarch                                      111/358
  Verifying        : iwl6050-firmware-41.28.5.1-107.el8.1.noarch                                          112/358
  Verifying        : iwl7260-firmware-1:25.30.13.0-107.el8.1.noarch                                       113/358
  Verifying        : jansson-2.14-1.el8.aarch64                                                           114/358
  Verifying        : json-c-0.13.1-3.el8.aarch64                                                          115/358
  Verifying        : kbd-2.0.4-10.el8.aarch64                                                             116/358
  Verifying        : kbd-legacy-2.0.4-10.el8.noarch                                                       117/358
  Verifying        : kbd-misc-2.0.4-10.el8.noarch                                                         118/358
  Verifying        : kernel-tools-4.18.0-372.9.1.el8.aarch64                                              119/358
  Verifying        : kernel-tools-libs-4.18.0-372.9.1.el8.aarch64                                         120/358
  Verifying        : kexec-tools-2.0.20-68.el8.aarch64                                                    121/358
  Verifying        : keyutils-libs-1.5.10-9.el8.aarch64                                                   122/358
  Verifying        : kmod-25-19.el8.aarch64                                                               123/358
  Verifying        : kmod-libs-25-19.el8.aarch64                                                          124/358
  Verifying        : kpartx-0.8.4-22.el8.aarch64                                                          125/358
  Verifying        : krb5-libs-1.18.2-14.el8.aarch64                                                      126/358
  Verifying        : less-530-1.el8.aarch64                                                               127/358
  Verifying        : libacl-2.2.53-1.el8.1.aarch64                                                        128/358
  Verifying        : libarchive-3.3.3-3.el8_5.aarch64                                                     129/358
  Verifying        : libassuan-2.5.1-3.el8.aarch64                                                        130/358
  Verifying        : libattr-2.4.48-3.el8.aarch64                                                         131/358
  Verifying        : libbasicobjects-0.1.1-39.el8.aarch64                                                 132/358
  Verifying        : libblkid-2.32.1-35.el8.aarch64                                                       133/358
  Verifying        : libbpf-0.4.0-3.el8.aarch64                                                           134/358
  Verifying        : libcap-2.48-2.el8.aarch64                                                            135/358
  Verifying        : libcap-ng-0.7.11-1.el8.aarch64                                                       136/358
  Verifying        : libcollection-0.7.0-39.el8.aarch64                                                   137/358
  Verifying        : libcom_err-1.45.6-4.el8.aarch64                                                      138/358
  Verifying        : libcomps-0.1.18-1.el8.aarch64                                                        139/358
  Verifying        : libcurl-7.61.1-22.el8.aarch64                                                        140/358
  Verifying        : libdaemon-0.14-15.el8.aarch64                                                        141/358
  Verifying        : libdb-5.3.28-42.el8_4.aarch64                                                        142/358
  Verifying        : libdb-utils-5.3.28-42.el8_4.aarch64                                                  143/358
  Verifying        : libdhash-0.5.0-39.el8.aarch64                                                        144/358
  Verifying        : libdnf-0.63.0-8.el8.aarch64                                                          145/358
  Verifying        : libedit-3.1-23.20170329cvs.el8.aarch64                                               146/358
  Verifying        : libevent-2.1.8-5.el8.aarch64                                                         147/358
  Verifying        : libfdisk-2.32.1-35.el8.aarch64                                                       148/358
  Verifying        : libffi-3.1-23.el8.aarch64                                                            149/358
  Verifying        : libgcc-8.5.0-10.el8.aarch64                                                          150/358
  Verifying        : libgcrypt-1.8.5-6.el8.aarch64                                                        151/358
  Verifying        : libgpg-error-1.31-1.el8.aarch64                                                      152/358
  Verifying        : libibverbs-37.2-1.el8.aarch64                                                        153/358
  Verifying        : libidn2-2.2.0-1.el8.aarch64                                                          154/358
  Verifying        : libini_config-1.3.1-39.el8.aarch64                                                   155/358
  Verifying        : libkcapi-1.2.0-2.el8.aarch64                                                         156/358
  Verifying        : libkcapi-hmaccalc-1.2.0-2.el8.aarch64                                                157/358
  Verifying        : libksba-1.3.5-7.el8.aarch64                                                          158/358
  Verifying        : libldb-2.4.1-1.el8.aarch64                                                           159/358
  Verifying        : libmnl-1.0.4-6.el8.aarch64                                                           160/358
  Verifying        : libmodulemd-2.13.0-1.el8.aarch64                                                     161/358
  Verifying        : libmount-2.32.1-35.el8.aarch64                                                       162/358
  Verifying        : libndp-1.7-6.el8.aarch64                                                             163/358
  Verifying        : libnetfilter_conntrack-1.0.6-5.el8.aarch64                                           164/358
  Verifying        : libnfnetlink-1.0.1-13.el8.aarch64                                                    165/358
  Verifying        : libnfsidmap-1:2.3.3-51.el8.aarch64                                                   166/358
  Verifying        : libnftnl-1.1.5-5.el8.aarch64                                                         167/358
  Verifying        : libnghttp2-1.33.0-3.el8_3.1.aarch64                                                  168/358
  Verifying        : libnl3-3.5.0-1.el8.aarch64                                                           169/358
  Verifying        : libnl3-cli-3.5.0-1.el8.aarch64                                                       170/358
  Verifying        : libnsl2-1.2.0-2.20180605git4a062cf.el8.aarch64                                       171/358
  Verifying        : libpath_utils-0.2.1-39.el8.aarch64                                                   172/358
  Verifying        : libpcap-14:1.9.1-5.el8.aarch64                                                       173/358
  Verifying        : libpipeline-1.5.0-2.el8.aarch64                                                      174/358
  Verifying        : libpkgconf-1.4.2-1.el8.aarch64                                                       175/358
  Verifying        : libpsl-0.20.2-6.el8.aarch64                                                          176/358
  Verifying        : libpwquality-1.4.4-3.el8.aarch64                                                     177/358
  Verifying        : libref_array-0.1.5-39.el8.aarch64                                                    178/358
  Verifying        : librepo-1.14.2-1.el8.aarch64                                                         179/358
  Verifying        : libreport-filesystem-2.9.5-15.el8.rocky.6.3.aarch64                                  180/358
  Verifying        : libseccomp-2.5.2-1.el8.aarch64                                                       181/358
  Verifying        : libsecret-0.18.6-1.el8.0.2.aarch64                                                   182/358
  Verifying        : libselinux-2.9-5.el8.aarch64                                                         183/358
  Verifying        : libselinux-utils-2.9-5.el8.aarch64                                                   184/358
  Verifying        : libsemanage-2.9-8.el8.aarch64                                                        185/358
  Verifying        : libsepol-2.9-3.el8.aarch64                                                           186/358
  Verifying        : libsigsegv-2.11-5.el8.aarch64                                                        187/358
  Verifying        : libsmartcols-2.32.1-35.el8.aarch64                                                   188/358
  Verifying        : libsolv-0.7.20-1.el8.aarch64                                                         189/358
  Verifying        : libss-1.45.6-4.el8.aarch64                                                           190/358
  Verifying        : libssh-0.9.6-3.el8.aarch64                                                           191/358
  Verifying        : libssh-config-0.9.6-3.el8.noarch                                                     192/358
  Verifying        : libsss_autofs-2.6.2-4.el8_6.aarch64                                                  193/358
  Verifying        : libsss_certmap-2.6.2-4.el8_6.aarch64                                                 194/358
  Verifying        : libsss_idmap-2.6.2-4.el8_6.aarch64                                                   195/358
  Verifying        : libsss_nss_idmap-2.6.2-4.el8_6.aarch64                                               196/358
  Verifying        : libsss_sudo-2.6.2-4.el8_6.aarch64                                                    197/358
  Verifying        : libstdc++-8.5.0-10.el8.aarch64                                                       198/358
  Verifying        : libsysfs-2.1.0-25.el8.aarch64                                                        199/358
  Verifying        : libtalloc-2.3.3-1.el8.aarch64                                                        200/358
  Verifying        : libtasn1-4.13-3.el8.aarch64                                                          201/358
  Verifying        : libtdb-1.4.4-1.el8.aarch64                                                           202/358
  Verifying        : libteam-1.31-2.el8.aarch64                                                           203/358
  Verifying        : libtevent-0.11.0-0.el8.aarch64                                                       204/358
  Verifying        : libtirpc-1.1.4-6.el8.aarch64                                                         205/358
  Verifying        : libunistring-0.9.9-3.el8.aarch64                                                     206/358
  Verifying        : libusbx-1.0.23-4.el8.aarch64                                                         207/358
  Verifying        : libuser-0.62-24.el8.aarch64                                                          208/358
  Verifying        : libutempter-1.1.6-14.el8.aarch64                                                     209/358
  Verifying        : libuuid-2.32.1-35.el8.aarch64                                                        210/358
  Verifying        : libverto-0.3.0-5.el8.aarch64                                                         211/358
  Verifying        : libxcrypt-4.1.1-6.el8.aarch64                                                        212/358
  Verifying        : libxml2-2.9.7-13.el8.aarch64                                                         213/358
  Verifying        : libyaml-0.1.7-5.el8.aarch64                                                          214/358
  Verifying        : libzstd-1.4.4-1.el8.aarch64                                                          215/358
  Verifying        : linux-firmware-20220210-107.git6342082c.el8.noarch                                   216/358
  Verifying        : lmdb-libs-0.9.24-1.el8.aarch64                                                       217/358
  Verifying        : logrotate-3.14.0-4.el8.aarch64                                                       218/358
  Verifying        : lshw-B.02.19.2-6.el8.aarch64                                                         219/358
  Verifying        : lsscsi-0.32-3.el8.aarch64                                                            220/358
  Verifying        : lua-libs-5.3.4-12.el8.aarch64                                                        221/358
  Verifying        : lz4-libs-1.8.3-3.el8_4.aarch64                                                       222/358
  Verifying        : lzo-2.08-14.el8.aarch64                                                              223/358
  Verifying        : man-db-2.7.6.1-18.el8.aarch64                                                        224/358
  Verifying        : memstrack-0.1.11-1.el8.aarch64                                                       225/358
  Verifying        : mozjs60-60.9.0-4.el8.aarch64                                                         226/358
  Verifying        : mpfr-3.1.6-1.el8.aarch64                                                             227/358
  Verifying        : nano-2.9.8-1.el8.aarch64                                                             228/358
  Verifying        : ncurses-6.1-9.20180224.el8.aarch64                                                   229/358
  Verifying        : ncurses-base-6.1-9.20180224.el8.noarch                                               230/358
  Verifying        : ncurses-libs-6.1-9.20180224.el8.aarch64                                              231/358
  Verifying        : net-tools-2.0-0.52.20160912git.el8.aarch64                                           232/358
  Verifying        : nettle-3.4.1-7.el8.aarch64                                                           233/358
  Verifying        : newt-0.52.20-11.el8.aarch64                                                          234/358
  Verifying        : nftables-1:0.9.3-25.el8.aarch64                                                      235/358
  Verifying        : npth-1.5-4.el8.aarch64                                                               236/358
  Verifying        : numactl-libs-2.0.12-13.el8.aarch64                                                   237/358
  Verifying        : openldap-2.4.46-18.el8.aarch64                                                       238/358
  Verifying        : openssh-8.0p1-13.el8.aarch64                                                         239/358
  Verifying        : openssh-clients-8.0p1-13.el8.aarch64                                                 240/358
  Verifying        : openssh-server-8.0p1-13.el8.aarch64                                                  241/358
  Verifying        : openssl-1:1.1.1k-6.el8_5.aarch64                                                     242/358
  Verifying        : openssl-libs-1:1.1.1k-6.el8_5.aarch64                                                243/358
  Verifying        : openssl-pkcs11-0.4.10-2.el8.aarch64                                                  244/358
  Verifying        : p11-kit-0.23.22-1.el8.aarch64                                                        245/358
  Verifying        : p11-kit-trust-0.23.22-1.el8.aarch64                                                  246/358
  Verifying        : pam-1.3.1-16.el8.aarch64                                                             247/358
  Verifying        : parted-3.2-39.el8.aarch64                                                            248/358
  Verifying        : passwd-0.80-4.el8.aarch64                                                            249/358
  Verifying        : pciutils-libs-3.7.0-1.el8.aarch64                                                    250/358
  Verifying        : pcre-8.42-6.el8.aarch64                                                              251/358
  Verifying        : pcre2-10.32-2.el8.aarch64                                                            252/358
  Verifying        : pigz-2.4-4.el8.aarch64                                                               253/358
  Verifying        : pkgconf-1.4.2-1.el8.aarch64                                                          254/358
  Verifying        : pkgconf-m4-1.4.2-1.el8.noarch                                                        255/358
  Verifying        : pkgconf-pkg-config-1.4.2-1.el8.aarch64                                               256/358
  Verifying        : platform-python-3.6.8-45.el8.rocky.0.aarch64                                         257/358
  Verifying        : platform-python-pip-9.0.3-22.el8.rocky.0.noarch                                      258/358
  Verifying        : platform-python-setuptools-39.2.0-6.el8.noarch                                       259/358
  Verifying        : policycoreutils-2.9-19.el8.aarch64                                                   260/358
  Verifying        : polkit-0.115-13.el8_5.2.aarch64                                                      261/358
  Verifying        : polkit-libs-0.115-13.el8_5.2.aarch64                                                 262/358
  Verifying        : polkit-pkla-compat-0.1-12.el8.aarch64                                                263/358
  Verifying        : popt-1.18-1.el8.aarch64                                                              264/358
  Verifying        : prefixdevname-0.1.0-6.el8.aarch64                                                    265/358
  Verifying        : procps-ng-3.3.15-6.el8.aarch64                                                       266/358
  Verifying        : psmisc-23.1-5.el8.aarch64                                                            267/358
  Verifying        : publicsuffix-list-dafsa-20180723-1.el8.noarch                                        268/358
  Verifying        : python3-dateutil-1:2.6.1-6.el8.noarch                                                269/358
  Verifying        : python3-dbus-1.2.4-15.el8.aarch64                                                    270/358
  Verifying        : python3-decorator-4.2.1-2.el8.noarch                                                 271/358
  Verifying        : python3-dnf-4.7.0-8.el8.noarch                                                       272/358
  Verifying        : python3-dnf-plugins-core-4.0.21-11.el8.noarch                                        273/358
  Verifying        : python3-firewall-0.9.3-13.el8.noarch                                                 274/358
  Verifying        : python3-gobject-base-3.28.3-2.el8.aarch64                                            275/358
  Verifying        : python3-gpg-1.13.1-11.el8.aarch64                                                    276/358
  Verifying        : python3-hawkey-0.63.0-8.el8.aarch64                                                  277/358
  Verifying        : python3-libcomps-0.1.18-1.el8.aarch64                                                278/358
  Verifying        : python3-libdnf-0.63.0-8.el8.aarch64                                                  279/358
  Verifying        : python3-libs-3.6.8-45.el8.rocky.0.aarch64                                            280/358
  Verifying        : python3-libselinux-2.9-5.el8.aarch64                                                 281/358
  Verifying        : python3-linux-procfs-0.7.0-1.el8.noarch                                              282/358
  Verifying        : python3-nftables-1:0.9.3-25.el8.aarch64                                              283/358
  Verifying        : python3-perf-4.18.0-372.9.1.el8.aarch64                                              284/358
  Verifying        : python3-pip-wheel-9.0.3-22.el8.rocky.0.noarch                                        285/358
  Verifying        : python3-pyudev-0.21.0-7.el8.noarch                                                   286/358
  Verifying        : python3-rpm-4.14.3-23.el8.aarch64                                                    287/358
  Verifying        : python3-setuptools-wheel-39.2.0-6.el8.noarch                                         288/358
  Verifying        : python3-six-1.11.0-8.el8.noarch                                                      289/358
  Verifying        : python3-slip-0.6.4-11.el8.noarch                                                     290/358
  Verifying        : python3-slip-dbus-0.6.4-11.el8.noarch                                                291/358
  Verifying        : python3-syspurpose-1.28.29-3.el8.aarch64                                             292/358
  Verifying        : readline-7.0-10.el8.aarch64                                                          293/358
  Verifying        : rocky-gpg-keys-8.6-3.el8.noarch                                                      294/358
  Verifying        : rocky-release-8.6-3.el8.noarch                                                       295/358
  Verifying        : rocky-repos-8.6-3.el8.noarch                                                         296/358
  Verifying        : rootfiles-8.1-22.el8.noarch                                                          297/358
  Verifying        : rpm-4.14.3-23.el8.aarch64                                                            298/358
  Verifying        : rpm-build-libs-4.14.3-23.el8.aarch64                                                 299/358
  Verifying        : rpm-libs-4.14.3-23.el8.aarch64                                                       300/358
  Verifying        : rpm-plugin-selinux-4.14.3-23.el8.aarch64                                             301/358
  Verifying        : rpm-plugin-systemd-inhibit-4.14.3-23.el8.aarch64                                     302/358
  Verifying        : sed-4.5-5.el8.aarch64                                                                303/358
  Verifying        : selinux-policy-3.14.3-95.el8.noarch                                                  304/358
  Verifying        : selinux-policy-targeted-3.14.3-95.el8.noarch                                         305/358
  Verifying        : setup-2.12.2-6.el8.noarch                                                            306/358
  Verifying        : sg3_utils-1.44-5.el8.aarch64                                                         307/358
  Verifying        : sg3_utils-libs-1.44-5.el8.aarch64                                                    308/358
  Verifying        : shadow-utils-2:4.6-16.el8.aarch64                                                    309/358
  Verifying        : shared-mime-info-1.9-3.el8.aarch64                                                   310/358
  Verifying        : slang-2.3.2-3.el8.aarch64                                                            311/358
  Verifying        : snappy-1.1.8-3.el8.aarch64                                                           312/358
  Verifying        : sqlite-3.26.0-15.el8.aarch64                                                         313/358
  Verifying        : sqlite-libs-3.26.0-15.el8.aarch64                                                    314/358
  Verifying        : squashfs-tools-4.3-20.el8.aarch64                                                    315/358
  Verifying        : sssd-client-2.6.2-4.el8_6.aarch64                                                    316/358
  Verifying        : sssd-common-2.6.2-4.el8_6.aarch64                                                    317/358
  Verifying        : sssd-kcm-2.6.2-4.el8_6.aarch64                                                       318/358
  Verifying        : sssd-nfs-idmap-2.6.2-4.el8_6.aarch64                                                 319/358
  Verifying        : sudo-1.8.29-8.el8.aarch64                                                            320/358
  Verifying        : systemd-239-58.el8.aarch64                                                           321/358
  Verifying        : systemd-libs-239-58.el8.aarch64                                                      322/358
  Verifying        : systemd-pam-239-58.el8.aarch64                                                       323/358
  Verifying        : systemd-udev-239-58.el8.aarch64                                                      324/358
  Verifying        : teamd-1.31-2.el8.aarch64                                                             325/358
  Verifying        : timedatex-0.5-3.el8.aarch64                                                          326/358
  Verifying        : tpm2-tss-2.3.2-4.el8.aarch64                                                         327/358
  Verifying        : trousers-0.3.15-1.el8.aarch64                                                        328/358
  Verifying        : trousers-lib-0.3.15-1.el8.aarch64                                                    329/358
  Verifying        : tuned-2.18.0-2.el8.noarch                                                            330/358
  Verifying        : tzdata-2022a-1.el8.noarch                                                            331/358
  Verifying        : util-linux-2.32.1-35.el8.aarch64                                                     332/358
  Verifying        : vim-minimal-2:8.0.1763-16.el8_5.13.aarch64                                           333/358
  Verifying        : virt-what-1.18-13.el8.aarch64                                                        334/358
  Verifying        : which-2.21-17.el8.aarch64                                                            335/358
  Verifying        : wpa_supplicant-1:2.10-1.el8.aarch64                                                  336/358
  Verifying        : xfsprogs-5.0.0-10.el8.aarch64                                                        337/358
  Verifying        : xz-5.2.4-4.el8_6.aarch64                                                             338/358
  Verifying        : xz-libs-5.2.4-4.el8_6.aarch64                                                        339/358
  Verifying        : yum-4.7.0-8.el8.noarch                                                               340/358
  Verifying        : zlib-1.2.11-18.el8_5.aarch64                                                         341/358
  Verifying        : cloud-utils-growpart-0.31-3.el8.noarch                                               342/358
  Verifying        : glibc-gconv-extra-2.28-189.1.el8.aarch64                                             343/358
  Verifying        : gpm-libs-1.20.7-17.el8.aarch64                                                       344/358
  Verifying        : libestr-0.1.10-1.el8.aarch64                                                         345/358
  Verifying        : libfastjson-0.99.9-1.el8.aarch64                                                     346/358
  Verifying        : libxkbcommon-0.9.1-1.el8.aarch64                                                     347/358
  Verifying        : pinentry-1.1.0-2.el8.aarch64                                                         348/358
  Verifying        : plymouth-0.9.4-11.20200615git1e36e30.el8.aarch64                                     349/358
  Verifying        : plymouth-core-libs-0.9.4-11.20200615git1e36e30.el8.aarch64                           350/358
  Verifying        : plymouth-scripts-0.9.4-11.20200615git1e36e30.el8.aarch64                             351/358
  Verifying        : python3-unbound-1.7.3-17.el8.aarch64                                                 352/358
  Verifying        : rsyslog-8.2102.0-7.el8_6.1.aarch64                                                   353/358
  Verifying        : unbound-libs-1.7.3-17.el8.aarch64                                                    354/358
  Verifying        : vim-common-2:8.0.1763-16.el8_5.13.aarch64                                            355/358
  Verifying        : vim-enhanced-2:8.0.1763-16.el8_5.13.aarch64                                          356/358
  Verifying        : vim-filesystem-2:8.0.1763-16.el8_5.13.noarch                                         357/358
  Verifying        : xkeyboard-config-2.28-1.el8.noarch                                                   358/358

Backup stored at /var/lib/authselect/backups/2022-06-29-13-57-22.X4N6rg
Profile "sssd" was selected.
The following nsswitch maps are overwritten by the profile:
- passwd
- group
- netgroup
- automount
- services

Make sure that SSSD service is configured and enabled. See SSSD documentation for more information.

Adding port '22/tcp' to default zone.
success
Locking password for user root.
passwd: Success
WARNING! No bootloader found.
Changing password for user rocky.
passwd: all authentication tokens updated successfully.
400 files removed
cmdline.txt looks like this, please review:
console=ttyAMA0,115200 console=tty1 root=PARTUUID=2de685ad-03 rootfstype=ext4 elevator=deadline rootwait
mkswap: /dev/disk/by-uuid/0151e950-1dff-4131-8c43-29baf05fc71b: warning: wiping old swap signature.
Setting up swapspace version 1, size = 488 MiB (511700992 bytes)
LABEL=_swap, UUID=0151e950-1dff-4131-8c43-29baf05fc71b
/ 100.0%

Unmounting directory /var/tmp/imgcreate-z33fku3n/install_root/boot
Unmounting directory /var/tmp/imgcreate-z33fku3n/install_root/
Unable to create appliance : Failed to unmap partitions for '/dev/loop1'
Traceback (most recent call last):
  File "/bin/appliance-creator", line 156, in main
    creator.unmount()
  File "/usr/lib/python3.6/site-packages/imgcreate/creator.py", line 587, in unmount
    self._unmount_instroot()
  File "/usr/lib/python3.6/site-packages/appcreate/appliance.py", line 646, in _unmount_instroot
    self.__instloop.cleanup()
  File "/usr/lib/python3.6/site-packages/appcreate/partitionedfs.py", line 257, in cleanup
    self.__unmap_partitions()
  File "/usr/lib/python3.6/site-packages/appcreate/partitionedfs.py", line 226, in __unmap_partitions
    d['disk'].device)
imgcreate.errors.MountError: Failed to unmap partitions for '/dev/loop1'

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "/bin/appliance-creator", line 193, in <module>
    sys.exit(main())
  File "/bin/appliance-creator", line 160, in main
    creator.cleanup()
  File "/usr/lib/python3.6/site-packages/imgcreate/creator.py", line 613, in cleanup
    self.unmount()
  File "/usr/lib/python3.6/site-packages/imgcreate/creator.py", line 587, in unmount
    self._unmount_instroot()
  File "/usr/lib/python3.6/site-packages/appcreate/appliance.py", line 646, in _unmount_instroot
    self.__instloop.cleanup()
  File "/usr/lib/python3.6/site-packages/appcreate/partitionedfs.py", line 257, in cleanup
    self.__unmap_partitions()
  File "/usr/lib/python3.6/site-packages/appcreate/partitionedfs.py", line 226, in __unmap_partitions
    d['disk'].device)
imgcreate.errors.MountError: Failed to unmap partitions for '/dev/loop1'
Exception ignored in: <bound method ImageCreator.__del__ of <appcreate.appliance.ApplianceImageCreator object at 0xffff9335df98>>
Traceback (most recent call last):
  File "/usr/lib/python3.6/site-packages/imgcreate/creator.py", line 114, in __del__
    self.cleanup()
  File "/usr/lib/python3.6/site-packages/imgcreate/creator.py", line 613, in cleanup
    self.unmount()
  File "/usr/lib/python3.6/site-packages/imgcreate/creator.py", line 587, in unmount
    self._unmount_instroot()
  File "/usr/lib/python3.6/site-packages/appcreate/appliance.py", line 646, in _unmount_instroot
    self.__instloop.cleanup()
  File "/usr/lib/python3.6/site-packages/appcreate/partitionedfs.py", line 257, in cleanup
    self.__unmap_partitions()
  File "/usr/lib/python3.6/site-packages/appcreate/partitionedfs.py", line 226, in __unmap_partitions
    d['disk'].device)
imgcreate.errors.MountError: Failed to unmap partitions for '/dev/loop1'
```

