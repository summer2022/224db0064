# SummerOfCode-2022

### 介绍
2022开源之夏活动

### 报名方式
请直接点击官网链接：https://summer-ospp.ac.cn/#/homepage 进行报名。

### 活动介绍

开源之夏是由“开源软件供应链点亮计划”发起并长期支持的一项暑期开源活动，旨在鼓励在校学生积极参与开源软件的开发维护，促进优秀开源软件社区的蓬勃发展，培养和发掘更多优秀的开发者。活动联合国内外各大开源社区，针对重要开源软件的开发与维护提供项目任务，并面向全球高校学生开放报名。

本次活动，龙蜥社区共设置了六个项目，每个任务均指派了专业导师对同学提供精心指导。有意向选择龙蜥社区项目的同学，可登录项目页了解详细信息，也可以直接联系项目导师咨询。

##### 中选学生可以获得什么？
- 结识开源界小伙伴和技术大牛
- 获得社区导师的1对1专业指导
- 获得开源项目的经验、经历，丰富个人简历
- 获得纪念品、奖金和证书：
- 通过结项考核的学生将获得项目奖金、结项证书及结项礼包；
- 通过结项考核的学生将有机会获得优秀学生证书。

##### 活动面向哪些学生？
- 本活动面向年满 18 周岁在校学生。
- 暑期即将毕业的学生，只要在申请时学生证处在有效期内，就可以提交申请。
- 海外学生可提供录取通知书、学生卡、在读证明等文件用于证明学生身份。

### 流程
1. 学生与社区项目沟通期：4 月 21 日- 5 月 20 日
1. 学生提交项目申请书：5 月 21 日 - 6 月 4 日
1. 项目开发：7 月 - 9 月
更多流程，详见https://summer-ospp.ac.cn/#/howitworks

### 项目解析

##### 视频：导师解析项目
https://openanolis.cn/activity/558342184799141955

### 项目详情
#### 1、制作树莓派龙蜥镜像
【项目详情】https://summer-ospp.ac.cn/#/org/prodetail/224db0064

【项目描述】
树莓派生态已经越来越被开发者接受和喜欢，并逐渐应用于各种嵌入式场景。通过该项目可以学习树莓派引导和镜像制作流程，并有机会进行内核相关开发适配。
项目计划输出适配树莓派镜像构建流程代码；并最终输出并发布适配树莓派（3b, 4）的龙蜥镜像。

【项目产出要求】
树莓派镜像构建流程代码；适配树莓派（3b, 4）的龙蜥镜像。

【项目技术要求】
精通 Linux，有一定 Linux 内核基础。

【成果提交地址】https://gitee.com/anolis-education/summer-of-code-2022

【难度】进阶

#### 2、将Vscode包集成到龙蜥生态
【项目详情】https://summer-ospp.ac.cn/#/org/prodetail/224db0071

【项目描述】
VS code 是一款比较流行的源代码编辑器，将其集成到Anolis OS中可丰富龙蜥社区的应用生态，更好地服务社区开发者。

【项目产出要求】
VS code仓库以及rpm包；
完成基于Vscode开发用户使用文档。

【项目技术要求】
学习包编译流程；学习基本nodejs相关内容。

【成果提交地址】https://gitee.com/anolis-education/summer-of-code-2022

【难度】基础

#### 3、制作sm3sum工具
【项目详情】https://summer-ospp.ac.cn/#/org/prodetail/224db0072

【项目描述】
SM3是中国定义的哈希算法，coreutils是GNU/linux的基础软件工具包，coreutils工具包已经支持了sha256sum, md5sum这一类用于计算哈希的工具，虽然最新的版本通过cksum -a sm3形式支持了SM3算法，由于用户习惯，以及版本原因，OpenAnolis社区仍然需要一款sm3sum工具，用于计算SM3哈希摘要，需要保持跟md5sum这类工具使用习惯的一致性。

【项目产出要求】
完成sm3sum工具的开发，测试与出包，在风格上需要保持跟md5sum的一致性。

【项目技术要求】
扎实的C语言功底。

【成果提交地址】https://gitee.com/anolis-education/summer-of-code-2022

【难度】进阶

#### 4、使用命令行参数启动微型虚拟机
【项目详情】https://summer-ospp.ac.cn/#/org/prodetail/224db0076

【项目描述】
Dragonball-sandbox是开源于阿里Openanolis社区下rust编写的轻量虚拟机项目，具备低开销、高隔离性、极致弹性等特点，直接落地在阿里云函数计算、ECI等云原生场景。本实验的目的是基于现有Dragonball-sandbox开源代码，实现一个用命令行参数启动虚拟机的功能。在这个过程中，学生也可以对Rust代码、虚拟化技术、开源社区协作方式等获得更深的了解。

【项目产出要求】
完成用命令行启动虚拟机的功能。完成相关代码和文档。

【项目技术要求】
熟悉rust语言，了解虚拟化技术。

【成果提交地址】https://gitee.com/anolis-education/summer-of-code-2022

【难度】进阶

#### 5、Inclavare Containers在龙蜥Anolis OS的适配
【项目详情】https://summer-ospp.ac.cn/#/org/prodetail/224ca0018

【项目描述】
Inclavare Containers是由阿里云和蚂蚁集团主导研发，并联合Intel等合作伙伴打造的业界首个面向机密计算场景的开源容器运行时。Inclavare Containers目前已经是CNCF sandbox项目之一。Inclavare Containers抹平了机密计算的高使用门槛，为用户的工作负载提供多种不同的 Enclave 形态，在安全和成本之间提供更多的选择和灵活性。后续需要在Anolis OS上进行适配，出包，并且基于Github CI/CD 完成集成测试等工作。

【项目产出要求】
完成Inclavare Containers在Anolis OS上的功能适配；
完成Inclavare Containers在Anolis OS上的出包；
学习Github Action并完成 CI/CD 集成测试。

【项目技术要求】
对操作系统和安全感兴趣，熟练掌握Linux；
有一定的C语言基础；
了解出包流程和集成测试流程。

【成果提交地址】https://gitee.com/anolis-education/summer-of-code-2022

【难度】基础

#### 6、eTPM：基于TEE/Enclave实现的TPM
【项目详情】https://summer-ospp.ac.cn/#/org/prodetail/224ca0055

【项目描述】
在TEE/Enclave中运行软件TPM，保证软件TPM访问的数据不会透出到TEE/Enclave之外，解决虚拟化场景中vTPM的后端安全问题。

【项目产出要求】
实现eTPM技术原型。

【项目技术要求】熟练掌握C编程语言；
对TPM工作原理比较了解，有TPM相关的开发经验。

【成果提交地址】https://gitee.com/anolis-education/summer-of-code-2022

【难度】进阶

### 交流沟通

- 欢迎加入微信群与导师交流：
<img src="https://oss.openanolis.cn/fragment/eabmnrwibacltudwcrxs" width=50% height=50% />

如果二维码过期，请添加小龙微信（微信号：openanolis_assis），并备注“高校”。

- 网站：https://openanolis.cn
- 钉钉交流群：33311793
- 微信公众号：OpenAnolis龙蜥

### 更多参与龙蜥社区的方式
https://gitee.com/anolis/docs/blob/main/README.md
